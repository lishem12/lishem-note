# 1.万物始祖Object深度剖析

Object类是一个顶级类，也是所有类的父类。

## 1.1 Object类是如何织入的

为什么所有类都可以调用Obejct的方法？

猜想：

1. 编译器编译进去的

   java文件到class文件过程

2. jvm加进去的

   也就是执行java命令的时候

3. 那开发工具也可以调用Object 的方法是怎么做到的？

### 字节码反汇编验证

用javap命令可以看到反编译：

```shell
javap ObjTest.class
```

就可以看到

**结论：**

1. JDK1.7 以前是在**编译器**处理，也就是说是javac 命令将extends Object加进去的。
2. JDK1.7及以后是JVM进行处理的，编译器不会加上 extends Object
3. IDEA 等开发工具做了智能处理，自动可以调用Object的方法。

## 1.2 Object类源码深入剖析

### 1.2.1 Object类方法总体介绍

![image-20211117101919681](01_JDK 源码.assets/image-20211117101919681.png)

重点关注 registerNatives、hashCode、equals 

### 1.2.2 本地方法怎么找

这里 针对 registerNatives 方法：

```java
package java.lang;

public class Object{
    // 本地方法
    private static native void registerNatives();
    // 对象初始化的时候自动调用此方法
    static {
        registerNatives();
    }
}
```

registerNatives如何找到调用C语言方法(OpenJDK)？？

答：本地方法调用遵循JNI命名规范，即要求本地方法名由 Java\_包名\_类名\_方法名 构成。所以，该方法对应的关键字就是【Java_java_lang_Object_registerNatives】然后全局搜索这个方法 （快捷键 ctrrl+shift+F），就能在Object.c里找到这个函数。

![image-20211117112912357](01_JDK 源码.assets/image-20211117112912357.png)

```c++
#include <limits.h>

#include "jni.h"
#include "jni_util.h"
#include "jvm.h"

#include "java_lang_Object.h"

//JAVA方法（返回值）----->C++函数对象
static JNINativeMethod methods[] = {
    //JAVA方法        返回值  （参数）                    c++函数
    {"hashCode",    "()I",                    (void *)&JVM_IHashCode},
    {"wait",        "(J)V",                   (void *)&JVM_MonitorWait},
    {"notify",      "()V",                    (void *)&JVM_MonitorNotify},
    {"notifyAll",   "()V",                    (void *)&JVM_MonitorNotifyAll},
    {"clone",       "()Ljava/lang/Object;",   (void *)&JVM_Clone},
};

JNIEXPORT void JNICALL//jni调用
    //全路径：java_lang_Object_registerNatives是java对应的包下方法
    Java_java_lang_Object_registerNatives(JNIEnv *env, jclass cls)
{
    //jni环境调用
    (*env)->RegisterNatives(env, cls,
                            methods, sizeof(methods)/sizeof(methods[0]));
}

JNIEXPORT jclass JNICALL
    Java_java_lang_Object_getClass(JNIEnv *env, jobject this)
{
    if (this == NULL) {
        JNU_ThrowNullPointerException(env, NULL);
        return 0;
    } else {
        return (*env)->GetObjectClass(env, this);
    }
}
```

至此，我们找到了所有native方法的源头。下面的将详细探究最核心的 hashcode 和 equals

- Object类的本地方法调用C方法是通过registerNatives加载的
- C里面通过methods函数对JAVA到C++的函数名做了调用映射
- C++的函数名做了调用映射

### 1.2.3 hashCode的内幕

#### **1）HashCode是内存地址吗？**

> 思考：肯定不是直接内存地址，因为JVM内存回收的时候，有什么新生代老生代倒腾的，但是一个对象HashCode不论怎么倒腾，是不会变的。

通过一个Demo验证hashCode不是内存地址：

```java
public class HashCodeTest {

    //目标：只要发生重复，说明hashcode不是内存地址
    public static void main(String[] args) {
        List<Integer> integerList = new ArrayList<Integer>();
        int num = 0;
        for (int i = 0; i < 150000; i++) {
            // 创建新对象
            Object obj = new Object();
            if (integerList.contains(obj.hashCode())){
                num++;
            }else {
                integerList.add(obj.hashCode());
            }
        }
        System.out.println(num + "个hashcode发生重复");
        System.out.println("List合计大小" + integerList.size() + "个");
    }
}
```

结果：

![image-20211117114107558](01_JDK 源码.assets/image-20211117114107558.png)

用此例说明 hashCode不是内存地址。

#### 2）不是内存地址是什么？

既然不是内存地址，那一定在某个地方存着，那在哪里存着呢？

在对象头里：

![image-20211117114418820](01_JDK 源码.assets/image-20211117114418820.png)

对象头分为两部分，一部分是Markword，另一部分是指向class描述的地址Klass

而我们要的hashCode就在Markword里。

32位：

![image-20211117114616093](01_JDK 源码.assets/image-20211117114616093.png)

64位：

![image-20211117114624950](01_JDK 源码.assets/image-20211117114624950.png)

#### 3）hashCode什么时候生成的？

先说结论：在你**没有调用的时候**，这个值是空的，当**第一次调用hashCode方法时**，会生成

代码验证

```java
package com.lishem.obj.hashcode;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

/**
 * 打印markword看到hashCode
 */
public class ShowHashCode {

    public static void main(String[] args) {
        ShowHashCode a = new ShowHashCode();
        //jvm的信息
        System.out.println(VM.current().details());
        System.out.println("-------------------------");
        //调用之前打印a对象的头信息
        //以表格的形式打印对象布局
        System.out.println(ClassLayout.parseInstance(a).toPrintable());

        System.out.println("-------------------------");
        //调用后再打印a对象的hashcode值
        System.out.println(Integer.toHexString(a.hashCode()));
        System.out.println(ClassLayout.parseInstance(a).toPrintable());

        System.out.println("-------------------------");
        //有线程加重量级锁的时候，再来看对象头
        new Thread(()->{
            try {
                synchronized (a){
                    Thread.sleep(5000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        System.out.println(Integer.toHexString(a.hashCode()));
        System.out.println(ClassLayout.parseInstance(a).toPrintable());
    }
}
```

![image-20211117142233937](01_JDK 源码.assets/image-20211117142233937.png)

![image-20211117142854389](01_JDK 源码.assets/image-20211117142854389.png)

#### 4）怎么生成的？

这里探究一下hashCode的生成以及移动过程。

先从Object.c开始，找到hashCode的映射

```java
static JNINativeMethod methods[] = {
    //JAVA方法        返回值  （参数）                    c++函数
    {"hashCode",    "()I",                    (void *)&JVM_IHashCode},
    {"wait",        "(J)V",                   (void *)&JVM_MonitorWait},
    {"notify",      "()V",                    (void *)&JVM_MonitorNotify},
    {"notifyAll",   "()V",                    (void *)&JVM_MonitorNotifyAll},
    {"clone",       "()Ljava/lang/Object;",   (void *)&JVM_Clone},
};
```

然后全局检索 JVM_IHashCode （ctrl+shift+F）,在jvm.cpp 里有这个函数的实现

```c++
// JVM_ENTRY是一个预加载宏，增加一些样板代码到jvm的所有function中
// 这个api是位于本地方法与jdk之间的一个连接层

//此处是生成hashCode的逻辑
JVM_ENTRY(jint, JVM_IHashCode(JNIEnv* env, jobject handle))
    JVMWrapper("JVM_IHashCode");
//调用了ObjectSynchronizer对象的FastHashCode(synchronizer.cpp)
return handle == NULL ? 0 : ObjectSynchronizer::FastHashCode (THREAD, JNIHandles::resolve_non_null(handle)) ;
JVM_END
```

然后搜索 FastHashCode，下面的函数有点长，大致过程如下图

![image-20211117144854434](01_JDK 源码.assets/image-20211117144854434.png)

```c++
intptr_t ObjectSynchronizer::FastHashCode (Thread * Self, oop obj) {
    //是否开启了偏向锁(Biased：偏向，倾向)
    if (UseBiasedLocking) {
        //如果当前对象处于偏向锁状态
        if (obj->mark()->has_bias_pattern()) {
            Handle hobj (Self, obj) ;
            assert (Universe::verify_in_progress() ||
                    !SafepointSynchronize::is_at_safepoint(),
                    "biases should not be seen by VM thread here");
            //那么就撤销偏向锁（达到无锁状态，revoke：废除）
            BiasedLocking::revoke_and_rebias(hobj, false, JavaThread::current());
            obj = hobj() ;
            //断言下，看看是否撤销成功（撤销后为无锁状态）
            assert(!obj->mark()->has_bias_pattern(), "biases should be revoked by now");
        }
    }

    // ……

    ObjectMonitor* monitor = NULL;
    markOop temp, test;
    intptr_t hash;
    //读出一个稳定的mark;防止对象obj处于膨胀状态；
    //如果正在膨胀，就等他膨胀完毕再读出来
    markOop mark = ReadStableMark (obj);

    //是否撤销了偏向锁（也就是无锁状态）（neutral：中立，不偏不斜的）
    if (mark->is_neutral()) {
        //从mark头上取hash值
        hash = mark->hash(); 
        //如果有，直接返回这个hashcode（xor）
        if (hash) {                       // if it has hash, just return it
            return hash;
        }
        //如果没有就新生成一个(get_next_hash)
        hash = get_next_hash(Self, obj);  // allocate a new hash code
        //生成后，原子性设置，将hash放在对象头里去，这样下次就可以直接取了
        temp = mark->copy_set_hash(hash); // merge the hash code into header
        // use (machine word version) atomic operation to install the hash
        test = (markOop) Atomic::cmpxchg_ptr(temp, obj->mark_addr(), mark);
        if (test == mark) {
            return hash;
        }
        // If atomic operation failed, we must inflate the header
        // into heavy weight monitor. We could add more code here
        // for fast path, but it does not worth the complexity.
        //如果已经升级成了重量级锁，那么找到它的monitor
        //也就是我们所说的内置锁(objectMonitor)，这是c里的数据类型
        //因为锁升级后，mark里的bit位已经不再存储hashcode，而是指向monitor的地址
        //而升级的markword呢？被移到了c的monitor里
    } else if (mark->has_monitor()) {
        //沿着monitor找header，也就是对象头
        monitor = mark->monitor();
        temp = monitor->header();
        assert (temp->is_neutral(), "invariant") ;
        //找到header后取hash返回
        hash = temp->hash();
        if (hash) {
            return hash;
        }
        // Skip to the following code to reduce code size
    } else if (Self->is_lock_owned((address)mark->locker())) {
        //轻量级锁的话，也是从java对象头移到了c里，叫helper
        temp = mark->displaced_mark_helper(); // this is a lightweight monitor owned
        assert (temp->is_neutral(), "invariant") ;
        hash = temp->hash();              // by current thread, check if the displaced
        //找到，返回
        if (hash) {                       // header contains hash code
            return hash;
        }
    }
}
```

为什么要先撤销偏向锁到无锁状态，再来生成hashcode？

- markword 里，hashcode存储的字节位置被偏向锁占了，偏向锁存储了锁持有者的线程id

#### 5）关于hashCode的生成算法

```c++
static inline intptr_t get_next_hash(Thread * Self, oop obj) {
    intptr_t value = 0 ;
    if (hashCode == 0) {
        // This form uses an unguarded global Park-Miller RNG,
        // so it's possible for two threads to race and generate the same RNG.
        // On MP system we'll have lots of RW access to a global, so the
        // mechanism induces lots of coherency traffic.
        value = os::random() ;//返回随机数
    } else if (hashCode == 1) {
        // This variation has the property of being stable (idempotent)
        // between STW operations.  This can be useful in some of the 1-0
        // synchronization schemes.
        //和地址相关，但不是地址；右移+异或算法
        intptr_t addrBits = cast_from_oop<intptr_t>(obj) >> 3 ;
        value = addrBits ^ (addrBits >> 5) ^ GVars.stwRandom ;//随机数位移异或计算
    } else
        if (hashCode == 2) {
            value = 1 ;            // 返回1
        } else if (hashCode == 3) {
            value = ++GVars.hcSequence ;//返回一个Sequence序列号
        } else if (hashCode == 4) {
            value = cast_from_oop<intptr_t>(obj) ;//也不是地址
        } else {
            //常用
            // Marsaglia's xor-shift scheme with thread-specific state
            // This is probably the best overall implementation -- we'll
            // likely make this the default in future releases.
            //马尔萨利亚教授写的xor-shift 随机数算法（异或随机算法)
            unsigned t = Self->_hashStateX ;
            t ^= (t << 11) ;
            Self->_hashStateX = Self->_hashStateY ;
            Self->_hashStateY = Self->_hashStateZ ;
            Self->_hashStateZ = Self->_hashStateW ;
            unsigned v = Self->_hashStateW ;
            v = (v ^ (v >> 19)) ^ (t ^ (t >> 8)) ;
            Self->_hashStateW = v ;
            value = v ;
        }

    value &= markOopDesc::hash_mask;
    if (value == 0) value = 0xBAD ;
    assert (value != markOopDesc::no_hash, "invariant") ;
    TEVENT (hashCode: GENERATE) ;
    return value;
}
```

#### 6）总结

通过分析虚拟机源码我们证明了hashCode不是直接用的内存地址，而是采用一定的算法来生成hashcode存储在MarkWord里，与锁共用一段bit位，这就造成了hashcode与锁的状态相关性。

- 如果是偏向锁

  一旦调用hashcode，偏向锁被撤销，hashcode被保存到markword中，对象被还原为无锁状态。

- 如果这时候线程需要对象锁呢？

  对象回不到偏向锁状态，而是直接升级为重量级锁。hashcode跟随markword被移动到c的object monitor。

### 1.2.4 equals那些问题

#### 1）equals源码

```java
public boolean equals(Object obj) {
    return (this == obj);
}
```

如果我们不做任何操作，equals将继承object的方法，那么它和==也没啥区别！

```java
package com.lishem.obj.euqlas;

public class DefaultEQ {
    String name;

    public DefaultEQ(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        DefaultEQ eq1 = new DefaultEQ("张三");
        DefaultEQ eq2 = new DefaultEQ("张三");
        DefaultEQ eq3 = eq1;

        //虽然俩对象外面看起来一样，eq和==都不行
        //因为我们没有改写equals，它使用默认object的，也就是内存地址
        System.out.println(eq1.equals(eq2)); // false
        System.out.println(eq1 == eq2); // false

        System.out.println("----");
        //1和3是同一个引用
        System.out.println(eq1.equals(eq3)); // true
        System.out.println(eq1 == eq3); // true

        System.out.println("===");
        //以上是对象，再来看基本类型
        int i1 = 1;
        Integer i2 = 1;
        Integer i = new Integer(1);
        Integer j = new Integer(1);

        Integer k = new Integer(2);

        //只要是基本类型，不管值还是包装成对象，都是直接比较大小
        System.out.println(i.equals(i1));  //比较的是值 true
        System.out.println(i == i1);  //拆箱 , true
        // 封装对象i被拆箱，变为值比较，1==1成立
        //相当于 System.out.println(1==1);

        System.out.println(i.equals(j));  // true
        System.out.println(i == j);   //  比较的是地址，这是俩对象 false

        System.out.println(i2 == i); // i2在常量池里，i在堆里，地址不一样 // false

        System.out.println(i.equals(k));  //1和2，不解释 false
    }
}
```

结论：

- “==”比较的是什么？
  - 用于基本数据（8种）类型（或包装类型）相互比较，比较二者的值是否相等。
  - 用于引用数据（类、接口、数组）类型相互比较，比较二者地址是否相等、
- equals比较的什么？
  - 默认情况下，所有对象继承Object，而Object的equals比较的就是内存地址
  - 所以默认情况下，这俩没啥区别

#### 2）那java的内存地址究竟是个啥

从java到jvm：

![image-20211117152045948](01_JDK 源码.assets/image-20211117152045948.png)

然后开辟空间：

![image-20211117152144311](01_JDK 源码.assets/image-20211117152144311.png)

在栈中创建两个局部变量user1和user2，指向堆里的内存

归根结底， == 比较的是两个对象堆内存地址，也就是说栈中局部变量表里存储的值。

#### 3）默认equals的问题

若有需求，若user1和user2，name一样我们就认为是同一个人，应该如何处理？

- 重写equals方法

#### 4）hashCode与equals

后面set的时候再说吧

#### 5）总结

- 特殊业务需求需要重写
- 例如map，key放自定义对象也需要重写
- 重写equals后必须要重写hashCode，要保持逻辑上的一致！

### 1.2.5 关于双等

#### 1）String的特殊性

```java
package com.lishem.obj.euqlas;

public class Intern {
    public static void main(String[] args) {
        String str1 = "张三";//常量池
        String str2 = new String("张三");

        // intern；内存地址是否相等(面试常问)
        System.out.println("str1与str2是否相等>>" +(str1==str2)); // false
        System.out.println("str1与str2是否相等>>" +(str1==str2.intern())); // true
        System.out.println("str1与str2是否相等>>" +(str1==str2)); // false
    }
}
```

![image-20211117153058696](01_JDK 源码.assets/image-20211117153058696.png)

new String（） 在堆上创建字符串对象。当调用intern() 方法时，JVM会将字符串添加到常量池中(堆引用指向常量池)。

注意：

> 1. 1.8版本只是将helloworld 在堆中的引用指向常量池，之前的版本是把helloworld复制到常量池
> 2. 堆：字符串常量池，方法区：运行时常量池



#### 2）valueOf的秘密

关于双等号地址问题，除了String.intern() ， 在基础类型里，如Integer，Long等同样有一个方法：valueOf需要注意

```java
public class ValueOf {

    public static void main(String[] args) {
        System.out.println(Integer.valueOf(127) == Integer.valueOf(127)); // true
        System.out.println(Integer.valueOf(128) == Integer.valueOf(128)); // false
    }
}
```

这个诡异的结果看源码：

```java
public static Integer valueOf(int i) {
    if (i >= IntegerCache.low && i <= IntegerCache.high)
        return IntegerCache.cache[i + (-IntegerCache.low)];
    return new Integer(i);
}
```

底层有个缓存区IntegerCache

# 2 深究ArrayList

## 2.1 ArrayList继承关系

![image-20211117153956208](01_JDK 源码.assets/image-20211117153956208.png)

从继承关系看：

**AbstractList类**

AbstractList，实现了List。List接口我们都知道，提供了相关的添加、删除、修改、遍历等功能

**RandmoAccess接口**

ArrayList 实现了RandmoAccess接口，即提供了随机访问功能； 即list.get(i)

**Cloneable接口**

ArrayList 实现了Cloneable接口，即覆盖了函数clone()，能被克隆

**Serializable接口**

ArrayList 实现java.io.Serializable接口，这意味着ArrayList支持序列化，能通过序列化去传输

## 2.2 动态数组与数据结构

### 2.2.1 概念介绍

ArrayList 是一个数组队列，相当于动态（扩容）数组。

我们直接来看对象头，对其有个简单认识和猜想：（com.alist.InitialList）

```java
package com.lishem.arraylist;

import org.openjdk.jol.info.ClassLayout;

import java.util.ArrayList;

public class ArrayListHeader {

    public static void main(String[] args) {
        int[] i = new int[8];
        ArrayList<Integer> list = new ArrayList(8);
        //将8个int类型依次放入数组和arrayList，注意，一个int占4个字节
        for (int j = 0; j < 8; j++) {
            i[j] = j;
            list.add(j);
        }

        System.out.println(ClassLayout.parseInstance(i).toPrintable());
        System.out.println("=============");
        System.out.println(ClassLayout.parseInstance(list).toPrintable());
        System.out.println(ClassLayout.parseClass(ArrayList.class));
    }
}
```

### 2.2.2 结果分析

![image-20211117202736889](01_JDK 源码.assets/image-20211117202736889.png)

从对象头，我们大致可以看出ArrayList的数据接口

- ArrayList底层用了一个数组存储数据：elementData
- 额外加了几组信息：modeCount(发生修改操作的次数)、size(当前数组长度)

### 2.2.3 结论

从结构可以看出，实际分配空间是10，而存进去的数据是8 (size是8)

> ArrayList外围暴露出来的只是一些操作的表象，底层数据的存储和操作都是基于数组的基础上
>
> 这就意味着，它的特性和数组一样：查询快！删除插入慢。

**为什么ArrayList访问快？**

1. ArrayList底层是数组实现的
2. 数组是一块连续的**内存**空间
3. 获取数据可以直接拿地址偏移量（get（i））

因此，访问的时间复杂度是O(1)

**为什么删除和增加慢？**

增删会带来元素的移动，增加数据会向后移动，删除数据会向前移动，所以影响效率

![image-20211117203355607](01_JDK 源码.assets/image-20211117203355607.png)

因此：插入、删除的时间复杂度是O(n)

## 2.3 ArrayList源码深入解析

### 2.3.1 源码分析之全局变量

```java
private static final int DEFAULT_CAPACITY = 10;//默认的初始化容量
private static final Object[] EMPTY_ELEMENTDATA = {};//空，对象数组，注意static，所有空arraylist共享
private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {}; //空，无参构造使用（1.8才有）
transient Object[] elementData; // 当前数据对象存放的地方，注意是transient，虽然数组实现了serializable接口，但是它的数据不会被默认的ObjectOutputStream序列化，想做网络传输，自己改写writeObject和readObject方法！
private int size;//当前数据的个数
private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;//数组最大长度？（扩容部分有彩蛋）
```

### 2.3.2 源码分析之构造器

**1）无参构造函数**

如果不传入参数，则使用默认无参构建方法创建ArrayList对象，如下：

```java
public ArrayList() {
    //默认构造函数，很简单，就是把default empty数组赋给了data
    //jdk8里才有DEFAULTCAPACITY_EMPTY_ELEMENTDATA这货，并且仅仅被用在了这个构造函数里
    //官方的解释是，为了区分判断第一次add的时候，数组初始化的容量
    //这个秘密藏在calculateCapacity里（下文会讲）
    this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
}
```

**2）带int类型的构造函数**

如果传入参数，则代表指定ArrayList的初始数组长度，传入参数如果是大于等于0，则使用用户的参数初始化，如果用户传入的参数小于0，则抛出异常，构造方法如下：

```java
public ArrayList(int initialCapacity) {
    if (initialCapacity > 0) {
        //以指定容量初始化Object数组
        this.elementData = new Object[initialCapacity];//初始化容量
    } else if (initialCapacity == 0) {
        //如果指定0的话，用empty数组
        this.elementData = EMPTY_ELEMENTDATA;
    } else {
        //否则，如果是负数的话，扔一个异常出来（哪有长度为负数的？？）
        throw new IllegalArgumentException("Illegal Capacity: "+
                                           initialCapacity);
    }
}
```

**3）带Collection对象的构造函数**

```java
public ArrayList(Collection<? extends E> c) {
    //集合转换成数组
    elementData = c.toArray();
    //将data长度赋值给size属性
    if ((size = elementData.length) != 0) {
        // 官方注释：c.toArray might (incorrectly) not return Object[] (see 6260652)
        // 翻译：toArray不一定会返回Object数组，参考jdk的bug号……汗！
        // 如果不是Object数组，转成Object[]
        if (elementData.getClass() != Object[].class)
            elementData = Arrays.copyOf(elementData, size, Object[].class);//数组赋值，类型转换
    } else {
        // 如果数据为空，将empty赋给data
        this.elementData = EMPTY_ELEMENTDATA;
    }
}
```

Collection构造器意味着，你可以使用以下一揽子集合对象：

![image-20201222180143381](01_JDK 源码.assets/image-20201222180143381.png)

问题：无参构造和0长度构造有什么区别？

```java
package com.alist;

import org.openjdk.jol.info.ClassLayout;

import java.util.ArrayList;

public class InitialList {
    public static void main(String[] args) {
        //两种方式构建list，有什么区别？
        ArrayList list1 = new ArrayList();
        ArrayList list2 = new ArrayList(0);

        //打印对象头
        System.out.println(ClassLayout.parseInstance(list1).toPrintable());
        System.out.println(ClassLayout.parseInstance(list2).toPrintable());

        System.out.println("========");

        //add一个元素之后再来打印试试
        list1.add(1);
        list2.add(1);

        System.out.println(ClassLayout.parseInstance(list1).toPrintable());
        System.out.println(ClassLayout.parseInstance(list2).toPrintable());
    }
}
```

![image-20211117204946616](01_JDK 源码.assets/image-20211117204946616.png)

原理：

```java
//calculateCapacity
//每次元素变动，比如add，会调用该函数判断容量情况
private static int calculateCapacity(Object[] elementData, int minCapacity) {
    //定义default empty数组的意义就在这里！用于扩容时判断当初采用的是哪种构造函数
    if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
        //如果是无参的构造函数，用的就是该default empty
        //那么第一次add时候，容量取default和min中较大者
        return Math.max(DEFAULT_CAPACITY, minCapacity);
    }
    //如果是另外两个构造函数，比如指定容量为5，或者初始参数collection为5
    //那就直接返回5，一定程度上，节约了内存空间
    return minCapacity;
}
```

结论：

- 刚构造时，都是空的！add时才初始化（这里容易误解，以为默认构造器data初始化就是10）
- 虽然list可以自动扩容，但尽量初始就预估并定义list的容量，少用无参的构造器，尤其小于10的时候
- default empty存在的意义：判断那种构造函数来的，初始阶段节约了扩容的空间占用

### 2.3.3 扩容

![image-20211117205135814](01_JDK 源码.assets/image-20211117205135814.png)

```java
public boolean add(E e) {
    //确认容量，不够则扩容
    ensureCapacityInternal(size + 1);
    //将元素追加到数组的末尾去，同时计数增size++
    elementData[size++] = e;
    return true;
}

private void ensureCapacityInternal(int minCapacity) {
    ensureExplicitCapacity(calculateCapacity(elementData, minCapacity));
}

private static int calculateCapacity(Object[] elementData, int minCapacity) {
    if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
        return Math.max(DEFAULT_CAPACITY, minCapacity);
    }
    return minCapacity;
}

private void ensureExplicitCapacity(int minCapacity) {
    modCount++;

    // overflow-conscious code
    if (minCapacity - elementData.length > 0)
        grow(minCapacity);
}

private void grow(int minCapacity) {
    //minCapacity：我需要的最小长度，也就是上面的size+1
    int oldCapacity = elementData.length;//先取出旧数组大小
    int newCapacity = oldCapacity + (oldCapacity >> 1);//扩容为旧数组的1.5倍；右移一位（除以2）
    if (newCapacity - minCapacity < 0)//如果扩容1.5后还不够
        newCapacity = minCapacity;//取需求量为新长度，数组的扩容还是比较保守和吝啬！
    if (newCapacity - MAX_ARRAY_SIZE > 0)//新长度大于数组最大长度，彩蛋来了！
        newCapacity = hugeCapacity(minCapacity);//看下面的huge方法 ↓
    // minCapacity is usually close to size, so this is a win:
    elementData = Arrays.copyOf(elementData, newCapacity);//返回一个新的数组对象
}


private static int hugeCapacity(int minCapacity) {
    if (minCapacity < 0) // 负数说明超出了Integer的范围，溢出了，抛异常
        throw new OutOfMemoryError();
    //否则：返回Integer的最大值，而不是最大值-8
    return (minCapacity > MAX_ARRAY_SIZE) ?
        Integer.MAX_VALUE :
    MAX_ARRAY_SIZE;
}

//这是为什么呢？我们一开始integer-8还有啥意义？
//让我们返回去，看这个变量的注释：
//翻译：有些VM会在array头上预留信息，企图大于这个值也行，但是不保证安全性，可能会溢出报错！

/**
 * The maximum size of array to allocate.
 * Some VMs reserve some header words in an array.
 * Attempts to allocate larger arrays may result in
 * OutOfMemoryError: Requested array size exceeds VM limit
 */
private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;
```

总结：

1. 按1.5倍扩容，如果1.5还不够，取你想要的容量（总之保证够你用的）
2. 数组最大容量是integer的max_value，但是达到这个值的时候，arraylist不保证稳定可靠！

### 2.2.4 get方法

![image-20211117205553223](01_JDK 源码.assets/image-20211117205553223.png)

```java
public E get(int index) { //返回list中指定位置的元素
    rangeCheck(index);//越界检查

    return elementData(index);//返回list中指定位置的元素，数组访问，贼快~
}
```

### 2.2.5 remove方法

![image-20211117205630472](01_JDK 源码.assets/image-20211117205630472.png)

```java
public E remove(int index) {
    rangeCheck(index);//数组越界检查

    modCount++;//结构性修改次数+1
    E oldValue = elementData(index);//将要移除的元素

    int numMoved = size - index - 1;//删除指定元素后，需要左移的元素个数(graph)
    if (numMoved > 0)//如果有需要左移的元素，就移动（移动后，要删除的元素就已经被覆盖了）
        //参数：src、src   dest、dest、移动的长度
        //从data的index+1到data的index，也就是元素挨个前移一格，一共移动num个
        System.arraycopy(elementData, index+1, elementData, index, numMoved);
    //左移后，最后一个位置还有值，给他搞成null，下一步gc会把对象收走，size计数减少
    //（借助断点查看data数组的最后一个元素的值）
    elementData[--size] = null; 

    return oldValue;//返回刚才要删除的值
}
```

总结：

1. 移除后 ，后面的节点通过数组拷贝的方式需要左移
2. 如果末端太长，remove是非常耗费性能的

### 2.3.6 set方法

```java
public E set(int index, E element) {
    rangeCheck(index);//越界检查
    E oldValue = elementData(index);//修改前的原素质
    elementData[index] = element;//新元素赋值
    return oldValue;//返回旧的元素
}
```

### 2.3.7 clear 方法

```java
public void clear() { //从列表中删除所有元素。该调用返回后，数组将为空    
    modCount++;//修改测试自增    
    // clear to let GC do its work    
    for (int i = 0; i < size; i++)        
        elementData[i] = null;//清除表元素    
    size = 0;//大小为0
}
```

清除就是设置为null、大小设置为0；设置null，方便gc

clear过后，size=0，但是table的大小并没有回缩！

## 2.4 ArralyList常见面试题

**1） 哪些集合实现了List接口和Collection接口，各自的优缺点是什么**

![image-20211117210026752](01_JDK 源码.assets/image-20211117210026752.png)

通过上面类图可用看出，List接口下有4个实现类，分别为：**LinkedList、ArrayList、Vector和Stack**。

| 序号 | 实现类     | 介绍                                                         |
| ---- | ---------- | ------------------------------------------------------------ |
| 1    | LinkedList | 底层数据结构是链表，查询慢，增删快，线程不安全，效率高，可以存储重复元素 |
| 2    | ArrayList  | 底层数据结构是数组，查询快，增删慢，线程不安全，效率高，可以存储重复元素 |
| 3    | Vector     | 底层数据结构是数组，查询快，增删慢，线程安全，效率低，可以存储重复元素 |
| 4    | Stack      | 底层数据结构是数组，先进后出（FILO）的特性                   |

**2）ArrayList提供了几种查询方式、效率如何？**

迭代器遍历方式：

```java
Integer value = null;
Iterator iter = list.iterator();
while (iter.hasNext()) {
    value = (Integer)iter.next();
}
```

索引访问遍历

```java
Integer value = null;
for (int i=0; i<list.size(); i++) {
    value = (Integer)list.get(i);        
}
```

for-each循环遍历

```java
public void show(List<Object> list){
    list.forEach( s -> System.out.println(s));
}
```

数据量不大的时候，三种方式差不多

数据量不断上升的时候 foreach 表现最好。

**3）ArrayList 可以存放null吗？**

可以

**4） ArrayList是如何扩容的？**

- 在用无参构造来创建对象的时候其实就是创建了一个空数组，长度为0。add时先分配一个默认大小10，后续扩容，每次扩容都是原容量的1.5倍。
- 在有参构造中，传入的参数是正整数就按照传入的参数来确定创建数组的大小。再进行扩容，每次扩容都是原容量的1.5倍

**5）ArrayList插入删除一定慢么**

不是，在尾部也很快。





