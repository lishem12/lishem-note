# 1.Tomcat环境搭建

## 1.1 下载源码

https://tomcat.apache.org/download-80.cgi

![image-20211118113252258](04_Tomcat.assets/image-20211118113252258.png)

解压即可

## 1.2 增加pom

在项目根目录，和java目录同级，增加pom文件，内容如下：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>org.apache.tomcat</groupId>
  <artifactId>apache-tomcat-8.5.63-src</artifactId>
  <name>Tomcat8.5</name>
  <version>8.5</version>
  <build>
    <!-- 指定源码目录-->
    <finalName>Tomcat8.5</finalName>
    <sourceDirectory>java</sourceDirectory>
    <resources>
      <resource>
        <directory>java</directory>
      </resource>
    </resources>
    <plugins>
      <!-- 引入编译插件 -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.8.1</version>
        <configuration>
          <encoding>UTF-8</encoding>
          <source>11</source>
          <target>11</target>
        </configuration>
      </plugin>
    </plugins>
  </build>
  <!-- tomcat 依赖基础包 -->
  <dependencies>
    <dependency>
      <groupId>org.easymock</groupId>
      <artifactId>easymock</artifactId>
      <version>3.4</version>
    </dependency>
    <dependency>
      <groupId>ant</groupId>
      <artifactId>ant</artifactId>
      <version>1.7.0</version>
    </dependency>
    <dependency>
      <groupId>wsdl4j</groupId>
      <artifactId>wsdl4j</artifactId>
      <version>1.6.2</version>
    </dependency>
    <dependency>
      <groupId>javax.xml</groupId>
      <artifactId>jaxrpc</artifactId>
      <version>1.1</version>
    </dependency>
    <dependency>
      <groupId>org.eclipse.jdt.core.compiler</groupId>
      <artifactId>ecj</artifactId>
      <version>4.5.1</version>
    </dependency>
    <dependency>
      <groupId>javax.xml.soap</groupId>
      <artifactId>javax.xml.soap-api</artifactId>
      <version>1.4.0</version>
    </dependency>
  </dependencies>
</project>
```

## 1.3 常见错误及其解决

找到org.apache.catalina.startup.Bootstrap类中的main 方法，启动，会报错

**1）找不到sun.rmi.rigistry**

![image-20211118130021108](04_Tomcat.assets/image-20211118130021108.png)

原因：sun的包对ide编译环境不可见造成的，鼠标放在代码中报红的地方，根据idea的提示操作即可

![image-20211118130103319](04_Tomcat.assets/image-20211118130103319.png)

注意：不要用maven去编译它，这个参数你加入的是idea的环境

**2）访问请求错误**

上面的错误解决之后，启动程序，访问 localhost:8080,会出现错误

![image-20211118130309609](04_Tomcat.assets/image-20211118130309609.png)

原因是Jsp引擎Jasper没有被初始化，从而无法编译JSP

我们在tomcat源码的org.apache.catalina.startup.ContextConfig 类的 configureStart 方法中，加入如下代码：

```java
webConfig();

// 初始化JSP解析引擎 -- 只有这一句是需要加的
context.addServletContainerInitializer(new JasperInitializer(),null);

if (!context.getIgnoreAnnotations()) {
  applicationAnnotationsConfig();
}
if (ok) {
  validateSecurityRoles();
}
```

然后启动，如果JDK是11 版本，就可以正常启动了。

# 2 Tomcat架构与源码剖析

## 2.1 Tomcat总体架构

![image-20211118130828245](04_Tomcat.assets/image-20211118130828245.png)

从Tomcat安装目录下的**/conf/server.xml** 文件里可以看到最顶层的是server。

对照上面的关系图，一个Tomcat实例对应一个server,一个 Server 中有一个或者多个 Service.

一个 Service 中有多个连接器和一个容器，Service组件本身没做其他事。

只是把连接器和容器组装起来。连接器与容器之间通过标准的 ServletRequest 和 ServletResponse 通信。

**名词解释：**

- Server：Server容器就代表一个Tomcat实例（Catalina实例），其下可以有一个或者多个Service容器。
- Service：Service是提供具体对外服务的（默认只有一个），一个Service容器中又可以**有多个Connector组件**（监听不同端口请求，解析请求）和一个**Servlet容器**（做具体的业务逻辑处理）；
- Engine和Host：Engine组件（引擎）是Servlet容器Catalina的核心，它支持在其下定义多个虚拟主机（Host），虚拟主机允许Tomcat引擎在将配置在一台机器上的多个域名，比如www.baidu.com、www.bat.com分割开来互不干扰；
- Context：每个虚拟主机又可以支持多个web应用部署在它下边，这就是我们所熟知的上下文对象Context，上下文是使用由Servlet规范中指定的Web应用程序格式表示，不论是压缩过的war包形式的文件还是未压缩的目录形式；
- Wrapper：在上下文中又可以部署多个servlet，并且每个servlet都会被一个包装组件（Wrapper）所包含（一个wrapper对应一个servlet）

去掉注释的server.xml:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Server port="8005" shutdown="SHUTDOWN">
  <Listener className="org.apache.catalina.startup.VersionLoggerListener" />
  <Listener className="org.apache.catalina.core.AprLifecycleListener" SSLEngine="on" />
  <Listener className="org.apache.catalina.core.JreMemoryLeakPreventionListener" />
  <Listener className="org.apache.catalina.mbeans.GlobalResourcesLifecycleListener" />
  <Listener className="org.apache.catalina.core.ThreadLocalLeakPreventionListener" />
  <GlobalNamingResources>
    <Resource name="UserDatabase" auth="Container"
              type="org.apache.catalina.UserDatabase"
              description="User database that can be updated and saved"
              factory="org.apache.catalina.users.MemoryUserDatabaseFactory"
              pathname="conf/tomcat-users.xml" />
  </GlobalNamingResources>
  <Service name="Catalina">

    <Connector port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />
    <Engine name="Catalina" defaultHost="localhost">
      <Realm className="org.apache.catalina.realm.LockOutRealm">
        <Realm className="org.apache.catalina.realm.UserDatabaseRealm"
               resourceName="UserDatabase"/>
      </Realm>
      <Host name="localhost"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">

        <Valve className="org.apache.catalina.valves.AccessLogValve" directory="logs"
               prefix="localhost_access_log" suffix=".txt"
               pattern="%h %l %u %t &quot;%r&quot; %s %b" />
      </Host>
    </Engine>
  </Service>
</Server>
```

## 2.2 Tomcat链接器

负责对外交流的连接器(Connector)

**连接器主要功能：**

1. 网络通信应用协议解析，读取请求数据
2. 将Tomcat的Request/Response转成标准的Servlet Request/Response

因此Tomcat设计者又设计了三个组件来完成这个三个功能，分别是**EndPoint**、**Processor**和**Adaptor**，其中**EndPoint**和**Processor**又一起抽象成**ProtocalHandler**组件，画图理解：

![image-20211118133025449](04_Tomcat.assets/image-20211118133025449.png)

下面的源码我们会详细看到处理的转交过程：

- Connector 给handler，handler最终调用endpoint
- processor负责Tomcat Request对象给Adaptor
- Adapter负责提供ServleRequest对象给容器

## 2.3 Tomcat源码剖析

### 2.3.1 start.sh 如何启动

用过Tomcat的我们都知道，可以通过Tomcat的/bin目录下的脚本shtartup.cmd或者startup.sh来启动tomcat，那么这个脚本肯定就是Tomcat的启动入口了，那么执行这个脚本后发生了什么呢？

![image-20211118162614046](04_Tomcat.assets/image-20211118162614046.png)

1. Tomcat本质上也是一个java程序，因此startup.sh脚本会启动一个JVM来运行Tomcat的启动类BootStrap
2. Bootstrap的主要任务是初始化Tomcat的类加载器，并创建Catalina
3. Catalina是一个启动类，它通过解析server.xml创建相应的组件，并调用Server的start法啊
4. Server组件的职责就是管理Service组件，它会负责调用Service的start方法
5. Service组件的职责就是管理连接器和顶层容器Engine，它会调用连接器和 Engine的start方法
6. Engin组件负责启动管理子容器，通过调用Host的start方法，将Tomcat的各层容器启动起来（这里是分层级的，上层容器管理下层容器）



### 2.3.2 生命周期统一管理组件

#### Lifecycle接口

Tomcat要启动，肯定要把架构中提到的组件进行实例化（实例化创建–>销毁等：生命周期）。

Tomcat中那么多组件，为了统一规范它们的生命周期，Tomcat抽象除了LifeCycle生命周期接口。

server.xml 中的各个节点都是它的实现类。

**Lifecycle生命周期接口方法：**

![image-20211118163226617](04_Tomcat.assets/image-20211118163226617.png)

我们这里把Lifecycle接口定义分为两部分：

- 生命周期方法，如：init() ，start()，stop()，destroy()

- 拓展接口方法，比如添加状态和监视器

    ```java
    public interface Lifecycle{
      // 添加监听器
      public void addLifecycleListener(LifecycleListener listener);
      // 获取所有监听器
      public LifecycleListener[] findLifecycleListeners();
      // 移除某个监听方法
      public void removeLifecycleListener(LifecycleListener listener);
      // ...
    }
    ```

> 因为所有组件都实现了Lifecycle接口
>
> 在父组件的init() 方法里创建子组件并调用子组件的init() 方法
>
> 在父组件的start() 方法里调用子组件的start() 方法
>
> 那么调用者就可以无差别的只调用最顶层组件，也就是Server组件的init() 方法和start() 方法，整个Tomcat就被启动起来了。

### 2.3.3 Tomcat启动入口

#### 1）启动流程图

startup.sh --> catalina.sh start --> java xxxx.jar org.apache.catalina.startup.Bootstrap(main) start(参数)

![image-20211118164119517](04_Tomcat.assets/image-20211118164119517.png)

#### 2）系统配置与入口

当找到org.apache.catalina.startup.Bootstrap类的main方法时，启动，但是真正执行的第一行代码不在这里

这个类有个 static  静态代码块，所以先会执行如下静态代码块：

```java
/**
 * 静态块【重要！】
 * 主要设置了两个系统变量， catalina.home 和 catalina.base
 * 这俩变量，用来确定tomcat运行环境的根目录。
 * 后面的查找都会基于它们而来。
 * 顺序如下：
 * catalina.home ：     -Dcatalina.home   >   tomcat根目录
 * catalina.base ：    -Dcatalina.base  >  catalina.home(默认)
 */
static {
  // Will always be non-null 这里会拿到项目根目录
  String userDir = System.getProperty("user.dir");

  // Home first
  String home = System.getProperty(Constants.CATALINA_HOME_PROP);
  File homeFile = null;

  if (home != null) {
    File f = new File(home);
    try {
      homeFile = f.getCanonicalFile();
    } catch (IOException ioe) {
      homeFile = f.getAbsoluteFile();
    }
  }

  if (homeFile == null) {
    // First fall-back. See if current directory is a bin directory
    // in a normal Tomcat install
    File bootstrapJar = new File(userDir, "bootstrap.jar");

    if (bootstrapJar.exists()) {
      File f = new File(userDir, "..");
      try {
        homeFile = f.getCanonicalFile();
      } catch (IOException ioe) {
        homeFile = f.getAbsoluteFile();
      }
    }
  }

  if (homeFile == null) {
    // Second fall-back. Use current directory
    File f = new File(userDir);
    try {
      homeFile = f.getCanonicalFile();
    } catch (IOException ioe) {
      homeFile = f.getAbsoluteFile();
    }
  }

  catalinaHomeFile = homeFile;
  System.setProperty(
    Constants.CATALINA_HOME_PROP, catalinaHomeFile.getPath());

  // Then base
  String base = System.getProperty(Constants.CATALINA_BASE_PROP);
  if (base == null) {
    catalinaBaseFile = catalinaHomeFile;
  } else {
    File baseFile = new File(base);
    try {
      baseFile = baseFile.getCanonicalFile();
    } catch (IOException ioe) {
      baseFile = baseFile.getAbsoluteFile();
    }
    catalinaBaseFile = baseFile;
  }
  System.setProperty(
    Constants.CATALINA_BASE_PROP, catalinaBaseFile.getPath());
}
```

总之，静态代码块就将catalina.home 和 catalina.base 设置好，并且放到 System.property中，其它地方可以取到。

然后才去执行main 方法中的逻辑。

```java
//目标
//两大阶段 1、初始化  2、启动阶段
//1、核心方法就是调用init方法；初始化deamon成员变量     （初始化阶段）
//2、当接收到start命令时，调用deamon的load和start方法启动    （启动阶段）
public static void main(String[] args) {
  //***********************初始化阶段***************************************
  // *****  注意！！在本类的方法启动前，顶部的static块打上断点，里面藏着秘密！******
  synchronized (daemonLock) {
    if (daemon == null) {
      // Don't set daemon until init() has completed
      Bootstrap bootstrap = new Bootstrap();//实例化一个引导类对象
      try {
        //                    1. 初始化Bootstrap，主要包括实例化Tomcat特有的类加载器和Catalina实例
        //简单理解为init方法，就是用来构造Catalina实例的(排除类加载器)
        bootstrap.init();  // ===> 初始化
      } catch (Throwable t) {
        handleThrowable(t);
        t.printStackTrace();
        return;
      }
      daemon = bootstrap;//赋值（volatile修饰）
    } else {
      // When running as a service the call to stop will be on a new
      // thread so make sure the correct class loader is used to
      // prevent a range of class not found exceptions.
      Thread.currentThread().setContextClassLoader(daemon.catalinaLoader);
    }
  }


  //***********************Tomcat启动阶段****************************************
  try {
    String command = "start";//设置为启动命令（默认）
    if (args.length > 0) {  // 如果参数有，那么参数为准，stop等其他命令~
      command = args[args.length - 1];
    }

    if (command.equals("startd")) {
      args[args.length - 1] = "start";
      daemon.load(args);
      daemon.start();
    } else if (command.equals("stopd")) {
      args[args.length - 1] = "stop";
      daemon.stop();
    } else if (command.equals("start")) {
      daemon.setAwait(true);//用来设置主线程不自动关闭    ===>  开始调用catalina里的方法，都是反射形式
      daemon.load(args);//===>   【重点】2、反射调用Catalina#load(args)方法，始化一些资源，优先加载conf/server.xml
      daemon.start();//===>  【重点】3、反射调用Catalina.start()  开始启动
      //总结
      //Tomcat是通过调用Catalina的load()和start()方法，完成了Tomcat的启动
      //load()方法负责组件的初始化
      //start()方法负责组件的启动
      if (null == daemon.getServer()) {
        System.exit(1);
      }
    } else if (command.equals("stop")) {
      daemon.stopServer(args);
    } else if (command.equals("configtest")) {
      daemon.load(args);
      if (null == daemon.getServer()) {
        System.exit(1);
      }
      System.exit(0);
    } else {
      log.warn("Bootstrap: command \"" + command + "\" does not exist.");
    }
  } catch (Throwable t) {
    // Unwrap the Exception for clearer error reporting
    if (t instanceof InvocationTargetException &&
        t.getCause() != null) {
      t = t.getCause();
    }
    handleThrowable(t);
    t.printStackTrace();
    System.exit(1);
  }
}
```

这么main 方法这么长，其实分为两部分，分为init() 和 start()  两部分，上面代码也用分割线隔开。那我们一点点看。

### 2.3.4 Bootstrap的init方法剖析

<img src="04_Tomcat.assets/image-20211118165640539.png" alt="image-20211118165640539" style="zoom:50%;" />

> 类加载器问题：
>
> 见下篇文章，有点复杂

https://blog.csdn.net/weixin_41835612/article/details/111401857

```java
// Bootstrap.java

//1、初始化类加载器
//2、加载catalina类，并且实例化
//3、反射调用Catalina的setParentClassLoader方法
//4、实例 赋值
public void init() throws Exception {
  // 1. 初始化Tomcat类加载器(3个类加载器)
  initClassLoaders();  // ===> classloader , 3个
  // 用上面生成的catalinaLoader作为启动线程的上下文类加载器
  Thread.currentThread().setContextClassLoader(catalinaLoader);

  SecurityClassLoad.securityClassLoad(catalinaLoader);

  // Load our startup class and call its process() method
  if (log.isDebugEnabled())
    log.debug("Loading startup class");
  // 2. 用loader来实例化Catalina实例【重要节点】
  Class<?> startupClass = catalinaLoader.loadClass("org.apache.catalina.startup.Catalina");
  Object startupInstance = startupClass.getConstructor().newInstance();

  // Set the shared extensions class loader
  if (log.isDebugEnabled())
    log.debug("Setting startup class properties");
  
  String methodName = "setParentClassLoader";
  Class<?>[] paramTypes = new Class[1];
  paramTypes[0] = Class.forName("java.lang.ClassLoader");
  Object[] paramValues = new Object[1];
  paramValues[0] = sharedLoader;
  // 3. 反射调用Catalina的setParentClassLoader方法，
  // 将sharedLoader设置为Catalina的parentClassLoader成员变量
  // 思考一下，为什么？？？
  //  ----  为了让 Catalina类去加载 shared下的一堆对象，注意看tomcat的classloader继承关系
  //  后面的webappLoader，jsploader 都是shared的子加载器。
  Method method =
    startupInstance.getClass().getMethod(methodName, paramTypes);
  method.invoke(startupInstance, paramValues);
  //4、将catalina实例赋值
  catalinaDaemon = startupInstance;

  // 结论： bootstrap只是个启动点和流程控制，真正干活的，交给了 catalina类，我们下面重点看它的动作
}
```

```java
// 创建commonLoader类加载器
// 创建catalinaLoader类加载器
// 创建sharedLoader类加载器
private void initClassLoaders() {
  try {
    commonLoader = createClassLoader("common", null);  // ===> 创建common
    if (commonLoader == null) {
      // no config file, default to this loader - we might be in a 'single' env.
      commonLoader = this.getClass().getClassLoader();
    }
    catalinaLoader = createClassLoader("server", commonLoader);  // ===> 同样的方法，common为父加载器
    sharedLoader = createClassLoader("shared", commonLoader);
  } catch (Throwable t) {
    handleThrowable(t);
    log.error("Class loader creation threw exception", t);
    System.exit(1);
  }
}
```

### 2.3.5 Catalina的load方法剖析

> org.apache.catalina.startup.Bootstrap#main中的load方法 调用的是catalina中的方法

load（包括下面的start）的调用流程核心技术在于，这些类都实现了 2.3.2 里的 生命周期接口。每个节点自己完成的任务后，会接着调用子节点（如果有的话）的同样的方法，引起链式反应。反映到流程图如下，下面的debug，包括start我们以图跟代码结合debug：



### 2.3.6 Catalina的start方法剖析

### 2.3.7 请求的处理

### 2.3.8 tomcat 的关闭



















