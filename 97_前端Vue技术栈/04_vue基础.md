# 1.VUE基础语法

vscode 安装 View In Browser

## 1.1 绑定字段

v-model  

{{}} 显示值  也可以写  v-text="XXX"  v-html = 'xxx'

其中 text 就是文本， html 会是渲染以后的

v-bind:src = "img" 绑定数据 其中 v-bind 可以省略

v-on:click="方法名（）" 不用带括号  v-on 可以简写为 @

## 1.2 监听字段

写在computed 块儿中，其中定义的方法，其实等同于 data模块中的变量

写在 watch 快儿中，也有类似的作用

```javascript
computed:{//初始  自动加载 ，和变量是共享的（在data中定义的变量，就不能再在computed中定义）
    info(){//zs
        console.log('info...');
        return this.username +',' + this.age  
    } 
}
// 监听 ：html->data  (v -> m)
watch:{
    //username -> age
    username:function(value){ //value：username的属性值
        this.age  = value + '***' ;
    }
}
```

还有一种写法：

```javascript
var vm = new Vue({
    el:"#xxx"
    //....
})
vm.$watch('username', function(value){
    this.age  = value + '===' ;
} )
```

## 1.3 class样式绑定

绑定变量：

```javascript
// html
<h3 :class="css1">info:{{info}}</h3>

// js
var vm = new Vue({
    el:"#xxx",
    data:{
        css1: 'mystyle1' // 这里记得单引号
    }
})

// css
.mystyle1 {
    background-color: blue;
    width: 100px;
    height: 100px;
}
```



```html
<!-- :class的值 是一个对象   {k1:v1,k2:v2...}
如果对象中的变量值true，则生效；否则不生效
-->
<h3 :class="{mystyle1:isMale, mystyle3:isFemale}">info---:{{info}}</h3>
```

绑定数组：

```html
<!-- 绑定数组 []-->
<h3 :class="['mystyle1', 'mystyle3']">info***:{{info}}</h3>
```

## 1.4 style样式绑定

注意：这样需要用驼峰命名法

```html
<!-- :style={样式名:样式值(<从data中获取>),x:x,...}-->
<!-- 不能 xxx-yyyy，而需要使用 xxxYyyy-->
<h3 :style="{backgroundColor:x,color:y}">info000000:{{info}}</h3>
```

v-if 成立才会显示

```html
<!--  取值->data -->
<h3 v-if="isMale">11111info111111:{{info}}</h3>
<h3 v-else>111info111:{{info}}</h3>
```

v-show 直接用bool值决定显示不显示

```html
<!-- v-show可以在源码中 追踪到隐藏的元素；v-if无法追踪-->
<h3 v-show="isMale">222%%%info%%%%222:{{info}}</h3>
<h3 v-show="!isMale">222***info***222:{{info}}</h3>
```

## 1.5 v-for 以及更新删除

```html
<!-- 遍历对象数组-->
<ul>
    <li v-for="(s,index) in queryStuentsResult" :key="index">
        {{index}} - {{s.name}} - {{s.age}} - 
        <input type="button" value="删除" @click="deleteStudentByIndex(index)">---
        <input type="button" value="更新" @click="updateStudentByIndex(index , {name:'x',age:100} )">
    </li>
    </li>
</ul>
```

```javascript
var vm = new Vue({
    el: '#vue01',  //选择器
    data: {//model ，变量
        students: [
            { name: 'zs', age: 23 },
            { name: 'ls', age: 24 },
            { name: 'ww', age: 25 },
            { name: 'zl', age: 26 },
            { name: 'sq', age: 27 }]  //对象数组
    },
    methods:{
        deleteStudentByIndex(index) {
            //删除数组元素:splice
            //删除完毕后，数组元素 会自动重新排序
            this.students.splice(index, 1);
        },
        updateStudentByIndex(index, stu) {
            this.students.splice(index, 1, stu);
        }
    }
}
```

## 1.6 筛选

```html
<ul>
    <li v-for="( name,value) in students[2]" :key="name">
        {{name}} --{{value}}
    </li>
    </li>
</ul>
```

```javascript
var vm = new Vue({
    el: '#vue01',  //选择器
    data: {//model ，变量
        students: [
            { name: 'zs', age: 23 },
            { name: 'ls', age: 24 },
            { name: 'ww', age: 25 },
            { name: 'zl', age: 26 },
            { name: 'sq', age: 27 }]  //对象数组
    },
    methods:{
        //..
    },
    computed:{
        //筛选后的新数组(筛选+排序)
        queryStuentsResult() {
            // 新数组
            var queryStudents;//s
            //筛选条件
            var { queryname, students, ordered } = this;//this 就是data
            //根据查询条件，筛选出新数组
            queryStudents = students.filter(
                stu => stu.name.indexOf(queryname) > -1
            )
            //排序
            queryStudents.sort(function (s1, s2) {
                if (ordered == 0) {//升序
                    return s1.age - s2.age
                    //3-2
                } else{//降序
                    return s2.age - s1.age
                }
            })
            return queryStudents;
        }
    }
}
```

> computed 运行时机：加载时，或者 data 中的数据有更改时

## 1.7 传播行为与事件

拿DOM：

```html
<input type="button" value="按钮---" @click="myclick" />
<!-- <input type="button" value="按钮"  @click="myclick()" /> -->
<input type="button" value="按钮2" @click="myclick2($event)" />
```

```javascript
var vm = new Vue({
    el: '#vue01',  //选择器
    data: {//model ，变量
		//...
    },
    methods: {
        //event.target:传入的当前dom对象
        // <input ...>
        myclick(event) {
            alert(event.target.value)
        },
        myclick2(event) {
            alert(event.target.value)
        }
    }
}
```

传播事件：

```html
<div :class="css1" @click='myclick3()'>
    outer
    <div :class="css2" @click.stop='myclick4()'>
        inner  加了stop就可以阻止传播行为，不加 就会传播到外框 click
    </div>
</div>

<!-- 阻止跳转到百度 -->
<a href="https://www.baidu.com" @click.prevent='myclick5()'>百度</a>

<!-- 按键会弹出 -->
<input @keyup.13='myclick6'>
```

```javascript
var vm = new Vue({
    el: '#vue01',  //选择器
    data: {//model ，变量
        //...
    },
    methods: {
        myclick3() {
            alert('outer')
        },
        myclick4() {
            alert('inner')
        },
        myclick6(event) {
            //event.target :当前dom对象
            // event.keyCode：按键的值 a : 65
            alert(event.keyCode + '--' + event.target.value)
        },
    }
}
```

## 1.8 表单

```html
<body >
    <div  id="vue02" v-cloak  >

        <p v-text-upper="myVariable"></p>
        <form  action=""  @submit.prevent="mysubmit">
            用户名：<input type="text" v-model="user.username" ><br>
            密码：<input type="password" v-model="user.password" ><br>
            性别：
            <input type="radio" value="male"  v-model="user.password">男<br>
            <input type="radio" value="female"  v-model="user.password">女<br>
            兴趣：
            <input type="checkbox" value="football"   v-model="user.hobbies">足球，
            <input type="checkbox" value="basketball"  v-model="user.hobbies">篮球，
            <input type="checkbox" value="pingpang"  v-model="user.hobbies">乒乓球，
            城市：
            <select name="" id=""  v-model="user.city">
                <option :value="c.id" v-for="c in optionalCities">
                    {{c.name}}
                </option>
            </select>
            <input type="submit" value="注册">

        </form>
    </div>

    <script type="text/javascript" src="js/vue.js"></script>
    <script type="text/javascript" >
        var  vm = new Vue({
            el: '#vue02',  //选择器
            //< input  v-model="user.username" >
            data:{
                myVariable : "Hello World",
                now : new Date(),
                user:{ 
                    username: '' ,
                    passowrd: '' ,
                    sex: 'male',
                    hobbies: [] ,
                    city: '' //选中
                },
                //可选
                optionalCities: [{id:1,name:'xa'},{id:2,name:'bj'},{id:3,name:'sh'}]
            },   
            methods:{//自定义方法
                mysubmit(){
                    console.log( JSON.stringify(this.user) );
                }
            }        
        })
    </script>
</body>
```

# 2.VUE生命周期

![Vue 实例生命周期](04_vue基础.assets/lifecycle.png)

## 2.1 过渡效果与过滤器

初始阶段：	v-enter/v-leave

过渡阶段：	v-enter-active / v-leave-active

结束阶段：	v-enter-to/v-leave-to

## 2.2 过滤器

```html
{{ now | dateFilter('YYYY-MM-DD')}} // now 是data域里面的数

<script type="text/javascript" src="js/moment.js"></script>
<script type="text/javascript" >
    //过滤：注册->使用  (西方日期 ->格式)    //es6:函数默认参数值
    Vue.filter('dateFilter' , function(value, dateFormat='YYYY-MM-DD HH:mm:ss' ){
        return  moment(value).format(dateFormat) ;
    })
</script>
```

## 2.3 解决闪现

渲染完毕以前不显示

style里面加这个：

```css
[v-cloak] {
  display: none;
}
```

主div里加这个：

```html
<div  id="vue02" v-cloak  >
    ...
</div>
```

## 2.4 自定义指令与自定义插件

### 2.4.1 注册全局指令

```html
<p v-text-upper="myVariable"></p>

<script>
    //注册全局指令
    Vue.directive('text-upper',function(el,binding){
        el.innerHTML= binding.value.toUpperCase();
    })
    
    data{
        myVariable:"Hello world"
    }
</script>
```

### 2.4.2 局部指令

```html
<p v-text-lower="myVariable"></p>
<script>
    new Vue({
        el: '#vue03',  //选择器
        //< input  v-model="user.username" >
        data:{
            myVariable : "Hello World",
        },
        //注册局部指令
        directives:{
            'text-lower' :{
                bind(el,binding){
                    el.innerHTML= binding.value.toLowerCase();
                }
            }
        }
    })
</script>
```

### 2.4.3 自定义插件

定义：

```javascript
(function () {
  const MyPlugin = {};
  MyPlugin.install = function (Vue, options) {
    // 1. 添加全局方法或属性
    Vue.myGlobalMethod = function () {
      alert('全局方法或属性');
    }

    // 2. 添加全局资源
    Vue.directive('my-directive', {//v-text-upper
      bind(el, binding, vnode, oldVnode) {
        // 逻辑...
      }
    })

    // 3. 注入组件选项  .每次实例vue对象中调用一次
    Vue.mixin({
      created: function () {
        // 逻辑...
        alert('mixin');
      }
    })

    // 4. 添加实例方法（局部）
    Vue.prototype.$myMethod = function (methodOptions) {
      // 逻辑...
      alert('局部');
    }
  }
  //将插件 暴露给外部使用
  window.MyPlugin = MyPlugin;
})()
```

使用：

```html
<script type="text/javascript" src="js/myPlugin.js"></script>
<script>
    //使用插件
    Vue.use(MyPlugin);
    //类名.静态方法()
    Vue.myGlobalMethod();
    //对象.方法()
    vm.$myMethod();
</script>
```

# 3. Vue脚手架

安装：

```shell
npm install -g vue-cli
npm install -g @vue/cli-init
```

下载模板：

```shell
vue init webpack my_vue
```

然后根据提示 进入启动

## 3.1 目录结构

![image-20211128215813588](04_vue基础.assets/image-20211128215813588.png)

## 3.2 导出

在项目根目录 执行

```shell
npm run build 
```

然后生成 dist 目录，复制出来，就是以后要运行的东西。

### 3.2.1 静态执行

```shell
npm install -g serve
```

然后用serve 在 dist 上级目录 运行

```shell
serve dist 
```

### 3.2.2 部署到tomcat服务器

1. 配置build / webpack.prod.conf.js

```json
output: {
    path: config.build.assetsRoot,
    filename: utils.assetsPath('js/[name].[chunkhash].js'),
    chunkFilename: utils.assetsPath('js/[id].[chunkhash].js'),
    publicPath: '/myvue/'    // 配置访问时的项目名
},
```

2. 打包成dist
3. 将dist拷贝到tomcat/webapps中，并且**再将dist修改成myvue**
4. 启动tomcat,运行,访问 localhost:端口号/myvue

## 3.3 vue-cli流程

### 3.3.1 index.html 注入流程

从入口看起，执行 npm run dev 的时候 其实执行了一下内容：

```json
"dev": "webpack-dev-server --inline --progress --config build/webpack.dev.conf.js",
```

可以看到，用了一个配置文件 webpack.dev.conf.js

```javascript
const baseWebpackConfig = require('./webpack.base.conf')
```

然后有从里面导入了 webpack.base.conf.js

```javascript
module.exports = {
    context: path.resolve(__dirname, '../'),
    entry: {
        app: './src/main.js' // 这个main.js 又叫app
    },
    //...
}
```

就可以找到main.js:

```javascript
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
})
```

根据web基础知识可知，访问localhost:8080/ 实际访问的是 index.html 的文件：

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>my_vue</title>
    </head>
    <body>
        <div id="app"></div>
        <!-- built files will be auto injected 构建时，自动会注入 -->
    </body>
</html>

```

注入的实际是  src/App.vue 中的内容：

```html
<template>
    <div id="app">
        <img src="./assets/logo.png">
        <router-view/>
    </div>
</template>

<script>
    export default {
        name: 'App'
    }
</script>
```

### 3.3.2  注入内容

很显然，一个图片，一个 **router-view**

```javascript
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'HelloWorld',
            component: HelloWorld
        }
    ]
})

```

当请求 / 的时候，就去请求HelloWorld

### 3.3.3 直接引入组件

1. 引入文件

   ```javascript
   import HelloWorld from './components/HelloWorld.vue';
   ```

2. 将引入文件 打成一个组件

   ```javascript
   import HelloWorld from './components/HelloWorld.vue'
   export default {
       // 组件
       components: { HelloWorld },
       name: 'App'
   }
   ```

3. 以标签形式使用组件

   ```html
   <hello-world/>
   ```

## 3.4 ESlint

规范性检查工具，

- 在 .eslintrc.js 里面配置检查规则
- 在 .eslintignore 配置的文件忽略

# 4. vuex

## 4.1 概念

vue的状态管理组件

![image-20211129103745592](04_vue基础.assets/image-20211129103745592.png)

动作触发 -> 数据改变 -> 视图更新  单项循环不可逆

代码层面一般用四部分：

![image-20211129104022635](04_vue基础.assets/image-20211129104022635.png)

## 4.2 状态管理实战

安装组件：

```shell
npm install --save vux
npm install --save vuex
```

按照上面的流程来写：

### 4.2.1 新建 **store.js**

```javascript
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// 共享变量 对应图中的State
const state = {
  count: 0
}

// 直接操作共享变量的对象  对应图中的Mutations
const mutations = {
  // 自增
  mulincrease (state) {
    state.count++
  },

  // 自减
  mulidecrease (state) {
    state.count--
  }
}

// 操作mutations的对象 对应图中的 Actions
const actions = {
  // 同步，自增，调用上面的 mulincrease
  actincrease ({commit}) {
    commit('mulincrease')
  },

  // 异步自减
  asycnDecrease ({commit}) {
    // 2秒后再执行
    setTimeout(() => {
      commit('mulidecrease')
    }, 2000)
  }
}

// 如果要对 共享变量state.count计算，则可以使用getters
const getters = {
  // 是否成年
  isAdult () {
    return state.count >= 18 ? '成年' : '未成年'
  }
}

// 将store.js中的对象 暴露给外界，用于其他组件的共享
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters
})
```

### 4.2.2 在main.js中引入store中的组件

```javascript
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// 引入准备封装的文件
import store from './store'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  store // 注入store对象
})

```

### 4.2.3 使用 store 对象

在 App.vue中测试：

```html
<template>
  <div id="app">
    {{ $store.state.count }}
    <button @click="countinc">+</button>
    <button @click="countdec">-</button>
    是否成年：{{ $store.getters.isAdult }}
    <br>
    是否成年：{{ vueIsAdult }}
    <img src="./assets/logo.png">
    <router-view/>
  </div>
</template>

<script>
import HelloWorld from './components/HelloWorld.vue'
export default {
  components: { HelloWorld },
  name: 'App',
  methods: {
    // 同步增加触发
    countinc () {
      return this.$store.dispatch('actincrease')
    },
    // 异步减少触发
    countdec () {
      return this.$store.dispatch('asycnDecrease')
    }
  },
  computed: {
    vueIsAdult () {
      return this.$store.getters.isAdult
    }
  }
}
</script>

<style>

</style>
```

## 4.3 优化

核心是直接去 取 getter 和 action 里面的方法

vuex 自带两个对象：**mapGetters** 和 **mapActions**

这两个对象直接关联 store.js 里面的 **actions** 和 **getters**

思路：

- 以前需要通过$store获取store.js中的 getter方法 和actions方法
- 现在 可以直接通过mapGetters,mapActions获取store.js中的 getter方法 和actions方法

```html
<template>
  <div id="app">
    {{ getCount }}
    <button @click="actincrease">+</button>
    <button @click="asycnDecrease">-</button>
    是否成年：{{ isAudlt }}
    <img src="./assets/logo.png">
    <router-view/>
  </div>
</template>

<script>
import HelloWorld from './components/HelloWorld.vue'
import {mapGetters, mapActions} from 'vuex'

export default {
  components: { HelloWorld },
  name: 'App',
  methods: {
    ...mapActions(['actincrease', 'asycnDecrease']),
    myMethod () {

    }
  },
  computed: {
    ...mapGetters({
      isAdult: 'isAdult',
      getCount: 'getCount'
    }),
    myActions1 () {
      return 1
    }
  }
}
</script>

<style>
</style>
```

# 5.Vue 请求和跨域

## 5.1 请求

```html
<div id="myvue">
    <button @click="myGetRequest()">get请求</button>
    <button @click="myPostRequest()">post请求</button>
</div>

<script src="./js/vue.js"></script>
<script src="./js/vue-resource.js"></script>
<script>
    new Vue({
        el: "#myvue",
        data:{} ,
        methods:{
            myGetRequest(){
                //请求
                this.$http.get('请求地址').then(response=>{
                    console.log(response.data);
                })
            },
            myPostRequest(){
                //post，表单提交： 
                // application/x-www.form-urlencoded，相当于第三个参数emulateJSON:true，一般建议写上
                this.$http.post('请求地址' ,{},{emulateJSON:true}).
                then(response =>
                     console.log(response)
                    )
            }
        }
    })
</script>
```

## 5.2 跨域请求

服务端 js：

```javascript
const http = require('http')
const urlModule = require('url')

//localhost:8080/myrequest?cb=myshow
//localhost:8080/myrequest2
//创建服务
let server = http.createServer();
//servlet: doGet(request,resposne)
server.on('request', function (req, res) {
    console.log('abcc')
    const { pathname, query } = urlModule.parse(req.url, true)
    if (pathname == '/myrequest') {
        var data = {
            id: 1,
            name: "zs",
            age: 23
        }
        //响应到哪里？
        //localhost:8080/myrequest?cb=myshow
        //myshow(data)   //data->json
        //响应到：myshow方法，并响应的数据：data
        var result = `${query.cb}( ${JSON.stringify(data)}  )`
        res.end(result)
    }
})

server.listen(8888, function () {
    console.log('sever running...')
})
```

在cmd 中运行这个 js： 

```shell
node myserver.js
```

客户端：

```html
<html>		
	<head>
		<meta charset="utf-8" />
		<title></title>
		<script src="./js/vue.js"></script>
		<script src="./js/vue-resource.js"></script>

		<!-- 定义方法-->
		<script>
				function myshow(data){
					console.log(data)
				}
		</script>

		<!--客户端加载 服务端调用 ，仅仅是一个 文件的引入 。-->
		<script src="http://localhost:8888/myrequest?cb=myshow"></script>
	</head>

	<div id="myvue">
			<button @click="myGetRequest()">get请求</button>
			<button @click="myPostRequest()">post请求</button>
	</div>

</html>

```

# 6.Promise

这个是优化写法的，之前的回调： 会陷入回调地狱

```javascript
const fs = require("fs")
const path = require("path");

// 读取文件内容
/**
 * 
 * fpath : 文件路径
 * frsuccess : 成功的回调函数
 * frerror :失败的回调函数
 */
function myReadFile(fpath, frsuccess, frerror) {
    // 读取文件时，如果有异常：err
    // 数据正常保存在data
    fs.readFile(fpath, 'utf-8', (err, data) => {
        if (err) {
            return frerror(err)
        }
        else {
            return frsuccess(data);
        }
    })
}

// 调用 这样写效率很低
myReadFile(path.join(__dirname, './file1.txt'), (data) => {
    console.log(data)
    // 继续读取file2
    myReadFile(path.join(__dirname, './file1.txt'), (data) => {
        console.log(data)
    }, (err) => {
        console.log(err.message)
    })
}, (err) => {
    console.log(err.message)
})
```

用Promise 后的写法：

```javascript
const fs = require("fs")
const path = require("path");

// 函数式编程 Promise，解决递归嵌套
function myReadFile2(fpath) {
    return new Promise((resolve, rejects) => {
        fs.readFile(fpath, 'utf-8', (err, data) => {
            if (err) {
                return rejects(err)
            }
            else {
                return resolve(data);
            }
        })
    })
}

// 调用Promise
myReadFile2(path.join(__dirname, './file1.txt')).then((data) => {
    console.log(data)
    // 如果成功，继续第二个文件
    return myReadFile2(path.join(__dirname, './file2.txt'))
}, (error) => {
    console.log(error)
}).then((data) => {
    console.log(data)
   // 如果成功，继续第三个文件
    return myReadFile2(path.join(__dirname, './file3.txt'))
}, (error) => {
    console.log(error)
}).then((data) => {
    console.log(data)
}, (error) => {
    console.log(error)
})

// 调用Promise catch 将error 放在一起
myReadFile2(path.join(__dirname, './file1.txt')).then((data) => {
    console.log(data)
    // 如果成功，继续第二个文件
    return myReadFile2(path.join(__dirname, './file2.txt'))
}).then((data) => {
    console.log(data)
    // 如果成功，继续第三个文件
    return myReadFile2(path.join(__dirname, './file3.txt'))
}).then((data) => {
    console.log(data)
}).catch(e=>{
    console.log(e)
})
```

# 7. 组件

## 7.1 全局组件

全局组件： 所有的vue对象中 都能够使用

```javascript
Vue.component('myComponent', Vue.extend({template: ' <h1>这是我的组件</h1>'}))
```



## 7.2 局部组件

只能在选中的那个 template 里面用

```javascript
new Vue({
    el : "#myvue2",
    components:{
        //myCom2组件 只能在myvue2中使用
        'myCom2':{
            template:"#mytemp2"
        }
    }
});
```



## 8. 自定义组件的调用和传值

```html
<html>

    <head>
        <meta charset="utf-8" />
        <title></title>
        <script src="./js/vue.js"></script>
    </head>
    <div id="myvue">
        <!-- <button @click="myclick">方法调用</button>-->
        <!-- com组件中引用了一个 按钮（包含了单击事件）-->
        <com @mycomclick='myshow'></com>
    </div>


    <template id="mytempid">
        <button @click="myclick">方法调用</button>
    </template>

    <script>
        var com = {
            template: '#mytempid',
            data() {//局部变量 ,xx(){return {k:v}}
                return {
                    comdata: {
                        num: 1
                    }
                }
            },
            methods: {
                myclick() {
                    this.$emit('mycomclick', this.comdata.num)  //myshow(a)
                }
            }
        }

        //将组件中的data，传递到vue中data
        new Vue({
            el: "#myvue",
            data: {
                mynum: 0
            },//全局变量  k:v
            methods: {
                // myclick(){
                // 	alert('hello world');
                // }
                myshow(param) {
                    alert("组件经过传递...终于成功了" + param);
                    this.mynum = param;
                    alert(this.mynum);
                }
            },
            components: {
                //com:com 
                com
            }
        });
    </script>

</html>
```





