# 1.Element UI

element-ui：前端脚手架，vue组件库

很多三方组件是基于element-ui 进行拓展

本次用 https://gitee.com/panjiachen/vue-admin-template  学习这个

```shell
# 克隆项目
git clone https://github.com/PanJiaChen/vue-admin-template.git

# 进入项目目录
cd vue-admin-template

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```



然后自己玩

# 2. nuxt 

服务端渲染技术，SEO 可以提高网站搜索排名

技术  nuxt：https://github.com/nuxt-community/starter-template