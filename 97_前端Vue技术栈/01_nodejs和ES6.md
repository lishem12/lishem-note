

# 1.Nodejs

可以用 JavaScript 实现类似服务端的功能。

## 1.1 安装

https://nodejs.org/zh-cn/		http://nodejs.cn/

下载LST版本，然后下一步安装，安装完以后会自动配置环境变量，可以在控制台输入如下指令验证：

```shell
node -v
npm -v
```

![image-20211126134040454](01_nodejs和ES6.assets/image-20211126134040454.png)

## 1.2 npm与cnpm

npm有点类似于maven，是管理JavaScript库的工具。

但是国内有墙，所以需要配置到淘宝源：

```shell
npm install -g cnpm --registry=https://registry.npm.taobao.org
```

今后用npm的指令都用cnpm，它会去走淘宝镜像。

## 1.3 包管理

### 1.3.1 直接安装

```shell
npm/cnpm install express   下载到当前目录（只有当前目录可用）
npm/cnpm install express  -g 下载到整个电脑（所有目录均可用）
```



### 1.3.2 自动下载

新建目录后，使用命令

```shell
npm init	  # 初始化，会问很多问题
npm init --yes #  初始化，跳过问题
npm set init.author.name "lishem" # 设置一些init 时的默认参数
```

然后即可得到 package.json  此文件相当于 maven的pom文件

```json
{
    "name": "test",
    "version": "1.0.0",
    "description": "",
    "main": "index.js",
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
    },
    "keywords": [],
    "author": "lishem",
    "license": "ISC"
}
```

然后手动加上如下内容（主要是devDependencies 和 dependencies）：

```json
{
    "name": "test",
    "version": "1.0.0",
    "description": "",
    "main": "index.js",
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1"
    },
    "keywords": [],
    "author": "lishem",
    "license": "ISC",
    "dependencies": {
        "connect-history-api-fallback": "^1.6.0",
        "vue": "^2.5.22",
        "vue-awesome-mui": "^1.7.3",
        "vue-router": "^3.0.1",
        "vuex": "^3.0.1"
    },
    "devDependencies": {
        "@vue/cli-plugin-babel": "^3.4.0",
        "@vue/cli-plugin-eslint": "^3.4.0",
        "@vue/cli-service": "^3.4.0",
        "babel-eslint": "^10.0.1",
        "eslint": "^5.8.0",
        "eslint-plugin-vue": "^5.0.0",
        "expose-loader": "^0.7.5",
    }
}
```

其中：

- dependencies，在生产环境中需要用到的依赖
- devDependencies，在开发、测试环境中用到的依赖

将需要的依赖写入package.json之后执行

```shell
npm/cnpm install
```

即可下载所有依赖包

## 1.4 package.json 详解

此文件至少有两部分内容：

- name：全部小写，没有空格，可以使用下划线或者横线
- version：x.x.x的格式，符合语义化规则

其他内容：

- description：描述信息，有助于搜索

- main：入口文件，一般都是index.js

- scripts：支持的脚本，默认是一个空的test

  - 可以将需要执行的脚本配置再script中，以后再执行脚本不需要写一段脚本，只需要npm run脚本名字

    ```shell
    "scripts": {
    	"dev": "webpack-dev-server"
    },
    
    # 这样配置之后，在控制台执行 
    npm run dev # 就等于在控制台输入 webpack-dev-server
    ```

- author：作者信息

- license：默认是ISC

- keywords：关键字，有助于人们使用npm search搜索时候发现你的项目

## 1.5 语义化版本规则

```shell
~4.17.1: ~表示>=4.17.1 ，但是< 4.18.x ;然后在范围内找最高版本
^4.17.1  ^表示>=4.17.1，但是 < 5.x.x ；然后在范围内找最高版本
lastest：在所有版本中找最高版本
```

![image-20211126191904019](01_nodejs和ES6.assets/image-20211126191904019.png)

- ~ 表示接受补丁版本更新
- ^ 表示接受小版本更新
- lastest 不管更新，来最新的

## 1.6 package 管理

### 1.6.1  包管理的两种方式

package管理有两种方式：

1. 项目需要添加包的时候，可以手动更改package.json文件，在 **dependencies** 和 **devDependencies** 中新增、删除或修改包

2. 用命令行的方式，增加--save 或 --save dev后缀

   ```shell
   npm install <package_name> --save  # 添加到 package.json里的dependencies 模块(生产环境)
   npm install <package_name> --save dev # 添加到package.json里的devDependencies模块（开发环境）
   ```

### 1.6.2 更新包

```shell
npm outdated # 查看是否有新版本
npm update <package_name> 指定版本  #  更新包到指定版本
```

### 1.6.3 卸载package

```shell
npm uninstall <package_name> --save
```

### 1.6.4 全局操作包

```shell
# 全局安装某个包
npm install -g <package_name>

# 全局更新某个包
npm upate -g <package_name>

# 更新全局所有包
npm update -g

# 查看全局包版本  这个好像不好用
npm outdated -g

# 卸载全局package
npm uninstall -g <package_name>
```



## 1.7 webpack打包

### 1.7.1 普通打包

可用将js等静态资源(js,css,图片) 进行压缩，从而提高性能

注意，其实css文件和静态资源文件(图片)  仅用webpack是不能打包的，需要另外引入style-loader css-loader 两个包：

下载：

```shell
cnpm install webpack -g
cnpm install webpack-cli -g
cnpm install style-loader css-loader --save-dev # 打包css和静态图片资源文件的
```

测试：

```shell
webpack -v
```

项目结构：

![image-20211126195050190](01_nodejs和ES6.assets/image-20211126195050190.png)

其中，webpack.config.js 中的内容如下：

```javascript
var path = require('path');
module.exports = {
    entry: './src/main.js' ,
    output: {
        //__dirname/.dist/bundle.js
        path: path.resolve(__dirname,'.dist'),
        filename : 'bundle.js'
    },
	mode: 'development',
    module: {
        rules: [
            {
                test: /\.css$/ ,
                use: ['style-loader','css-loader']
            }
        ]
    }
}
```

然后在webpack.config.js 同级目录用 cmd 执行 **webpack-dev-server** 命令 即可

其中index.html是测试文件，内容如下：

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title></title>
        <script src=".dist/bundle.js"></script>
    </head>
    <body>
    </body>
</html>
```

### 1.7.2 热部署打包

能否实现 修改js/css后，不打包，直接html生效，需要安装插件：

```shell
npm install webpack-dev-server -g
```

这个命令必须 **全局安装**，才可以生效，

然后使用：webpack-dev-server 启动命令，就开启了一个服务器，静态渲染资源，本地改了文件后，刷新页面会立即生效。

这种启动模式自动将打包后的boundle.js ->加载到内存中（boundle.js），使用时，直接在当前目录中引用

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title></title>
        <script src="bundle.js"></script>
    </head>
    <body>
    </body>
</html>
```

优点：

1. 热更新，css/js ->html
2. 在内存中，因此速度快

## 1.8 scripts

前面提到scripts 是启动时执行的脚本，积累下这个脚本能干嘛

```shell
"scripts":{
	"dev" : "webpack-dev-server -port 8888 --open"
}
```

端口8888 启动后自动打开浏览器

# 2.ES6

ESMAScript6.0 是一套规范（2015年产生）：

- javascript是ESMAScript的具体实现
- NodeJs是javascript的脚本库

## 2.1 转换器

在项目根目录，新建文件 .babelrc

里面的内容：

```json
{
    "presets":['es2015']
}
```

安装转换器：

```shell
cnpm install babel-preset-es2015 --save-dev
```

安装命令行工具：

```shell
cnpm  install babel-cli -g
```

使用：

正常使用：  node xxx.js

如果遇到 Nodejs 不支持的ES6，则切换使用：babel-node xxx.js

## 2.2 ES6规范之变量定义

**var**：可以自动提升

ES6 新增了**let**命令，用来声明变量。它的用法类似于**var**，但是所声明的变量，只在**let**命令所在的代码块内有效。

let 作用范围更小，不存在自动提升，不存在预解析

const 也不会自动提升，常量

> const 定义变量后不可修改，和C++ 的const类似，const实际上保证的，并不是变量的值不得改动，而是变量指向的那个内存地址所保存的数据不得改动。
>
> 对于简单类型的数据（数值、字符串、布尔值），值就保存在变量指向的那个内存地址，因此等同于常量。
>
> 但对于复合类型的数据（主要是对象和数组），变量指向的内存地址，保存的只是一个指向实际数据的指针，const只能保证这个指针是固定的（即总是指向另一个固定的地址），至于它指向的数据结构是不是可变的，就完全不能控制了。因此，将一个对象声明为常量必须非常小心。
> 

## 2.3 ES6之模板字符串

```javascript
console.log(`张三的年龄是：${age}`);
```

## 2.4 ES6之默认参数

```javascript
function test(num = 99){
    console.log(num);
}
```

## 2.5 ES6之箭头函数

1. 不需要function关键字
2. 省略return
3. 继承this

```javascript
function add(a,b){
    return a+b;
}
// 这两个等价
(a,b)=>(a+b)
```

## 2.6 对象初始化和解构

### 2.6.1 对象初始化：

```javascript
function person(pname,page){
    return {
        name:pname,
        age:page
    };
}
// ES6写法，属性名省略。
function Person(name,age){
    return{
        name,
        age
    };
}
```

### 2.6.2 解构对象：

```javascript
var person ={
    name:"zs",
    age:23
}

var pname = person.name;
var page = person.age;
console.log(`${pname},${page}`);
// ES6解构，但是 定义的变量名一定和 后面的属性名一致
var {name,age} = person ;
console.log(`${pname},${page}`);
```

### 2.6.3 解构数组：

```javascript
const fruit = ['apple','orange'] ;
let [one,two] = fruit ;
console.log(`${one},${two}`);
```

### 2.6.4 传播行为

```javascript
// 传播对象
let student = {name:"zs",age:23};
let student2 = {...student,height:174};
console.log(student2)

// 传播数组
const fruit = ['apple','orange'] ;
const fruit2 = [...fruit,'peer'];
console.log(fruit2)
```

## 2.7 导入导出

**require/exports** 是 **CommonJS** 的标准，适用范围如 Node.js

**import/export** 是 **ES6** 的标准，适用范围如 React，ES6

### 2.7.1 require 和 exports 

model.js:

```javascript
exports.name = "colin";
exports.sayHello = function() {
    console.log("hello");
};
```

getModule.js

方式一：间接

```javascript
var myModule = require('./module');
console.log(myModule.name);
myModule.sayHello();
```

方式二：直接

```javascript
var { name, sayHello } = require('./module');
console.log(name);
sayHello();
```

方式三：别名

```javascript
var { name: name_2, sayHello: sayHello_2 } = require('./module');
console.log(name_2);
sayHello_2();
```

### 2.7.2 import和export

module.js：

```javascript
export const name = 'colin';
export function sayHello(){
    console.log("hello");
}
```

getModule.js

方式一：间接（**不可用**）

```javascript
import myModule from './module'
console.log(myModule.name);
myModule.sayHello();
```

报错：

  Cannot use import statement outside a module

方式二：直接

```javascript
import { name, sayHello } from './module'
console.log(name);
sayHello();
```

方式三：别名

```javascript
import { name as name_2, sayHello as sayHello_2 } from './module'
console.log(name_2);
sayHello_2();
```



### 2.7.3 直接写法

#### require/exports

module.js

```javascript
var name = "colin";
module.exports = name;
```

getModule.js

```javascript
var name = require('./module');
console.log(name);
```

#### import/export

module.js

```javascript
export default name = 'colin';
```

getModule.js

```javascript
import name from './module'
console.log(name);
```

## 2.8 ES6数据类型

### 基本类型

string  --  number  -- boolean  -- undefined  --  null

- undefined 是只定义，未赋值
- null 需要显示的赋值为null

### 引用类型

​	Object：任何对象

​	Object 有两个子类，Array 是数组，Function是函数

### 类型判断

**==** 宽松的判断（自带类型转换）

**===** 严格的判断 （建议使用）

**typeof**：判断是否是某个类型 ，以“字符串形式”体现

**instanceof** ：判断是否某个具体类型

## 2.9 prototype 原型

作用：可以向对象 **增加属性 或者方法**

每个函数都自带一个prorotype属性 ，默认情况下 指向一个空对象{} (称为原型对象，prototype对象)
原型对象（prototype对象） 都有一个constructor属性，它指向函数本身

![image-20211127122524533](01_nodejs和ES6.assets/image-20211127122524533.png)

增加方法：

```javascript
function Myfun(){
}

Myfun.prototype.say = function () {
    console.log("name1")
};

var myFun = new Myfun();
myFun.say();
```







































































