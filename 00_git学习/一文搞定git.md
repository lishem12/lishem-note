# 1、理论部分

## 1.1、Git的优势

1. 大部分操作在本地完成，不需要联网
2. 完整性保证（提交每条数据都进行hash计算）
3. 尽可能添加数据而不是删除或修改数据
4. 分支操作非常快捷流畅
   1. 因为使用快照
   2. 每个分支只是创建一个指针
5. 与linux命令全面兼容

## 1.2、Git结构

![image-20211012145605972](一文搞定git.assets/image-20211012145605972.png)

# 2、Git命令行操作

## 2.1、本地库初始化

切换到目录，右键，git bash here ，输入：

```shell
git init
```

之后得到一个.git 隐藏文件夹。

## 2.2、设置签名

注意：这个签名是用于区分不同开发人员的身份，和代码托管中心的账号、密码没有任何关系

签名有两个级别

（1）项目级别/仓库级别

```shell
git config user.name lishem
git config user.email lishem13@163.com
```

保存在当前文件夹的 .git/config 文件里

（2）系统用户级别

```shell
git config --global user.name lishem
git config --global user.email lishem13@163.com
```

保存位置：

- windows：C:/Users/username  的 .gitconfig  文件中
- linux/mac ：~/.gitconfig  （用户目录下）

（3）级别优先级

1. 就近原则：项目级别优先于系统用户级别
2. 没有项目级别签名，就用系统级别签名
3. 二者都没有，不可以~

## 2.3、基本操作

说明： [file name] 如果有多个文件，空格分隔 

### 2.3.1 状态查看

```shell
git status
```

可以看到当前在哪个分支，有没有**待添加**的，有没有**待提交**的

### 2.3.2 添加、撤回

```shell
git add [file name]
git add . # 全部添加
```

将 新建/修改 的文件添加到**暂存区**

```shell
git rm --cached [file name]  # 从暂存区撤回
```

从**暂存区**撤回

### 2.3.3 提交

```shell
# 会弹出vim编辑器说明的
git commit [file name] # 提交暂存区的某个文件
git commit . # 提交暂存区的所有文件

# 不会弹出vim编辑器说明
git commit -m "说明内容" [file name] # 提交暂存区单个文件
git commit -m "说明内容" .  # 提交暂存区所有文件

# 恢复修改
git restore [file name] # 恢复某文件到最新的git版本
```

### 2.3.4 查看历史记录

```shell
git log # 查看版本记录 空格向下翻页，b向上翻页，q退出
git log --pretty=oneline # 每次提交日志只显示一行(hash值和日志)
git log --oneline # 每次提交日志只显示一行且只显示部分hash值
git reflog # 显示历史只显示一行，并且显示指针
```

```shell
git reflog # 显示历史只显示一行,并且显示指针(要移动到版本多少步)


git reflog
f89bb22 (HEAD -> master) HEAD@{0}: commit: 二次    # HEAD指针在这呢
fe99829 HEAD@{1}: commit: 第一次
58394d3 HEAD@{2}: commit: 批量提交
6140fa6 HEAD@{3}: commit: jiu shi zhe xie
aaea42b HEAD@{4}: commit: 第二次提交
c202d9a HEAD@{5}: commit (initial): 1
HEAD@{移动到当前版本需要多少步}
```

### 2.3.5 前进后退

三种方式

1. 基于索引值操作

   ```shell
   git reset --hard [局部索引值] # 回退到hash版本
   ```

2. 基于^ 符号

   ```shell
   git reset --hard HEAD^^ # 一个^表示后退1步
   ```

3. 基于~符号

   ```shell
   git reset --head HEAD~n # 向后后退n步
   ```

### 2.3.6 reset命令三个参数对比

首先明确一下，git三个空间：工作区 -- 暂存区 -- 本地库

```shell
--soft		# 仅在本地库移动版本指针，工作区和暂存区不影响
--mixed		# 在本地库移动版本指针，重置暂存区
--hard		# 在本地库移动版本指针，重置暂存区，重置工作区
```

### 2.3.7 删除文件找回

要找回的文件，必须是**已经提交过本地库**！

```shell
git restore [file name] # 删除操作未提交到本地库，restore恢复文件
git reset --hard HEAD 	# 删除操作已经提交到本地库，用历史版本
```

### 2.3.8 比较文件差异

```shell
git diff [file name] # 将工作区的文件和暂存区的比较
git diff [本地库中历史版本名] [文件名] # 将工作区中的文件和本地库历史记录比较
git diff [本地库中历史版本名]  # 不带文件名比较所有文件
```



## 2.4、分支管理

![image-20211012193004260](一文搞定git.assets/image-20211012193004260.png)

分支开发可以同时推进多个功能开发，提高开发效率。

如果某分支开发失败，删除分支就好了，不会对其他分支有任何影响

分支基本操作：

```shell
git branch [分支名]	  	  # 创建分支
git branch -v			    # 查看分支
git branch -d [分支名]       # 删除分支
git checkout [分支名]    	   # 切换分支
git merge [有新内容分支名]    # 合并分支   
```

合并有冲突怎么办？

```shell
1. 编辑文件，删除特殊符号
2. 把文件修改到合并后的状态
3. git add [文件名]
4. git commit -m "合并说明" # 注意：这个commit 后面不能带具体文件名
```

# 3、Git基本原理

哈希、版本保存机制、分支管理机制

## 4.1 、Git底层采用 **SHA-1** 哈希算法

哈希算法特点：

1. 不管输入数据量多大，输入同一个哈希换发，得到加密结果长度固定
2. 哈希算法确定，输入数据确定，输出数据就固定
3. 哈希算法确定，输入数据有变化，输出数据一定有变化
4. 哈希算法不可逆

## 4.2、Git保存版本的机制

### （1）文件管理机制 -- 快照流

​	Git把数据看作是小型文件系统的一组快照。每次提交更新时Git都会对当前的**全部文件制作一个快照并保存这个快照的索引**。为了高效，如果文件没修改，Git不会重新存储该文件，而是**只保留一个链接指向之前存储的文件**。

### （2）分支管理机制 -- 指针

​	Git维护一个HEAD指针，指向当前工作的分支指针，如果创建分支，就是创建了一个分支指针，切换分支，就是HEAD指针指向要切换分支的分支指针。

<img src="一文搞定git.assets/image-20211012200849606.png" alt="image-20211012200849606" style="zoom:50%;" />

上图说明：因为文件修改的时候，新版本的文件指针指向旧版本的文件，所以这个箭头是往左指的。右侧为更新的版本。

# 4、github与git

创建账号、新建仓库 不说了

## 4.1、创建远程库地址别名

别名是给远程的url 地址 起的，以后推送的时候就可以用别名

```shell
git remote -v # 查看当前所有远程地址别名
git remote add [别名] [远程地址] # 添加别名，别名一般是 origin
```

## 4.2、推送

```shell
git push [别名] [分支名] # 推送
```

如果第一次推送，要输入账号密码，以后就不要了

账号密码保存在 控制面板 -- 凭据管理器 -- windows凭据 中

## 4.3、克隆

```shell
git clone [远程地址]
```

克隆做了三件事：

1. 完成的把远程库下载到本地
2. 创建origin 为远程地址为别名
3. 初始化本地库

## 4.4、团队成员邀请

邀请后才能提交，在Settings里Manage access

## 4.5、拉取

```shell
git fetch [远程库地址别名 origin] [远程分支名 master]  # fetch 拿来，抓取下来
git pull [远程库地址别名] [远程库分支名]  # pull = fetch + merge，拉下来，然后和本地库合并
```

## 4.6、解决冲突

如果**不是**基于Github远程库的**最新版**所做修改，不能推送，必须先拉取pull

拉取下了后如果进入冲突状态，则按照之前"分支冲突解决"【2.4节】操作即可

## 4.7、跨团队协作

4.4节说的是 A 的仓库，邀请 B 为开发人员

这里讲的是 A 的仓库不让 B 加入团队，仅仅让 B 拿一个副本出去继续开发

B找到A的仓库，Fork到自己的仓库中，Fork出来的仓库中，会有说明：Froked from A / a-repository

![image-20211012213545174](一文搞定git.assets/image-20211012213545174.png)

B 在副本仓库中开发.....

开发完成，点Pull request - New pull request -- create pull request

创建一个 pull request，并且发送开发说明发送给A

A 上线后就可以看到 1个新的Pull request

![image-20211012214041615](一文搞定git.assets/image-20211012214041615.png)

A审阅后 可以继续沟通 Conversation  也可以合并代码

![image-20211012214135807](一文搞定git.assets/image-20211012214135807.png)

就完成了协同开发

# 5、Git工作流

## 5.1 集中式工作流

所有修改都提交到 Master  这个分支上。

## 5.2 GitFlow工作流

Gitflow 工作流通过为功能开发、 发布准备和维护**设立了 独立的分支**，让发布迭代过程更流畅。严格的分支模型也为大型项目提供了一些非常必要的结构。

![image-20211012214348722](一文搞定git.assets/image-20211012214348722.png)

## 5.3 Forking 工作流

Forking 工作流是在 GitFlow 基础上，充分利用了 Git 的 Fork  和 pull request  的功能以达到**代码审核的目的**。更适合安全可靠地管理大团队的开发者，而且能接受不信任贡献者的提交。

分支详解：

1. 主干分支 master

   主要负责管理正在运行的生产环境代码,永远保持与正在运行的生产环境完全一致

2. 开发分支 develop

   主要负责管理正在开发过程中的代码。一般情况下应该是最新的代码。

3. bug  修理分支 hotfix

   主要负责管理生产环境下出现的紧急修复的代码。从主干分支分出，修理完毕并测试上线后，并回主干分支。并回后，视情况可以删除该分支。

4. 准生产分支（预发布分支） release

   较大的版本上线前，会从开发分支中分出准生产分支，进行最后阶段的集成测试。该版本上线后，会合并到主干分支。生产环境运行一段阶段较稳定后可以视情况删除。

5. 功能分支 feature

   为了不影响较短周期的开发工作，一般把中长期开发模块，会从开发分支中独立出来。开发完成后会合并到开发分支。

# 6、Gitlab服务器搭建

https://about.gitlab.com/

http://gitblit.github.io/gitblit/

都要自己搭服务器了，遇到的问题应该自己能解决，



























