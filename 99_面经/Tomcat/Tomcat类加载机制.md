# Tomcat 类加载机制详解

# 1.Tomcat 类加载机制需要考虑的问题

Tomcat作为Servlet容器，它负责加载我们的Servlet类，此外它还负责加载Servlet所依赖的 JAR 包。并且Tomcat本身也是也是一个Java程序，因此它需要加载自己的类和依赖的JAR包，所以可能要考虑这几个问题：

1. 假如在Tomcat中运行了两个Web应用程序，两个Web应用中有同名的 Servlet，但是功能不同，Tomcat需要同时加载和管理这两个同名的Servlet 类，保证它们不会冲突，也就是说，Web应用之间的类需要隔离
2. 假如两个Web应用都**依赖同一个第三方的JAR包**，比如Spring，那Spring的JAR包被加载到内存后，Tomcat要保证这两个Web应用能够共享，也就是说Spring的JAR包只被加载一次，否则随着依赖的第三方JAR包增多，JVM 的内存会膨胀。
3. Tomcat自身也是一个Java程序，需要隔离Tomcat本身的类和Web应用的类，避免互相影响。比如Web应用中定义了一个同名类导致Tomcat本身类无法加载。

所以，Tomcat是如何解决这些问题？通过设计多层次的类加载器。

![img](Tomcat类加载机制.assets/15597971411e99712caa40adf2ca18fc.png)

## 1.1 WebAppClassLoader

首先来看第一个问题，假如我们要使用JVM默认ApplicationLoader来加载Web应用，AppClassLoader只能加载一个Servlet类，在加载第二个同名Servlet类时，AppClassLoader会返回第一个Servlet类的Class实例，这是因为在AppClassLoader看来，同名的Servlet类只能被加载一次。

因此Tomcat的解决方案是自定义一个类加载WebClassLoader，并且给每个Web应用创建一个类加载器实例。Context容器组件对应一个Web应用，因此，每个Context容器负责创建和维护一个WebAppClassLoader加载器实例，不同的加载器实例加载的类被认为是不同的类，即使它们的类名相同。这就相当于在Java虚拟机内部创建了一个个相互隔离的Java类空间，每个Web应用都有自己的类空间，Web应用之间通过各自的类加载器相互隔离。

## 1.2 SharedClassLoader

再看第二个问题，本质需求是两个Web应用之间怎么共享库类，并且不能重复加载相同的类。我们知道，在双亲委派机制里，每个子加载器都可以通过父加载器去加载类，那么把需要共享的类放到父加载器的加载路径下应该就可以了，应用程序也正是通过这种方式共享JRE的核心类。

因此Tomcat的设计者又加了一个类加载器SharedClassLoader，作为WebAppClassLoader的父加载器，专门来加载Web应用之间共享的类。如果WebAppClassLoader自己没有加载到某个类，就会委托父加载器SharedClassLoader去加载这个类。

SharedClassLoader会在指定目录下加载共享类，之后返回给WebAppClassLoader，这样共享的问题就解决了。

## 1.3 CatalinaClassloader

第三个问题，如何隔离Tomcat本身的类和Web应用的类。我们知道，要共享可以通过父子关系，要隔离就可以通过兄弟关系。

兄弟关系就是指两个类加载器时平行的，他们可能拥有同一个父加载器，但是两个兄弟加载器加载的类是隔离的。基于此Tomcat又设计一个类加载器CatalinaClassLoader，专门加载Tomcat自身类。

如果Tomcat和各Web应用之间需要共享一些类时怎么办?

## 1.4 CommonClassLoader

老办法，还是增加一个CommonClassLoader，作为CltalinaClassLoader和SharedClassLoader的父加载器。

这样做到了：

CommonClassLoader能加载的类都可以CatalinaClassLoader和SharedClassLoader使用，而CatalinaClassLoader和 SharedClassLoader能加载的类则与对方相互隔离。WebAppClassLoader可以使用SharedClassLoader加载到的类，但各个WebAppClassLoader实例之间相互隔离。

## 1.5 线程类加载器

在JVM的实现里还有一条隐含的规则，在默认情况下，如果一个类由类加载器A加载，那么这个类的依赖类也是由相同的类加载器加载。比如：

Spring作为Bean工厂，它需要创建业务类的实例，并且在创建业务类实例之前需要加载这些类。Spring是调用Class.forName来加载业务类的。我们来看一下forName的代码：

```java
public static Class<?> forName(String className) throws ClassNotFoundException{
	Class<?> caller = Reflection.getCallerClass()
	return forName0(className, true, ClassLoader.getClassLoader(caller), caller);
}
```

可以看到在forName的函数里，会用调用者也就是Spring 的加载器去加载业务类。

前面提到，Web应用之间共享的JAR包可以交给SharedClassLoader来加载，从而避免重复加载。Spring作为共享的第三方Jar包，它本身是由SharedClassLoader来加载的，Spring又要去加载业务类，按照前面那条规则，加载Spring类加载器也会用来加载业务类，但是业务类在Web应用目录下，不在SharedClassLoader的加载路径下，这该怎么办呢？

于是线程上下文加载器就登场了。它其实是一种加载器传递机制。为什么叫做“线程上下文加载器”呢？因为这个类加载器保存在线程私有数据里，只要是同一个线程，一旦设置了上下文加载器，在线程后续执行的过程中就能把这个类加载器取出来用。

因此Tomcat为每个Web应用创建了一个WebAppClassLoader类加载器，并在启动Web应用的线程里设置线程上下文加载器，这样Spring在启动时，就将线程上下文加载器取出来，用来加载Bean。Spring取线程上下文加载的代码如下：

```java
ClassLoader cl = Thread.currentThread().getContextClassLoader();
```

# 2.Tomcat类加载机制默认实现

上面我们介绍了几种Tomcat的自定义类加载器，但其实除了上述几种自定义类加载器，Tomcat自身也是Java应用，肯定也需要JDK类加载器来加载。Tomcat相关的类加载器的加载范围如下：

## 2.1 BootstrapClassLoader

BootstrapClassLoader用于负责加载JVM提供的提出运行时类（即rt.jar）以及${JAVA_HOME\jre\lib\ext}下的类。按照之前JVM类加载器介绍，其实这是Bootstrap和Extension两个类加载器的功能。到底是一个类加载器还是两个类加载器，是由具体的JVM决定的。

## 2.2 SystemClassLoader

这里说的SystemClassLoader 就是之前介绍的AppClassLoader，不同的是在Tomcat的运行环境下，它不再去加载CLASSPATH中的类，而是去加载${CATALINA_HOME\bin}目录下的3个jar（这里是启动脚本catalina.sh中写死的），主要包含以下三个jar：

- bootstrap.jar
- tomcat-juli.jar（如果`$CATALINA_BASE/bin`目录下也有这个包，会使用`$CATALINA_BASE/bin`目录下的）
- commons-deamo.jar

## 2.3 CommonClassLoader

CommonClassLoader 是Tomcat自定义的类加载器，用于加载Tomcat容器自身和Web应用之间需要共享的类，所以该类加载的类对Tomcat自身和Web应用都可见。通常情况下，应用的类文件不应该放在CommonClassLoader中。CommonClassLoader会扫描${CATALINA_BASE}/conf/catalina.properites  中common loader属性中指定的路径中的类文件。

默认情况下，它会按顺序去以下路径中加载

```properties
common.loader="${catalina.base}/lib","${catalina.base}/lib/*.jar","${catalina.home}/lib","${catalina.home}/lib/*.jar"
```

## 2.4 WebAppClassLoader

每一个部署在Tomcat中的web应用，Tomcat都会为其创建一个WebappClassloader，它会去加载应用WEB-INF/classes目录下所有未打包的classes和resources，然后再去加载WEB-INF/lib目录下的所有jar文件。每个应用的WebappClassloader都不同，因此，它加载的类只对本应用可见，其他应用不可见（这是实现web应用隔离的关键）。

这里我们来介绍一个重要的概念——Tomcat类加载机制打破了双亲委托机制。为什么Tomcat要打破双亲委托机制？

上面我们说了使用JDK默认类加载机制无法解决多个web应用加载同名类的问题，所以自定义了WebAppClassLoader用于解决该问题。其实说到底Tomcat打破JDK自定义类加载器的原因是**Servlet规范，优先加载Web应用目录下的类，只要该类不覆盖JRE核心类**。

从Web应用的视角来看，当有类加载的请求时，class或者resource的查找顺序是这样的：

1. JVM中的类库，如rt.jar和$JAVA_HOME/jre/lib/ext目录下的jar
2. 应用的/WEB-INF/classes目录
3. 应用的/WEB-INF/lib/*.jar
4. SystemClassloader加载的类（如上所述）
5. CommonClassloader加载的类（如上所述）

如果你的应用配置了``**，**那么查找顺序就会变为：

1. JVM中的类库，如rt.jar和$JAVA_HOME/jre/lib/ext目录下的jar
2. SystemClassloader加载的类（如上所述）
3. CommonClassloader加载的类（如上所述）
4. 应用的/WEB-INF/classes目录
5. 应用的/WEB-INF/lib/*.jar

可以发现，如果配置了delegate = true，其实WebAppClassLoader的加载机制就是标准的双亲委托机制了。

讲到我们发现，好像少了两个类加载器，CatalinaClassLoader和SharedClassLoader。是因为在Tomcat默认实现中直接使用的是CommonClassLoader作为CatalinaClassLoader和SharedClassLoader。



# 3.源码分析

## 3.1 类加载器构建

org.apache.catalina.startup.Bootstrap#main方法中在创建Bootstrap后会调用bootstrap.init()方法，如下：

```java
//1、初始化类加载器
//2、加载catalina类，并且实例化
//3、反射调用Catalina的setParentClassLoader方法
//4、实例 赋值
public void init() throws Exception {
    // 1. 初始化Tomcat类加载器(3个类加载器)
    initClassLoaders();  // ===> classloader , 3个
    // 用上面生成的catalinaLoader作为启动线程的上下文类加载器
    Thread.currentThread().setContextClassLoader(catalinaLoader);

    SecurityClassLoad.securityClassLoad(catalinaLoader);

    // Load our startup class and call its process() method
    if (log.isDebugEnabled())
        log.debug("Loading startup class");
    // 2. 用loader来实例化Catalina实例【重要节点】
    Class<?> startupClass = catalinaLoader.loadClass("org.apache.catalina.startup.Catalina");
    Object startupInstance = startupClass.getConstructor().newInstance();

    // Set the shared extensions class loader
    if (log.isDebugEnabled())
        log.debug("Setting startup class properties");
    String methodName = "setParentClassLoader";
    Class<?>[] paramTypes = new Class[1];
    paramTypes[0] = Class.forName("java.lang.ClassLoader");
    Object[] paramValues = new Object[1];
    // 设置为sharedLoader
    paramValues[0] = sharedLoader;
    // 3. 反射调用Catalina的setParentClassLoader方法，
    // 将sharedLoader设置为Catalina的parentClassLoader成员变量
    // 思考一下，为什么？？？
    //  ----  为了让 Catalina类去加载 shared下的一堆对象，注意看tomcat的classloader继承关系
    //  后面的webappLoader，jsploader 都是shared的子加载器。
    Method method =
        startupInstance.getClass().getMethod(methodName, paramTypes);
    method.invoke(startupInstance, paramValues);
    //4、将catalina实例赋值
    catalinaDaemon = startupInstance;
    // 结论： bootstrap只是个启动点和流程控制，真正干活的，交给了 catalina类，我们下面重点看它的动作
}
```

这里首先会调用initClassLoaders方法初始化类加载器，然后通过catalinaLoader加载 **org.apache.catalina.startup.Catalina**，然后反射实例化Catalina对象，并反射setParentClassLoader方法，将Catalina的parentClassLoader成员变量设置为sharedLoader。



下面看看 initClassLoader吧

## 3.2 initClassLoaders

```java
// 创建commonLoader类加载器
// 创建catalinaLoader类加载器
// 创建sharedLoader类加载器
private void initClassLoaders() {
    try {
        commonLoader = createClassLoader("common", null);  // ===> 创建common
        if (commonLoader == null) {
            // no config file, default to this loader - we might be in a 'single' env.
            commonLoader = this.getClass().getClassLoader();
        }
        catalinaLoader = createClassLoader("server", commonLoader);  // ===> 同样的方法，common为父加载器
        sharedLoader = createClassLoader("shared", commonLoader);
    } catch (Throwable t) {
        handleThrowable(t);
        log.error("Class loader creation threw exception", t);
        System.exit(1);
    }
}
```

```java
private ClassLoader createClassLoader(String name, ClassLoader parent)
    throws Exception {
    // 1. 从catalina.properties中获取”name.loader“属性
    // 默认实现中common.loader = ${catalina.base}/lib,${catalina.base}/lib/*.jar,${catalina.home}/lib,${catalina.home}/lib/*.jar
    // server.loader = 
    // shared.loader =

    // 详细系统参数的内容，参考 CatalinaProperties 类的源码  ===>  进！
    // 【知识点：tomcat系统配置的加载路径和顺序】
    String value = CatalinaProperties.getProperty(name + ".loader");  // ===> 获取系统参数
    //  如果在catalina.properties找不到value，直接返回参数的parent
    if ((value == null) || (value.equals("")))
        return parent;
    //debug前后查看value的值试试！
    value = replace(value);   // ===>  字符替换，把里面的  ${xxx} 替换成系统变量里对应的字符串
    //  value  ：  类加载的目录，表示类加载器要在这个目录加载class等文件
    List<Repository> repositories = new ArrayList<>();

    String[] repositoryPaths = getPaths(value);

    for (String repository : repositoryPaths) {  // 遍历指定的加载目录
        // Check for a JAR URL repository
        try {
            @SuppressWarnings("unused")
            URL url = new URL(repository);
            repositories.add(new Repository(repository, RepositoryType.URL));
            continue;
        } catch (MalformedURLException e) {
            // Ignore
        }
        // 根据不同的类型，采用不同的加载策略，生成不同的 Repository
        // Local repository
        if (repository.endsWith("*.jar")) {
            repository = repository.substring
                (0, repository.length() - "*.jar".length());
            repositories.add(new Repository(repository, RepositoryType.GLOB));
        } else if (repository.endsWith(".jar")) {
            repositories.add(new Repository(repository, RepositoryType.JAR));
        } else {
            repositories.add(new Repository(repository, RepositoryType.DIR));
        }
    }
	// 创建类加载器
    return ClassLoaderFactory.createClassLoader(repositories, parent);  // 最后生成 classloader
}
```





## 3.3 WebAppClassLoader



