# 1.Session源码分析

客户端创建Socket链接后，会尝试连接，如果成功会调用到**primeConnection**方法中来发送ConnectRequest连接请求，这里便是设置Session会话。这里就是研究服务端Session会话处理流程。

## 1.1 服务端Session属性分析

Zookeeper服务端会话操作如下图：  

![image-20211106144615528](10_zookeeper源码02.assets/image-20211106144615528.png)

服务端通过SessionTrackerImpl和ExpiryQueue来保存Session会话信息。

**SessionTrackerImpl** 有以下属性：

1. **sessionById**：用来存储ID和session的映射

   ```java
   protected final ConcurrentHashMap<Long, SessionImpl> sessionsById = 
           new ConcurrentHashMap<Long, SessionImpl>();
   ```

2. **sessionExpiryQueue** 失效队列

   ```java
   private final ExpiryQueue<SessionImpl> sessionExpiryQueue;
   // 构造方法中初始化
   this.sessionExpiryQueue = new ExpiryQueue<SessionImpl>(tickTime);
   ```

3. **sessionsWithTimeout**  存储id和过期时间

   ```java
   protected final ConcurrentMap<Long, Integer> sessionsWithTimeout;
   // 构造方法构造时外部传入
   this.sessionsWithTimeout = sessionsWithTimeout;
   ```

4. 一个计数值  用来生成下一个SessionId

   ```java
   private final AtomicLong nextSessionId = new AtomicLong();
   ```

**ExpiryQueue**失效队列有以下属性：

1. **elemMap**  ，存储的是SessionImpl 实例和失效时间对应 的集合

   ```java
   this.sessionExpiryQueue = new ExpiryQueue<SessionImpl>(tickTime);
   private final ConcurrentHashMap<E, Long> elemMap = new ConcurrentHashMap<E, Long>();
   ```

2. **expiryMap**  ，存储的是 过期时间 和 Set<SessionImpl> 的映射

   ```java
   private final ConcurrentHashMap<Long, Set<E>> expiryMap = new ConcurrentHashMap<Long, Set<E>>();
   ```

3. **nextExpirationTime** 下一次失效时间

   ```java
   private final AtomicLong nextExpirationTime = new AtomicLong();
   /*
   {(System.nanoTime() /1000000)/expirationInterval+1}*expirationInterval 
   当前系统时间毫秒值ms=System.nanoTime() / 1000000。 
   nextExpirationTime=当前系统时间毫秒值+expirationInterval(失效间隔)
   */
   ```

4. **expirationInterval** ，失效间隔，默认10S，可以通过sessionlessCnxnTimeout修改。即是通过配置文件的tickTime修改。

## 1.2 Session创建 

### 1.2.1 首次和非首次的分水岭

加入客户端发起请求后，后端如何识别是第一次创建请求？在前面的案例中，通过NIOServerCnxn.readPayload ()中有所体现，NIOServerCnxn.readPayload() 部分关键源码如下：  

> 如何找到这段代码？
>
> IOWorkRequest类的doWork()方法， 调用doIO()，调用 readPayload()

```java
// NIOServerCnxn.java
private void readPayload() throws IOException, InterruptedException, ClientCnxnLimitException {
    if (incomingBuffer.remaining() != 0) { // have we read length bytes?
        int rc = sock.read(incomingBuffer); // sock is non-blocking, so ok
        if (rc < 0) {
            handleFailedRead();
        }
    }

    if (incomingBuffer.remaining() == 0) { // have we read length bytes?
        incomingBuffer.flip();
        packetReceived(4 + incomingBuffer.remaining());
        //此时如果initialized=false，表示第一次连接 需要创建Session(createSession)
        //调用readConnectRequest()后，在readConnectRequest()方法中会将initialized设置为true
        if (!initialized) {//还没有初始化->会话未创建
            System.out.println("会话-1：会话未连接，准备首次连接会话.....");
            //创建会话Session 并且处理 request
            readConnectRequest();
        } else {
            System.out.println("1-1：会话已连接，非首次连接会话.....");
            // 处理request
            readRequest();
        }
        lenBuffer.clear();
        incomingBuffer = lenBuffer;
    }
}
```

此时如果 initialized=false ，表示第一次连接 需要创建 Session(createSession) 。

### 1.2.2 读取连接请求

此处调用**readConnectRequest**() 后，在 **readConnectRequest**() 方法中会将 initialized 设置为 true ，只有在处理完连接请求之后才会把 initialized 设置为 true ，才可以处理客户端其他命令。

```java
// NIOServerCnxn.java
private void readConnectRequest() throws IOException, InterruptedException, ClientCnxnLimitException {
    if (!isZKServerRunning()) {
        throw new IOException("ZooKeeperServer not running");
    }
    // 会话创建流程 直译是处理连接请求
    zkServer.processConnectRequest(this, incomingBuffer);
    // 下次通信时，判断是否创建会话的标识
    initialized = true;
}
```

### 1.2.3 处理连接请求

上面方法还调用了 **processConnectRequest** 处理连接请求, processConnectRequest 第一次从请求中获取的 sessionId=0 ,此时会把创建 Session 作为一个业务，会调用 createSession() 方法，processConnectRequest 方法如下： 

```java
// ZookeeperServer.java
@SuppressFBWarnings(value = "IS2_INCONSISTENT_SYNC", justification = "the value won't change after startup")
public void processConnectRequest(ServerCnxn cnxn, ByteBuffer incomingBuffer)
    throws IOException, ClientCnxnLimitException {

    BinaryInputArchive bia = BinaryInputArchive.getArchive(new ByteBufferInputStream(incomingBuffer));
    System.out.println("会话-2：建立远程链接.....");
    ConnectRequest connReq = new ConnectRequest();
    /**
     * 读取链接信息
     */
    connReq.deserialize(bia, "connect");
    LOG.debug(
        "Session establishment request from client {} client's lastZxid is 0x{}",
        cnxn.getRemoteSocketAddress(),
        Long.toHexString(connReq.getLastZxidSeen()));
    // 第一次链接  sessionId = 0,毕竟这个对象第一次创建
    long sessionId = connReq.getSessionId();
    System.out.println("会话-2：第一次链接sessionId："+sessionId);
    int tokensNeeded = 1;

    // 是否开启session对整个服务带来的负载的权重
    if (connThrottle.isConnectionWeightEnabled()) {
        if (sessionId == 0) {
            if (localSessionEnabled) {
                tokensNeeded = connThrottle.getRequiredTokensForLocal();
            } else {
                tokensNeeded = connThrottle.getRequiredTokensForGlobal();
            }
        } else {
            tokensNeeded = connThrottle.getRequiredTokensForRenew();
        }
    }

    // 限流算法，看是否可以建立链接
    if (!connThrottle.checkLimit(tokensNeeded)) {
        throw new ClientCnxnLimitException();
    }
    ServerMetrics.getMetrics().CONNECTION_TOKEN_DEFICIT.add(connThrottle.getDeficit());

    ServerMetrics.getMetrics().CONNECTION_REQUEST_COUNT.add(1);

    boolean readOnly = false;
    try {
        readOnly = bia.readBool("readOnly");
        cnxn.isOldClient = false;
    } catch (IOException e) {
        // this is ok -- just a packet from an old client which
        // doesn't contain readOnly field
        LOG.warn(
            "Connection request from old client {}; will be dropped if server is in r-o mode",
            cnxn.getRemoteSocketAddress());
    }
    if (!readOnly && this instanceof ReadOnlyZooKeeperServer) {
        String msg = "Refusing session request for not-read-only client " + cnxn.getRemoteSocketAddress();
        LOG.info(msg);
        throw new CloseRequestException(msg, ServerCnxn.DisconnectReason.NOT_READ_ONLY_CLIENT);
    }
	// 看这里！
    // 当前会话事务ID是否大于最大事务ID，如果大于最大事务ID，则抛出异常
    if (connReq.getLastZxidSeen() > zkDb.dataTree.lastProcessedZxid) {
        String msg = "Refusing session request for client "
            + cnxn.getRemoteSocketAddress()
            + " as it has seen zxid 0x"
            + Long.toHexString(connReq.getLastZxidSeen())
            + " our last zxid is 0x"
            + Long.toHexString(getZKDatabase().getDataTreeLastProcessedZxid())
            + " client must try another server";

        LOG.info(msg);
        throw new CloseRequestException(msg, ServerCnxn.DisconnectReason.CLIENT_ZXID_AHEAD);
    }

    //读取会话信息
    //超时时间，这个超时事件会被下面判断放入minSessionTimeout和 maxSessionTimeout 之间
    int sessionTimeout = connReq.getTimeOut(); 
    byte[] passwd = connReq.getPasswd(); //密码
    int minSessionTimeout = getMinSessionTimeout(); //服务端设置的超时时间
    //客户端超时时间和服务端超时时间比较
    if (sessionTimeout < minSessionTimeout) {
        sessionTimeout = minSessionTimeout;
    }
    //服务端设置最大超时时间
    int maxSessionTimeout = getMaxSessionTimeout();
    //如果超时时间>服务端最大超时时间，则超时时间设置为服务端最大超时时间
    if (sessionTimeout > maxSessionTimeout) {
        sessionTimeout = maxSessionTimeout;
    }
    //设置超时时间
    cnxn.setSessionTimeout(sessionTimeout);
    // We don't want to receive any packets until we are sure that the
    // session is setup
    //会话未初始化完成之前，不接受任何请求
    cnxn.disableRecv();
    //sessionId=0，所以需要创建Session
    if (sessionId == 0) {
        //创建Session
        long id = createSession(cnxn, passwd, sessionTimeout);
        System.out.println("3:sessionId=0,此时创建sessionId="+id);
        LOG.debug(
            "Client attempting to establish new session: session = 0x{}, zxid = 0x{}, timeout = {}, address = {}",
            Long.toHexString(id),
            Long.toHexString(connReq.getLastZxidSeen()),
            connReq.getTimeOut(),
            cnxn.getRemoteSocketAddress());
    } else {
        validateSession(cnxn, sessionId);
        LOG.debug(
            "Client attempting to renew session: session = 0x{}, zxid = 0x{}, timeout = {}, address = {}",
            Long.toHexString(sessionId),
            Long.toHexString(connReq.getLastZxidSeen()),
            connReq.getTimeOut(),
            cnxn.getRemoteSocketAddress());
        if (serverCnxnFactory != null) {
            serverCnxnFactory.closeSession(sessionId, ServerCnxn.DisconnectReason.CLIENT_RECONNECT);
        }
        if (secureServerCnxnFactory != null) {
            secureServerCnxnFactory.closeSession(sessionId, ServerCnxn.DisconnectReason.CLIENT_RECONNECT);
        }
        cnxn.setSessionId(sessionId);
        reopenSession(cnxn, sessionId, passwd, sessionTimeout);
        ServerMetrics.getMetrics().CONNECTION_REVALIDATE_COUNT.add(1);

    }
}
```

### 1.2.4 创建Session

创建会话调用**createSession**()，该方法会首先创建一个sessionId，并把该sessionId作为会话ID创建一个创建session会话的请求，**并将该请求交给业务链作为一个业务处理**， createSession() 源码如下：  

```java
/**
 * 创建Session
 */
long createSession(ServerCnxn cnxn, byte[] passwd, int timeout) {
    if (passwd == null) {
        // Possible since it's just deserialized from a packet on the wire.
        passwd = new byte[0];
    }
    //创建sessionId !
    long sessionId = sessionTracker.createSession(timeout);
    Random r = new Random(sessionId ^ superSecret);
    r.nextBytes(passwd);
    ByteBuffer to = ByteBuffer.allocate(4);
    to.putInt(timeout);
    //设置会话ID
    cnxn.setSessionId(sessionId);
    //创建一个OpCode.createSession的请求（创建会话的业务）
    Request si = new Request(cnxn, sessionId, 0, OpCode.createSession, to, null);
    //提交业务,这里将 创建session当做一个Request处理。
    submitRequest(si);
    return sessionId;
}
```

会话信息的跟踪其实就是将会话信息添加到队列中，任何地方可以根据会话ID找到会话信息，trackSession 方法实现了Session创建、Session队列存储、 Session 过期队列存储， trackSession 方法源码如下：  

```java
/***
 * 创建Session
 */
public long createSession(int sessionTimeout) {
    //获取下一个SessionID（将当前值增加1）
    //使用AtomicLong能让long的操作保持原子型
    long sessionId = nextSessionId.getAndIncrement();
    //Session跟踪配置
    trackSession(sessionId, sessionTimeout);
    System.out.println("使用SessionTrackerImpl创建会话，并将会话加入跟踪队列中");
    return sessionId;
}
```

会话信息的跟踪其实就是将会话信息添加到队列中，任何地方可以根据会话ID找到会话信息，trackSession 方法实现了Session创建、Session队列存储、 Session 过期队列存储， trackSession方法源码如下：

```java
/***
 * Session跟踪配置，这里真正new了session
 */
@Override
public synchronized boolean trackSession(long id, int sessionTimeout) {
    boolean added = false;

    //获取一个Session，如果为空，则以SessionID创建一个Session
    //sessionsById:存储的是会话信息session key：sessionid
    SessionImpl session = sessionsById.get(id);
    if (session == null) {
        //创建Session，设置超时时间
        session = new SessionImpl(id, sessionTimeout);
    }

    //session存入到sessionsById中，可以根据ID获取到Session
    //ConcurrentHashMap线程安全
    //putIfAbsent在放入数据时，如果存在重复的key，那么putIfAbsent不会放入值
    //如果传入key对应的value已经存在，就返回存在的value，不进行替换。如果不存在，就添加key和value，返回null
    SessionImpl existedSession = sessionsById.putIfAbsent(id, session);

    //如果会话session存在,就获取session并赋值给当前的session
    if (existedSession != null) {
        session = existedSession;
    } else {
        //设置添加成功
        added = true;
    }

    //将Session添加到失效队列中
    updateSessionExpiry(session, sessionTimeout);
    return added;
}
```

### 1.2.5 session当业务处理

在 **PrepRequestProcessor** 的 run 方法中调用 pRequestHelper()，关键代码如下：  

```java
//create/close session don't require request record
case OpCode.createSession:
case OpCode.closeSession:
if (!request.isLocalSession()) {
    pRequest2Txn(request.type, zks.getNextZxid(), request, null, true);
}
break;
```

pRequest2Txn():

```java
case OpCode.createSession:
    request.request.rewind();
    int to = request.request.getInt();
    request.setTxn(new CreateSessionTxn(to));
    request.request.rewind();
    // only add the global session tracker but not to ZKDb
    zks.sessionTracker.trackSession(request.sessionId, to);
    zks.setOwner(request.sessionId, request.getOwner());
    break;
```

在 **SyncRequestProcessor** 对txn(创建session的操作)进行持久化(**写日志**)，在 **FinalRequestProcessor** 会对Session进行提交，其实就是把 Session 的ID和 Timeout 存到 sessionsWithTimeout 中去。  

```java
FinalRequestProcessor.applyRequest()
    方法代码：ProcessTxnResult rc = zks.processTxn(request);
ZooKeeperServer.processTxn(org.apache.zookeeper.server.Request)
    方法代码：processTxnForSessionEvents(request, hdr, request.getTxn());

```

上面调用链路中 processTxnForSessionEvents(request, hdr, request.getTxn()); 方法代码如下：  

```java
private void processTxnForSessionEvents(Request request, TxnHeader hdr, Record txn) {
    int opCode = (request == null) ? hdr.getType() : request.type;
    long sessionId = (request == null) ? hdr.getClientId() : request.sessionId;

    // OpCode.createSession业务
    if (opCode == OpCode.createSession) {
        if (hdr != null && txn instanceof CreateSessionTxn) {
            CreateSessionTxn cst = (CreateSessionTxn) txn;
            // 将sessionId,TimeOut提交到sessionWithTimeout中
            sessionTracker.commitSession(sessionId, cst.getTimeOut());
        } else if (request == null || !request.isLocalSession()) {
            LOG.warn("*****>>>>> Got {} {}",  txn.getClass(), txn.toString());
        }
    } else if (opCode == OpCode.closeSession) {
        sessionTracker.removeSession(sessionId);
    }
}
```

上面方法主要处理了 OpCode.createSession 并且将 sessionId、TimeOut 提交到sessionsWithTimeout 中，而提交到 sessionsWithTimeout 的方法SessionTrackerImpl.commitSession() 代码如下：  

```java
public synchronized boolean commitSession(long id, int sessionTimeout) {
    return sessionsWithTimeout.put(id, sessionTimeout) == null;
}
```

## 1.3 Session刷新

服务端无论接受什么请求命令(增删或ping等请求)都会更新Session的过期时间 。我们做增删或者ping命令的时候，都会经过 RequestThrottler ， **RequestThrottler** 的run方法中调用 zks.submitRequestNow() ，而 ks.submitRequestNow(request) 中调用了 touch(si.cnxn); ，该方法源码如下：  

```java
void touch(ServerCnxn cnxn) throws MissingSessionException {
    if (cnxn == null) {
        return;
    }
    long id = cnxn.getSessionId();
    int to = cnxn.getSessionTimeout(); // 默认30秒
    //更新会话
    System.out.println("b.准备调用SessionTrackerImpl.touchSession()刷新会话");
    if (!sessionTracker.touchSession(id, to)) {
        throw new MissingSessionException("No session with sessionid 0x"
                                          + Long.toHexString(id)
                                          + " exists, probably expired and removed");
    }
}
```

touchSession() 方法更新sessionExpiryQueue失效队列中的失效时间，源码如下：  

```java
public synchronized boolean touchSession(long sessionId, int timeout) {
    //根据SessionId获取Session
    SessionImpl s = sessionsById.get(sessionId);

    if (s == null) {
        logTraceTouchInvalidSession(sessionId, timeout);
        return false;
    }

    if (s.isClosing()) {
        logTraceTouchClosingSession(sessionId, timeout);
        return false;
    }

    //更新session过期时间
    System.out.println("c.会话不为空，会话也未关闭，准备调用updateSessionExpiry()刷新会话");
    updateSessionExpiry(s, timeout);
    return true;
}
```

update() 方法会在当前时间的基础上增加timeout，并更新失效时间为newExpiryTime，关键源码如下  :

```java
private void updateSessionExpiry(SessionImpl s, int timeout) {
    //打印日志
    logTraceTouchSession(s.sessionId, timeout, "");
    //s:操作的会话session
    //timeout:过期时间
    sessionExpiryQueue.update(s, timeout);
}

public Long update(E elem, int timeout) {
    /***
     * elemMap:session->过期时间
     * 取出一个对象
     *  key:session
     *  value:过期时间
     */
    Long prevExpiryTime = elemMap.get(elem);
    //获取系统时间
    long now = Time.currentElapsedTime();
    //计算该会话新的的过期时间!!!!
    Long newExpiryTime = roundToNextInterval(now + timeout);
    System.out.println("d.剩余过期时间："+now+",增加过期时间："+timeout+",刷新会话后过期时间："+newExpiryTime);

    //如果会话新的活期时间和过去过期时间一致，说明不需要更新，直接返回
    if (newExpiryTime.equals(prevExpiryTime)) {
        // No change, so nothing to update
        return null;
    }

    /**
     * 获取newExpiryTime时间段所有过期的会话
     */
    Set<E> set = expiryMap.get(newExpiryTime);
    //判断会话是否为空
    if (set == null) {
        // Construct a ConcurrentHashSet using a ConcurrentHashMap
        //Collections.newSetFromMap：用于生成对Map进行包装的Set，
        //这个Set和被包装的Map拥有相同的key顺序，
        //相同的并发特性（也就是说如果对ConcurrentHashMap进行包装，得到的Set也将线程安全）
        set = Collections.newSetFromMap(new ConcurrentHashMap<E, Boolean>());
        // Put the new set in the map, but only if another thread
        // hasn't beaten us to it
        //续期之后的时间：set
        //putIfAbsent：表示添加
        //             如果此时对象中没有该数据，此时就添加到对象中，返回null
        //             如果对象中存在我要添加的数据，此时再添加会失败，会返回存在的对象
        Set<E> existingSet = expiryMap.putIfAbsent(newExpiryTime, set);
        //
        if (existingSet != null) {
            set = existingSet;
        }
    }
    //将会话添加到Set中
    set.add(elem);

    // Map the elem to the new expiry time. If a different previous
    // mapping was present, clean up the previous expiry bucket.
    //重置会话过期时间
    prevExpiryTime = elemMap.put(elem, newExpiryTime);
    //获取该会话存在旧的过期时间,根据旧的过期时间在expiryMap获取在该过期时间过期的session集合,从该集合中删除该会话
    if (prevExpiryTime != null && !newExpiryTime.equals(prevExpiryTime)) {
        Set<E> prevSet = expiryMap.get(prevExpiryTime);
        if (prevSet != null) {
            prevSet.remove(elem);
        }
    }
    return newExpiryTime;
}
```

## 1.4 Session过期

### 1.4.1 主流程

在1.2.4 创建Session之后，在trackSession() 方法内，调用updateSessionExpiry(session, sessionTimeout);将会话添加到失效队列中。

```java
// SessionTrackerImpl.java
private void updateSessionExpiry(SessionImpl s, int timeout) {
    //打印日志
    logTraceTouchSession(s.sessionId, timeout, "");
    //s:操作的会话session
    //timeout:过期时间
    sessionExpiryQueue.update(s, timeout);
}

// ExpiryQueue.java
/***
 * elemMap:session->过期时间
 * 取出一个对象
 *  key:session
 *  value:过期时间
 */
public Long update(E elem, int timeout) {
    Long prevExpiryTime = elemMap.get(elem);
    //获取系统时间
    long now = Time.currentElapsedTime();
    //计算该会话新的的过期时间
    Long newExpiryTime = roundToNextInterval(now + timeout);
    System.out.println("d.剩余过期时间："+now+",增加过期时间："+timeout+",刷新会话后过期时间："+newExpiryTime);

    //如果会话新的活期时间和过去过期时间一致，说明不需要更新，直接返回
    if (newExpiryTime.equals(prevExpiryTime)) {
        // No change, so nothing to update
        return null;
    }

    /***
     * 获取newExpiryTime时间段所有过期的会话
     */
    Set<E> set = expiryMap.get(newExpiryTime);
    //判断会话是否为空
    if (set == null) {
        //Collections.newSetFromMap：用于生成对Map进行包装的Set，
        //这个Set和被包装的Map拥有相同的key顺序，
        //相同的并发特性（也就是说如果对ConcurrentHashMap进行包装，得到的Set也将线程安全）
        set = Collections.newSetFromMap(new ConcurrentHashMap<E, Boolean>());
        // Put the new set in the map, but only if another thread
        // hasn't beaten us to it
        //续期之后的时间：set
        //putIfAbsent：表示添加
        //             如果此时对象中没有该数据，此时就添加到对象中，返回null
        //             如果对象中存在我要添加的数据，此时再添加会失败，会返回存在的对象
        Set<E> existingSet = expiryMap.putIfAbsent(newExpiryTime, set);
        if (existingSet != null) {
            set = existingSet;
        }
    }
    //将会话添加到Set中
    set.add(elem);

    //重置会话过期时间
    prevExpiryTime = elemMap.put(elem, newExpiryTime);
    //获取该会话存在旧的过期时间,根据旧的过期时间在expiryMap获取在该过期时间过期的session集合,从该集合中删除该会话
    if (prevExpiryTime != null && !newExpiryTime.equals(prevExpiryTime)) {
        Set<E> prevSet = expiryMap.get(prevExpiryTime);
        if (prevSet != null) {
            prevSet.remove(elem);
        }
    }
    return newExpiryTime;
}
```



**SessionTrackerImpl** 是一个线程类，继承了 ZooKeeperCriticalThread ，我们可以看它的run方法，它首先获取了下一个会话过期时间，并休眠等待会话过期时间到期，然后获取过期的客户端会话集合并循环关闭，源码如下：  

```java
public class SessionTrackerImpl extends ZooKeeperCriticalThread implements SessionTracker {

    @Override
    public void run() {
        try {
            while (running) {
                // 获取下一个失效时间waitTime
                long waitTime = sessionExpiryQueue.getWaitTime();
                if (waitTime > 0) {
                    Thread.sleep(waitTime);
                    continue;
                }
                // 获取失效的客户端会话集合
                for (SessionImpl s : sessionExpiryQueue.poll()) {
                    ServerMetrics.getMetrics().STALE_SESSIONS_EXPIRED.add(1);
                    // 关闭会话
                    setSessionClosing(s.sessionId);
                    // 让客户端失效
                    expirer.expire(s);
                }
            }
        } catch (InterruptedException e) {
            handleException(this.getName(), e);
        }
        LOG.info("SessionTrackerImpl exited loop!");
    }
}
```

上面方法中调用了 sessionExpiryQueue.poll() ，该方法代码主要是获取过期时间对应的客户端会话集合，源码如下：  

```java
public Set<E> poll() {
    long now = Time.currentElapsedTime();
    long expirationTime = nextExpirationTime.get();
    if (now < expirationTime) {
        return Collections.emptySet();
    }

    Set<E> set = null;
    long newExpirationTime = expirationTime + expirationInterval;
    // 更新 nextExpirationTime，每次都是加同一个expirationInterval
    if (nextExpirationTime.compareAndSet(expirationTime, newExpirationTime)) {
        // 获取失效时间的客户端实现类集合，这就是expiryMap的意义
        set = expiryMap.remove(expirationTime);
    }
    if (set == null) {
        return Collections.emptySet();
    }
    return set;
}
```

### 1.4.2 关闭Session

上面的 setSessionClosing() 方法其实是把Session会话的 isClosing 状态设置为了true,方法源码如下：  

```java
public synchronized void setSessionClosing(long sessionId) {
    if (LOG.isTraceEnabled()) {
        LOG.trace("Session closing: 0x{}", Long.toHexString(sessionId));
    }

    SessionImpl s = sessionsById.get(sessionId);
    if (s == null) {
        return;
    }
    s.isClosing = true;
}
```

### 1.4.3 closeSession当业务处理

而让客户端失效的方法 expirer.expire(s); 其实也是一个业务操作，主要调用了ZooKeeperServer.expire() 方法，而该方法获取SessionId后，又创建了一个OpCode.closeSession 的请求，并交给业务链处理，我们查看 ZooKeeperServer.expire() 方法源码如下：  

```java
/**
 * Session失效
 * @param session
 */
public void expire(Session session) {
    long sessionId = session.getSessionId();
    LOG.info(
        "Expiring session 0x{}, timeout of {}ms exceeded",
        Long.toHexString(sessionId),
        session.getTimeout());
    close(sessionId);
}

/**
 * 关闭Session业务
 * @param sessionId
 */
private void close(long sessionId) {
    Request si = new Request(null, sessionId, 0, OpCode.closeSession, null, null);
    submitRequest(si);
}
```

在 **PrepRequestProcessor**.pRequest2Txn() 方法中 OpCode.closeSession 操作里最后部分代理明确将会话Session的isClosing设置为了true，源码如下  

```java
case OpCode.closeSession:
// We don't want to do this check since the session expiration thread
// queues up this operation without being the session owner.
// this request is the last of the session so it should be ok
//zks.sessionTracker.checkSession(request.sessionId, request.getOwner());
long startTime = Time.currentElapsedTime();
synchronized (zks.outstandingChanges) {
    // need to move getEphemerals into zks.outstandingChanges
    // synchronized block, otherwise there will be a race
    // condition with the on flying deleteNode txn, and we'll
    // delete the node again here, which is not correct
    Set<String> es = zks.getZKDatabase().getEphemerals(request.sessionId);
    for (ChangeRecord c : zks.outstandingChanges) {
        if (c.stat == null) {
            // Doing a delete
            es.remove(c.path);
        } else if (c.stat.getEphemeralOwner() == request.sessionId) {
            es.add(c.path);
        }
    }
    for (String path2Delete : es) {
        if (digestEnabled) {
            parentPath = getParentPathAndValidate(path2Delete);
            parentRecord = getRecordForPath(parentPath);
            parentRecord = parentRecord.duplicate(request.getHdr().getZxid());
            parentRecord.stat.setPzxid(request.getHdr().getZxid());
            parentRecord.precalculatedDigest = precalculateDigest(
                DigestOpCode.UPDATE, parentPath, parentRecord.data, parentRecord.stat);
            addChangeRecord(parentRecord);
        }
        nodeRecord = new ChangeRecord(
            request.getHdr().getZxid(), path2Delete, null, 0, null);
        nodeRecord.precalculatedDigest = precalculateDigest(
            DigestOpCode.REMOVE, path2Delete);
        addChangeRecord(nodeRecord);
    }
    if (ZooKeeperServer.isCloseSessionTxnEnabled()) {
        request.setTxn(new CloseSessionTxn(new ArrayList<String>(es)));
    }
    // 调用真正的关闭
    zks.sessionTracker.setSessionClosing(request.sessionId);
}
ServerMetrics.getMetrics().CLOSE_SESSION_PREP_TIME.add(Time.currentElapsedTime() - startTime);
break;
```

业务链处理对象 **FinalRequestProcessor**.processRequest() 方法调用了ZooKeeperServer.processTxn() ，并且在 processTxn() 方法中执行了processTxnForSessionEvents ，而 processTxnForSessionEvents() 方法正好移除了会话信息，方法源码如下：  

```java
// ZookeeperServer.java
private void processTxnForSessionEvents(Request request, TxnHeader hdr, Record txn) {
    int opCode = (request == null) ? hdr.getType() : request.type;
    long sessionId = (request == null) ? hdr.getClientId() : request.sessionId;

    // OpCode.createSession业务
    if (opCode == OpCode.createSession) {
        if (hdr != null && txn instanceof CreateSessionTxn) {
            CreateSessionTxn cst = (CreateSessionTxn) txn;
            // 将sessionId,TimeOut提交到sessionWithTimeout中
            sessionTracker.commitSession(sessionId, cst.getTimeOut());
        } else if (request == null || !request.isLocalSession()) {
            LOG.warn("*****>>>>> Got {} {}",  txn.getClass(), txn.toString());
        }
    } else if (opCode == OpCode.closeSession) {
        // 移除对应会话
        sessionTracker.removeSession(sessionId);
    }
}

// SessionTrackerImpl.java
public synchronized void removeSession(long sessionId) {
    LOG.debug("Removing session 0x{}", Long.toHexString(sessionId));
    SessionImpl s = sessionsById.remove(sessionId);
    // 会话ID移除
    sessionsWithTimeout.remove(sessionId);
    if (LOG.isTraceEnabled()) {
        ZooTrace.logTraceMessage(
            LOG,
            ZooTrace.SESSION_TRACE_MASK,
            "SessionTrackerImpl --- Removing session 0x" + Long.toHexString(sessionId));
    }
    if (s != null) {
        // 超时会话移除
        sessionExpiryQueue.remove(s);
    }
}
```

## 1.5 Session销毁时间机制总结

不知道是不是时间轮机制，看懂要紧。

### 1.5.1 创建session为销毁做的铺垫

创建session的时候，调用了 SessionTrackerImpl的trackSession 方法

```java
// SessionTrackerImpl.java
public synchronized boolean trackSession(long id, int sessionTimeout){
    SessionImpl session = sessionsById.get(id);
    if (session == null) {
        //创建Session，设置超时时间
        session = new SessionImpl(id, sessionTimeout);
    }
    SessionImpl existedSession = sessionsById.putIfAbsent(id, session);
    //如果会话session存在,就获取session并赋值给当前的session
    if (existedSession != null) {
        session = existedSession;
    } else {
        //设置添加成功
        added = true;
    }
    //将Session添加到失效队列中
    updateSessionExpiry(session, sessionTimeout);
    return added;
} 
```

已经有了布局，将 new 出来的session放入失效队列中：

```java
// SessionTrackerImpl.java
private void updateSessionExpiry(SessionImpl s, int timeout) {
    //打印日志
    logTraceTouchSession(s.sessionId, timeout, "");
    //s:操作的会话session
    //timeout:过期时间
    sessionExpiryQueue.update(s, timeout);
}
```

这里用到了 session失效队列的update 方法，看下它做了什么：

```java
// ExpiryQueue.java
public Long update(E elem, int timeout) {
    // elemMap 映射时  session-过期时间
    Long prevExpiryTime = elemMap.get(elem);
    //获取系统时间
    long now = Time.currentElapsedTime();
    //计算该会话新的的过期时间
    Long newExpiryTime = roundToNextInterval(now + timeout);
    System.out.println("d.剩余过期时间："+now+",增加过期时间："+timeout+",刷新会话后过期时间："+newExpiryTime);

    //如果会话新的活期时间和过去过期时间一致，说明不需要更新，直接返回
    if (newExpiryTime.equals(prevExpiryTime)) {
        // No change, so nothing to update
        return null;
    }

    /***
         * 获取newExpiryTime时间段所有过期的会话
         */
    Set<E> set = expiryMap.get(newExpiryTime);
    //判断会话是否为空
    if (set == null) {
        // Construct a ConcurrentHashSet using a ConcurrentHashMap
        //Collections.newSetFromMap：用于生成对Map进行包装的Set，
        //这个Set和被包装的Map拥有相同的key顺序，
        //相同的并发特性（也就是说如果对ConcurrentHashMap进行包装，得到的Set也将线程安全）
        set = Collections.newSetFromMap(new ConcurrentHashMap<E, Boolean>());
        // Put the new set in the map, but only if another thread
        // hasn't beaten us to it
        //续期之后的时间：set
        //putIfAbsent：表示添加
        //             如果此时对象中没有该数据，此时就添加到对象中，返回null
        //             如果对象中存在我要添加的数据，此时再添加会失败，会返回存在的对象
        Set<E> existingSet = expiryMap.putIfAbsent(newExpiryTime, set);
        if (existingSet != null) {
            set = existingSet;
        }
    }
    //将会话添加到Set中
    set.add(elem);

    // Map the elem to the new expiry time. If a different previous
    // mapping was present, clean up the previous expiry bucket.
    //重置会话过期时间
    prevExpiryTime = elemMap.put(elem, newExpiryTime);
    //获取该会话存在旧的过期时间,根据旧的过期时间在expiryMap获取在该过期时间过期的session集合,从该集合中删除该会话
    if (prevExpiryTime != null && !newExpiryTime.equals(prevExpiryTime)) {
        Set<E> prevSet = expiryMap.get(prevExpiryTime);
        if (prevSet != null) {
            prevSet.remove(elem);
        }
    }
    return newExpiryTime;
}
```

上面的方法将session 和 过期时间分别放入了两个集合对象：

```java
// elemMap： session-过期时间
private final ConcurrentHashMap<E, Long> elemMap = new ConcurrentHashMap<E, Long>();
// expiryMap： 过期时间- 此时间所有过期的session
private final ConcurrentHashMap<Long, Set<E>> expiryMap = new ConcurrentHashMap<Long, Set<E>>();
```

需要注意的是，计算该session 过期时间的时候调用了一个roundToNextInterval()方法：

```java
private long roundToNextInterval(long time) {
    return (time / expirationInterval + 1) * expirationInterval;
}
```

该方法会将session的过期时间由 连续的long 变量，划分为expirationInterval 的倍数。所以就会出现同一个时间点，多个Session同时失效的场景。   此函数将时间划分出来了多少段。

### 1.5.2 失效处理

回到SessionTrackerImpl 看，它是一个线程，有run方法：

```java
// SessionTrackerImpl.java
@Override
public void run() {
    try {
        while (running) {
            // 获取下一个失效时间waitTime
            long waitTime = sessionExpiryQueue.getWaitTime();
            if (waitTime > 0) {
                Thread.sleep(waitTime);
                continue;
            }
            // 获取失效的客户端会话集合
            for (SessionImpl s : sessionExpiryQueue.poll()) {
                ServerMetrics.getMetrics().STALE_SESSIONS_EXPIRED.add(1);
                // 关闭会话
                setSessionClosing(s.sessionId);
                // 让客户端失效
                expirer.expire(s);
            }
        }
    } catch (InterruptedException e) {
        handleException(this.getName(), e);
    }
    LOG.info("SessionTrackerImpl exited loop!");
}

// ExpiryQueue.java
private final AtomicLong nextExpirationTime = new AtomicLong();

public long getWaitTime() {
    long now = Time.currentElapsedTime();
    long expirationTime = nextExpirationTime.get();
    return now < expirationTime ? (expirationTime - now) : 0L;
}

public Set<E> poll() {
    long now = Time.currentElapsedTime();
    long expirationTime = nextExpirationTime.get();
    if (now < expirationTime) {
        return Collections.emptySet();
    }

    Set<E> set = null;
    long newExpirationTime = expirationTime + expirationInterval;
    if (nextExpirationTime.compareAndSet(expirationTime, newExpirationTime)) {
        // 获取失效时间的客户端实现类集合，这就是expiryMap的意义
        set = expiryMap.remove(expirationTime);
    }
    if (set == null) {
        return Collections.emptySet();
    }
    return set;
}
```

ExpiryQueue 中有个字段：nextExpirationTime是重点！

- now < expirationTime的情况，还没到需要检查关闭会话的时候，线程阻塞
- now >= expirationTime，wait() 阻塞直接过去，执行下面的 sessionExpiryQueue.poll()，在poll() 中不仅查出了这个时间片需要失效的session，同时更新了**expirationTime**字段，用的是CAS操作，此时**expirationTime**就是下一个需要检查的时间片。 expirationTime + expirationInterval

通过这种将连续的时间划分为有长度间隔的方式，节约了性能，又能有序释放失效session

# 2.Zookeeper集群启动流程

## 2.1 Zookeeper 集群配置

![image-20211107212646315](10_zookeeper源码02.assets/image-20211107212646315.png)

然后分别配置启动类：

![image-20211107212802937](10_zookeeper源码02.assets/image-20211107212802937.png)

![image-20211107212815725](10_zookeeper源码02.assets/image-20211107212815725.png)

![image-20211107212824608](10_zookeeper源码02.assets/image-20211107212824608.png)

## 2.2 集群启动流程分析

![image-20211107212922349](10_zookeeper源码02.assets/image-20211107212922349.png)

在 QuorumPeerMain 的 initializeAndRun() 方法中，能走到集群模式运行。

![image-20211107213354927](10_zookeeper源码02.assets/image-20211107213354927.png)

在QuorumPeer中：

```java
/**
 * 启动选举--run 方法执行
 */
@Override
public synchronized void start() {
    if (!getView().containsKey(myid)) {
        throw new RuntimeException("My id " + myid + " not in the peer list");
    }
    //加载数据库数据
    loadDataBase();
    //启动网络通信
    startServerCnxnFactory();
    try {
        //启动AdminServer
        adminServer.start();
    } catch (AdminServerException e) {
        LOG.warn("Problem starting AdminServer", e);
        System.out.println(e);
    }
    //启动选举策略
    startLeaderElection();
    //启动JVM监听
    startJvmPauseMonitor();
    //启动线程
    super.start();
}
```

quorumPeer.start() 方法启动的主要步骤 :

1. loadDataBase()加载数据。
2. startServerCnxnFactory 用来开启acceptThread、SelectorThread和workerPool线程池。
3. 开启Leader选举startLeaderElection。
4. 开启JVM监控线程startJvmPauseMonitor。
5. 调用父类super.start();进行Leader选举。  

startLeaderElection() 开启Leader选举方法做了2件事，首先创建初始化选票选自己，接着创建选举投票方式，源码如下  

```java
public synchronized void startLeaderElection() {
    try {
        // 创建当前选票，启动时先投票投给自己
        if (getPeerState() == ServerState.LOOKING) {
            currentVote = new Vote(myid, getLastLoggedZxid(), getCurrentEpoch());
        }
    } catch (IOException e) {
        RuntimeException re = new RuntimeException(e.getMessage());
        re.setStackTrace(e.getStackTrace());
        throw re;
    }
    // 创建选举方式 createElectionAlgorithm
    this.electionAlg = createElectionAlgorithm(electionType);
}
```

createElectionAlgorithm() 创建选举算法只有第3种，其他2种均已废弃，方法源码如下：  

```java
@SuppressWarnings("deprecation")
protected Election createElectionAlgorithm(int electionAlgorithm) {
    Election le = null;

    //TODO: use a factory rather than a switch
    switch (electionAlgorithm) {
        case 1:
            throw new UnsupportedOperationException("Election Algorithm 1 is not supported.");
        case 2:
            throw new UnsupportedOperationException("Election Algorithm 2 is not supported.");
        case 3:
            // 选举方式只有electionAlgorithm = 3 ，其他的均已废弃
            QuorumCnxManager qcm = createCnxnManager();
            QuorumCnxManager oldQcm = qcmRef.getAndSet(qcm);
            if (oldQcm != null) {
                LOG.warn("Clobbering already-set QuorumCnxManager (restarting leader election?)");
                oldQcm.halt();
            }
            QuorumCnxManager.Listener listener = qcm.listener;
            if (listener != null) {
                listener.start();
                FastLeaderElection fle = new FastLeaderElection(this, qcm);
                fle.start();
                le = fle;
            } else {
                LOG.error("Null listener when initializing cnx manager");
            }
            break;
        default:
            assert false;
    }
    return le;
}
```

这个方法创建了以下三个对象：

1. 创建**QuorumCnxManager**对象
2. QuorumCnxManager.Listener  
3. FastLeaderElection  

# 3.Zookeeper集群Leader选举

## 3.1 Paxos算法介绍

Zookeeper选举主要依赖于FastLeaderElection算法，其他算法均已淘汰，但FastLeaderElection算法又是典型的Paxos算法，所以我们要先学习下Paxos算法，这样更有助于掌握FastLeaderElection算法。

### 3.1.1 Paxos介绍

分布式事务中常见的事务模型有2PC和3PC，无论是2PC提交还是3PC提交都无法彻底解决分布式的一致性问题以及无法解决太过保守及容错性不好。Google Chubby的作者Mike Burrows说过，世上只有一种一致性算法，那就是Paxos，所有其他一致性算法都是Paxos算法的不完整版。Paxos算法是公认的晦涩，很难讲清楚，但是工程上也很难实现，所以有很多Paxos算法的工程实现，如Chubby， Raft，**ZAB**，微信的PhxPaxos等。这一篇会介绍这个公认为难于理解但是行之有效的Paxos算法。Paxos算法是莱斯利·兰伯特(Leslie Lamport)1990年提出的一种**基于消息传递的一致性算法**，它曾就此发表了《The Part-Time Parliament》，《Paxos Made Simple》，由于采用故事的方式来解释此算法，感觉还是很难理

### 3.1.2 Paxos算法背景

Paxos算法是基于消息传递且具有高度容错特性的一致性算法，是目前公认的解决分布式一致性问题最有效的算法之一，其解决的问题就是在分布式系统中如何就某个值（决议）达成一致。 面试的时候：不要把这个Paxos算法达到的目的和分布式事务联系起来，而是针对Zookeeper这样的master-slave集群对某个决议达成一致，也就是副本之间写或者leader选举达成一致。我觉得这个算法和狭义的分布式事务不是一样的。 在常见的分布式系统中，总会发生诸如机器宕机或网络异常（包括消息的延迟、丢失、重复、乱序，还有网络分区）(也就是会发生异常的分布式系统)等情况。Paxos算法需要解决的问题就是如何在一个可能发生上述异常的分布式系统中，快速且正确地在集群内部对某个数据的值达成一致。也可以理解成分布式系统中达成状态的一致性。

### 3.1.3 Paxos算法理解

Paxos 算法是分布式一致性算法用来**解决一个分布式系统如何就某个值(决议)达成一致的问题**。一个典型的场景是，在一个分布式数据库系统中，如果各节点的初始状态一致，每个节点都执行相同的操作序列，那么他们最后能得到一个一致的状态。为保证每个节点执行相同的命令序列，需要在每一条指令上执行一个”**一致性算法**”以保证每个节点看到的指令一致。 分布式系统中一般是通过多副本来保证可靠性，而多个副本之间会存在数据不一致的情况。所以必须有一个一致性算法来保证数据的一致，描述如下：

​	假如在分布式系统中初始是各个节点的数据是一致的，每个节点都顺序执行系列操作，然后每个节点最终的数据还是一致的。

Paxos算法就是解决这种分布式场景中的一致性问题。对于**一般的开发人员来说，只需要知道paxos是一个分布式选举算法即可**。多个节点之间存在两种通讯模型：共享内存（Shared memory）、消息传递（Messages passing），Paxos是**基于消息传递的通讯模型**的。

### 3.1.4 Paxos相关概念

在Paxos算法中，有三种角色：

- Proposer：发起者
- Acceptor：接受者
- Learners：学习者

在具体的实现中，一个进程可能同时充当多种角色。比如一个进程可能既是Proposer又是Acceptor又是Learner。Proposer负责提出提案，Acceptor负责对提案作出裁决（accept与否），learner负责学习提案结果。 还有一个很重要的概念叫提案（Proposal）。最终要达成一致的value就在提案里。只要Proposer发的提案被Acceptor接受（半数以上的Acceptor同意才行），Proposer就认为该提案里的value被选定了。Acceptor告诉Learner哪个value被选定，Learner就认为那个value被选定。只要Acceptor接受了某个提案，Acceptor就认为该提案里的value被选定了。 为了避免单点故障，会有一个Acceptor集合，Proposer向Acceptor集合发送提案，Acceptor集合中的每个成员都有可能同意该提案且每个Acceptor只能批准一个提案，只有当一半以上的成员同意了一个提案，就认为该提案被选定了。

![image-20211108110028137](10_zookeeper源码02.assets/image-20211108110028137.png)

## 3.2 ZAB协议

### 3.2.1 ZAB概念

Zookeeper使用ZAB协议作为数据一致性算法，ZAB(Zookeeper Atomic Broadcast)，全称为：原子消息广播协议

ZAB是在**Paxos算法基础上拓展改造而来的**。ZAB协议支持**原子广播**、**崩溃恢复**，ZAB协议保证Leader广播的变更序列被顺序的处理。

Zookeeper根据ZAB协议建立了主备模型保证zookeeper集群中各副本之间的数据一致性。

### 3.2.2 ZAB三种状态

每个结点都属于以下三种中的一种：

- Looking：系统刚启动时或者Leader崩溃后处于选举状态
- Following：Follower节点所处的状态，Follower与Leader处于数据同步阶段。
- Leading：leader所处状态，当前集群中有一个Leader为主进程。

### 3.2.3 核心协议

- 所有事物请求必须由一个全局唯一的服务器来协调处理，这样的服务器被称为Leader服务器，而余下的服务器称为Follower服务器。
- Leader服务器负责将客户端事务请求转换成一个事务Proposal（提案），并将该Proposal分发给集群中所有的Follower服务器。
- 之后Leader服务器需要等到所有Follower服务器的反馈,一旦超过半数的Follower服务器进行了正确的反馈，那么Leader就会再次向所有的Follower服务器分发Commit消息，要求其将前一个Proposal进行提交。

## 3.3 QuorumPeer工作流程

![image-20211108151702133](10_zookeeper源码02.assets/image-20211108151702133.png)

每台服务器在启动的过程中，会启动一个QuorumPeer，负责各台服务器之间的底层Leader选举过程中的网络通信对应类就是**QuorumCnxManager**。

Zookeeper 对于每个节点 **QuorumPeer** 的设计相当的灵活， QuorumPeer 主要包括四个组件：

1. 客户端请求接收器( ServerCnxnFactory )

    负责维护与客户端的连接（接受客户端的请求并发出相应的指令）

    这就是单机版的CnxyFactory，负责和客户端通信的

    ```java
    // QuorumPeerMain.java
    /**
     * 集群模式启动
     */
    public void runFromConfig(QuorumPeerConfig config) throws IOException, AdminServerException {
      //注册JMX
      //...
      //设置网络通信方式
      if (config.getClientPortAddress() != null) {
        cnxnFactory = ServerCnxnFactory.createFactory();
        cnxnFactory.configure(config.getClientPortAddress(), config.getMaxClientCnxns(), config.getClientPortListenBacklog(), false);
        //单机版在这里会server.start()
        //集群版没有调用，ZK服务未启动
      }
    
      //...
      //创建QuorumPeer，该对象是zookeeper执行同步,选主过程的线程
      quorumPeer = getQuorumPeer();
      //通信配置
      quorumPeer.setCnxnFactory(cnxnFactory);
    }
    ```

    

2. 数据引擎( ZKDatabase )

    负责存储/加载/查找数据(基于目录树结构的KV+操作日志+客户端Session);

3. 选举器( Election )

    负责选举集群的一个Leader节点;

4. 核心功能组件( Leader/Follower/Observer )

    Leader/Follower/Observer确认是QuorumPeer节点应该完成的核心职责;

这里类有点乱，理一下：

![image-20211108195243909](10_zookeeper源码02.assets/image-20211108195243909.png)

QuorumPeer 工作流程：

1. 初始化配置
2. 加载当前存在的数据
3. 启动网络通信组件
4. 启动控制台
5. 开启选举协调者，并执行选举（会一直执行）

![image-20211108152554861](10_zookeeper源码02.assets/image-20211108152554861.png)

## 3.4 QuorumCnxManager源码分析

QuorumCnxManager内部维护了一些列队列，用来保存接收到的、待发送的消息以及消息的发送器，除接收队列外，其他队列都按照SID分组行程队列集合。如一个集群中除了自身还有3台机器，那么就会为这3台机器分别创建一个发送队列，互不干扰。

```java
//消息接收队列，用于存放那些其他服务器接收到的消息。（接收投票信息）
public final BlockingQueue<Message> recvQueue;
//消息发送队列，用于保存那些待发送的消息，按照SID进行分组。
final ConcurrentHashMap<Long, BlockingQueue<ByteBuffer>> queueSendMap;
//发送器集合，每个SenderWorker消息发送器，都对应一台远程Zookeeper服务器，否则消息的发送，也按照SID进行分组。
final ConcurrentHashMap<Long, SendWorker> senderWorkerMap;
//最近发送过的消息，为每个SID保留最近发送过的一个消息。
final ConcurrentHashMap<Long, ByteBuffer> lastMessageSent;

public QuorumCnxManager(QuorumPeer self, final long mySid, Map<Long, QuorumPeer.QuorumServer> view,
                        QuorumAuthServer authServer, QuorumAuthLearner authLearner, int socketTimeout, boolean listenOnAllIPs,
                        int quorumCnxnThreadsSize, boolean quorumSaslAuthEnabled) {
  //消息接收队列，用于存放那些其他服务器接收到的消息。（接收投票信息）
  this.recvQueue = new CircularBlockingQueue<>(RECV_CAPACITY);
  //消息发送队列，用于保存那些待发送的消息，按照SID进行分组。
  this.queueSendMap = new ConcurrentHashMap<>();
  //发送器集合，每个SenderWorker消息发送器，都对应一台远程Zookeeper服务器，否则消息的发送，也按照SID进行分组。
  this.senderWorkerMap = new ConcurrentHashMap<>();
  //最近发送过的消息，为每个SID保留最近发送过的一个消息。
  this.lastMessageSent = new ConcurrentHashMap<>();

  String cnxToValue = System.getProperty("zookeeper.cnxTimeout");
  if (cnxToValue != null) {
    this.cnxTO = Integer.parseInt(cnxToValue);
  }
  // ...
}
```

QuorumCnxManager.Listener ：

​	是QuorumCnxManager的内部类，继承自ZooKeeperThread，也是一个线程。

​	为了能够相互投票，Zookeeper集群中的所有机器都需要建立起网络连接。QuorumCnxManager在启动时会创建一个ServerSocket来监听Leader选举的通信端口。开启监听后，Zookeeper能够不断地接收到来自其他服务器地创建连接请求，在接收到其他服务器地TCP连接请求时，会进行处理。为了避免两台机器之间重复地创建TCP连接，Zookeeper只允许**SID大的服务器主动和其他机器建立连接**，否则断开连接。在接收到创建连接请求后，服务器通过对比自己和远程服务器的SID值来判断是否接收连接请求，如果当前服务器发现自己的SID更大，那么会断开当前连接，然后自己主动和远程服务器将连接（自己作为“客户端”）。一旦连接建立，就会根据远程服务器的SID来创建相应的消息发送器SendWorker和消息发送器RecvWorker，并启动。

QuorumCnxManager.Listener 监听启动可以查看 QuorumCnxManager.Listener 的 run 方法，源代码如下，可以断点调试看到此时监听的正是我们所说的投票端口：

![image-20211108154616095](10_zookeeper源码02.assets/image-20211108154616095.png)

上面是监听器，各个服务之间进行通信我们需要开启 ListenerHandler 线程，在QuorumCnxManager.Listener.ListenerHandler 的run方法中有一个方法 acceptConnections() 调用，该方法就是用于接受每次选举投票的信息，如果只有一个节点或者没有投票信息的时候，此时方法会阻塞，一旦执行选举，程序会往下执行，我们可以先启动1台服务，再启动第2台、第3台，此时会收到有客户端参与投票链接，程序会往下执行，源码如下：

```java
private void acceptConnections() {
  int numRetries = 0;
  Socket client = null;

  while ((!shutdown) && (portBindMaxRetry == 0 || numRetries < portBindMaxRetry)) {
    try {
      // 选举开启套接字
      serverSocket = createNewServerSocket();
      LOG.info("{} is accepting connections now, my election bind port: {}", QuorumCnxManager.this.mySid, address.toString());
      while (!shutdown) {
        try {
          // 阻塞等待客户端链接
          client = serverSocket.accept();
          setSockOpts(client);
          LOG.info("Received connection request from {}", client.getRemoteSocketAddress());
          // Receive and handle the connection request
          // asynchronously if the quorum sasl authentication is
          // enabled. This is required because sasl server
          // authentication process may take few seconds to finish,
          // this may delay next peer connection requests.
          if (quorumSaslAuthEnabled) {
            receiveConnectionAsync(client);
          } else {
            receiveConnection(client);
            System.out.println("有客户端链接，参与投票-->"+client.getPort());
          }
          numRetries = 0;
```

上面虽然能证明投票访问了当前监听的端口，但怎么知道是哪台服务呢？我们可以沿着receiveConnection() 源码继续研究，源码如下：

```java
public void receiveConnection(final Socket sock) {
  DataInputStream din = null;
  try {
    din = new DataInputStream(new BufferedInputStream(sock.getInputStream()));

    LOG.debug("Sync handling of connection request received from: {}", sock.getRemoteSocketAddress());
    handleConnection(sock, din);
  } catch (IOException e) {
    LOG.error("Exception handling connection, addr: {}, closing server connection", sock.getRemoteSocketAddress());
    LOG.debug("Exception details: ", e);
    closeSocket(sock);
  }
}
```

receiveConnection() 方法只是获取了数据流，并没做特殊处理，并且调用了 handleConnection()方法，该方法源码如下:

```java
private void handleConnection(Socket sock, DataInputStream din) throws IOException {
  Long sid = null, protocolVersion = null;
  MultipleAddresses electionAddr = null;

  try {
    protocolVersion = din.readLong();
    if (protocolVersion >= 0) { // this is a server id and not a protocol version
      sid = protocolVersion;
    } else {
      try {
        // 获取SID，也就是myid的值
        InitialMessage init = InitialMessage.parse(protocolVersion, din);
        sid = init.sid;
        if (!init.electionAddr.isEmpty()) {
          electionAddr = new MultipleAddresses(init.electionAddr,
                                   Duration.ofMillis(self.getMultiAddressReachabilityCheckTimeoutMs()));
        }
        LOG.debug("Initial message parsed by {}: {}", self.getId(), init.toString());
      } catch (InitialMessage.InitialMessageException ex) {
        LOG.error("Initial message parsing error!", ex);
        closeSocket(sock);
        return;
      }
    }
    // 输出SID
    System.out.println("参与投票的MyId"+ sid);
```

总之，Listener的作用就是和别的客户端通信建立起来了

## 3.5 FastLeaderElection算法源码分析



![image-20211108162638685](10_zookeeper源码02.assets/image-20211108162638685.png)

在 Zookeeper 集群中，主要分为三者角色，而每一个节点同时只能扮演一种角色，这三种角色分别是：

1. **Leader** 接受所有Follower的提案请求并统一协调发起提案的投票，负责与所有的Follower进行内部的数据交换(同步);
2. **Follower** 直接为客户端提供服务并参与提案的投票，同时与 Leader 进行数据交换(同步);
3. **Observer** 直接为客户端服务但并不参与提案的投票，同时也与 Leader 进行数据交换(同步);

FastLeaderElection 选举算法是标准的 Fast Paxos 算法实现，可解决 LeaderElection 选举算法收敛速度慢的问题。

### 3.5.1 FastLeaderElection开始

创建 FastLeaderElection 只需要 new FastLeaderElection() 即可，如下代码：

```java
public FastLeaderElection(QuorumPeer self, QuorumCnxManager manager) {
  this.stop = false;
  this.manager = manager;
  starter(self, manager);
}
```

创建 FastLeaderElection 会调用 starter() 方法，该方法会创建 sendqueue 、 recvqueue 队列、Messenger 对象，其中 Messenger 对象的作用非常关键，方法源码如下：

```java
private void starter(QuorumPeer self, QuorumCnxManager manager) {
  this.self = self;
  proposedLeader = -1;
  proposedZxid = -1;
  //创建sendqueue(投票存储的队列信息)、recvqueue队列(接收的投票信息)、Messenger对象
  sendqueue = new LinkedBlockingQueue<ToSend>();
  recvqueue = new LinkedBlockingQueue<Notification>();
  this.messenger = new Messenger(manager);
}
```

创建Messenger的时候，会创建 WorkerSender 并封装成 wsThread 线程，创建 WorkerReceiver 并封装成 wrThread 线程，看名字就很容易理解， wsThread 用于发送数据， wrThread 用于接收数据，Messenger 创建源码如下：

```java
Messenger(QuorumCnxManager manager) {

  this.ws = new WorkerSender(manager);
  // wsThread是 workerSender任务的封装
  this.wsThread = new Thread(this.ws, "WorkerSender[myid=" + self.getId() + "]");
  this.wsThread.setDaemon(true);

  this.wr = new WorkerReceiver(manager);
  // wrThread是 WorkerReceiver任务的封装
  this.wrThread = new Thread(this.wr, "WorkerReceiver[myid=" + self.getId() + "]");
  this.wrThread.setDaemon(true);
}
```

创建完 FastLeaderElection 后接着会调用它的 start() 方法启动选举算法，代码如下：

```java
// QuorumPeer.java  的 createElectionAlgorithm 方法
//创建算法
FastLeaderElection fle = new FastLeaderElection(this, qcm);
fle.start();
```

启动选举算法会调用start()方法，start()方法如下:

```java
// FastLeaderElection.java
public void start() {
  this.messenger.start();
}

void start() {
  this.wsThread.start();
  this.wrThread.start();
}
```

### 3.5.2 WorkerSender发送投票

wsThread 由 WorkerSender 封装而来，此时会调用 WorkerSender 的 run 方法，run方法会调用process() 方法，源码如下：

```java
// FastLeaderElection.java
public void run() {
  while (!stop) {
    try {
      //需要发送的投票信息
      ToSend m = sendqueue.poll(3000, TimeUnit.MILLISECONDS);
      if (m == null) {
        continue;
      }
      //处理消息
      process(m);
    } catch (InterruptedException e) {
      break;
    }
  }
  LOG.info("WorkerSender is down");
}
```

process 方法调用了 manager 的 toSend 方法，此时是把对应的sid作为了消息发送出去，这里其实是发送投票信息，源码如下：

```java
void process(ToSend m) {
  //取出数据
  ByteBuffer requestBuffer = buildMsg(m.state.ordinal(), m.leader, m.zxid, m.electionEpoch, m.peerEpoch, m.configData);
  //执行发送
  manager.toSend(m.sid, requestBuffer);
}
```

投票可以投自己，也可以投别人，如果是选票选自己，只需要把投票信息添加到 recvQueue 中即可，源码如下：

```java
public void toSend(Long sid, ByteBuffer b) {
  /*
   * If sending message to myself, then simply enqueue it (loopback).
   */
  if (this.mySid == sid) {
    b.position(0);
    // 自己给自己的投票
    addToRecvQueue(new Message(b.duplicate(), sid));
    /*
    * Otherwise send to the corresponding thread to send.
    */
  } else {
    /*
     * 发送投票信息给其他节点
     */
    BlockingQueue<ByteBuffer> bq = queueSendMap.computeIfAbsent(sid, serverId -> new CircularBlockingQueue<>(SEND_CAPACITY));
    addToSendQueue(bq, b);
    connectOne(sid);
  }
}
```

### 3.5.3 WorkerReceiver 接收投票

在 WorkerReceiver.run 方法中会从 recvQueue 中获取 Message ，并把发送给其他服务的投票封装到sendqueue 队列中，交给 WorkerSender 发送处理，源码如下：

```java
public void run() {

  Message response;
  while (!stop) {
    // Sleeps on receive
    try {
      //从recvQueue中获取Message对象
      response = manager.pollRecvQueue(3000, TimeUnit.MILLISECONDS);
      if (response == null) {
        continue;
      }
      // ...
      /*
       * 发送给其他服务的投票封装成ToSend，并存入到sendqueue中
       */
      if (!validVoter(response.sid)) {
        Vote current = self.getCurrentVote();
        QuorumVerifier qv = self.getQuorumVerifier();
        ToSend notmsg = new ToSend(
          ToSend.mType.notification,
          current.getId(),
          current.getZxid(),
          logicalclock.get(),
          self.getPeerState(),
          response.sid,
          current.getPeerEpoch(),
          qv.toString().getBytes(UTF_8));
				// 存入队列中
        sendqueue.offer(notmsg);
       // ...
```



## 3.6 Zookeeper 选举投票剖析

选举是个很复杂的过程，要考虑很多场景，而且选举过程中有很多概念需要理解

### 3.6.1 选举概念

#### 1）服务状态：

```java
public enum ServerState {
  //代表没有当前集群中没有Leader,此时是投票选举状态
  LOOKING,
  //代表已经是伴随者状态
  FOLLOWING,
  //代表已经是领导者状态
  LEADING,
  //代表已经是观察者状态（观察者不参与投票过程）
  OBSERVING
}
```

#### 2）服务角色：

```java
//Learner 是随从服务和观察者的统称
public enum LearnerType {
  //随从者角色
  PARTICIPANT,
  //观察者角色
  OBSERVER
}
```

#### 3）投票消息广播：

```java
    public static class Notification {
        /*
         * Format version, introduced in 3.4.6
         */

        public static final int CURRENTVERSION = 0x2;
        int version;

        /*
         * 被推荐leader的ID
         */ long leader;

        /*
         * 被推荐leader的zxid
         */ long zxid;

        /*
         * 投票轮次
         */ long electionEpoch;

        /*
         * 当前投票者的服务状态 （LOOKING）
         */ QuorumPeer.ServerState state;

        /*
         * 当前投票者的ID
         */ long sid;

        //QuorumVerifier作为集群验证器，主要完成判断一组server在
        //已给定的配置的server列表中，是否能够构成集群
        QuorumVerifier qv;
        /*
         * 被推荐leader的投票轮次
         */ long peerEpoch;
    }
```

#### 4）选票模型：

```java
public class Vote {
  //投票版本号，作为一个标识
  private final int version;
  //当前服务的ID
  private final long id;
  //当前服务事务ID
  private final long zxid;
  //当前服务投票的轮次
  private final long electionEpoch;
  //被推举服务器的投票轮次
  private final long peerEpoch;
  //当前服务器所处的状态
  private final ServerState state;
}
```

#### 5）消息发送对象：

```java
    public static class ToSend {

        //支持的消息类型
        enum mType {
            crequest, //请求
            challenge, //确认
            notification,//通知
            ack //确认回执
        }

        ToSend(mType type, long leader, long zxid, long electionEpoch, ServerState state, long sid, long peerEpoch, byte[] configData) {

            this.leader = leader;
            this.zxid = zxid;
            this.electionEpoch = electionEpoch;
            this.state = state;
            this.sid = sid;
            this.peerEpoch = peerEpoch;
            this.configData = configData;
        }

        /*
         * Proposed leader in the case of notification
         * 被投票推举为leader的服务ID
         */ long leader;

        /*
         * id contains the tag for acks, and zxid for notifications
         */ long zxid;

        /*
         * Epoch
         * 投票轮次
         */ long electionEpoch;

        /*
         * Current state;
         * 服务状态
         */ QuorumPeer.ServerState state;

        /*
         * Address of recipient
         * 消息接收方服务ID
         */ long sid;

        /*
         * Used to send a QuorumVerifier (configuration info)
         */ byte[] configData = dummyData;

        /*
         * Leader epoch
         */ long peerEpoch;

    }
```

### 3.6.2 选举过程

QuorumPeer本身是个线程，在集群启动的时候会执行 quorumPeer.start(); ，此时会调用它重写的start() 方法，最后会调用父类的 start() 方法，所以该线程会启动执行，因此会执行它的run方法，而run方法正是选举流程的入口，我们看run方法关键源码如下：

```java
reconfigFlagClear();
if (shuttingDownLE) {
  shuttingDownLE = false;
  startLeaderElection();
}
//设置投票节点(设置Leader节点) lookForLeader:寻找Leader
setCurrentVote(makeLEStrategy().lookForLeader());
```

所有节点初始状态都为LOOKING，会进入到选举流程，选举流程首先要获取算法，获取算法的方法是makeLEStrategy() ，该方法返回的是 FastLeaderElection 实例，核心选举流程是FastLeaderElection 中的 **lookForLeader**() 方法。

```java
/*
 * 获取选举算法
 */
@SuppressWarnings("deprecation")
protected Election makeLEStrategy() {
  LOG.debug("Initializing leader election protocol...");
  return electionAlg;
}
```

**lookForLeader**() 是选举过程的关键流程，源码分析如下:

```java
/**
 * Leader节点选举
 * Starts a new round of leader election. Whenever our QuorumPeer
 * changes its state to LOOKING, this method is invoked, and it
 * sends notifications to all other peers.
 */
public Vote lookForLeader() throws InterruptedException {
  try {
    self.jmxLeaderElectionBean = new LeaderElectionBean();
    MBeanRegistry.getInstance().register(self.jmxLeaderElectionBean, self.jmxLocalPeerBean);
  } catch (Exception e) {
    LOG.warn("Failed to register with JMX", e);
    self.jmxLeaderElectionBean = null;
  }

  self.start_fle = Time.currentElapsedTime();
  try {
    /*
     * 接收的投票票池
     */
    Map<Long, Vote> recvset = new HashMap<Long, Vote>();

    /*
     * 投票结果
     */
    Map<Long, Vote> outofelection = new HashMap<Long, Vote>();

    int notTimeout = minNotificationInterval;

    //最开始投票--投自己
    synchronized (this) {
      //投票轮次自增 epoch = 投票轮次(记录当前服务投票的次数)
      logicalclock.incrementAndGet();
      //投票
      //投票,首次推举自己为leader，投票信息：myid  zxid  epoch
      //确认我要投票的对象(提案)   getInitId():获取自己的ID
      //                       getInitLastLoggedZxid() 获取自己的最大事务操作ID   递增
      //                       getPeerEpoch() 获取投票选举轮次  递增
      updateProposal(getInitId(), getInitLastLoggedZxid(), getPeerEpoch());
    }

    /***
     * 广播发出投票,这里只是将投票信息添加到sendqueue中
     * WorkerSender的run方法会将sendqueue中的数据存储到queueSendMap中（该集合会排除自己）
     * connectAll()方法会链接所有服务进行投票结果投递
     */
    sendNotifications();

    SyncedLearnerTracker voteSet;

    //====================投票选举流程开始====================
    //如果当前server状态依然是LOOKING状态，且未选出leader则直到找到为止
    while ((self.getPeerState() == ServerState.LOOKING) && (!stop)) {
      /*
       * 从接收队列中拿到投票信息
       * 该过程属于阻塞过程，直到本次阻塞超时
       */
      Notification n = recvqueue.poll(notTimeout, TimeUnit.MILLISECONDS);

      /*
       * 如果没有收到足够的投票，就继续发出广播进行投票，否则处理投票信息。
       */
      if (n == null) {
        //如果集群中有其他节点信息，就开始广播
        if (manager.haveDelivered()) {
          //开始广播自己的投票信息
          sendNotifications();
        } else {
          //如果服务器不存在，尝试与每个服务器建立连接。
          manager.connectAll();
        }

        /*
         * 延长从队列获取选票时长
         */
        int tmpTimeOut = notTimeout * 2;
        notTimeout = Math.min(tmpTimeOut, maxNotificationInterval);
      } else if (validVoter(n.sid) && validVoter(n.leader)) {
        //判断sid是否属于当前集群内的节点
        switch (n.state) {
          case LOOKING:
            //如果当前选举人是LOOKING状态
            if (getInitLastLoggedZxid() == -1) {
              break;
            }
            if (n.zxid == -1) {
              break;
            }
            //收到的epoch是不是比当前选举的epoch要大，如果大那么代表是新一轮选举
            if (n.electionEpoch > logicalclock.get()) {
              logicalclock.set(n.electionEpoch);
              recvset.clear();
              //==============选举选举选举选举选举==============
              //进行选票PK，如果自己的票没有PK过其他投递的票，则将自己的票变更为其他
              if (totalOrderPredicate(n.leader, n.zxid, n.peerEpoch, getInitId(), getInitLastLoggedZxid(), getPeerEpoch())) {
                //新的投票（提案）
                updateProposal(n.leader, n.zxid, n.peerEpoch);
              } else {
                //投票自己
                updateProposal(getInitId(), getInitLastLoggedZxid(), getPeerEpoch());
              }
              //重新发出投票
              sendNotifications();
            } else if (n.electionEpoch < logicalclock.get()) {
              //如果收到的epoch比逻辑中的epoch还小，那么表示这个投票无效
              break;
            } else if (totalOrderPredicate(n.leader, n.zxid, n.peerEpoch, proposedLeader, proposedZxid, proposedEpoch)) {
              //如果收到的epoch和逻辑epoch相等，则去对比myid 、zxid、epoch进行投票
              updateProposal(n.leader, n.zxid, n.peerEpoch);
              //重新发出投票
              sendNotifications();
            }

            //把投票结果存到本地，用来做最终判断
            recvset.put(n.sid, new Vote(n.leader, n.zxid, n.electionEpoch, n.peerEpoch));

            /**
             * 统计选票，把所有投票结果合到一起
             */
            voteSet = getVoteTracker(recvset, new Vote(proposedLeader, proposedZxid, logicalclock.get(), proposedEpoch));

            //判断是否有超过半数的票数是支持同一个服务ID
            if (voteSet.hasAllQuorums()) {
              //如果此刻在票池汇总还有未取出的投票，则和选举出的投票PK，如果取出的票优于当前推举的投票，则重新投票
              while ((n = recvqueue.poll(finalizeWait, TimeUnit.MILLISECONDS)) != null) {
                if (totalOrderPredicate(n.leader, n.zxid, n.peerEpoch, proposedLeader, proposedZxid, proposedEpoch)) {
                  recvqueue.put(n);
                  break;
                }
              }

              //如果票池中没有可PK的投票，则就认为选举出来的服务为leader
              if (n == null) {
                //修改各个服务的状态，
                setPeerState(proposedLeader, voteSet);
                Vote endVote = new Vote(proposedLeader, proposedZxid, logicalclock.get(), proposedEpoch);
                //清除投票池
                leaveInstance(endVote);
                return endVote;
              }
            }
            break;
          case OBSERVING:
            LOG.debug("Notification from observer: {}", n.sid);
            break;
          case FOLLOWING:
          case LEADING:
            //如果新收到的选票发送者角色是leader角色状态且选票轮次和自己的选票轮次一样
            if (n.electionEpoch == logicalclock.get()) {
              //则将leader角色投递的这张选票放入自己的选票池中
              recvset.put(n.sid, new Vote(n.leader, n.zxid, n.electionEpoch, n.peerEpoch, n.state));
              //判断是否有超过半数的票数是推荐了n推荐的leader且n.leader也确实是LEADING状态
              voteSet = getVoteTracker(recvset, new Vote(n.version, n.leader, n.zxid, n.electionEpoch, n.peerEpoch, n.state));
              if (voteSet.hasAllQuorums() && checkLeader(recvset, n.leader, n.electionEpoch)) {
                //则指定n推荐的为真正的leader同时修改其他服务对应的状态
                setPeerState(n.leader, voteSet);
                Vote endVote = new Vote(n.leader, n.zxid, n.electionEpoch, n.peerEpoch);
                //清空票池
                leaveInstance(endVote);
                return endVote;
              }
            }

            //如果轮次不一致，则将N的投票记录到outofelection中
            outofelection.put(n.sid, new Vote(n.version, n.leader, n.zxid, n.electionEpoch, n.peerEpoch, n.state));
            voteSet = getVoteTracker(outofelection, new Vote(n.version, n.leader, n.zxid, n.electionEpoch, n.peerEpoch, n.state));
            //判断是否有超过半数的票数是推荐了n推荐的leader且n.leader也确实是LEADING状态
            if (voteSet.hasAllQuorums() && checkLeader(outofelection, n.leader, n.electionEpoch)) {
              synchronized (this) {
                //更新当前服务选举轮次
                logicalclock.set(n.electionEpoch);
                //则指定n推荐的为真正的leader同时修改其他服务对应的状态
                setPeerState(n.leader, voteSet);
              }
              Vote endVote = new Vote(n.leader, n.zxid, n.electionEpoch, n.peerEpoch);
              //清空票池
              leaveInstance(endVote);
              return endVote;
            }
            break;
          default:
            LOG.warn("Notification state unrecognized: {} (n.state), {}(n.sid)", n.state, n.sid);
            break;
        }
      } else {
        if (!validVoter(n.leader)) {
          LOG.warn("Ignoring notification for non-cluster member sid {} from sid {}", n.leader, n.sid);
        }
        if (!validVoter(n.sid)) {
          LOG.warn("Ignoring notification for sid {} from non-quorum member sid {}", n.leader, n.sid);
        }
      }
    }
    return null;
  } finally {
    try {
      if (self.jmxLeaderElectionBean != null) {
        MBeanRegistry.getInstance().unregister(self.jmxLeaderElectionBean);
      }
    } catch (Exception e) {
      LOG.warn("Failed to unregister with JMX", e);
    }
    self.jmxLeaderElectionBean = null;
    LOG.debug("Number of connection processing threads: {}", manager.getConnectionThreadCount());
  }
}
```

上面多个地方都用到了过半数以上的方法 hasAllQuorums() 该方法用到了 QuorumMaj 类，代码如下：

```java
public boolean hasAllQuorums() {
  for (QuorumVerifierAcksetPair qvAckset : qvAcksetPairs) {
    // 过半数以上算法过程
    if (!qvAckset.getQuorumVerifier().containsQuorum(qvAckset.getAckset())) {
      return false;
    }
  }
  return true;
}
```

QuorumMaj 构造函数中体现了过半数以上的操作，代码如下：

```java
public QuorumMaj(Map<Long, QuorumServer> allMembers) {
  this.allMembers = allMembers;
  for (QuorumServer qs : allMembers.values()) {
    // 获取所有角色为PARTICIPANT的成员，Observer不参与投票，所以过半计算中不计入
    if (qs.type == LearnerType.PARTICIPANT) {
      votingMembers.put(Long.valueOf(qs.id), qs);
    } else {
      observingMembers.put(Long.valueOf(qs.id), qs);
    }
  }
  half = votingMembers.size() / 2;
}
```



### 3.6.3 投票规则

我们来看一下选票PK的方法 totalOrderPredicate() ，该方法其实就是Leader选举规则，规则有如下三个：

1. 比较 epoche(zxid高32bit)，如果其他节点的epoche比自己的大，选举 epoch大的节点（理由：epoch 表示年代，epoch越大表示数据越新）代码：(newEpoch > curEpoch)；
2. 比较 zxid， 如果epoche相同，就比较两个节点的zxid的大小，选举 zxid大的节点（理由：zxid表示节点所提交事务最大的id，zxid越大代表该节点的数据越完整）代码：(newEpoch == curEpoch)&& (newZxid > curZxid)；
3. 比较 serviceId，如果 epoch和zxid都相等，就比较服务的serverId，选举 serviceId大的节点（理由： serviceId 表示机器性能，他是在配置zookeeper集群时确定的，所以我们配置zookeeper集群的时候可以把服务性能更高的集群的serverId设置大些，让性能好的机器担任leader角色）代码 ：(newEpoch == curEpoch) && ((newZxid == curZxid) && (newId > curId))。

源码如下：

```java
protected boolean totalOrderPredicate(
  long newId,     // 别的节点传过来要和我本地节点PK的节点的id，对应的myid的值
  long newZxid,   // 别的节点传过来要和我本地节点PK的事务ID值，zxid，zxid越大说明说句越新
  long newEpoch, //  别的节点传过来要和我本地节点PK的投票轮次，投票轮次越大，说明投票次数越多，投票结果越新
  long curId,    // 本地投票的节点的id，对应的myid的值
  long curZxid,  // 本地投票的节点的事务ID值，zxid，zxid越大说明说句越新
  long curEpoch) {//本地投票的节的投票轮次，投票轮次越大，说明投票次数越多，投票结果越新
  if (self.getQuorumVerifier().getWeight(newId) == 0) {
    return false;
  }

  return ((newEpoch > curEpoch)
          || (
            (newEpoch == curEpoch) && ((newZxid > curZxid)
                                       ||
                                       ((newZxid == curZxid) && (newId > curId)))
          )
         );

  /*
         * 对应上面代码的解释（两个节点之间使用比较的方法来选举，三种比较规则）
         * 1- newEpoch > curEpoch：收到的epoch大于当前的epoch胜出选举
         * 2- (newEpoch == curEpoch) 如果收到的epoch等于当前epoch,那么收到的zxid大于当前zxid胜出选举
         * 3- (newEpoch == curEpoch) && (newZxid == curZxid)
         *    如果收到的epoch等于当前epoch，zxid等于当前zxid，则比较myid
         *    收到的myid大于当前myid的胜出选举
         */
  //1：newEpoch   选举轮次
  //2：newZxid    事务ID
  //3：newId      节点的ID
  /*return (   (newEpoch > curEpoch)
                || ((newEpoch == curEpoch) && ((newZxid > curZxid)
                || ((newZxid == curZxid) && (newId > curId)))));*/
}
```



# 4.Zookeeper集群数据同步

所有事务操作都将由leader执行，并且会把数据同步到其他节点，比如follower、observer，我们可以分析leader和follower的操作行为即可分析出数据同步流程。

## 4.1 Zookeeper同步流程说明

![image-20211108201917957](10_zookeeper源码02.assets/image-20211108201917957.png)

整体流程：

1. 当角色确立之后，leader调用leader.lead();方法运行，创建一个接收连接的LearnerCnxAcceptor线程，在LearnerCnxAcceptor线程内部又建立一个阻塞的LearnerCnxAcceptorHandler线程等待Learner端的连接。Learner端以follower为例，follower调用follower.followLeader();方法首先查找leader的Socket服务端，然后建立连接。当follower建立连接后，leader端会建立一个LearnerHandler线程相对应，用来处理follower与leader的数据包传输。
2. follower端封装当前zk服务器的Zxid和Leader.FOLLOWERINFO的LearnerInfo数据包发送给leader
3. leader端这时处于getEpochToPropose方法的阻塞时期，需要得到Learner端超过一半的服务器发送Epoch
4. getEpochToPropose解阻塞之后，LearnerHandler线程会把超过一半的Epoch与leader比较得到最新的newLeaderZxid，并封装成Leader.LEADERINFO包发送给Learner端
5. Learner端得到最新的Epoch，会更新当前服务器的Epoch。并把当前服务器所处的lastLoggedZxid位置封装成Leader.ACKEPOCH发送给leader
6. 此时leader端处于waitForEpochAck方法的阻塞时期，需要得到Learner端超过一半的服务器发送EpochACK
7. 当waitForEpochAck阻塞之后便可以在LearnerHandler线程内决定用那种方式进行同步。如果Learner端的lastLoggedZxid>leader端的，Learner端将会被删除多余的部分。如果小于leader端的，将会以不同方式进行同步
8. leader端发送Leader.NEWLEADER数据包给Learner端（6、7步骤都是另开一个线程来发送这些数据包）
9. Learner端同步之后，会在一个while循环内处理各种leader端发送数据包，包括两阶段提交的Leader.PROPOSAL、Leader.COMMIT、Leader.INFORM等。在同步数据后会处理Leader.NEWLEADER数据包，然后发送Leader.ACK给leader端
10. 此时leader端处于waitForNewLeaderAck阻塞等待超过一半节点发送ACK。

我们回到 QuorumPeer.run() 方法，根据确认的不同角色执行不同操作展开分析。

## 4.2 Zookeeper Follower同步流程

Follower主要连接Leader实现数据同步，我们看看Follower做的事，我们仍然沿着QuorumPeer.run()展开学习，关键代码如下：

```java
case FOLLOWING:
try {
  // 创建Follower对象，并且设置Follower
  setFollower(makeFollower(logFactory));
  /***
   * 这里会进行一个死循环，主要做的逻辑
   * 1、连接Leader
   * 2、和Leader进行数据同步
   * 3、同步完毕后，正常接收Leader的请求，并且执行对应的逻辑，包括Request、Propose、Commit等请求
   */
  follower.followLeader();
} catch (Exception e) {
  LOG.warn("Unexpected exception", e);
} finally {
  // 如果集群超过半数服务宕机或者Leader宕机，那么首先设置调用shutdown()，然后设置状态为LOOKING，会重新触发选举
  follower.shutdown();
  setFollower(null);
  updateServerState();
}
break;
```

创建Follower的方法比较简单，代码如下：

```java
/**
 * 把自己作为Follower
 */
protected Follower makeFollower(FileTxnSnapLog logFactory) throws IOException {
  return new Follower(this, new FollowerZooKeeperServer(logFactory, this, this.zkDb));
}
```

我们看一下整个Follower在数据同步中做的所有操作 follower.followLeader(); ，源码如下：

```java
/**
 * Follower调用Leader的方法
 * @throws InterruptedException
 */
void followLeader() throws InterruptedException {
  self.end_fle = Time.currentElapsedTime();
  long electionTimeTaken = self.end_fle - self.start_fle;
  self.setElectionTimeTaken(electionTimeTaken);
  ServerMetrics.getMetrics().ELECTION_TIME.add(electionTimeTaken);
  LOG.info("FOLLOWING - LEADER ELECTION TOOK - {} {}", electionTimeTaken, QuorumPeer.FLE_TIME_UNIT);
  self.start_fle = 0;
  self.end_fle = 0;
  fzk.registerJMX(new FollowerBean(this, zk), self.jmxLocalPeerBean);

  long connectionTime = 0;
  boolean completedSync = false;

  try {
    self.setZabState(QuorumPeer.ZabState.DISCOVERY);
    //寻找Leader
    QuorumServer leaderServer = findLeader();
    try {
      //创建链接
      connectToLeader(leaderServer.addr, leaderServer.hostname);
      connectionTime = System.currentTimeMillis();
      //follower向leader提交节点信息用于计算新的 epoch 值
      long newEpochZxid = registerWithLeader(Leader.FOLLOWERINFO);
      if (self.isReconfigStateChange()) {
        throw new Exception("learned about role change");
      }
      //check to see if the leader zxid is lower than ours
      //this should never happen but is just a safety check
      long newEpoch = ZxidUtils.getEpochFromZxid(newEpochZxid);
      if (newEpoch < self.getAcceptedEpoch()) {
        LOG.error("Proposed leader epoch "
                  + ZxidUtils.zxidToString(newEpochZxid)
                  + " is less than our accepted epoch "
                  + ZxidUtils.zxidToString(self.getAcceptedEpoch()));
        throw new IOException("Error: Epoch of leader is lower");
      }
      long startTime = Time.currentElapsedTime();
      try {
        self.setLeaderAddressAndId(leaderServer.addr, leaderServer.getId());
        self.setZabState(QuorumPeer.ZabState.SYNCHRONIZATION);
        //和Leader同步数据
        syncWithLeader(newEpochZxid);
        self.setZabState(QuorumPeer.ZabState.BROADCAST);
        completedSync = true;
      } finally {
        long syncTime = Time.currentElapsedTime() - startTime;
        ServerMetrics.getMetrics().FOLLOWER_SYNC_TIME.add(syncTime);
      }
      if (self.getObserverMasterPort() > 0) {
        LOG.info("Starting ObserverMaster");

        om = new ObserverMaster(self, fzk, self.getObserverMasterPort());
        om.start();
      } else {
        om = null;
      }
      // 读取Leader发送的数据包
      QuorumPacket qp = new QuorumPacket();
      while (this.isRunning()) {
        //读取数据包
        readPacket(qp);
        //处理数据包
        processPacket(qp);
      }
    } catch (Exception e) {
      LOG.warn("Exception when following the leader", e);
      closeSocket();

      // clear pending revalidations
      pendingRevalidations.clear();
    }
  } finally {
    if (om != null) {
      om.stop();
    }
    zk.unregisterJMX(this);

    if (connectionTime != 0) {
      long connectionDuration = System.currentTimeMillis() - connectionTime;
      LOG.info(
        "Disconnected from leader (with address: {}). Was connected for {}ms. Sync state: {}",
        leaderAddr,
        connectionDuration,
        completedSync);
      messageTracker.dumpToLog(leaderAddr.toString());
    }
  }
}
```

上面源码中的 follower.followLeader() 方法主要做了如下几件事：

1. 寻找Leader
2. 和Leader创建链接
3. 向Leader注册Follower，会将当前Follower节点信息发送给Leader节点
4. 和Leader同步历史数据
5. 读取Leader发送的数据包
6. 同步Leader数据包

我们对 follower.followLeader() 调用的其他方法进行剖析，其中 findLeader() 是寻找当前Leader节点的，源代码如下：

```java
protected QuorumServer findLeader() {
  QuorumServer leaderServer = null;
  // Find the leader by id
  Vote current = self.getCurrentVote();
  // 这里就是循环找到了Leader节点信息
  for (QuorumServer s : self.getView().values()) {
    if (s.id == current.getId()) {
      // Ensure we have the leader's correct IP address before
      // attempting to connect.
      s.recreateSocketAddresses();
      leaderServer = s;
      break;
    }
  }
  if (leaderServer == null) {
    LOG.warn("Couldn't find the leader with id = {}", current.getId());
  }
  return leaderServer;
}
```

followLeader() 中调用了 registerWithLeader(Leader.FOLLOWERINFO); 该方法是向Leader注册Follower，会将当前Follower节点信息发送给Leader节点，Follower节点信息发给Leader是必须的，是Leader同步数据个基础，源码如下：

```java
/**
     * Once connected to the leader or learner master, perform the handshake
     * protocol to establish a following / observing connection.
     * @param pktType
     * @return the zxid the Leader sends for synchronization purposes.
     * @throws IOException
     */
protected long registerWithLeader(int pktType) throws IOException {
  /*
   * Send follower info, including last zxid and sid(服务标识，myid)
   * 发送 follower info 信息，包括 last zxid 和 sid
   */
  long lastLoggedZxid = self.getLastLoggedZxid();
  QuorumPacket qp = new QuorumPacket();
  qp.setType(pktType);
  qp.setZxid(ZxidUtils.makeZxid(self.getAcceptedEpoch(), 0));

  /*
   * Add sid to payload
   */
  LearnerInfo li = new LearnerInfo(self.getId(), 0x10000, self.getQuorumVerifier().getVersion());
  ByteArrayOutputStream bsid = new ByteArrayOutputStream();
  BinaryOutputArchive boa = BinaryOutputArchive.getArchive(bsid);
  //发送 follower info 信息，包括 last zxid 和 sid
  boa.writeRecord(li, "LearnerInfo");
  qp.setData(bsid.toByteArray());

  //follower 向 leader 发送 FOLLOWERINFO 信息，包括 zxid，sid，protocol version
  writePacket(qp, true);
  readPacket(qp);
  final long newEpoch = ZxidUtils.getEpochFromZxid(qp.getZxid());
  if (qp.getType() == Leader.LEADERINFO) {
    // we are connected to a 1.0 server so accept the new epoch and read the next packet
    leaderProtocolVersion = ByteBuffer.wrap(qp.getData()).getInt();
    byte[] epochBytes = new byte[4];
    final ByteBuffer wrappedEpochBytes = ByteBuffer.wrap(epochBytes);
    if (newEpoch > self.getAcceptedEpoch()) {
      wrappedEpochBytes.putInt((int) self.getCurrentEpoch());
      self.setAcceptedEpoch(newEpoch);
    } else if (newEpoch == self.getAcceptedEpoch()) {
      // since we have already acked an epoch equal to the leaders, we cannot ack
      // again, but we still need to send our lastZxid to the leader so that we can
      // sync with it if it does assume leadership of the epoch.
      // the -1 indicates that this reply should not count as an ack for the new epoch
      wrappedEpochBytes.putInt(-1);
    } else {
      throw new IOException("Leaders epoch, "
                            + newEpoch
                            + " is less than accepted epoch, "
                            + self.getAcceptedEpoch());
    }
    //向Leader写ackNewEpoch
    QuorumPacket ackNewEpoch = new QuorumPacket(Leader.ACKEPOCH, lastLoggedZxid, epochBytes, null);
    writePacket(ackNewEpoch, true);
    return ZxidUtils.makeZxid(newEpoch, 0);
  } else {
    if (newEpoch > self.getAcceptedEpoch()) {
      self.setAcceptedEpoch(newEpoch);
    }
    if (qp.getType() != Leader.NEWLEADER) {
      LOG.error("First packet should have been NEWLEADER");
      throw new IOException("First packet should have been NEWLEADER");
    }
    return qp.getZxid();
  }
}
```

followLeader() 中最后读取数据包执行同步的方法中调用了 readPacket(qp); ，这个方法就是读取Leader的数据包的封装，源码如下：

```java
/**
 * 从Leader读取数据包
 */
void readPacket(QuorumPacket pp) throws IOException {
  synchronized (leaderIs) {
    leaderIs.readRecord(pp, "packet");
    messageTracker.trackReceived(pp.getType());
  }
  if (LOG.isTraceEnabled()) {
    final long traceMask =
      (pp.getType() == Leader.PING) ? ZooTrace.SERVER_PING_TRACE_MASK
      : ZooTrace.SERVER_PACKET_TRACE_MASK;

    ZooTrace.logQuorumPacket(LOG, traceMask, 'i', pp);
  }
}
```



## 4.3 Zookeeper Leader同步流程

我们查看 QuorumPeer.run() 方法的LEADING部分，可以看到先创建了Leader对象，并设置了Leader，然后调用了 leader.lead() ， leader.lead() 是执行的核心业务流程，源码如下：

```java
case LEADING:
// Leader需要执行的逻辑
try {
  // 创建Leader对象，并且设置Leader
  setLeader(makeLeader(logFactory));
  /*
  * 这里会进行一个死循环，主要做的逻辑有
  * 1、接受Follower的连接
  * 2、进行Follower进行数据同步
  * 3、同步完成后，正常接收请求（主要包括客户端发来的请求、
  * 集群Follower转发的事务请求等等）
  */
  leader.lead();
  // 如果集群超过半数服务宕机，那么首先设置Leader对象为null
  setLeader(null);
} catch (Exception e) {
  LOG.warn("Unexpected exception", e);
} finally {
  if (leader != null) {
    leader.shutdown("Forcing shutdown");
    setLeader(null);
  }
  // 设置zk状态为LOOKING，需要重新触发选举
  updateServerState();
}
break;
```

leader.lead() 方法是Leader执行的核心业务流程，源码如下：

```java
/**
 * Leader主要功能的方法
 */
void lead() throws IOException, InterruptedException {
  self.end_fle = Time.currentElapsedTime();
  long electionTimeTaken = self.end_fle - self.start_fle;
  self.setElectionTimeTaken(electionTimeTaken);
  ServerMetrics.getMetrics().ELECTION_TIME.add(electionTimeTaken);
  LOG.info("LEADING - LEADER ELECTION TOOK - {} {}", electionTimeTaken, QuorumPeer.FLE_TIME_UNIT);
  self.start_fle = 0;
  self.end_fle = 0;

  zk.registerJMX(new LeaderBean(this, zk), self.jmxLocalPeerBean);

  try {
    self.setZabState(QuorumPeer.ZabState.DISCOVERY);
    self.tick.set(0);
    //从快照和事务日志中加载数据
    zk.loadData();

    leaderStateSummary = new StateSummary(self.getCurrentEpoch(), zk.getLastProcessedZxid());

    // 创建一个线程，接收Follower/Observer的连接
    cnxAcceptor = new LearnerCnxAcceptor();
    // 开启线程
    cnxAcceptor.start();

    /***
     * 等待超过一半的(Follower和Observer)连接，这里才会往下执行，返回新的epoch
     */
    long epoch = getEpochToPropose(self.getId(), self.getAcceptedEpoch());

    /**
     * 把epoch转成zxid设置到zk
     */
    zk.setZxid(ZxidUtils.makeZxid(epoch, 0));

    synchronized (this) {
      lastProposed = zk.getZxid();
    }

    /***
     * 新的提案，数据包封装
     */
    newLeaderProposal.packet = new QuorumPacket(NEWLEADER, zk.getZxid(), null, null);

    if ((newLeaderProposal.packet.getZxid() & 0xffffffffL) != 0) {
      LOG.info("NEWLEADER proposal has Zxid of {}", Long.toHexString(newLeaderProposal.packet.getZxid()));
    }

    QuorumVerifier lastSeenQV = self.getLastSeenQuorumVerifier();
    QuorumVerifier curQV = self.getQuorumVerifier();
    if (curQV.getVersion() == 0 && curQV.getVersion() == lastSeenQV.getVersion()) {
      // This was added in ZOOKEEPER-1783. The initial config has version 0 (not explicitly
      // specified by the user; the lack of version in a config file is interpreted as version=0).
      // As soon as a config is established we would like to increase its version so that it
      // takes presedence over other initial configs that were not established (such as a config
      // of a server trying to join the ensemble, which may be a partial view of the system, not the full config).
      // We chose to set the new version to the one of the NEWLEADER message. However, before we can do that
      // there must be agreement on the new version, so we can only change the version when sending/receiving UPTODATE,
      // not when sending/receiving NEWLEADER. In other words, we can't change curQV here since its the committed quorum verifier,
      // and there's still no agreement on the new version that we'd like to use. Instead, we use
      // lastSeenQuorumVerifier which is being sent with NEWLEADER message
      // so its a good way to let followers know about the new version. (The original reason for sending
      // lastSeenQuorumVerifier with NEWLEADER is so that the leader completes any potentially uncommitted reconfigs
      // that it finds before starting to propose operations. Here we're reusing the same code path for
      // reaching consensus on the new version number.)

      // It is important that this is done before the leader executes waitForEpochAck,
      // so before LearnerHandlers return from their waitForEpochAck
      // hence before they construct the NEWLEADER message containing
      // the last-seen-quorumverifier of the leader, which we change below
      try {
        LOG.debug(String.format("set lastSeenQuorumVerifier to currentQuorumVerifier (%s)", curQV.toString()));
        QuorumVerifier newQV = self.configFromString(curQV.toString());
        newQV.setVersion(zk.getZxid());
        self.setLastSeenQuorumVerifier(newQV, true);
      } catch (Exception e) {
        throw new IOException(e);
      }
    }

    newLeaderProposal.addQuorumVerifier(self.getQuorumVerifier());
    if (self.getLastSeenQuorumVerifier().getVersion() > self.getQuorumVerifier().getVersion()) {
      newLeaderProposal.addQuorumVerifier(self.getLastSeenQuorumVerifier());
    }

    // 等待超过一半的(Follower和Observer)获取了新的epoch，并且返回了Leader.ACKEPOCH
    // 这里才会往下执行。 反之，阻塞在这里
    // 阻塞等待接收过半节点(Follower和Observer)发送的ACKEPOCH信息；
    //此时说明已经确定了本轮选举后epoch值
    waitForEpochAck(self.getId(), leaderStateSummary);
    // 设置当前新的epoch
    self.setCurrentEpoch(epoch);
    self.setLeaderAddressAndId(self.getQuorumAddress(), self.getId());
    self.setZabState(QuorumPeer.ZabState.SYNCHRONIZATION);

    try {
      // 阻塞等待 超过半数的节点(Follower和Observer)发送了 NEWLEADER ACK 信息；
      //此时说明过半的 follower 节点已经完成数据同步
      waitForNewLeaderAck(self.getId(), zk.getZxid());
    } catch (InterruptedException e) {
      shutdown("Waiting for a quorum of followers, only synced with sids: [ "
               + newLeaderProposal.ackSetsToString()
               + " ]");
      HashSet<Long> followerSet = new HashSet<Long>();

      for (LearnerHandler f : getLearners()) {
        if (self.getQuorumVerifier().getVotingMembers().containsKey(f.getSid())) {
          followerSet.add(f.getSid());
        }
      }
      boolean initTicksShouldBeIncreased = true;
      for (Proposal.QuorumVerifierAcksetPair qvAckset : newLeaderProposal.qvAcksetPairs) {
        if (!qvAckset.getQuorumVerifier().containsQuorum(followerSet)) {
          initTicksShouldBeIncreased = false;
          break;
        }
      }
      if (initTicksShouldBeIncreased) {
        LOG.warn("Enough followers present. Perhaps the initTicks need to be increased.");
      }
      return;
    }
    // 走到这里说明集群中数据已经同步完成，可以正常运行
    // 开启zkServer，并且同时开启请求调用链接收请求执行
    startZkServer();

    /**
     * WARNING: do not use this for anything other than QA testing
     * on a real cluster. Specifically to enable verification that quorum
     * can handle the lower 32bit roll-over issue identified in
     * ZOOKEEPER-1277. Without this option it would take a very long
     * time (on order of a month say) to see the 4 billion writes
     * necessary to cause the roll-over to occur.
     *
     * This field allows you to override the zxid of the server. Typically
     * you'll want to set it to something like 0xfffffff0 and then
     * start the quorum, run some operations and see the re-election.
     */
    String initialZxid = System.getProperty("zookeeper.testingonly.initialZxid");
    if (initialZxid != null) {
      long zxid = Long.parseLong(initialZxid);
      zk.setZxid((zk.getZxid() & 0xffffffff00000000L) | zxid);
    }

    if (!System.getProperty("zookeeper.leaderServes", "yes").equals("no")) {
      self.setZooKeeperServer(zk);
    }

    self.setZabState(QuorumPeer.ZabState.BROADCAST);
    self.adminServer.setZooKeeperServer(zk);

    // We ping twice a tick, so we only update the tick every other
    // iteration
    boolean tickSkip = true;
    // If not null then shutdown this leader
    String shutdownMessage = null;

    // 进行一个死循环，每次休眠self.tickTime / 2，和对所有的(Observer/Follower)发起心跳检测
    while (true) {
      synchronized (this) {
        long start = Time.currentElapsedTime();
        long cur = start;
        long end = start + self.tickTime / 2;
        while (cur < end) {
          wait(end - cur);
          cur = Time.currentElapsedTime();
        }

        if (!tickSkip) {
          self.tick.incrementAndGet();
        }

        // We use an instance of SyncedLearnerTracker to
        // track synced learners to make sure we still have a
        // quorum of current (and potentially next pending) view.
        SyncedLearnerTracker syncedAckSet = new SyncedLearnerTracker();
        syncedAckSet.addQuorumVerifier(self.getQuorumVerifier());
        if (self.getLastSeenQuorumVerifier() != null
            && self.getLastSeenQuorumVerifier().getVersion() > self.getQuorumVerifier().getVersion()) {
          syncedAckSet.addQuorumVerifier(self.getLastSeenQuorumVerifier());
        }
        // 将Follower加入该容器
        syncedAckSet.addAck(self.getId());

        for (LearnerHandler f : getLearners()) {
          if (f.synced()) {
            syncedAckSet.addAck(f.getSid());
          }
        }

        // check leader running status
        if (!this.isRunning()) {
          // set shutdown flag
          shutdownMessage = "Unexpected internal error";
          break;
        }

        // 判断是否有超过一半Follower在集群中
        if (!tickSkip && !syncedAckSet.hasAllQuorums()) {
          // Lost quorum of last committed and/or last proposed
          // config, set shutdown flag
          // 如果没有，那么调用shutdown关闭一些对象，然后return，重新选举
          shutdownMessage = "Not sufficient followers synced, only synced with sids: [ "
            + syncedAckSet.ackSetsToString()
            + " ]";
          break;
        }
        tickSkip = !tickSkip;
      }
      for (LearnerHandler f : getLearners()) {
        f.ping();
      }
    }
    if (shutdownMessage != null) {
      // 没有过半Follower在集群中，调用shutdown关闭一些对象，重新选举
      shutdown(shutdownMessage);
      // leader goes in looking state
    }
  } finally {
    zk.unregisterJMX(this);
  }
}
```

leader.lead() 方法会执行如下几个操作:

1. 从快照和事务日志中加载数据
2. 创建一个线程，接收Follower/Observer的连接
3. 等待超过一半的(Follower和Observer)连接，再继续往下执行程序
4. 等待超过一半的(Follower和Observer)获取了新的epoch，并且返回了Leader.ACKEPOCH，再继续往下执行程序
5. 等待超过一半的(Follower和Observer)进行数据同步成功，并且返回了Leader.ACK，再继续往下执行程序
6. 数据同步完成，开启zkServer，并且同时开启请求调用链接收请求执行
7. 进行一个死循环，每次休眠self.tickTime / 2，和对所有的(Observer/Follower)发起心跳检测
8. 集群中没有过半Follower在集群中，调用shutdown关闭一些对象，重新选举

lead() 方法中会创建 LearnerCnxAcceptor ，该对象是一个线程，主要用于接收followers的连接，这里加了CountDownLatch根据配置的同步的地址的数量（例如：server.2=127.0.0.1:12881:13881 配置同步的端口是12881只有一个）， LearnerCnxAcceptor 的run方法源码如下：

```java
@Override
public void run() {
  if (!stop.get() && !serverSockets.isEmpty()) {
    ExecutorService executor = Executors.newFixedThreadPool(serverSockets.size());
    CountDownLatch latch = new CountDownLatch(serverSockets.size());

    //创建socket链接，并等待Follower节点链接
    serverSockets.forEach(serverSocket ->
                          executor.submit(new LearnerCnxAcceptorHandler(serverSocket, latch)));

    try {
      latch.await();
    } catch (InterruptedException ie) {
      LOG.error("Interrupted while sleeping in LearnerCnxAcceptor.", ie);
    } finally {
      closeSockets();
      executor.shutdown();
      try {
        if (!executor.awaitTermination(1, TimeUnit.SECONDS)) {
          LOG.error("not all the LearnerCnxAcceptorHandler terminated properly");
        }
      } catch (InterruptedException ie) {
        LOG.error("Interrupted while terminating LearnerCnxAcceptor.", ie);
      }
    }
  }
}

```

LearnerCnxAcceptor 的run方法中创建了 LearnerCnxAcceptorHandler 对象，在接收到链接后，就会调用 LearnerCnxAcceptorHandler ，而 LearnerCnxAcceptorHandler 是一个线程，它的run方法中调用了 acceptConnections() 方法，源码如下：

![image-20211108215918383](10_zookeeper源码02.assets/image-20211108215918383.png)

acceptConnections() 方法会在这里阻塞接收followers的连接，当有连接过来会生成一个socket对象。然后根据当前socket生成一个LearnerHandler线程 ，每个Learner者都会开启一个LearnerHandler线程，方法源码如下

```java
private void acceptConnections() throws IOException {
  Socket socket = null;
  boolean error = false;
  try {
    //接收请求
    socket = serverSocket.accept();

    // start with the initLimit, once the ack is processed
    // in LearnerHandler switch to the syncLimit
    socket.setSoTimeout(self.tickTime * self.initLimit);
    socket.setTcpNoDelay(nodelay);
    //获取数据流
    BufferedInputStream is = new BufferedInputStream(socket.getInputStream());
    //创建LearnerHandler线程
    LearnerHandler fh = new LearnerHandler(socket, is, Leader.this);
    fh.start();
  } catch (SocketException e) {
    error = true;
    if (stop.get()) {
      LOG.warn("Exception while shutting down acceptor.", e);
    } else {
      throw e;
    }
  } catch (SaslException e) {
    LOG.error("Exception while connecting to quorum learner", e);
    error = true;
  } catch (Exception e) {
    error = true;
    throw e;
  } finally {
    // Don't leak sockets on errors
    if (error && socket != null && !socket.isClosed()) {
      try {
        socket.close();
      } catch (IOException e) {
        LOG.warn("Error closing socket: " + socket, e);
      }
    }
  }
}
```

LearnerHandler.run 这里就是读取或写数据包与Learner交换数据包。如果没有数据包读取，则会阻塞当前方法 ia.readRecord(qp, "packet"); ，源码如下：

![image-20211108220247956](10_zookeeper源码02.assets/image-20211108220247956.png)

我们再回到 leader.lead() 方法，其中调用了 getEpochToPropose() 方法，该方法是判断connectingFollowers发给leader端的Epoch是否过半，如果过半则会解阻塞，不过半会一直阻塞着，直到Follower把自己的Epoch数据包发送过来并符合过半机制，源码如下：

![image-20211108220314699](10_zookeeper源码02.assets/image-20211108220314699.png)

在 lead() 方法中，当发送的Epoch过半之后，把当前zxid设置到zk，并等待EpochAck，关键源码如下：

![image-20211108220334456](10_zookeeper源码02.assets/image-20211108220334456.png)

waitForEpochAck() 方法也会等待超过一半的(Follower和Observer)获取了新的epoch，并且返回了Leader.ACKEPOCH，才会解除阻塞，否则会一直阻塞。等待EpochAck解阻塞后，把得到最新的epoch更新到当前服务，设置当前leader节点的zab状态是 SYNCHRONIZATION ，方法源码如下：

![image-20211108220415808](10_zookeeper源码02.assets/image-20211108220415808.png)

lead() 方法中还需要等待超过一半的(Follower和Observer)进行数据同步成功，并且返回了Leader.ACK，程序才会解除阻塞，如下代码：

![image-20211108220445024](10_zookeeper源码02.assets/image-20211108220445024.png)

## 4.4 LearnerHandler数据同步操作

LearnerHandler 线程是对应于 Learner 连接 Leader 端后，建立的一个与 Learner 端交换数据的线程。每一个 Learner 端都会创建一个 LearnerHandler 线程。

我们详细讲解 LearnerHandler.run() 方法。

![image-20211108220523329](10_zookeeper源码02.assets/image-20211108220523329.png)

readRecord 读取数据包 不断从 learner 节点读数据，如果没读到将会阻塞 readRecord 。

![image-20211108220543790](10_zookeeper源码02.assets/image-20211108220543790.png)

如果数据包类型不是Leader.FOLLOWERINFO或Leader.OBSERVERINFO将会返回，因为咱们这里本身就是Leader节点，读数据肯定是读非Leader节点数据。

![image-20211108220603680](10_zookeeper源码02.assets/image-20211108220603680.png)

获取 learnerInfoData 来获取sid和版本信息。

![image-20211108220617026](10_zookeeper源码02.assets/image-20211108220617026.png)

获取followerInfo和lastAcceptedEpoch，信息如下：

![image-20211108220632551](10_zookeeper源码02.assets/image-20211108220632551.png)

![image-20211108220643870](10_zookeeper源码02.assets/image-20211108220643870.png)

把Leader.NEWLEADER数据包放入到queuedPackets，并向其他节点发送，源码如下：

![image-20211108220702481](10_zookeeper源码02.assets/image-20211108220702481.png)

# 5.Zookeeper集群选举与同步总结

## 5.1 选举

本质都是由QuorumPeer的run() 方法循环去做的。

如果判断当前节点是 LOOKING，则会进入选举流程，进入 FastLeaderElection的lookForLeader() 方法， 循环从recvqueue中拿别的节点的投票数据，PK，统计过半数的票。

其中PK算法如下：

```java
protected boolean totalOrderPredicate(
    long newId,     // 别的节点传过来要和我本地节点PK的节点的id，对应的myid的值
    long newZxid,   // 别的节点传过来要和我本地节点PK的事务ID值，zxid，zxid越大说明说句越新
    long newEpoch, //  别的节点传过来要和我本地节点PK的投票轮次，投票轮次越大，说明投票次数越多，投票结果越新
    long curId,    // 本地投票的节点的id，对应的myid的值
    long curZxid,  // 本地投票的节点的事务ID值，zxid，zxid越大说明说句越新
    long curEpoch) {//本地投票的节的投票轮次，投票轮次越大，说明投票次数越多，投票结果越新
    if (self.getQuorumVerifier().getWeight(newId) == 0) {
        return false;
    }

    return ((newEpoch > curEpoch)
            || (
                (newEpoch == curEpoch) && ((newZxid > curZxid)
                                           ||
                                           ((newZxid == curZxid) && (newId > curId)))
            )
           );
```



## 5.2 同步

同样还是QuorumPeer的run() 方法循环去做的。

### 5.2.1 如果发现当前节点是LEADING

进入leader.lead();方法，创建LearnerCnxAcceptor 对象，

LearnerCnxAcceptor对象是个线程对象，里面又创建了线程池，由线程池去执行 **LearnerCnxAcceptorHandler**中的run方法，**与别的节点建立链接**，而后由LearnerHandler线程对象进行数据同步通信。

Leader节点通信具体(LearnerHandler.java)，就是给所有Follower节点同步：

1. 计算epoch
2. 同步epoch
3. 同步数据
   1. Leader本地数据同步
   2. 增量数据同步

### 5.2.2 如果发现当前节点是FOLLOWING

进入follower.followLeader();方法，里面直接就是个死循环，做了如下事：

1. registerWithLeader()向Leader注册数据，获取newEpochZxid
2. 创建和Leader的连接
3. 第一次同步数据，保障初试数据一致
4. 循环获取（同步）增量数据



Follow只负责读数据，接收到写事件传给Leader

































  