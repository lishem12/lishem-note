# 1.Netty介绍

## 1.1 Netty简介

Java、开源、基于NIO、网络编程框架

官网： https://netty.io/  

### 1.1.1 异步、事件驱动

在请求执行过程中，如果会阻塞等待，就是同步操作，反之就是异步操作

![image-20211017185941297](04_netty基础组件.assets/image-20211017185941297.png)

### 1.1.2 核心架构

![image-20211017190135145](04_netty基础组件.assets/image-20211017190135145.png)

- 核心

  - 可拓展的事件模型

  - 统一的通讯API

    无论是http还是socket都是用了统一API，简化了操作

  - 零拷贝机制和字节缓冲区

- 传输服务

  - 支持socket和datagram
  - 支持http协议
  - 支持In-VM Pipe（管道协议）

- 协议支持

  - http和websocket
  - SSL安全套接字
  - ...

### 1.1.3 Netty优势

- Netty是基于Java的NIO实现的，Netty将各种传输类型、协议的实现API进行了统一封装，实现了阻塞和非阻塞Socket。
- 基于事件模型实现，可以清晰的分离关注点，让开发者可以聚焦业务，提升开发效率。
- 高度可定制的线程模型-单线程、一个或多个线程池，如SEDA（Staged Event-DrivenArchitecture）：
  - SEDA：把一个请求处理过程分成几个Stage，不同资源消耗的Stage使用不同数量的线程来
    处理，Stage期间使用事件驱动的异步通信模式。
- Netty只依赖了JDK底层api，没有其他的依赖，如：Netty 3.X依赖JDK5以上，Netty4.x依赖JDK6以上。
- Netty在网络通信方面更加的高性能、低延迟，尽可能的减少不必要的内存拷贝，提高性能。  

## 1.2 Netty应用场景

Dubbo、Flink、Spark，Elasticsearch 底层

## 1.3 Netty VS NIO

- NIO的类库和API繁杂，使用麻烦，需要熟练掌握Selector、ServerSocketChannel、SocketChannel、ByteBuffer等。
- 需要具备其他的额外技能做铺垫，例如熟悉Java多线程编程。这是因为NIO编程涉及到Reactor模式，你必须对多线程和网路编程非常熟悉，才能编写出高质量的NIO程序。
- 可靠性能力补齐，工作量和难度都非常大。例如客户端面临断连重连、网络闪断、半包读写、失败缓存、网络拥塞和异常码流的处理等问题，NIO编程的特点是功能开发相对容易，但是可靠性能力补齐的工作量和难度都非常大。
- JDK NIO的BUG，例如臭名昭著的epoll bug，它会导致Selector空轮询，最终导致CPU 100%。官方声称在JDK 1.6版本的update18修复了该问题，但是直到JDK 1.7版本该问题仍旧存在，只不过该BUG发生概率降低了一些而已，它并没有得到根本性解决。  

# 2.Netty入门案例

## 2.1 创建项目  

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>itcast-myrpc</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
        <!-- Netty 通讯依赖组件 -->
        <dependency>
            <groupId>io.netty</groupId>
            <artifactId>netty-all</artifactId>
            <version>4.1.50.Final</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- java编译插件 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.2</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
```

## 2.2 Netty服务端

### 2.2.1 Netty服务端启动配置

实现流程：

1. 主线程配置，不处理任何业务逻辑，只是接收客户的连接请求。
2. 工作线程配置，线程数默认是：cpu*2。
3. 创建服务器启动类。
4. 服务端配置： 设置线程组、配置server通道、worker线程的处理器。
5. 同步阻塞方式启动服务端， 防止自动关闭。
6. Netty优雅关闭。  

```java
public class MyRPCServer {

    /***
     * 开启Netty服务
     * @param port
     */
    public void start(int port){
        //：创建1个线程boss获取客户端链接
        EventLoopGroup boss = new NioEventLoopGroup(1);
        //：处理业务->线程->worker
        EventLoopGroup worker = new NioEventLoopGroup();

        try {
            //：创建服务端对象->ServerBootstrap
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.childOption(ChannelOption.ALLOCATOR, UnpooledByteBufAllocator.DEFAULT);
            //：给服务端对象绑定端口
            ChannelFuture future =
                    serverBootstrap
                    //boss处理当前链接对象
                    //worker处理业务
                    .group(boss, worker)
                    //数据怎么来？什么协议？TCP
                    .channel(NioServerSocketChannel.class)  //TCP协议通道
                    //怎么处理业务？Handler
                    // 如果是单一业务，继承ChannelInboundHandlerAdapter处理
                    // 如果是一个业务处理链，就继承ChannelInitializer，
                    // 就创建ChannelHandler对象，将业务处理对象添加到Pipeline中
                    //.childHandler(new MyChannelHandler()) //处理对应的业务->数据入站
                    .childHandler(new MyChannelInitializer()) //处理对应的业务->数据入站
                    .bind(port).sync();//监听指定端口并且启动服务

            //如果future获取到当应用对应的状态是否关闭
            future.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            //关闭线程池
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }
}
```

### 2.2.2 Netty通道初始化  

MyChannelInitializer通道的初始化管理器：  

```java
public class MyChannelInitializer extends ChannelInitializer<SocketChannel> {

    /***
     * 多个业务链添加
     * @param ch
     * @throws Exception
     */
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new MyChannelHandler());
    }
}
```

### 2.2.3 Netty通道接收处理器  

MyChannelHandler连接通道接收处理器：  

```java
public class MyChannelHandler extends ChannelInboundHandlerAdapter {

    /***
     * 读取客户端传入的数据
     * @param ctx:上下文对象
     * @param msg:客户端传入的数据
     * @throws Exception
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf byteBuf = (ByteBuf) msg;
        String content = byteBuf.toString(CharsetUtil.UTF_8);
        System.out.println("客户端数据："+content);

        //响应数据给客户端--->出站
        ctx.writeAndFlush(Unpooled.copiedBuffer("Success!",CharsetUtil.UTF_8));
        //手动释放数据
        //ReferenceCountUtil.release(byteBuf);
        //需要回收的资源下沉
        ctx.fireChannelRead(msg);
    }

    /***
     * 异常处理
     * @param ctx
     * @param cause
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
```

## 2.3 Netty客户端

### 2.3.1 Netty客户端启动配置

实现流程：

1. 定义工作线程组
2. 通过Bootstrap方式创建客户端
3. 连接到远程服务
4. 同步阻塞方式启动服务  

```java
public class MyClientServer {

    /***
     * 启动客户端
     * @param host:要访问要链接的服务端IP
     * @param port:要访问要链接的服务端Port
     */
    public void start(String host,int port){
        //4:向远程服务发送数据->线程
        EventLoopGroup worker = new NioEventLoopGroup();

        try {
            //1:创建启动客户端对象
            Bootstrap bootstrap = new Bootstrap();
            ChannelFuture future =
                bootstrap
                .group(worker)
                //TCP请求对应的Channel
                .channel(NioSocketChannel.class)
                //业务处理
                .handler(new MyClientChannelHandler())
                //2:为启动对象配置要访问的服务IP、Port,建立远程连接
                .connect(host, port).sync();
            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            worker.shutdownGracefully();
        }
    }
}
```



### 2.3.2 Netty通道接收处理器

```java
public class MyClientChannelHandler extends SimpleChannelInboundHandler<ByteBuf> {

    /***
     * 获取响应数据
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        System.out.println("来自服务端的数据:"+msg.toString(CharsetUtil.UTF_8));
    }

    /***
     * 向服务端发送数据
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //ByteBuf->数据需要手动清理、手动释放，否则会积压到内存中，最终会引发内存溢出
        ctx.writeAndFlush(Unpooled.copiedBuffer("beijing!".getBytes(CharsetUtil.UTF_8)));
    }

    /***
     * 异常处理
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        ctx.close();
    }
}
```

### 2.3.3 测试

```java
public class RPCServerTest {
    /***
     * Netty服务端启动
     */
    @Test
    public void testMyRPCServerStart(){
        System.setProperty("io.netty.noUnsafe", "true");
        MyRPCServer rpcServer = new MyRPCServer();
        rpcServer.start(5566);
    }

    /****
     * Netty客户端
     */
    @Test
    public void testMyClientServerStart(){
        MyClientServer myClientServer = new MyClientServer();
        myClientServer.start("127.0.0.1",5566);
    }
}
```

先运行服务端，再运行客户端

![image-20211017194215148](04_netty基础组件.assets/image-20211017194215148.png)

![image-20211017194221894](04_netty基础组件.assets/image-20211017194221894.png)

# 3.Netty高性能架构设计

NIO、Reactor 线程模型

## 3.1 Java中的IO模型

### 3.1.1 BIO

这种模式存在的问题：  

- 客户端的并发数与后端的线程数成1:1的比例，线程的创建、销毁是非常消耗系统资源的，随着并发量增大，服务端性能将显著下降，甚至会发生线程堆栈溢出等错误。
- 当连接创建后，如果该线程没有操作时，会进行阻塞操作，这样极大的浪费了服务器资源  

```java
public class BIOServer {

    public static void main(String[] args) throws IOException {
        //创佳ServerSocket
        ServerSocket serverSocket = new ServerSocket(6666);
        //创建线程池
        ExecutorService executorService = Executors.newCachedThreadPool();

        //循环处理链接对象
        while (true){
            System.out.println("阻塞等待客户端链接！");
            Socket socket = serverSocket.accept();

            //读取数据
            executorService.execute(()->{
                try {
                    //获取数据流
                    InputStream is = socket.getInputStream();
                    //定义缓冲区
                    byte[] buffer = new byte[1024];
                    //循环读取数据
                    int len = 0;
                    while ((len=is.read(buffer))!=-1){
                        System.out.println(new String(buffer,0,len,"UTF-8"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }
}
```

### 3.1.2 NIO

NIO相关的代码都放在了java.nio包下，其三大核心组件：Buffer（缓冲区）、Channel（通道）、Selector（选择器/多路复用器）

**Buffer**

在NIO中，所有的读写操作都是基于缓冲区完成的，底层是通过数组实现的，常用的缓冲区是ByteBuffer，每一种java基本类型都有对应的缓冲区对象（除了Boolean类型），如：CharBuffer、IntBuffer、LongBuffer等。

**Channel**

在BIO中是基于Stream实现，而在NIO中是基于通道实现，与流不同的是，通道是双向的，既可以读也可以写。

**Selector**

Selector是多路复用器，它会不断的轮询注册在其上的Channel，如果某个Channel上发生读或写事件，这个Channel就处于就绪状态，会被Selector轮询出来，然后通过SelectionKey获取就绪Channel的集合，进行IO的读写操作。

基本示意图如下：

![image-20211017202451657](04_netty基础组件.assets/image-20211017202451657.png)

可以看出，NIO模型要优于BIO模型，主要是：  

- 通过多路复用器就可以实现一个线程处理多个通道，避免了多线程之间的上下文切换导致系统开销过大。
- NIO无需为每一个连接开一个线程处理，并且只有通道真正有有事件时，才进行读写操作，这样大大的减少了系统开销  

```java
public class NIOServer {

    /***
     * 向Selector注册Channel
     */
    public Selector getSelector() throws Exception{
        //创建Selector
        Selector selector = Selector.open();

        //创建Channel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);//非阻塞

        //创建Socket，并绑定指定端口
        ServerSocket socket = serverSocketChannel.socket();
        socket.bind(new InetSocketAddress(6677));

        //向Selector注册Channel,事件为连接准备就绪事件
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        return selector;
    }


    /***
     * 循环处理每个Channel事件
     */
    public void listen() throws Exception {
        Selector selector = getSelector();
        //循环处理每个Channel
        while (true){
            //该方法会阻塞，直到至少有一个准备就绪的事件发生
            selector.select();
            //获取所有准备就绪的事件
            Set<SelectionKey> selectionKeys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = selectionKeys.iterator();
            while (iterator.hasNext()){
                //获取准备就绪的事件对象
                SelectionKey selectionKey = iterator.next();
                //处理对应的事件信息
                process(selectionKey,selector);
                //从Selector中移除该Channel
                iterator.remove();
            }
        }
    }

    /***
     * 数据处理
     */
    public void process(SelectionKey key, Selector selector) throws IOException {
        //判断事件,并做相关处理
        if(key.isAcceptable()){
            //连接准备就绪事件
            ServerSocketChannel server = (ServerSocketChannel) key.channel();
            SocketChannel channel = server.accept();
            //向Selector注册读事件
            channel.configureBlocking(false);
            channel.register(selector,SelectionKey.OP_READ);
        }else if(key.isReadable()){
            //读准备就绪事件
            SocketChannel channel = (SocketChannel) key.channel();
            //创建ByteBuffer并且设置最大大小为1024
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            //将Channel中的数据写入到ByteBuffer中
            int len = channel.read(byteBuffer);
            while (len != -1) {
                //重置读的位置
                byteBuffer.flip();
                while(byteBuffer.hasRemaining()){
                    //读数据
                    System.out.print((char) byteBuffer.get());
                }
                //重置ByteBuffer指针数据，准备写入数据到ByteBuffer中
                byteBuffer.clear();
                len = channel.read(byteBuffer);
            }
        }
    }

    public static void main(String[] args) throws Exception {
        new NIOServer().listen();
    }
}
```



### 3.1.3 AIO

在NIO中，Selector多路复用器在做轮询时，如果没有事件发生，也会进行阻塞，如何能把这个阻塞也优化掉呢？那么AIO就在这样的背景下诞生了。

AIO是asynchronous I/O的简称，是异步IO，该异步IO是需要依赖于操作系统底层的异步IO实现。

AIO的基本流程是：用户线程通过系统调用，告知kernel内核启动某个IO操作，用户线程返回。kernel内核在整个IO操作（包括数据准备、数据复制）完成后，通知用户程序，用户执行后续的业务操作。  

- kernel的数据准备

  将数据从网络物理设备（网卡）读取到内核缓冲区。

- kernel的数据复制

  将数据从内核缓冲区拷贝到用户程序空间的缓冲  

![image-20211017202733742](04_netty基础组件.assets/image-20211017202733742.png)

目前AIO模型存在的不足：

- 需要完成事件的注册与传递，这里边需要底层操作系统提供大量的支持，去做大量的工作。
- Windows 系统下通过 IOCP 实现了真正的异步 I/O。但是，就目前的业界形式来说，Windows 系统，很少作为百万级以上或者说高并发应用的服务器操作系统来使用  
- 而在 Linux 系统下，异步IO模型在2.6版本才引入，目前并不完善。所以，这也是在 Linux 下，实现高并发网络编程时都是以 NIO 多路复用模型模式为主。  

## 3.2 Reactor线程模型  

一种并发编程模型

Reactor模型中定义的三种角色：  

- Reactor：负责监听和分配事件，将I/O事件分派给对应的Handler。新的事件包含连接建立就绪、读就绪、写就绪等。
- Acceptor：处理客户端新连接，并分派请求到处理器链中。
- Handler：将自身与事件绑定，执行非阻塞读/写任务，完成channel的读入，完成处理业务逻辑后，负责将结果写出channel。  

常见的Reactor线程模型有三种，如下：  

- Reactor单线程模型
- Reactor多线程模型
- 主从Reactor多线程模型  

### 3.2.1 单Reactor单线程模型  

![image-20211017203130979](04_netty基础组件.assets/image-20211017203130979.png)

说明：

- Reactor充当多路复用器角色，监听多路连接的请求，由单线程完成
- Reactor收到客户端发来的请求时，如果是新建连接通过Acceptor完成，其他的请求由Handler完成。
- Handler完成业务逻辑的处理，基本的流程是：Read --> 业务处理 --> Send 。  

优缺点：

- 优点：
  - 结构简单，由单线程完成，没有多线程、进程通信等问题。
  - 适合用在一些业务逻辑比较简单、对于性能要求不高的应用场景。  
- 缺点
  - 由于是单线程操作，不能充分发挥多核CPU的性能。  
  - 当Reactor线程负载过重之后，处理速度将变慢，这会导致大量客户端连接超时，超时之后往往会进行重发，这更加重Reactor线程的负载，最终会导致大量消息积压和处理超时，成为系统的性能瓶颈  
  - 可靠性差，如果该线程进入死循环或意外终止，就会导致整个通信系统不可用，容易造成单点故障。  

### 3.2.2 单Reactor多线程模型  

![image-20211017203313189](04_netty基础组件.assets/image-20211017203313189.png)

说明：  

- 在Reactor多线程模型**相比较单线程模型**而言，**不同点在于**，Handler不会处理业务逻辑，只是负责响应用户请求，真正的业务逻辑，在另外的线程中完成。
- 这样可以降低Reactor的性能开销，充分利用CPU资源，从而更专注的做事件分发工作了，提升整个应用的吞吐。  

这个模型存在的问题：  

- 多线程数据共享和访问比较复杂。如果子线程完成业务处理后，把结果传递给主线程Reactor进行发送，就会涉及共享数据的互斥和保护机制。  
- Reactor承担所有事件的监听和响应，只在主线程中运行，可能会存在性能问题。例如并发百万客户端连接，或者服务端需要对客户端握手进行安全认证，但是认证本身非常损耗性能。  

为了解决性能问题，产生了第三种主从Reactor多线程模型。  

### 3.2.3 主从Reactor多线程模型  

![image-20211017203455363](04_netty基础组件.assets/image-20211017203455363.png)

在主从模型中，将Reactor分成2部分：  

- MainReactor负责监听server socket，用来处理网络IO连接建立操作，将建立的socketChannel指定注册给SubReactor。
- SubReactor主要完成**和建立起来的socket的数据交互**和事件业务处理操作。  

该模型的优点：  

- 响应快，不必为单个同步事件所阻塞，虽然Reactor本身依然是同步的。
- 可扩展性强，可以方便地通过增加SubReactor实例个数来充分利用CPU资源。
- 可复用性高，Reactor模型本身与具体事件处理逻辑无关，具有很高的复用性  

## 3.3. Netty功能设计  

![image-20211017203646697](04_netty基础组件.assets/image-20211017203646697.png)

Netty 功能特性如下：  

- **传输服务**，支持 BIO 和 NIO。
- **容器集成**，支持 OSGI(开放服务网关协议)、JBossMC、Spring、Guice 容器。
- **协议支持**，HTTP、Protobuf、二进制、文本、WebSocket 等一系列常见协议都支持。还支持通过实行编码解码逻辑来实现自定义协议。
- **Core 核心**，可扩展事件模型、通用通信 API、支持零拷贝的 ByteBuf 缓冲对象。  

## 3.4 Netty模型

Netty模型是基于Reactor模型实现的，对于以上三种模型都有非常好的支持，也非常的灵活，一般情况，在服务端会采用主从架构模型，基本示意图如下  

![image-20211017203948198](04_netty基础组件.assets/image-20211017203948198.png)

- 在Netty模型中，负责处理新连接事件的是BossGroup，负责处理其他事件的是WorkGroup。Group就是线程池的概念。
- NioEventLoop表示一个不断循环的执行处理任务的线程，用于监听绑定在其上的读/写事件。
- 通过Pipeline（管道）执行业务逻辑的处理，Pipeline中会有多个ChannelHandler，真正的业务逻辑是在ChannelHandler中完成的。  

# 4.Netty核心组件剖析

## 4.1. Channel  

Channel可以理解为是socket连接，在客户端与服务端连接的时候就会建立一个Channel，它负责基本的IO操作，比如：bind()、connect()，read()，write() 等。  

主要作用：

1. 通过Channel可获得当前网络连接的通道状态。
2. 通过Channel可获得网络连接的配置参数（缓冲区大小等）。
3. Channel提供异步的网络I/O操作，比如连接的建立、数据的读写、端口的绑定等。  

不同协议、不同的阻塞类型的连接都有不同的 Channel 类型与之对应，常用的 Channel 类型:  

- NioSocketChannel，NIO的客户端 TCP Socket 连接。
- NioServerSocketChannel，NIO的服务器端 TCP Socket 连接。
- NioDatagramChannel， UDP 连接。
- NioSctpChannel，客户端 Sctp 连接。
- NioSctpServerChannel，Sctp 服务器端连接，这些通道涵盖了 UDP 和 TCP 网络 IO 以及文件IO。  

## 4.2. EventLoop、EventLoopGroup

有了 Channel 连接服务，连接之间可以消息流动。如果服务器发出的消息称作“出站”消息，服务器接受的消息称作“入站”消息。那么消息的**“出站”/“入站”**就会产生**事件（Event）**。  

例如：连接已激活；数据读取；用户事件；异常事件；打开链接；关闭链接等等  

有了事件，就需要一个机制去监控和协调事件，这个机制（组件）就是EventLoop。

在 Netty 中每个 **Channel** 都会被分配到一个 **EventLoop**。一个 **EventLoop** 可以服务于多个 **Channel**。

每个 EventLoop 会占用一个 Thread，同时这个 Thread 会处理 EventLoop 上面发生的所有 IO 操作和事件。  

![image-20211017205909993](04_netty基础组件.assets/image-20211017205909993.png)

上图关系为：

- 一个 EventLoopGroup 包含一个或者多个 EventLoop;  
- 一个 EventLoop 在它的生命周期内只和一个 Thread 绑定;  
- 所有由 EventLoop 处理的 I/O 事件都将在它专有的 Thread 上被处理;  
- 一个 Channel 在它的生命周期内只注册于一个 EventLoop;  
- 一个 EventLoop 可能会被分配给一个或多个 Channel  

EventLoopGroup 是用来生成 EventLoop 的，在前面的例子中，第一行代码就是 new NioEventLoopGroup();  

```java
// 主线程，不处理任何业务逻辑，只是接收客户的连接请求
EventLoopGroup boss = new NioEventLoopGroup(1);
// 工作线程，线程数默认是：cpu*2
EventLoopGroup worker = new NioEventLoopGroup();
```

如果没有指定线程数大小，默认线程数为：cpu核数*2，源码如下：  

```java
static {
    DEFAULT_EVENT_LOOP_THREADS = Math.max(1, SystemPropertyUtil.getInt(
        "io.netty.eventLoopThreads", NettyRuntime.availableProcessors() * 2));

    if (logger.isDebugEnabled()) {
        logger.debug("-Dio.netty.eventLoopThreads: {}", DEFAULT_EVENT_LOOP_THREADS);
    }
}

protected MultithreadEventLoopGroup(int nThreads, Executor executor, Object... args) {
    super(nThreads == 0 ? DEFAULT_EVENT_LOOP_THREADS : nThreads, executor, args);
}
```

## 4.3. ChannelHandler  

ChannelHandler对使用者而言，可以说是最重要的组件了，因为对于数据的入站和出站的业务逻辑的编写都是在ChannelHandler中完成的。

在前面的例子中，MyChannelHandler就是实现了channelRead方法，获取到客户端传来的数据。

对于数据的出站和入站，有着不同的ChannelHandler类型与之对应：

- ChannelInboundHandler 入站事件处理器
- ChannelOutBoundHandler 出站事件处理器  

![image-20211017210603509](04_netty基础组件.assets/image-20211017210603509.png)

ChannelHandlerAdapter提供了一些方法的默认实现，可减少用户对于ChannelHandler的编写。  

> ChannelInboundHandlerAdapter 与 SimpleChannelInboundHandler的区别： 
>
> - 在服务端编写ChannelHandler时继承的是ChannelInboundHandlerAdapter
>
> - 在客户端编写ChannelHandler时继承的是SimpleChannelInboundHandler
>
> - 两者的区别在于，前者不会释放消息数据的引用，而后者会释放消息数据的引用  
>
> - ```java
>   // SimpleChannelInboundHandler.java
>   @Override
>   public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
>       boolean release = true;
>       try {
>           if (acceptInboundMessage(msg)) {
>               @SuppressWarnings("unchecked")
>               I imsg = (I) msg;
>               channelRead0(ctx, imsg);
>           } else {
>               release = false;
>               ctx.fireChannelRead(msg);
>           }
>       } finally {
>           if (autoRelease && release) {
>               ReferenceCountUtil.release(msg);
>           }
>       }
>   }
>   ```

## 4.4. ChannelPipeline

在Channel的数据传递过程中，对应着有很多的业务逻辑需要处理，比如：编码解码处理、读写操作等，那么对于每种业务逻辑实现都需要有个ChannelHandler完成，也就意味着，一个**Channel**对应着多个**ChannelHandler**，多个ChannelHandler如何去管理它们，它们的执行顺序又该是怎么样的，这就需要**ChannelPipeline**进行管理了。  

一个Channel包含了一个ChannelPipeline，而ChannelPipeline中维护了一个ChannelHandler的列表。

ChannelHandler与Channel和ChannelPipeline之间的映射关系，由ChannelHandlerContext进行维护。

它们关系如下：  

![image-20211017211224396](04_netty基础组件.assets/image-20211017211224396.png)

ChannelHandler按照加入的顺序会组成一个双向链表，入站事件从链表的head往后传递到最后一个ChannelHandler，出站事件从链表的tail向前传递，直到最后一个ChannelHandler，**两种类型的ChannelHandler相互不会影响**  。

## 4.5. Bootstrap

Bootstrap是引导的意思，它的作用是配置整个Netty程序，将各个组件都串起来，最后绑定端口、启动Netty服务。

Netty中提供了2种类型的引导类，一种用于客户端(Bootstrap)，而另一种(ServerBootstrap)用于服务器。

它们的区别在于：  

- ServerBootstrap 将绑定到一个端口，因为服务器必须要监听连接，而 Bootstrap 则是由想要连接到远程节点的客户端应用程序所使用的。  
- 引导一个客户端只需要一个EventLoopGroup，但是一个ServerBootstrap则需要两个。
  - 因为服务器需要两组不同的 Channel。
  - 第一组将只包含一个 ServerChannel，代表服务器自身的已绑定到某个本地端口的正在监听的套接字。
  - 第二组将包含所有已创建的用来处理传入客户端连接。

![image-20211017211526726](04_netty基础组件.assets/image-20211017211526726.png)

与ServerChannel相关联的EventLoopGroup 将分配一个负责为传入连接请求创建 Channel 的EventLoop。一旦连接被接受，第二个EventLoopGroup就会给它的 Channel 分配一个 EventLoop。

## 4.6. Future

Future提供了一种在操作完成时通知应用程序的方式。这个对象可以看作是一个异步操作的结果的占位符，它将在未来的某个时刻完成，并提供对其结果的访问。  

JDK 预置了 interface java.util.concurrent.Future，但是其所提供的实现，**只允许手动检查对应的操作是否已经完成**，或者一直阻塞直到它完成。这是非常繁琐的，所以 Netty 提供了它自己的实现——ChannelFuture，用于在执行异步操作的时候使用。  

- ChannelFuture提供了几种额外的方法，这些方法使得我们能够注册一个或者多个ChannelFutureListener实例。  
- 监听器的回调方法operationComplete()，将会在对应的 操作完成时被调用 。然后监听器可以判断该操作是成功地完成了还是出错了。
- 每个 Netty 的出站 I/O 操作都将返回一个 ChannelFuture，也就是说，它们都不会阻塞。 所以说，Netty完全是异步和事件驱动的。  

```java
// AbstractBootstrap的 bind 方法
private ChannelFuture doBind(final SocketAddress localAddress) {
    final ChannelFuture regFuture = initAndRegister();
    final Channel channel = regFuture.channel();
    if (regFuture.cause() != null) {
        return regFuture;
    }

    if (regFuture.isDone()) {
        ChannelPromise promise = channel.newPromise();
        doBind0(regFuture, channel, localAddress, promise);
        return promise;
    } else {
        final PendingRegistrationPromise promise = new PendingRegistrationPromise(channel);
        // 设置监听
        regFuture.addListener(new ChannelFutureListener() {
            // 操作完成的回调操作
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                Throwable cause = future.cause();
                if (cause != null) {
                    promise.setFailure(cause);
                } else {
                    promise.registered();
                    doBind0(regFuture, channel, localAddress, promise);
                }
            }
        });
        return promise;
    }
}
```

## 4.7 ByteBuf详解  

### 4.7.1 工作原理 

Java NIO 提供了ByteBuffer 作为它 的字节容器，但是这个类使用起来过于复杂，而且也有些繁琐。

Netty 的 ByteBuffer 替代品是 ByteBuf，一个强大的实现，既解决了JDK API 的局限性， 又为网络应用程序的开发者提供了更好的API。

从结构上来说，ByteBuf 由一串字节数组构成。数组中每个字节用来存放信息。

ByteBuf **提供了两个索引**，一个用于**读取**数据，一个用于**写入**数据。这两个索引通过在字节数组中移动，来定位需要读或者写信息的位置。

当从 ByteBuf 读取时，它的 **readerIndex**（读索引）将会根据读取的字节数递增。

当写 ByteBuf 时，它的 **writerIndex**（写索引） 也会根据写入的字节数进行递增。

ByteBuf内部空间结构：

```x
+-------------------+------------------+------------------+------------------+
| discardable bytes |  readable bytes  |  writable bytes  |  capacity bytes  |
|                   |     (CONTENT)    |                  |                  |
+-------------------+------------------+------------------+------------------+
discardable bytes -- 可丢弃的字节空间  
readable bytes -- 可读的字节空间  
readable bytes -- 可读的字节空间  
capacity bytes -- 最大的可容量空间
```

如果 readerIndex 超过了 writerIndex 的时候，Netty 会抛出 IndexOutOf-BoundsException 异常。  

### 4.7.2 索引指针详解 

ByteBuf的三个指针：  

- readerIndex（读指针）  

  指示读取的起始位置， 每读取一个字节， readerIndex自增累加1。 如果readerIndex 与writerIndex 相等，ByteBuf 不可读 。  

- writerIndex（写指针）

  指示写入的起始位置， 每写入一个字节， writeIndex自增累加1。如果增加到 writerIndex 与capacity（） 容量相等，表示 ByteBuf 已经不可写。

- maxCapacity（最大容量）

  指示ByteBuf 可以扩容的最大容量， 如果向ByteBuf写入数据时， 容量不足， 可以进行扩容。  

ByteBuf内部指针：  

![image-20211017213740519](04_netty基础组件.assets/image-20211017213740519.png)

源码：

```java
public abstract class AbstractByteBuf extends ByteBuf {
    // ...
    int readerIndex; // 读指针
    int writerIndex; // 写指针
    private int markedReaderIndex; // 标记的读指针
    private int markedWriterIndex; // 标记的写指针
    private int maxCapacity; // 最大容量
    // ...
}
```

### 4.7.3 缓冲区的使用  

#### 1）读测试：

```java
/**
 * ByteBuf 读测试
 */
public class ByteBufRead {

    public static void main(String[] args) {
        //构造
        ByteBuf byteBuf = Unpooled.copiedBuffer("hello world", CharsetUtil.UTF_8);
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 11
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 53

        //方法一：内部通过移动readerIndex进行读取
        while (byteBuf.isReadable()) {
            System.out.println((char) byteBuf.readByte());
        }
        //方法二：通过下标直接读取
        for (int i = 0; i < byteBuf.readableBytes(); i++) {
            System.out.println((char) byteBuf.getByte(i));
        }
        //方法三：转化为byte[]进行读取
        byte[] bytes = byteBuf.array();
        for (byte b : bytes) {
            System.out.println((char) b);
        }
    }
}
```

#### 2）写测试：

```java
/**
 * ByteBuf写测试
 */
public class ByteBufWrite {

    public static void main(String[] args) {
        //构造空的字节缓冲区，初始大小为10，最大为20
        ByteBuf byteBuf = Unpooled.buffer(10, 20);
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 10
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 0
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 10
        for (int i = 0; i < 5; i++) {
            byteBuf.writeInt(i); //写入int类型，一个int占4个字节
        }
        System.out.println("ok");
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 20
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 20
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 0
        while (byteBuf.isReadable()) {
            System.out.println(byteBuf.readInt());
        }
    }
}
```

#### 3）丢弃测试

```python
# 通过discardReadBytes()方可以将已经读取的数据进行丢弃处理，就可以回收已经读取的字节空间
调用 discardReadBytes() 以前
+-------------------+------------------+------------------+
| discardable bytes |  readable bytes  |  writable bytes  |
+-------------------+------------------+------------------+
|                   |                  |                  |
0       <=      readerIndex   <=    writerIndex   <=   capacity

执行 discardReadBytes()后：
+------------------+--------------------------------------+
|  readable bytes  |    writable bytes (got more space)   |
+------------------+--------------------------------------+
readerIndex(0)<=writerIndex (decreased)       <=       capacity
```

```java
/**
 * 丢弃测试
 */
public class ByteBufDiscard {

    public static void main(String[] args) {
        ByteBuf byteBuf = Unpooled.copiedBuffer("hello world", CharsetUtil.UTF_8);
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 11
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 53
        while (byteBuf.isReadable()) {
            System.out.println((char) byteBuf.readByte());
        }

        byteBuf.discardReadBytes(); //丢弃已读的字节空间
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); // 0
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 64

    }
}
```

#### 4）清理功能：

```python
# 通过clear() 重置readerIndex 、 writerIndex 为0，需要注意的是，重置并没有删除真正的内容  
调用 clear() 以前
+-------------------+------------------+------------------+
| discardable bytes |  readable bytes  |  writable bytes  |
+-------------------+------------------+------------------+
|                   |                  |                  |
0       <=      readerIndex   <=    writerIndex   <=   capacity

调用 clear() 以后
+---------------------------------------------------------+
|                writable bytes (got more space)          |
+---------------------------------------------------------+
0 = readerIndex = writerIndex            <=            capacity
```

为什么没有真正删除其中的内容？  

> 如果你设计的时候，会消耗时间复杂度去清空每块内存里的实际数据？
>
> 接口我提供给你们了，你们想怎么用怎么用，反正和clear效果一样

那保留的数据有什么用？

> 调用clear方法后， 如果Buffer中仍有未读的数据，且后续还需要这些数据，但是此时想要先写入一些数据，那么使用compact()方法。compact()方法将所有未读的数据拷贝到Buffer起始处。然后将position设到最后一个未读元素正后面。limit属性依然像clear()方法一样，设置成capacity。Buffer准备好新的写入数据了，并且不会覆盖未读的数据。  

```java
/**
 * ByteBuf清理功能
 */
public class ByteBufClear {

    public static void main(String[] args) {
        ByteBuf byteBuf = Unpooled.copiedBuffer("hello world", CharsetUtil.UTF_8);
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); // 64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); //11
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); //53
        byteBuf.clear(); //重置readerIndex 、 writerIndex 为0
        System.out.println("byteBuf的容量为：" + byteBuf.capacity()); //64
        System.out.println("byteBuf的可读容量为：" + byteBuf.readableBytes()); //0
        System.out.println("byteBuf的可写容量为：" + byteBuf.writableBytes()); // 64
    }
}
```

### 4.7.4 缓冲区使用模式 

根据存放缓冲区的不同分为三类：  

- 堆缓冲区（HeapByteBuf），内存的分配和回收速度比较快，可以被JVM自动回收，缺点是，如果进行socket的IO读写，需要额外做一次内存复制，将堆内存对应的缓冲区复制到内核Channel中，性能会有一定程度的下降。
  由于在堆上被 JVM 管理，在不被使用时可以快速释放。可以通过 ByteBuf.array() 来获取 byte[] 数据。
- 直接缓冲区（DirectByteBuf），非堆内存，它在堆外进行内存分配，相比堆内存，它的分配和回收速度会慢一些，但是将它写入或从Socket Channel中读取时，由于减少了一次内存拷贝，速度比堆内存块。
- 复合缓冲区，顾名思义就是将上述两类缓冲区聚合在一起。Netty 提供了一个CompsiteByteBuf，可以将堆缓冲区和直接缓冲区的数据放在一起，让使用更加方便。  

```java
// 默认使用的是DirectByteBuf，如果需要使用HeapByteBuf模式，则需要进行系统参数的设置
System.setProperty("io.netty.noUnsafe", "true"); //netty中IO操作都是基于Unsafe完成的 
// ByteBuf 的分配要设置为非池化，否则不能切换到堆缓冲器模式
serverBootstrap.childOption(ChannelOption.ALLOCATOR, UnpooledByteBufAllocator.DEFAULT);
```

### 4.7.5 ByteBuf 的分配 

Netty 提供了两种 **ByteBufAllocator** 的实现，分别是：  

- PooledByteBufAllocator，实现了 ByteBuf 的对象的池化，提高性能减少并最大限度地减少内存碎片。
- UnpooledByteBufAllocator，没有实现对象的池化，每次会生成新的对象实例。  

```java
//通过ChannelHandlerContext获取ByteBufAllocator实例
ctx.alloc();
//通过channel也可以获取
channel.alloc();

//Netty默认使用了PooledByteBufAllocator
//可以在引导类中设置非池化模式
serverBootstrap.childOption(ChannelOption.ALLOCATOR, UnpooledByteBufAllocator.DEFAULT);

//或通过系统参数设置
System.setProperty("io.netty.allocator.type", "pooled");
System.setProperty("io.netty.allocator.type", "unpooled");
```

### 4.7.6 ByteBuf的释放  

ByteBuf如果采用的是堆缓冲区模式的话，可以由GC回收，但是如果采用的是直接缓冲区，就不受GC的管理，就得手动释放，否则会发生内存泄露。

关于ByteBuf的释放，分为手动释放与自动释放。  

#### 4.7.6.1 手动释放

手动释放，就是在使用完成后，调用ReferenceCountUtil.release(byteBuf); 进行释放。

通过release方法减去 byteBuf 的使用计数，Netty 会自动回收 byteBuf  

```java
/**
* 获取客户端发来的数据
* *
@param ctx
* @param msg
* @throws Exception
*/
@Override
public void channelRead(ChannelHandlerContext ctx, Object msg) throws
    Exception {
    ByteBuf byteBuf = (ByteBuf) msg;
    String msgStr = byteBuf.toString(CharsetUtil.UTF_8);
    System.out.println("客户端发来数据：" + msgStr);
    //释放资源
    ReferenceCountUtil.release(byteBuf);
}
```

手动释放可以达到目的，但是这种方式会比较繁琐，如果一旦忘记释放就可能会造成内存泄露  

#### 4.7.6.2 自动释放

自动释放有三种方式，分别是：入站的TailHandler、继承SimpleChannelInboundHandler、HeadHandler的出站释放  

- TailHandler：
  Netty的ChannelPipleline的流水线的末端是TailHandler，默认情况下如果每个入站处理器Handler都把消息往下传，TailHandler会释放掉ReferenceCounted类型的消息。  

  ```java
  /**
  * 获取客户端发来的数据
  * @param ctx
  * @param msg
  * @throws Exception
  */
  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws
      Exception {
      ByteBuf byteBuf = (ByteBuf) msg;
      String msgStr = byteBuf.toString(CharsetUtil.UTF_8);
      System.out.println("客户端发来数据：" + msgStr);
      //向客户端发送数据
      ctx.writeAndFlush(Unpooled.copiedBuffer("ok", CharsetUtil.UTF_8));
      ctx.fireChannelRead(msg); //将ByteBuf向下传递
  }
  ```

  在DefaultChannelPipeline中的TailContext内部类会在最后执行：  

  ```java
  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) {
      onUnhandledInboundMessage(ctx, msg);
  }
  
  //最后会执行
  protected void onUnhandledInboundMessage(Object msg) {
      try {
          logger.debug(
              "Discarded inbound message {} that reached at the tail of the
              pipeline. " +
              "Please check your pipeline configuration.", msg);
      } finally {
          ReferenceCountUtil.release(msg); //释放资源
      }
  }
  ```

  需要注意的是，如果没有进行向下传递，那么在TailHandler中是不会进行释放操作的。  

  ChannelPipeline的作用与特性:  

  > 1. 内部结构是一个双向链表
  > 2. 每个节点都有一个ChannelHandler实例，这个实例可以是ChannelInboundHandler类型或ChannelOutboundHandler类型。
  > 3. hannelInboundHandler只处理inbound事件，ChannelOutboundHandler只处理outbound事件。
  > 4. inbound入站事件处理顺序是由链表的头到链表尾，outbound事件的处理顺序是由链表尾到链表头。
  > 5. inbound入站事件由netty内部触发，最终由netty外部的代码消费。  
  > 6. outbound事件由netty外部的代码触发，最终由netty内部消费  

- SimpleChannelInboundHandler  

  当ChannelHandler继承了SimpleChannelInboundHandler后，在SimpleChannelInboundHandler的channelRead()方法中，将会进行资源的释放，我们的业务代码也需要写入到channelRead0()中。  

  ```java
  //SimpleChannelInboundHandler中的channelRead()
  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws
      Exception {
      boolean release = true;
      try {
          if (acceptInboundMessage(msg)) {
              @SuppressWarnings("unchecked")
              I imsg = (I) msg;
              channelRead0(ctx, imsg);
          } else {
              release = false;
              ctx.fireChannelRead(msg);
          }
      } finally {
          if (autoRelease && release) {
              ReferenceCountUtil.release(msg); //在这里释放
          }
      }
  }
  ```

  使用：

  ```java
  public class MyClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
      @Override
      protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg)
          throws Exception {
          System.out.println("接收到服务端的消息：" + msg.toString(CharsetUtil.UTF_8));
      } 
      @Override
      public void channelActive(ChannelHandlerContext ctx) throws Exception {
          // 向服务端发送数据
          String msg = "hello";
          ctx.writeAndFlush(Unpooled.copiedBuffer(msg, CharsetUtil.UTF_8));
      } 
      @Override
      public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
          throws Exception {
          cause.printStackTrace();
          ctx.close();
      }
  }
  ```

- HeadHandler  

  出站处理流程中，申请分配到的 ByteBuf，通过 HeadHandler 完成自动释放。
  出站处理用到的 Bytebuf 缓冲区，一般是要发送的消息，通常由应用所申请。在出站流程开始的时候，通过调用 ctx.writeAndFlush(msg)，Bytebuf 缓冲区开始进入出站处理的 pipeline 流水线。
  在每一个出站Handler中的处理完成后，最后消息会来到出站的最后一棒 HeadHandler，再经过一轮复杂的调用，在flush完成后终将被release掉。  

  ```java
  public class MyClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
      @Override
      protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg)
          throws Exception {
          System.out.println("接收到服务端的消息：" + msg.toString(CharsetUtil.UTF_8));
      } 
      @Override
      public void channelActive(ChannelHandlerContext ctx) throws Exception {
          // 向服务端发送数据
          String msg = "hello";
          ctx.writeAndFlush(Unpooled.copiedBuffer(msg, CharsetUtil.UTF_8));
      } 
      @Override
      public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
          throws Exception {
          cause.printStackTrace();
          ctx.close();
      }
  }
  ```

  

### 4.7.7 小结

- 入站处理流程中，如果对原消息不做处理，调用 ctx.fireChannelRead(msg) 把原消息往下传，由流水线最后一棒 TailHandler 完成自动释放。
- 如果截断了入站处理流水线，则可以继承 SimpleChannelInboundHandler ，完成入站ByteBuf自动释放。
- 出站处理过程中，申请分配到的 ByteBuf，通过 HeadHandler 完成自动释放。
- 入站处理中，如果将原消息转化为新的消息并调用 ctx.fireChannelRead(newMsg)往下传，那必须把原消息release掉;
- 入站处理中，如果已经不再调用 ctx.fireChannelRead(msg) 传递任何消息，也没有继承SimpleChannelInboundHandler 完成自动释放，那更要把原消息release掉;  

通过下图将Netty中的核心组件串起来。  

![image-20211017222120268](04_netty基础组件.assets/image-20211017222120268.png)

# 5.Netty应用实例-通讯系统

注意：Netty中Bootstrap有handle()和childHander()方法

区别：handler在初始化时就会执行，而childHandler会在客户端成功connect后才执行。

## 5.1 工程结构

![image-20211018150528599](04_netty基础组件.assets/image-20211018150528599.png)

```xml
<modules>
    <module>netty-server</module>
    <module>netty-client</module>
</modules>
<dependencies>
    <!-- Netty 核心组件依赖 -->
    <dependency>
        <groupId>io.netty</groupId>
        <artifactId>netty-all</artifactId>
        <version>4.1.16.Final</version>
    </dependency>
</dependencies>
```

## 5.2 服务端

1. 服务端工程NettyServer
2. 接收客户请求， 并打印客户端发送的消息
3. 消息采用内置String作为编码与解码器
4. 开启信息输入监听线程， 发送消息至客户端

```java
package com.lishem.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 服务端
 */
public class NettyProviderServer {

    private int port;

    public NettyProviderServer(int port) {
        this.port = port;
    }

    // netty 服务端启动
    public void runServer() {
        // 用来接收进来的连接
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        // 用来处理已经被接收的连接，一旦bossGroup接收到连接，就会把连接信息注册到workerGroup上
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        try {
            // nio服务的启动类
            ServerBootstrap sbs = new ServerBootstrap();
            // 配置nio服务参数
            sbs.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                // tcp最大缓存链接个数,它是tcp的参数, tcp_max_syn_backlog(半连接上限数量, CENTOS6.5默认是128)
                .option(ChannelOption.SO_BACKLOG, 128)
                //保持连接
                .childOption(ChannelOption.SO_KEEPALIVE, true)
                .handler(new LoggingHandler(LogLevel.INFO)) // 日志级别
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        //管道注册handler
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        // 编码通道处理
                        pipeline.addLast("decode", new StringDecoder());
                        pipeline.addLast("encode", new StringEncoder());
                        // 处理接收到的请求
                        pipeline.addLast(new NettyServerHandler());
                    }
                })// 配置接收区缓冲
                .childOption(ChannelOption.SO_RCVBUF, 4* 1024)
                // 配置发送区缓冲
                .childOption(ChannelOption.SO_SNDBUF, 4*1024);

            System.err.println("-------server 启动------");

            // 监听输入端消息并发送给所有客户端
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (true) {
                            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                            String str = in.readLine();
                            // 遍历 然后每个都发出去
                            for (Channel channel : NettyServerHandler.channelList) {
                                channel.writeAndFlush(str);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            // 绑定端口，开始接受链接
            ChannelFuture cf = sbs.bind(port).sync();
            cf.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
```

```java
package com.lishem.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.ArrayList;
import java.util.List;

public class NettyServerHandler extends ChannelInboundHandlerAdapter {

    public static List<Channel> channelList = new ArrayList<Channel>();

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        channelList.add(ctx.channel());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("server--收到消息： " + msg);
    }

    // 出现异常的处理
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.err.println("server--读取数据出现异常:");
        cause.printStackTrace();
        ctx.close();
    }
}
```

```java
package com.lishem.server;

public class Main {

    // 开启netty服务线程
    public static void main(String[] args) throws Exception {
        new NettyProviderServer(9911).runServer();
    }
}

```

## 5.3 客户端

1. 客户端工程NettyClient
2. 发起请求， 与服务端建立连接。
3. 监听服务端下发消息, 并将信息打印出来。
4. 开启信息输入监听线程， 将消息发送至服务端。  

```java
package com.lishem.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class NettyClientServer {

    // 要请求的服务器的ip地址
    private String ip;
    // 服务器的端口
    private int port;

    public NettyClientServer(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    // 启动服务
    public void runServer() throws Exception {

        EventLoopGroup bossGroup = new NioEventLoopGroup();

        Bootstrap bs = new Bootstrap();

        bs.group(bossGroup)
            .channel(NioSocketChannel.class)
            .option(ChannelOption.SO_KEEPALIVE, true)
            .handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    //管道注册handler
                    ChannelPipeline pipeline = socketChannel.pipeline();
                    //编码通道处理
                    pipeline.addLast("decode", new StringDecoder());
                    //转码通道处理
                    pipeline.addLast("encode", new StringEncoder());

                    // 处理来自服务端的响应信息
                    socketChannel.pipeline().addLast(new NettyClientHandler());
                }
            });
        System.out.println("-------client 启动------");
        // 客户端开启
        ChannelFuture cf = bs.connect(ip, port).sync();

        String reqStr = "客户端发起连接请求";

        Channel channel = cf.channel();

        // 发送客户端的请求
        channel.writeAndFlush(reqStr);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (true) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                        String msg = in.readLine();
                        channel.writeAndFlush(msg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}

```

```java
package com.lishem.client;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class NettyClientHandler extends ChannelInboundHandlerAdapter {

    public static Channel serverChannel = null;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        serverChannel = ctx.channel();
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        System.out.println("client--收到消息：" + msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.err.println("client 读取数据出现异常");
        cause.printStackTrace();
        ctx.close();
    }
}
```

```java
package com.lishem.client;

public class Main {

    public static void main(String[] args) throws Exception {
        new NettyClientServer("127.0.0.1", 9911).runServer();
    }
}
```



