本文将带我初识RPC 并掌握RPC原理用到的技术点

# 1.概述

## 1.1 什么是RPC

Remote Procedure Call 远程过程调用，简称RPC

关键字： 分布式、透明调用

## 1.2 主流框架

Dubbo、Spring Cloud



# 2.RPC原理

![image-20211012221810209](01_RPC_序列化_动态代理.assets/image-20211012221810209.png)



所涉及到的技术：

**序列化**：网络传输数据传输的是二进制数据

**动态代理**：真正执行操作的是代理对象

**NIO通信**：网络传输

**服务注册**：你总要知道你调的服务在哪吧

# 3. 序列化技术

## 3.1 JDK序列化

1. 实现 **java.io.Serializable** 接口，并且生成serialVersionUID  （必须先实现接口！）
   1. idea 如果自动生成里面没有  在settings -- Editor -- Inspections -- 搜索UID 勾选 Serializable class without 'serialVersionUID'后面的复选框
   2. 如果不显式指定  serialVersionUID ，jdk会根据class文件自动计算一个值，当类文件发生变化， 自动计算的值也会变化，旧的对象遇到更新过的类便不能正确反序列化。
   3. 虚拟机是否允许反序列化，不仅取决于类路径和功能代码，还要看两个类序列化ID是否一致
2. 如果想要某个字段不参与序列化，则在此字段上增加关键字 **transient** （关键字！不是注解）
3. **static** 关键字修饰的 字段， 也不会被序列化
4. 想要将父类对象也序列化，需要让父类也实现序列化接口

```java
public class JDKSerialization {

    public static void serialize() throws Exception {
        //将序列化后的数据存入到D:/TestCode/tradeUser.clazz中
        String basePath =  "D:/TestCode/";
        FileOutputStream fos = new FileOutputStream(basePath + "tradeUser.clazz");
        //创建tradeUser对象
        TradeUser tradeUser = new TradeUser();
        tradeUser.setName("Mirson");
        //将tradeUser写入到tradeUser.clazz中
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(tradeUser);
        oos.flush();
        oos.close();

        //读取D:/TestCode/tradeUser.clazz
        FileInputStream fis = new FileInputStream(basePath + "tradeUser.clazz");
        ObjectInputStream ois = new ObjectInputStream(fis);
        //将读取的数据反序列化到对象中
        TradeUser deTradeUser = (TradeUser) ois.readObject();
        ois.close();
        System.out.println("=== 反序列化结果 ==== ");
        System.out.println(deTradeUser);
    }

    public static void main(String[] args) throws Exception {
        serialize();
    }
}
```

序列化：ObjectOutputStream

反序列化：ObjectInputStream

## 3.2 JSON序列化

Http协议的RPC框架中，一般选择JSON方式

- 优点：拓展性、可读性、通用性
- 缺点：空间开销大，解析率和压缩率低

下面的代码演示用到了Gson

```java
public class GsonSerialization{
    
	/**
     * 采用单例模式, 需要保证线程安全性
     */
	private volatile static GsonSerialization singleton = null;
    
	/**
     * Gson解析对象
     */
	private Gson gson;
    
	/**
     * Gson封装工具的初始化， 主要处理JSON的解析配置
     */
	private GsonSerialization() {
	    // 1. 对日期做格式化处理
        // 2. 开启对MAP的解析转换支持， {key1: value1, key2:value2}
		gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").enableComplexMapKeySerialization().create();
	}
    
	/**
     * 单例的实现(做了线程安全处理)
     * @return
     */
	public static GsonSerialization getSingleton() {
	    if (singleton == null) {  
	        synchronized (GsonSerialization.class) {
		        if (singleton == null) {
		        	singleton = new GsonSerialization();
		        }  
	        }  
	    }  
	    return singleton;  
    }
    
    /**
     * 将JSON转为指定的JAVA对象，支持泛型传递
     * @param jsonStr
     * @param t
     * @param <T>
     * @return
     */
	public <T>  T cvtJson2Obj(String jsonStr, Class<T> t) {
		return gson.fromJson(jsonStr, t);
	}

    /**
     * 将JsonObject转换为指定的JAVA对象
     * @param jsonObj
     * @param t
     * @param <T>
     * @return
     */
	public <T>  T cvtJson2Obj(JsonObject jsonObj, Class<T> t) {
		return gson.fromJson(jsonObj, t);
	}

    /**
     * 将JSON字符串转换为type对象
     * @param jsonObj
     * @param type
     * @param <T>
     * @return
     */
	public <T>  T cvtJson2List(String jsonObj, Type type ) {
		return gson.fromJson(jsonObj, type);
	}

    /**
     * 将JSON转换为List对象
     * Usage: Type type = new TypeToken<List<obj>>() {}.getType();
     * @param jsonObj
     * @param <T>
     * @return
     */
    public <T> List<T> cvtJson2List(String jsonObj, Class<T[]> clazz) {
        T[] arr = gson.fromJson(jsonObj, clazz);
        return Arrays.asList(arr);
    }


    /**
     * 将JSON字符串转换为MAP对象
     * @param json
     * @return
     */
	public Map<String, String> cvtJson2Map(String json) {
		return gson.fromJson(json, new TypeToken<Map<String, String>>() {
		}.getType());
	}
	
	/**
	 * 对象转换为JSON
	 * @param obj
	 * @return
	 */
	public String cvtObj2Json(Object obj) {
		return gson.toJson(obj);
	}
	
	/**
	 * 采用GSON的解析方式获取JSON某属性
	 * 方法用途: <br>
	 * 实现步骤: <br>
	 * @param jsonStr
	 * @param name
	 * @return
	 */
	public String getJsonProp(String jsonStr, String name) {
		JsonParser parser = new JsonParser();
		JsonElement jsonEl = parser.parse(jsonStr);
		JsonObject jsonObj = jsonEl.getAsJsonObject();
		return jsonObj.get(name).getAsString();
	}

    /**
     * 获取JsonObject对象
     * @param jsonObject
     * @return
     */
	public JsonObject getJsonObject(String jsonObject) {
		JsonParser parser = new JsonParser();
		return parser.parse(jsonObject).getAsJsonObject();
	}

    /**
     * 获取JsonArray对象
     * @param jsonObject
     * @return
     */
	public JsonArray getJsonArray(String jsonObject) {
        JsonParser parser = new JsonParser();
        return parser.parse(jsonObject).getAsJsonArray();
    }

	/**
	 * 生成包含参数名值对的Json字符串
	 * @param paramNameList
	 * @param paramValueList
	 * @return
	 */
	public String generateParamJsonData(String[] paramNameList, Object[] paramValueList) {

		JsonObject jsonObj = new JsonObject();
		for(int i=0; i<paramNameList.length;i++){
			if (paramValueList[i] instanceof String){
				jsonObj.addProperty(paramNameList[i], String.valueOf(paramValueList[i]));
			} else{
				jsonObj.addProperty(paramNameList[i], cvtObj2Json(paramValueList[i]));
			}
		}
		return gson.toJson(jsonObj);
	}

	/**
	 * 从指定 jsonObject 中找出 subJsonName 命名的子对象
	 * @param jsonObject
	 * @param subJsonName
	 * @param t 子对象class类型
	 * @return
	 */
	public <T> T cvtSubJson2Obj(JsonObject jsonObject, String subJsonName,
			Class<T> t) {
		String param = jsonObject.getAsJsonPrimitive(subJsonName).getAsString();
		return cvtJson2Obj(param,t);
	}
    
    /**
     * 序列化测试
     */
    public static void serialize(){
        TradeUser tradeUser = new TradeUser();
        tradeUser.setUserNo("100001");
        tradeUser.setName("Mirson");

        // 将对象做序列化处理
        String result = GsonSerialization.getSingleton().cvtObj2Json(tradeUser);
        System.out.println("=== 序列化结果 ==== ");
        System.out.println(result);

        // 将Json做反序列化处理
        TradeUser deTradeUser = GsonSerialization.getSingleton().cvtJson2Obj(result, TradeUser.class);
        System.out.println("=== 反序列化结果 ==== ");
        System.out.println(deTradeUser);
    }
    
	public static void main(String[] args) {
		serialize();
	}
}
```



## 3.3 Hessian2序列化

Hessian 是一个动态类型，二进制序列化，并且支持跨语言特性的序列化框架。

Hessian 性能上要比 JDK、JSON 序列化高效很多，并且生成的字节数也更小。有非常好的兼容性和稳定性，所以 Hessian 更加适合作为 RPC 框架远程通信的序列化协议。

Hessian自身缺陷：

- 对Linked系列对象不支持，比如LinkedHashMap、LinkedHashSet 等，但可以通过CollectionSerializer类修复。
- Locale 类不支持，可以通过扩展 ContextSerializerFactory 类修复。
- Byte/Short 在反序列化的时候会转成 Integer。

```java
public class Hessian2Serialization {

    public static void serialize() throws Exception {
        TradeUser tradeUser = new TradeUser();
        tradeUser.setName("Mirson");
        tradeUser.setUserNo("100001");

        //tradeUser对象序列化处理
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Hessian2Output output = new Hessian2Output(bos);
        output.writeObject(tradeUser);
        output.flushBuffer();
        byte[] data = bos.toByteArray();
        System.out.println("=== 序列化结果 ==== ");
        System.out.println(data);
        bos.close();

        //tradeUser对象反序列化处理
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        Hessian2Input input = new Hessian2Input(bis);
        TradeUser deTradeUser = (TradeUser) input.readObject();
        input.close();
        System.out.println("=== 反序列化结果 ==== ");
        System.out.println(deTradeUser);

    }
    
    public static void main(String[] args) throws Exception {
        serialize();
    }
}
```

## 3.4 Protobuf序列化

Protobuf 是 Google 推出的开源序列库，它是一种轻便、高效的结构化数据存储格式，可以用于结构化数据序列化，支持 Java、Python、C++、Go 等多种语言。

Protobuf 使用的时候需要定义 IDL（Interface description language），然后使用不同语言的 IDL编译器，生成序列化工具类，它具备以下优点：

- 压缩比高，体积小，序列化后体积相比 JSON、Hessian 小很多；
- IDL 能清晰地描述语义，可以帮助并保证应用程序之间的类型不会丢失，无需类似 XML 解析器；
- 序列化反序列化速度很快，不需要通过反射获取类型；
- 消息格式的扩展、升级和兼容性都不错，可以做到向后兼容。

这个操作有点复杂。。



# 4 动态代理

RPC调用方只拿到了接口，并没有拿到实现类，那这个方法调用是怎么成功的？一定是系统帮我们生成了一个实现类，完成了远程调用的过程。系统生成这个类的过程就用到了动态代理技术

## 4.1  JDK动态代理

- 被代理类必须实现接口
- 代理类继承自 InvocationHandler ，并且持有被代理对象
- 代理对象由Proxy.newProxyInstance(ClassLoader loader, Class<?>[] interfaces,  InvocationHandler h) 方法创建

```java
public class JdkProxyTest {
    /**
     * 定义用户的接口
     */
    public interface User {
        String job();
    }


    /**
     * 实际调用对象
     */
    public static class Teacher implements User {
        public String job() {
            return "i am  teacher";
        }
    }

    /**
     * 创建JDK动态代理类
     */
    public static class JDKProxy implements InvocationHandler {

        private Object target;

        public JDKProxy() {

        }

        public JDKProxy(Object target) {
            this.target = target;
        }

        public Object getInstance(Object target) {
            this.target = target;
            Class<?> clazz = target.getClass();
            return Proxy.newProxyInstance(clazz.getClassLoader(), clazz.getInterfaces(), this);
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            System.out.println("调用前");
            Object invoke = method.invoke(this.target, args);
            System.out.println(invoke);
            System.out.println("调用后");
            return invoke;
        }
    }

    /**
     * 动态代理测试
     */
    public static void main(String[] args) {
        // 第一种调用方式  HM
        // 构建代理器
        JDKProxy proxy = new JDKProxy(new Teacher());
        ClassLoader classLoader = Teacher.class.getClassLoader();
        // 生成代理类
        User user = (User) Proxy.newProxyInstance(classLoader, new Class[]{User.class}, proxy);
        // 接口调用
        System.out.println(user.job());

        // 第二种调用方式 GP
        User obj = (User) new JDKProxy().getInstance(new Teacher());
        obj.job();
    }
}
```

## 4.2 CGLib动态代理

CGLib 代理的目标对象不需要实现任何接口，它是通过**动态继承目标对象**实现的动态代理

```java
// 被代理类
public class OrderService {

    /**
     * 下单类
     */
    public String placeOrder(String userName) {
        System.out.println("用户：" + userName + "开始下单");
        return "成功";
    }
}

// 代理类
public class OrderServiceCglib implements MethodInterceptor {

    public Object getProxy(Object target) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        // 设置回调方法
        enhancer.setCallback(this);
        // 创建代理对象
        return enhancer.create();
    }

    /**
     * 实现MethodInterceptor接口中重写的方法
     */
    @Override
    public Object intercept(Object object, Method method, Object[] args, MethodProxy proxy) throws Throwable {

        System.out.println("校验下单参数：" + Arrays.asList(args));
        Object result = proxy.invokeSuper(object, args);
        System.out.println("记录下单日志结果： " + result);
        return result;
    }
}

public class Test{
    /**
     * cglib 演示
     */
    public static void main(String[] args){
        // 1. 创建Cglib动态代理
        OrderServiceCglib cglib = new OrderServiceCglib();
        // 2. 配置实现类
        OrderService orderService = (OrderService) cglib.getProxy(new OrderService());
        // 3. 通过动态代理调用接口
        orderService.placeOrder("mirson");
    }
}

```













