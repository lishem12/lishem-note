Zookeeper是一个高可用的分布式数据管理和协同框架，并且能够很好的保证分布式环境中的数据一致性。

在Hadoop、HBase。Kafka中，Zookeeper都作为核心组件来使用

# 1.Zookeeper源码导入

zookeeper 3.7版本以后构建工具就开始用maven了，这个导入还是比较顺利的

## 1.1 工程导入

https://zookeeper.apache.org/ 官网

https://github.com/apache/zookeeper  github源码地址

下载3.7版本，并用IDEA打开，开始导包

![image-20211030152041797](09_zookeeper源码01.assets/image-20211030152041797.png)

## 1.2 错误解决

### 1.2.1 Info类

zookeeper-server  模块中

创建 org.apache.zookeeper.version.Info 类：

```java
package org.apache.zookeeper.version;

/**
 * 就是一个版本信息，随便写，但是要有如下变量
 */
public interface Info {
    public static final int MAJOR=3;
    public static final int MINOR=4;
    public static final int MICRO=6;
    public static final String QUALIFIER=null;
    public static final int REVISION=-1;
    public static final String REVISION_HASH = "1";
    public static final String BUILD_DATE="2021-10-30 15:22:06";
}
```

### 1.2.2 启动配置

主启动类在 zookeeper-server  模块的  org.apache.zookeeper.server.quorum.QuorumPeerMain 这里，

在启动时要配置：

![image-20211030152422181](09_zookeeper源码01.assets/image-20211030152422181.png)

写清楚配置文件地址，我这里用的是相对路径，也可以用绝对路径

### 1.2.3 编译 jute模块

![image-20211030152508067](09_zookeeper源码01.assets/image-20211030152508067.png)

双击即可

### 1.2.4 添加一些maven依赖

在 zookeeper-server  模块的pom.xml文件 175 行添加如下依赖

```xml
<!--引入依赖-->
<dependency>
  <groupId>io.dropwizard.metrics</groupId>
  <artifactId>metrics-core</artifactId>
  <version>3.1.0</version>
</dependency>
<dependency>
  <groupId>org.xerial.snappy</groupId>
  <artifactId>snappy-java</artifactId>
  <version>1.1.7.3</version>
</dependency>
<dependency>
  <groupId>org.eclipse.jetty</groupId>
  <artifactId>jetty-server</artifactId>
</dependency>
<dependency>
  <groupId>org.eclipse.jetty</groupId>
  <artifactId>jetty-servlet</artifactId>
</dependency>
<dependency>
  <groupId>commons-cli</groupId>
  <artifactId>commons-cli</artifactId>
</dependency>
```

别的还有一些错误，但是已经不影响使用了

## 1.3 Zookeeper 命令

### 1.3.1 怎么玩跑起来的项目

Zookeeper 需要启动服务端和客户端

- 服务端：

org.apache.zookeeper.server.quorum.QuorumPeerMain 这个类的main方法启动，需要依赖zoo.cfg 配置文件

具体怎么操作上面已经看写过了

- 客户端

org.apache.zookeeper.ZooKeeperMain 这个类的main 方法启动，启动以后就可以像命令行一样操作zookeeper

> 要先启动服务端再启动客户端

![image-20211030155908649](09_zookeeper源码01.assets/image-20211030155908649.png)

### 1.3.2 zookeeper可视化工具

https://issues.apache.org/jira/secure/attachment/12436620/ZooInspector.zip

ZooInspector 搜这东西，有手就行

### 1.3.3 常用命令

1. 节点列表

    ```shell
    ls /
    [dubbo,zookeeper]
    ls /dubbo
    [...]
    ```

2. 查看节点状态

    ```shell
    stat /lishem
    cZxid = 0xb		 # 节点被创建时的事物的ID
    ctime = Sat Oct 30 16:02:33 CST 2021 # 创建时间
    mZxid = 0xb 		# 节点最后一次被修改时的事物的ID
    mtime = Sat Oct 30 16:02:33 CST 2021 	# 最后一次修改时间
    pZxid = 0xb		 # 子节点列表最近一次呗修改的事物ID
    cversion = 0 		# 子节点版本号
    dataVersion = 0 		# 数据版本号
    aclVersion = 0		 # ACL版本号
    ephemeralOwner = 0x0 # 创建临时节点的事物ID，持久节点事物为0
    dataLength = 0 		# 数据长度，每个节点都可保存数据
    numChildren = 0 	# 子节点个数
    ```

3. 创建节点

    ```shell
    create /lishem
    Created /lishem
    
    create /hello java
    Created /hello
    ```

    后面有空格跟参数表示节点下的内容

4. 查看节点下数据

    ```shell
    get /hello
    java
    ```

5. 删除节点

    ```shell
    # 删除没有子节点的节点： 
    delete /dubbo/code
    
    # 删除所有子节点： 
    deleteall /dubbo/code
    ```

6. 历史操作命令

    ```shell
    history
    
    5 - create /hello java
    6 - get /hello
    7 - ls /
    8 - create /hello/a/a/a/b
    9 - create /hello/a
    10 - create /hello/a/b
    11 - delete /hello/a
    12 - delete /hello/a/b
    13 - ls
    14 - ls /hello/a
    15 - history
    ```

## 1.4 Zookeeper 案例应用

就是用一下源码自己启动的zookeeper 作为注册中心

这东西没什么好说的，启动服务端 dubbo-provider-v2 ，然后启动客户端 dubbo-consumer，访问

http://localhost:18081/car/v1/BYD

就可以了

![image-20211030194943497](09_zookeeper源码01.assets/image-20211030194943497.png)

# 2 Zookeeper单机版启动流程

ZooKeeper 可以以 **standalone** 、**分布式**的方式部署。

standalone 模式下只有一台机器作为服务器, ZooKeeper 会丧失高可用特性；

分布式是使用多个机器,每台机器上部署一个 ZooKeeper 服务器,即使有服务器宕机,只要少于半数, ZooKeeper 集群依然可以正常对外提供服务，集群状态下 Zookeeper 是具备高可用特性。

接下来我们看一下standalone启动流程

![image-20211030203358573](09_zookeeper源码01.assets/image-20211030203358573.png)

## 2.1 ZK启动入口分析

通过zkServer.cmd 我们可以看到，启动类是：org.apache.zookeeper.server.quorum.QuorumPeerMain

重点看两个方法：

main() 方法和 initializeAndRun() 方法

main方法只是做了初始化配置，但调用了 **initializeAndRun**() 方法，initializeAndRun() 方法中会根据配置来决定启动单机Zookeeper还是集群Zookeeper，源码如下：

```java
// QuorumPeerMain.java
public class QuorumPeerMain{

    /**
     * Zookeeper启动主方法
     */
    public static void main(String[] args) {
        QuorumPeerMain main = new QuorumPeerMain();
        try {
            //初始化配置，并启动服务
            main.initializeAndRun(args);
        } catch (Exception e) {
            //发生异常
        }
        //退出
        ServiceUtils.requestSystemExit(ExitCode.EXECUTION_FINISHED.getValue());
    }

    /***
     * 初始化并启动ZK服务
     *    1：解析配置文件
     *    2：启动清理计划
     *    3：调用ZookeeperServerMian.main()
     */
    protected void initializeAndRun(String[] args) throws ConfigException, IOException, AdminServerException {
        //解析zoo.cfg文件
        QuorumPeerConfig config = new QuorumPeerConfig();
        if (args.length == 1) {
            config.parse(args[0]);
        }

        // 启动数据清理计划
        DatadirCleanupManager purgeMgr = new DatadirCleanupManager(
            config.getDataDir(),        //数据存储目录
            config.getDataLogDir(),     //事务日志目录，默认为dataDir
            config.getSnapRetainCount(),//保留多少个快照数据，默认为3个
            config.getPurgeInterval()); //多少小时清理1次数据，默认不清理，在zoo.cfg中autopurge.purgeInterval=1设置时间
        purgeMgr.start();

        // !!! 决定是单机版还是集群版
        if (args.length == 1 && config.isDistributed()) {
            //集群模式运行
            runFromConfig(config);
        } else {
            //单机模式运行
            ZooKeeperServerMain.main(args);
        }
    }
}
```

## 2.2 单机启动源码剖析  

![image-20211030204032562](09_zookeeper源码01.assets/image-20211030204032562.png)

### 2.2.1 单机版启动入口

按照上面的分析，我们找到 **ZooKeeperServerMain.main(args)** 方法，该方法又调用了ZooKeeperServerMain的initializeAndRun() 方法，源码如下：

```java
// ZooKeeperServerMain.java
public class ZooKeeperServerMain{
    /*
     * 启动Zookeeper服务
     * @param args the configfile or the port datadir [ticktime]
     */
    public static void main(String[] args) {
        ZooKeeperServerMain main = new ZooKeeperServerMain();
        try {
            main.initializeAndRun(args);
        } catch (Exception e) {
            //发生异常
        }
        //正常退出
        ServiceUtils.requestSystemExit(ExitCode.EXECUTION_FINISHED.getValue());
    }

    /***
     * 初始化配置，并运行Zookeeper服务
     *  1: 解析配置文件
     *  2：调用runFromConfig()方法执行启动
     */
    protected void initializeAndRun(String[] args) throws ConfigException, IOException, AdminServerException {
        //注册JMX [1]
        try {
            ManagedUtil.registerLog4jMBeans();
        } catch (JMException e) {
            LOG.warn("Unable to register log4j JMX control", e);
        }

        // 解析配置文件 -2.2.2
        ServerConfig config = new ServerConfig();
        if (args.length == 1) {
            config.parse(args[0]);
        } else {
            config.parse(args);
        }

        //根据解析的配置运行Zookeeper服务  - 2.2.3
        runFromConfig(config);
    }
}
```

- [1] : JMX（Java Management Extension Java管理拓展）是一个为应用程序、设备、系统等植入管理功能的框架。JMX可以跨越一系列异构操作系统平台，系统体系结构和网络传输协议，灵活的开发无缝集成的系统、网络和服务管理应用。 -- 不是重点

### 2.2.2 配置文件解析

```java
// ServerConfig.java
public class ServerConfig{

    public void parse(String path) throws ConfigException {
        //解析zoo.cfg，并加载到config中
        QuorumPeerConfig config = new QuorumPeerConfig();
        config.parse(path);

        // let qpconfig parse the file and then pull the stuff we are
        // interested in
        readFrom(config);
    }

    /**
     * 从QuorumPeerConfig中拿一堆配置到本对象中.
     * @param config
     */
    public void readFrom(QuorumPeerConfig config) {
        clientPortAddress = config.getClientPortAddress();
        secureClientPortAddress = config.getSecureClientPortAddress();
        dataDir = config.getDataDir();
        dataLogDir = config.getDataLogDir();
        tickTime = config.getTickTime();
        maxClientCnxns = config.getMaxClientCnxns();
        minSessionTimeout = config.getMinSessionTimeout();
        maxSessionTimeout = config.getMaxSessionTimeout();
        jvmPauseMonitorToRun = config.isJvmPauseMonitorToRun();
        jvmPauseInfoThresholdMs = config.getJvmPauseInfoThresholdMs();
        jvmPauseWarnThresholdMs = config.getJvmPauseWarnThresholdMs();
        jvmPauseSleepTimeMs = config.getJvmPauseSleepTimeMs();
        metricsProviderClassName = config.getMetricsProviderClassName();
        metricsProviderConfiguration = config.getMetricsProviderConfiguration();
        listenBacklog = config.getClientPortListenBacklog();
        initialConfig = config.getInitialConfig();
    }
}

public class QuorumPeerConfig {
    
    public void parse(String path) throws ConfigException {
        // ...
        parseProperties(cfg);
        // ...
    }
    
    public void parseProperties(Properties zkProp) throws IOException, ConfigException {
        // ... emmm 没什么好看的
    }
}
```

就是ServerConfig 的解析配置又复用了 QuorumPeerConfig 的解析配置

然后把QuorumPeerConfig 解析后的东西 逐个拿过来

### 2.2.3 单机启动主流程

runFromConfig 方法是单机版启动的主要方法，该方法会做如下几件事：  

1. 初始化各类运行指标，比如一次提交数据最大花费多长时间、批量同步数据大小等。
2. 初始化权限操作，例如IP权限、Digest权限。
3. 创建事务日志操作对象，Zookeeper中每次增加节点、修改数据、删除数据都是一次事务操作，都会记录日志。
4. 定义Jvm监控变量和常量，例如警告时间、告警阀值次数、提示阀值次数等。
5. 创建ZookeeperServer，这里只是创建，并不在ZooKeeperServerMain类中启动。
6. 启动Zookeeper的控制台管理对象AdminServer，该对象采用Jetty启动。
7. 创建ServerCnxnFactory，该对象其实是Zookeeper网络通信对象，默认使用了NIOServerCnxnFactory。
8. 在ServerCnxnFactory中启动ZookeeperServer服务。
9. 创建并启动ContainerManager，该对象通过Timer定时执行,清理过期的容器节点和TTL节点,执行周期为分钟。
10. 防止主线程结束，阻塞主线程

源代码如下：

```java
// ZooKeeperServerMain.java
/**
* 根据解析的配置运行Zookeeper服务
* @param config 当前使用的配置
* @throws IOException
* @throws AdminServerException
*/
public void runFromConfig(ServerConfig config) throws IOException, AdminServerException {
    FileTxnSnapLog txnLog = null;
    try {
        try {
            //创建MetricsProvider，它是一个收集指标并将当前值发布到外部设施的系统，与选举有关。
            metricsProvider = MetricsProviderBootstrap.startMetricsProvider(
                config.getMetricsProviderClassName(),
                config.getMetricsProviderConfiguration());
        } catch (MetricsProviderLifeCycleException error) {
            throw new IOException("Cannot boot MetricsProvider " + config.getMetricsProviderClassName(), error);
        }
        
        ServerMetrics.metricsProviderInitialized(metricsProvider); // 各类运行指标收集器
        ProviderRegistry.initialize();// 初始化权限
        
        //============根据参数创建日志目录（事务日志目录、快照日志目录） FileTxnSnapLogL:事务数据操作
        // 这个东西就是主要的写对象
        txnLog = new FileTxnSnapLog(config.dataLogDir, config.dataDir);
        //JvmPauseMonitor监控
        JvmPauseMonitor jvmPauseMonitor = null;
        if (config.jvmPauseMonitorToRun) {
            jvmPauseMonitor = new JvmPauseMonitor(config);
        }
        //=========创建ZookeeperServer服务实例，并初始化参数 但是此时未启动该服务
        final ZooKeeperServer zkServer = new ZooKeeperServer(jvmPauseMonitor, txnLog, config.tickTime, config.minSessionTimeout, config.maxSessionTimeout, config.listenBacklog, null, config.initialConfig);
        txnLog.setServerStats(zkServer.serverStats());

        // 注册服务关闭程序
        final CountDownLatch shutdownLatch = new CountDownLatch(1);
        zkServer.registerServerShutdownHandler(new ZooKeeperServerShutdownHandler(shutdownLatch));

        // 启动服务管理器(JettyAdminServer)-启动Jetty
        adminServer = AdminServerFactory.createAdminServer();
        adminServer.setZooKeeperServer(zkServer);
        adminServer.start();

        boolean needStartZKServer = true;
        if (config.getClientPortAddress() != null) {
            // 2.2.4  还有 2.3
            //======================网络通信组件   Start=========================
            //cnxnFactory负责zk的网络请求，createFactory中
            //从系统配置中读取ZOOKEEPER_SERVER_CNXN_FACTORY，默认是没有这个配置的，因此默认是使用NIOServerCnxnFactory，基于java的NIO实现，
            cnxnFactory = ServerCnxnFactory.createFactory();
            //0.0.0.0/0.0.0.0:2181、单个客户端连接数超过限制、请求的传入连接队列的最大长度，-1不限制
            cnxnFactory.configure(config.getClientPortAddress(), config.getMaxClientCnxns(), config.getClientPortListenBacklog(), false);
            //======================网络通信组件   END=========================

            //启动Zookeeper服务 2.2.6
            cnxnFactory.startup(zkServer);
            // 是否需要启动Zookeeper服务，zookeeper服务已经启动了，不需要再次启动，设置为false
            needStartZKServer = false;
        }
        
        //采用了SSL（我们没有使用，不做讲解）
        if (config.getSecureClientPortAddress() != null) {
            secureCnxnFactory = ServerCnxnFactory.createFactory();
            secureCnxnFactory.configure(config.getSecureClientPortAddress(), config.getMaxClientCnxns(), config.getClientPortListenBacklog(), true);
            secureCnxnFactory.startup(zkServer, needStartZKServer);
        }

        //创建ContainerManager，它只由Leader调用，通过Timer定时执行,清理过期的容器节点和TTL节点,执行周期为分钟级别
        containerManager = new ContainerManager(
            zkServer.getZKDatabase(),
            zkServer.firstProcessor,
            Integer.getInteger("znode.container.checkIntervalMs", (int) TimeUnit.MINUTES.toMillis(1)),
            Integer.getInteger("znode.container.maxPerMinute", 10000),
            Long.getLong("znode.container.maxNeverUsedIntervalMs", 0)
        );
        containerManager.start();
        //为服务器启动和注册服务器停止日志添加审计日志。
        ZKAuditProvider.addZKStartStopAuditLog();

        //服务器正常启动时,运行到此处阻塞,只有server的state变为ERROR或SHUTDOWN时继续运行后面的代码
        shutdownLatch.await();

        shutdown();

        if (cnxnFactory != null) {
            cnxnFactory.join();
        }
        if (secureCnxnFactory != null) {
            secureCnxnFactory.join();
        }
        if (zkServer.canShutdown()) {
            zkServer.shutdown(true);
        }
    } catch (InterruptedException e) {
        // warn, but generally this is ok
        LOG.warn("Server interrupted", e);
    } finally {
        if (txnLog != null) {
            txnLog.close();
        }
        if (metricsProvider != null) {
            try {
                metricsProvider.stop();
            } catch (Throwable error) {
                LOG.warn("Error while stopping metrics", error);
            }
        }
    }
}
```

### 2.2.4 网络通信对象创建

```java
// ServerCnxnFactory.java
/***
* 创建通信组件
*/
public static ServerCnxnFactory createFactory() throws IOException {
    //加载配置，如果有其他通信方式可以配置该变量
    // ZOOKEEPER_SERVER_CNXN_FACTORY 系统变量中配置该参数可改变通信对象，默认NIO，可选Netty
    String serverCnxnFactoryName = System.getProperty(ZOOKEEPER_SERVER_CNXN_FACTORY);
    //如果没有配置该变量ZOOKEEPER_SERVER_CNXN_FACTORY,用默认的通信方式
    if (serverCnxnFactoryName == null) {
        //网络传输采用了NIO
        serverCnxnFactoryName = NIOServerCnxnFactory.class.getName();
    }
    try {
        //创建NIOServerCnxnFactory
        ServerCnxnFactory serverCnxnFactory = (ServerCnxnFactory) Class.forName(serverCnxnFactoryName)
            .getDeclaredConstructor()
            .newInstance();
        LOG.info("Using {} as server connection factory", serverCnxnFactoryName);
        return serverCnxnFactory;
    } catch (Exception e) {
        IOException ioe = new IOException("Couldn't instantiate " + serverCnxnFactoryName, e);
        throw ioe;
    }
}
```

### 2.2.5 网络对象参数配置

```java
// NIOServerCnxnFactory.java
/***
* 初始化thread，完成socket相关配置
* @param addr：服务暴露地址
* @param maxcc：最大会话链接数量
* @param backlog：
* @param secure：是否开启SSL
* @throws IOException
*/
@Override
public void configure(InetSocketAddress addr, int maxcc, int backlog, boolean secure) throws IOException {
    if (secure) {
        throw new UnsupportedOperationException("SSL isn't supported in NIOServerCnxn");
    }
    //Saasl验证配置
    configureSaslLogin();

    //最大链接数量配置
    maxClientCnxns = maxcc;
    initMaxCnxns();
    //会话超时时间
    sessionlessCnxnTimeout = Integer.getInteger(ZOOKEEPER_NIO_SESSIONLESS_CNXN_TIMEOUT, 10000);
    //无会话超时时间
    cnxnExpiryQueue = new ExpiryQueue<NIOServerCnxn>(sessionlessCnxnTimeout);
    //会话过期时间
    expirerThread = new ConnectionExpirerThread();
    //CPU核数
    int numCores = Runtime.getRuntime().availableProcessors();
    //selector thread线程数计算，该线程用于处理链接
    numSelectorThreads = Integer.getInteger(
        ZOOKEEPER_NIO_NUM_SELECTOR_THREADS,
        Math.max((int) Math.sqrt((float) numCores / 2), 1));
    if (numSelectorThreads < 1) {
        throw new IOException("numSelectorThreads must be at least 1");
    }
    //worker thread线程数计算，该线程执行基本的套接字读写
    numWorkerThreads = Integer.getInteger(ZOOKEEPER_NIO_NUM_WORKER_THREADS, 2 * numCores);
    //connection expiration thread线程数计算，该线程处理已过期的session
    workerShutdownTimeoutMS = Long.getLong(ZOOKEEPER_NIO_SHUTDOWN_TIMEOUT, 5000);

    //创建selector线程
    for (int i = 0; i < numSelectorThreads; ++i) {
        selectorThreads.add(new SelectorThread(i));
    }

    listenBacklog = backlog;
    //打开selector套接字通道
    this.ss = ServerSocketChannel.open();
    ss.socket().setReuseAddress(true);
    //绑定端口
    if (listenBacklog == -1) {
        ss.socket().bind(addr);
    } else {
        ss.socket().bind(addr, listenBacklog);
    }
    ss.configureBlocking(false);
    //创建链接处理线程
    acceptThread = new AcceptThread(ss, addr, selectorThreads);
}
```

### 2.2.6 单机启动  

cnxnFactory.startup(zkServer); 方法其实就是启动了 ZookeeperServer ，它调用NIOServerCnxnFactory 的 startup 方法，该方法中会调用 ZookeeperServer 的 startup 方法启动服务， ZooKeeperServerMain 运行到 shutdownLatch.await(); 主线程会阻塞住,源码如下：

```java
@Override
public void startup(ZooKeeperServer zks, boolean startServer) throws IOException, InterruptedException {
    //NIO启动对应线程 这里会启动3.2 里面的线程
    start();
    //设置Zookeeper的ServerCnxnFactory（客户端与服务端进行通信的对象，就是当前对象NIOServerCnxnFactory）
    setZooKeeperServer(zks);
    if (startServer) {
        //加载会话和数据
        zks.startdata();
        //启动Zookeeper服务
        zks.startup();
    }
}

// NIOServerCnxnFactory.java
@Override
public void start() {
    stopped = false;
    // new 了一个线程池启动
    if (workerPool == null) {
        workerPool = new WorkerService("NIOWorker", numWorkerThreads, false);
    }
    for (SelectorThread thread : selectorThreads) {
        if (thread.getState() == Thread.State.NEW) {
            thread.start();
        }
    }
    // ensure thread is started once and only once
    if (acceptThread.getState() == Thread.State.NEW) {
        acceptThread.start();
    }
    if (expirerThread.getState() == Thread.State.NEW) {
        expirerThread.start();
    }
}

public synchronized void startup() {
    startupWithServerState(State.RUNNING);
}

/****
* 启动Zookeeper服务
* @param state
*/
private void startupWithServerState(State state) {
    //创建sessionTracker，它是ZooKeeperServer用来跟踪会话的基本接口。
    if (sessionTracker == null) {
        createSessionTracker();
    }
    //启动SessionTracker
    startSessionTracker();
    //======创建请求处理器链【创建业务处理器】
    setupRequestProcessors();
    //启动节流器
    startRequestThrottler();
    //注册JMX
    registerJMX();
    //启动JVM监控
    startJvmPauseMonitor();
    //设置Zookeeper上下文参数
    registerMetrics();
    //设置状态为运行中
    setState(state);
    //启动节点路径计数器
    requestPathMetricsCollector.start();
    //关闭本地会话
    localSessionEnabled = sessionTracker.isLocalSessionsEnabled();
    //唤醒所有等待的线程
    notifyAll();
}
```

# 3 ZK网络通信源码

Zookeeper 作为一个服务器,自然要与客户端进行网络通信,如何高效的与客户端进行通信,让网络 IO 不成为 ZooKeeper 的瓶颈是 ZooKeeper 急需解决的问题, ZooKeeper 中使用 **ServerCnxnFactory** 管理与客户端的连接,其有两个实现,一个是 **NIOServerCnxnFactory** ,使用Java原生 NIO 实现;一个是**NettyServerCnxnFactory** ,使用netty实现;使用 ServerCnxn 代表一个客户端与服务端的连接。

从单机版启动中可以发现 Zookeeper 默认通信组件为 **NIOServerCnxnFactory** ，他们和ServerCnxnFactory 的关系如下图：

![image-20211105132312002](09_zookeeper源码01.assets/image-20211105132312002.png)

## 3.1 NIOServerCnxnFactory 工作流程

一般使用Java NIO的思路为使用1个线程组监听**OP_ACCEPT**事件，负责处理客户端的连接；

使用1个线程组监听客户端连接的**OP_READ**和**OP_WRITE**事件,处理IO事件(netty也是这种实现方式)。

但ZooKeeper并不是如此划分线程功能的, NIOServerCnxnFactory 启动时会启动四类线程：

1. accept thread：该线程接收来自客户端的连接，并将其分配给selector thread(启动一个线程)。
2. selector thread：该线程执行select()，由于在处理大量连接时，select()会成为性能瓶颈,因此启动多个selector thread,使用系统属性zookeeper.nio.numSelectorThreads配置该类线程数,默认个数为 核心数/2。
3. worker thread：该线程执行基本的套接字读写,使用系统属性zookeeper.nio.numWorkerThreads配置该类线程数,默认为核心数∗2。如果该类线程数为0,则另外启动一线程进行IO处理,见下文worker thread介绍。
4. connection expiration thread：若连接上的session已过期,则关闭该连接。

![image-20211030224950683](09_zookeeper源码01.assets/image-20211030224950683.png)

ZooKeeper 中对线程需要处理的工作做了更细的拆分,解决了有大量客户端连接的情况下, selector.select() 会成为性能瓶颈,将 selector.select() 拆分出来,交由 selector thread 处理。



## 3.2 NIOServerCnxnFactory 源码

AcceptThread 和 SelectorThread 都是对 Selector的封装

### 3.2.1 AcceptThread源码

![image-20211030225829906](09_zookeeper源码01.assets/image-20211030225829906.png)

在 NIOServerCnxnFactory 类中有一个 AccpetThread 线程，为什么说它是一个线程？我们看下它的继承关系： 

AcceptThread > AbstractSelectThread > ZooKeeperThread > Thread ，该线程接收来自客户端的连接，并将其分配给selector thread(启动一个线程)。

```java
private class AcceptThread extends AbstractSelectThread {

    //Socket链接通道
    private final ServerSocketChannel acceptSocket;
    //要处理的就绪事件
    private final SelectionKey acceptKey;
    private final RateLogger acceptErrorLogger = new RateLogger(LOG);
    private final Collection<SelectorThread> selectorThreads;
    //处理连接的线程集合
    private Iterator<SelectorThread> selectorIterator;
    private volatile boolean reconfiguring = false;

    public AcceptThread(ServerSocketChannel ss, InetSocketAddress addr, Set<SelectorThread> selectorThreads) throws IOException {
        super("NIOServerCxnFactory.AcceptThread:" + addr);
        // 外部传入的 ServerSocketChannel
        this.acceptSocket = ss;
        // ServerSocketChannel 注册selector 和 OP_ACCEPT
        this.acceptKey = acceptSocket.register(selector, SelectionKey.OP_ACCEPT);
        // 封装 selectorThreads 对象
        this.selectorThreads = Collections.unmodifiableList(new ArrayList<SelectorThread>(selectorThreads));
        selectorIterator = this.selectorThreads.iterator();
    }

    public void run() {
        try {
            while (!stopped && !acceptSocket.socket().isClosed()) {
                try {
                    // 执行select()
                    select();
                } catch (RuntimeException e) {
                    LOG.warn("Ignoring unexpected runtime exception", e);
                } catch (Exception e) {
                    LOG.warn("Ignoring unexpected exception", e);
                }
            }
        } finally {
            closeSelector();
            // This will wake up the selector threads, and tell the
            // worker thread pool to begin shutdown.
            if (!reconfiguring) {
                NIOServerCnxnFactory.this.stop();
            }
            LOG.info("accept thread exitted run method");
        }
    }

    public void setReconfiguring() {
        reconfiguring = true;
    }

    private void select() {
        try {
            //Selector的select()方法可以选择已经准备就绪的事件 -- 阻塞
            selector.select();
            //获取准备就绪的事件
            Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
            while (!stopped && selectedKeys.hasNext()) {
                //当前准备就绪事件信息
                SelectionKey key = selectedKeys.next();
                selectedKeys.remove();

                if (!key.isValid()) {
                    continue;
                }
                //检测通道是否可以执行读操作
                if (key.isAcceptable()) {
                    //调用doAccept()
                    if (!doAccept()) {
                        // If unable to pull a new connection off the accept
                        // queue, pause accepting to give us time to free
                        // up file descriptors and so the accept thread
                        // doesn't spin in a tight loop.
                        pauseAccept(10);
                    }
                } else {
                    LOG.warn("Unexpected ops in accept select {}", key.readyOps());
                }
            }
        } catch (IOException e) {
            LOG.warn("Ignoring IOException while selecting", e);
        }
    }

    /**
         * Mask off the listen socket interest ops and use select() to sleep
         * so that other threads can wake us up by calling wakeup() on the
         * selector.
         */
    private void pauseAccept(long millisecs) {
        acceptKey.interestOps(0);
        try {
            selector.select(millisecs);
        } catch (IOException e) {
            // ignore
        } finally {
            acceptKey.interestOps(SelectionKey.OP_ACCEPT);
        }
    }

    /**
     * 接收新的socket连接,每个IP地址有其连接个数上限.使用轮询为新连接分配selector thread
     */
    private boolean doAccept() {
        boolean accepted = false;
        SocketChannel sc = null;
        try {
            //监听新的链接
            sc = acceptSocket.accept();
            accepted = true;
            if (limitTotalNumberOfCnxns()) {
                throw new IOException("Too many connections max allowed is " + maxCnxns);
            }
            //获取远程计算机地址信息
            InetAddress ia = sc.socket().getInetAddress();
            System.out.println("AcceptThread----------链接服务的IP：" + ia.getHostName());
            int cnxncount = getClientCnxnCount(ia);

            //判断是否超出最大客户端连接的限制
            if (maxClientCnxns > 0 && cnxncount >= maxClientCnxns) {
                throw new IOException("Too many connections from " + ia + " - max is " + maxClientCnxns);
            }
            //调整此通道的阻塞模式。
            sc.configureBlocking(false);

            // 轮询将此连接分配给一个selector thread
            if (!selectorIterator.hasNext()) {
                selectorIterator = selectorThreads.iterator();
            }
            SelectorThread selectorThread = selectorIterator.next();
            // 将新连接加入selector thread 的acceptedQueue中，并唤醒SelectorThread
            // !!!!里，把SocketChannel 扔给 selectorThread
            if (!selectorThread.addAcceptedConnection(sc)) {
                throw new IOException("Unable to add connection to selector queue"
                                      + (stopped ? " (shutdown in progress)" : ""));
            }
            acceptErrorLogger.flush();
        } catch (IOException e) {
            // accept, maxClientCnxns, configureBlocking
            ServerMetrics.getMetrics().CONNECTION_REJECTED.add(1);
            acceptErrorLogger.rateLimitLog("Error accepting new connection: " + e.getMessage());
            fastCloseSock(sc);
        }
        return accepted;
    }
}
```

这里可能有点晕，多说两句：

- 在NIOServerCnxnFactory的configure 方法中，也就是创建网络通信组件的配置那一步，已经创建了**acceptThread**和**selectorThreads**，并且在**AcceptThread**的构造方法中将创建好的**selectorThreads**数组传入

- **AcceptThread**的构造方法还将父类的 selector 对象注册到 ServerSocketChannel中，同时设置这个对象只对ACCEPT事件感兴趣，所以它就是专门用于接收新的请求的

  ```java
  // configure 方法创建 AcceptThread 对象
  //创建selector线程
  for (int i = 0; i < numSelectorThreads; ++i) {
      selectorThreads.add(new SelectorThread(i));
  }
  this.ss = ServerSocketChannel.open();
  acceptThread = new AcceptThread(ss, addr/*InetSocketAddress 就是个地址*/, selectorThreads);
  ```

  

- 在AcceptThread接受到新的请求时，由于其构造决定了只会由**OP_ACCEPT** 唤醒，所以每次有新请求都唤醒然后去处理

- **AcceptThread**接收到新的请求后，通过SocketChannel sc = acceptSocket.accept(); 并将SocketChannel给**selectorThread**。

### 3.2.2 SelectorThread 源码

![image-20211030232613718](09_zookeeper源码01.assets/image-20211030232613718.png)

该线程的主要作用是从Socket读取数据，并封装成 workRequest ，并将 workRequest 交给workerPool 工作线程池处理，同时将acceptedQueue中未处理的链接取出，并为每个链接绑定OP_READ 读事件，并封装对应的上下文对象 NIOServerCnxn 。

```java
public class SelectorThread extends AbstractSelectThread {

    private final int id;
    //acceptedQueue客户端链接队列
    //在selector thread中.其中包含了accept thread接收的客户端连接,
    //由selector thread负责将客户端连接注册到selector上,监听OP_READ和OP_WRITE.
    private final Queue<SocketChannel> acceptedQueue;
    //updateQueue就是存放那些需要监听OP_READ和OP_WRITE事件的socketChannel
    //updateQueue和acceptedQueue一样,也是LinkedBlockingQueue<SocketChannel>类型的,在selector thread中
    private final Queue<SelectionKey> updateQueue;

    public SelectorThread(int id) throws IOException {
        super("NIOServerCxnFactory.SelectorThread-" + id);
        this.id = id;
        acceptedQueue = new LinkedBlockingQueue<SocketChannel>();
        updateQueue = new LinkedBlockingQueue<SelectionKey>();
    }

    /**
     * 将新的请求添加到acceptedQueue，并唤醒SelectorThread
     */
    public boolean addAcceptedConnection(SocketChannel accepted) {
        //将accepted添加到acceptedQueue
        if (stopped || !acceptedQueue.offer(accepted)) {
            return false;
        }
        wakeupSelector();
        return true;
    }

    /**
         * Place interest op update requests onto a queue so that only the
         * selector thread modifies interest ops, because interest ops
         * reads/sets are potentially blocking operations if other select
         * operations are happening.
         */
    public boolean addInterestOpsUpdateRequest(SelectionKey sk) {
        if (stopped || !updateQueue.offer(sk)) {
            return false;
        }
        wakeupSelector();
        return true;
    }

    /**
         * The main loop for the thread selects() on the connections and
         * dispatches ready I/O work requests, then registers all pending
         * newly accepted connections and updates any interest ops on the
         * queue.
         */
    public void run() {
        try {
            while (!stopped) {
                try {
                    //调用select()读取就绪的IO事件,交由worker thread处理,在ZookeeperServer的processPacket()中处理数据
                    select();
                    //把acceptedQueue队列中接收的连接，取出来注册OP_READ读事件，（这里只读）
                    //并添加NIOServerCnxn对象与当前key绑定。
                    //相当于给每个连接添加附加对象NIOServerCnxn（上下文对象）
                    processAcceptedConnections();
                    //遍历所有updateQueue，更新updateQueue中连接的监听事件（这里加了写）
                    processInterestOpsUpdateRequests();
                } catch (RuntimeException e) {
                    LOG.warn("Ignoring unexpected runtime exception", e);
                } catch (Exception e) {
                    LOG.warn("Ignoring unexpected exception", e);
                }
            }

            // Close connections still pending on the selector. Any others
            // with in-flight work, let drain out of the work queue.
            for (SelectionKey key : selector.keys()) {
                NIOServerCnxn cnxn = (NIOServerCnxn) key.attachment();
                if (cnxn.isSelectable()) {
                    cnxn.close(ServerCnxn.DisconnectReason.SERVER_SHUTDOWN);
                }
                cleanupSelectionKey(key);
            }
            SocketChannel accepted;
            while ((accepted = acceptedQueue.poll()) != null) {
                fastCloseSock(accepted);
            }
            updateQueue.clear();
        } finally {
            closeSelector();
            // This will wake up the accept thread and the other selector
            // threads, and tell the worker thread pool to begin shutdown.
            NIOServerCnxnFactory.this.stop();
            LOG.info("selector thread exitted run method");
        }
    }

    private void select() {
        try {
            //获取准备就绪事件 由selector.wakeup()唤醒
            selector.select();
            //获取准备就绪事件注册信息
            Set<SelectionKey> selected = selector.selectedKeys();
            ArrayList<SelectionKey> selectedList = new ArrayList<SelectionKey>(selected);
            Collections.shuffle(selectedList);
            Iterator<SelectionKey> selectedKeys = selectedList.iterator();
            while (!stopped && selectedKeys.hasNext()) {
                SelectionKey key = selectedKeys.next();
                selected.remove(key);

                if (!key.isValid()) {
                    cleanupSelectionKey(key);
                    continue;
                }
                //处理和key对应的数据
                if (key.isReadable() || key.isWritable()) {
                    //处理当前会话的网络IO
                    handleIO(key);
                } else {
                    LOG.warn("Unexpected ops in select {}", key.readyOps());
                }
            }
        } catch (IOException e) {
            LOG.warn("Ignoring IOException while selecting", e);
        }
    }

    /**
     * Schedule I/O for processing on the connection associated with
     * the given SelectionKey. If a worker thread pool is not being used,
     * I/O is run directly by this thread.
     */
    private void handleIO(SelectionKey key) {
        //SelectorThread封装到IOWorkRequest
        IOWorkRequest workRequest = new IOWorkRequest(this, key);
        NIOServerCnxn cnxn = (NIOServerCnxn) key.attachment();

        // Stop selecting this key while processing on its
        // connection
        cnxn.disableSelectable();
        key.interestOps(0);
        touchCnxn(cnxn);
        //创建WorkerService-将当前会话信息处理的任务添加到线程池中
        workerPool.schedule(workRequest);
    }

    /**
	 	 * 遍历已分配给该线程但尚未放置在选择器上的已接受连接的队列。
     */
    private void processAcceptedConnections() {
        SocketChannel accepted;
        //循环acceptedQueue中的所有请求
        while (!stopped && (accepted = acceptedQueue.poll()) != null) {
            SelectionKey key = null;
            try {
                //注册OP_READ事件
                key = accepted.register(selector, SelectionKey.OP_READ);
                //把链接封装成一个NIOServerCnxn对象（NIO上下文对象）
                NIOServerCnxn cnxn = createConnection(accepted, key, this);
                key.attach(cnxn);
                //把当前连接的NIOServerCnxn(NIO上下文对象)添加到cnxns集合
                addCnxn(cnxn);
            } catch (IOException e) {
                // register, createConnection
                cleanupSelectionKey(key);
                fastCloseSock(accepted);
            }
        }
    }

    /**
         * 遍历所有updateQueue，更新updateQueue中连接的监听事件（OP_READ或者OP_WRITE）
         */
    private void processInterestOpsUpdateRequests() {
        SelectionKey key;
        //遍历所有updateQueue，更新updateQueue中连接的监听事件
        while (!stopped && (key = updateQueue.poll()) != null) {
            if (!key.isValid()) {
                cleanupSelectionKey(key);
            }
            NIOServerCnxn cnxn = (NIOServerCnxn) key.attachment();
            if (cnxn.isSelectable()) {
                //为当前链接添加OP_READ或者OP_WRITE
                key.interestOps(cnxn.getInterestOps());
            }
        }
    }

}
```

执行流程：

- 在NIOServerCnxnFactory的configure 方法中new出来，本身继承自 AbstractSelectThread，AbstractSelectThread又是Selector的封装，所以 SelectorThread 也是Selector的封装
- 第一次 select()  方法中的 selector.select(); 阻塞，每次AcceptThread接受到新的连接请求，轮训选一个SelectorThread 出来，调用  addAcceptedConnection(SocketChannel accepted) 方法，
- addAcceptedConnection 会将SocketChannel放入**acceptedQueue** 队列中，并且唤醒 Selector，让阻塞过去。第一次过去的时候selector.selectedKeys();里面还没东西 (因为没注册绑定)
- 阻塞过去后调用 processAcceptedConnections()方法，为SocketChannel注册selector和OP_READ事件，封装NIOServerCnxn(NIO上下文对象)添加到cnxns集合。
- 然后去更新updateQueue中连接的监听事件，删掉无用的监听，方法：processInterestOpsUpdateRequests()
- 第二次进入select() ，这时候就有事件了，调用handleIO(key)方法，封装IOWorkRequest对象

### 3.2.3 IOWorkRequest源码

本类主要用于处理IO，但并未对数据做出处理，数据处理将有业务链对象RequestProcessor处理，调用关系图如下：

![image-20211031152705164](09_zookeeper源码01.assets/image-20211031152705164.png)

ZooKeeper 中通过 WorkerService 管理一组 worker thread 线程

```java
private final ArrayList<ExecutorService> workers = new ArrayList<ExecutorService>();
```

前面我们在看 SelectorThread 的时候，能够看到 workerPool 的schedule方法被执行

```java
/**
 * IOWorkRequest is a small wrapper class to allow doIO() calls to be
 * run on a connection using a WorkerService.
 */
private class IOWorkRequest extends WorkerService.WorkRequest {

    //要执行的SelectorThread
    private final SelectorThread selectorThread;
    private final SelectionKey key;
    private final NIOServerCnxn cnxn;

    IOWorkRequest(SelectorThread selectorThread, SelectionKey key) {
        this.selectorThread = selectorThread;
        this.key = key;
        this.cnxn = (NIOServerCnxn) key.attachment();
    }

    /***
     * 数据读取工作
     * @throws InterruptedException
     */
    public void doWork() throws InterruptedException {
        if (!key.isValid()) {
            selectorThread.cleanupSelectionKey(key);
            return;
        }

        if (key.isReadable() || key.isWritable()) {
            //执行IO数据处理
            cnxn.doIO(key);

            // Check if we shutdown or doIO() closed this connection
            if (stopped) {
                cnxn.close(ServerCnxn.DisconnectReason.SERVER_SHUTDOWN);
                return;
            }
            if (!key.isValid()) {
                selectorThread.cleanupSelectionKey(key);
                return;
            }
            touchCnxn(cnxn);
        }

        // Mark this connection as once again ready for selection
        cnxn.enableSelectable();
        // Push an update request on the queue to resume selecting
        // on the current set of interest ops, which may have changed
        // as a result of the I/O operations we just performed.
        if (!selectorThread.addInterestOpsUpdateRequest(key)) {
            cnxn.close(ServerCnxn.DisconnectReason.CONNECTION_MODE_CHANGED);
        }
    }

    @Override
    public void cleanup() {
        cnxn.close(ServerCnxn.DisconnectReason.CLEAN_UP);
    }
}
```

自己的理解：

本类就是对象的封装，封装了三个字段：SelectorThread，SelectionKey，NIOServerCnxn。然后由SelectorThread的handleIO方法封装并交给workerPool 执行。

workPool是线程池的封装，调用其schedule方法，会简介调用其里面封装的线程池，去执行 workRequest 中的doWork()方法，进而调用doIO()

```java
// SelectorThread 类
private void handleIO(SelectionKey key) {
    //SelectorThread封装到IOWorkRequest
    IOWorkRequest workRequest = new IOWorkRequest(this, key);
    NIOServerCnxn cnxn = (NIOServerCnxn) key.attachment();

    // Stop selecting this key while processing on its
    // connection
    cnxn.disableSelectable();
    key.interestOps(0);
    touchCnxn(cnxn);
    //创建WorkerService-将当前会话信息处理的任务添加到线程池中
    workerPool.schedule(workRequest);
}

// WorkService类：
private final ArrayList<ExecutorService> workers = new ArrayList<ExecutorService>();

public void schedule(WorkRequest workRequest) {
    schedule(workRequest, 0);
}

public void schedule(WorkRequest workRequest, long id) {
    if (stopped) {
        workRequest.cleanup();
        return;
    }
    // 封装ScheduledWorkRequest对象
    ScheduledWorkRequest scheduledWorkRequest = new ScheduledWorkRequest(workRequest);

    // If we have a worker thread pool, use that; otherwise, do the work
    // directly.
    int size = workers.size();
    if (size > 0) {
        try {
            // make sure to map negative ids as well to [0, size-1]
            int workerNum = ((int) (id % size) + size) % size;
            ExecutorService worker = workers.get(workerNum);
            // 执行 scheduledWorkRequest
            worker.execute(scheduledWorkRequest);
        } catch (RejectedExecutionException e) {
            LOG.warn("ExecutorService rejected execution", e);
            workRequest.cleanup();
        }
    } else {
        // When there is no worker thread pool, do the work directly
        // and wait for its completion
        scheduledWorkRequest.run();
    }
}

// 又是个小Task，用worker去执行
private class ScheduledWorkRequest implements Runnable {

    private final WorkRequest workRequest;

    ScheduledWorkRequest(WorkRequest workRequest) {
        this.workRequest = workRequest;
    }

    @Override
    public void run() {
        try {
            // Check if stopped while request was on queue
            if (stopped) {
                workRequest.cleanup();
                return;
            }
            // 又回来调用了 workRequest 的doWork方法
            workRequest.doWork();
        } catch (Exception e) {
            LOG.warn("Unexpected exception", e);
            workRequest.cleanup();
        }
    }
}

/***
 * IOWorkRequest类
 * 数据读取工作
 * @throws InterruptedException
 */
public void doWork() throws InterruptedException {
    if (!key.isValid()) {
        selectorThread.cleanupSelectionKey(key);
        return;
    }

    if (key.isReadable() || key.isWritable()) {
        //执行IO数据处理
        cnxn.doIO(key);

        // Check if we shutdown or doIO() closed this connection
        if (stopped) {
            cnxn.close(ServerCnxn.DisconnectReason.SERVER_SHUTDOWN);
            return;
        }
        if (!key.isValid()) {
            selectorThread.cleanupSelectionKey(key);
            return;
        }
        touchCnxn(cnxn);
    }

    // Mark this connection as once again ready for selection
    cnxn.enableSelectable();

    if (!selectorThread.addInterestOpsUpdateRequest(key)) {
        cnxn.close(ServerCnxn.DisconnectReason.CONNECTION_MODE_CHANGED);
    }
}
```

IOServerCnxn.doIO() -> readPayload() -> readRequest() -> ZookeeperServer.processPacket() 

processPacket() 里面能拿到发过来的数据

### 3.2.4 ConnectionExpirerThread剖析

后台启动 ConnectionExpirerThread 清理线程清理过期的 session ,线程中无限循环,执行工作如下

这东西在configure里初始化，在start() 里面启动

```java
/**
     * This thread is responsible for closing stale connections so that
     * connections on which no session is established are properly expired.
     */
private class ConnectionExpirerThread extends ZooKeeperThread {

    ConnectionExpirerThread() {
        super("ConnnectionExpirer");
    }

    public void run() {
        try {
            while (!stopped) {
                //获取需要等待的时间
                long waitTime = cnxnExpiryQueue.getWaitTime();
                //执行休眠
                if (waitTime > 0) {
                    Thread.sleep(waitTime);
                    continue;
                }
                //清理过期会话
                for (NIOServerCnxn conn : cnxnExpiryQueue.poll()) {
                    ServerMetrics.getMetrics().SESSIONLESS_CONNECTIONS_EXPIRED.add(1);
                    conn.close(ServerCnxn.DisconnectReason.CONNECTION_EXPIRED);
                }
            }

        } catch (InterruptedException e) {
            LOG.info("ConnnectionExpirerThread interrupted");
        }
    }
}
```

## 3.3 ZK通信优劣总结

Zookeeper在通信方面默认使用了NIO，并支持扩展Netty实现网络数据传输。相比传统IO，NIO在网络数据传输方面有很多明显优势：

1. 传统IO在处理数据传输请求时，针对每个传输请求生成一个线程，如果IO异常，那么线程阻塞，在IO恢复后唤醒处理线程。在同时处理大量连接时，会实例化大量的线程对象。每个线程的实例化和回收都需要消耗资源，jvm需要为其分配TLAB，然后初始化TLAB，最后绑定线程，线程结束时又需要回收TLAB，这些都需要CPU资源。
2. NIO使用selector来轮询IO流，内部使用poll或者epoll，以事件驱动形式来相应IO事件的处理。同一时间只需实例化很少的线程对象，通过对线程的复用来提高CPU资源的使用效率。
3. CPU轮流为每个线程分配时间片的形式，间接的实现单物理核处理多线程。当线程越多时，每个线程分配到的时间片越短，或者循环分配的周期越长，CPU很多时间都耗费在了线程的切换上。线程切换包含线程上个线程数据的同步(TLAB同步)，同步变量同步至主存，下个线程数据的加载等等，他们都是很耗费CPU资源的。
4. 在同时处理大量连接，但活跃连接不多时，NIO的事件响应模式相比于传统IO有着极大的性能提升。NIO还提供了FileChannel，以zero-copy的形式传输数据，相较于传统的IO，数据不需要拷贝至用户空间，可直接由物理硬件(磁盘等)通过内核缓冲区后直接传递至网关，极大的提高了性能。
5. NIO提供了MappedByteBuffer，其将文件直接映射到内存（这里的内存指的是虚拟内存，并不是物理内存），能极大的提高IO吞吐能力。

ZK在使用NIO通信虽然大幅提升了数据传输能力，但也存在一些代码诟病问题：

1. Zookeeper通信源码部分学习成本高，需要掌握NIO和多线程
2. 多线程使用频率高，消耗资源多，但性能得到提升
3. Zookeeper数据处理调用链路复杂，多处存在内部类，代码结构不清晰，写法比较经典



# 4. RequestPorcess处理请求源码剖析

## 4.1 RequestPorcess 的创建

在程序启动，进入 ZookeeperMain 的 runFromConfig() 方法后，调用 cnxnFactory.startup(zkServer);最终调用如下方法：

```java
@Override
public void startup(ZooKeeperServer zks, boolean startServer) throws IOException, InterruptedException {
    //NIO启动对应线程
    start();
    //设置Zookeeper的ServerCnxnFactory（客户端与服务端进行通信的对象，就是当前对象NIOServerCnxnFactory）
    setZooKeeperServer(zks);
    if (startServer) {
        //加载会话和数据
        zks.startdata();
        //启动Zookeeper服务 在这里创建了 RequestPorcess 
        zks.startup();
    }
}

public synchronized void startup() {
    startupWithServerState(State.RUNNING);
}

/****
     * 启动Zookeeper服务
     * @param state
     */
private void startupWithServerState(State state) {
    //创建sessionTracker，它是ZooKeeperServer用来跟踪会话的基本接口。
    if (sessionTracker == null) {
        createSessionTracker();
    }
    //启动SessionTracker
    startSessionTracker();
    //======创建请求处理器链【创建业务处理器】 RequestProcessors!!!
    setupRequestProcessors();
    //启动节流器
    startRequestThrottler();
    //注册JMX
    registerJMX();
    //启动JVM监控
    startJvmPauseMonitor();
    //设置Zookeeper上下文参数
    registerMetrics();
    //设置状态为运行中
    setState(state);
    //启动节点路径计数器
    requestPathMetricsCollector.start();
    //关闭本地会话
    localSessionEnabled = sessionTracker.isLocalSessionsEnabled();
    //唤醒所有等待的线程
    notifyAll();
}

/**
 * 初始化业务处理流程
 */
protected void setupRequestProcessors() {
    //创建FinalRequestProcessor
    RequestProcessor finalProcessor = new FinalRequestProcessor(this);
    //创建SyncRequestProcessor，并将finalProcessor作为它的下一个业务链
    RequestProcessor syncProcessor = new SyncRequestProcessor(this, finalProcessor);
    //启动syncProcessor
    ((SyncRequestProcessor) syncProcessor).start();
    //创建RequestProcessor，并作为第一个处理业务的RequestProcessor，将syncProcessor作为它的下一个业务链
    firstProcessor = new PrepRequestProcessor(this, syncProcessor);
    //启动firstProcessor
    ((PrepRequestProcessor) firstProcessor).start();
}
```

创建步骤：

1. 创建finalProcessor。
2. 创建syncProcessor，并将finalProcessor作为它的下一个业务链。
3. 启动syncProcessor。
4. 创建firstProcessor(PrepRequestProcessor)，将syncProcessor作为firstProcessor的下一个业务链。
5. 启动firstProcessor。

SyncRequestProcessor 和 PrepRequestProcessor 构造代码如下：

```java
/**
 * 创建SyncRequestProcessor
 * @param zks
 * @param nextProcessor  下一个责任链 FinalRequestProcessor
 */
public SyncRequestProcessor(ZooKeeperServer zks, RequestProcessor nextProcessor) {
    super("SyncThread:" + zks.getServerId(), zks.getZooKeeperServerListener());
    this.zks = zks;
    // 下一个责任链
    this.nextProcessor = nextProcessor;
    this.toFlush = new ArrayDeque<>(zks.getMaxBatchSize());
}

/**
 * 创建 PrepRequestProcessor
 * @param zks
 * @param nextProcessor 下一个责任链 SyncRequestProcessor
 */
public PrepRequestProcessor(ZooKeeperServer zks, RequestProcessor nextProcessor) {
    super(
        "ProcessThread(sid:" + zks.getServerId()
        + " cport:" + zks.getClientPort()
        + "):", zks.getZooKeeperServerListener());
    // 下一个责任链
    this.nextProcessor = nextProcessor;
    this.zks = zks;
    this.digestEnabled = ZooKeeperServer.isDigestEnabled();
    if (this.digestEnabled) {
        this.digestCalculator = new DigestCalculator();
    }
}
```

![image-20211031161658394](09_zookeeper源码01.assets/image-20211031161658394.png)

PrepRequestProcessor 和 SyncRequestProcessor 的结构一样，都是实现了 Thread 的一个线程，所以在这里初始化时便启动了这两个线程。

## 4.2 PrepRequestProcessor 剖析

PrepRequestProcessor 是请求处理器的第1个处理器 -- 主要任务是权限校验

ZooKeeperServer.processPacket()>submitRequest()>enqueueRequest()>RequestThrottler.submitRequest() ，我们来看下 RequestThrottler.submitRequest() 源码,它将当前请求添加到submittedRequests 队列中了，源码如下：

```java
// RequestThrottler.java
public void submitRequest(Request request) {
    if (stopping) {
        LOG.debug("Shutdown in progress. Request cannot be processed");
        dropRequest(request);
    } else {
        request.requestThrottleQueueTime = Time.currentElapsedTime();
        // 将当前请求添加到submittedRequests队列中
        submittedRequests.add(request);
    }
}
```

而 RequestThrottler 继承了  ZooKeeperCriticalThread > ZooKeeperThread > Thread ，也就是说当前 RequestThrottler 是个线程，我们看看它的 run 方法做了什么事，源码如下：

```java
// RequestThrottler.java
private final LinkedBlockingQueue<Request> submittedRequests = new LinkedBlockingQueue<Request>();

@Override
public void run() {
  try {
    while (true) {
      // 一些状态判断 略、、
      Request request = submittedRequests.take();
      //..
      // A dropped stale request will be null
      if (request != null) {
        if (request.isStale()) {
          ServerMetrics.getMetrics().STALE_REQUESTS.add(1);
        }
        final long elapsedTime = Time.currentElapsedTime() - request.requestThrottleQueueTime;
        ServerMetrics.getMetrics().REQUEST_THROTTLE_QUEUE_TIME.add(elapsedTime);
        if (shouldThrottleOp(request, elapsedTime)) {
          request.setIsThrottled(true);
          ServerMetrics.getMetrics().THROTTLED_OPS.add(1);
        }
        // 调用ZookeeperServer.submitRequestNow(request) 方法
        zks.submitRequestNow(request);
      }
    }
  } catch (InterruptedException e) {
    LOG.error("Unexpected interruption", e);
  }
  int dropped = drainQueue();
  LOG.info("RequestThrottler shutdown. Dropped {} requests", dropped);
}

```

RequestThrottler 调用了 ZooKeeperServer.submitRequestNow() 方法，而该方法又调用了firstProcessor 的方法，源码如下：

```java
//ZookeeperServer.java
public void submitRequestNow(Request si) {
	// 略..
    try {
        touch(si.cnxn);
        boolean validpacket = Request.isValid(si.type);
        if (validpacket) {
            setLocalSessionFlag(si);
            // firstProcessor 就是PreRequestProcessor
            firstProcessor.processRequest(si);
            if (si.cnxn != null) {
                incInProcess();
            }
        } else {
            LOG.warn("Received packet at server of unknown type {}", si.type);
            // Update request accounting/throttling limits
            requestFinished(si);
            new UnimplementedRequestProcessor().processRequest(si);
        }
    } catch (MissingSessionException e) {
        LOG.debug("Dropping request.", e);
        // Update request accounting/throttling limits
        requestFinished(si);
    } catch (RequestProcessorException e) {
        LOG.error("Unable to process request", e);
        // Update request accounting/throttling limits
        requestFinished(si);
    }
}
```

ZooKeeperServer.submitRequestNow() 方法调用了 firstProcessor.processRequest() 方法，而这里的 firstProcessor 就是初始化业务处理链中的 PrepRequestProcessor ，也就是说三个RequestProecessor 中最先调用的是 PrepRequestProcessor 。

PrepRequestProcessor.processRequest() 方法将当前请求添加到了队列 submittedRequests 中，源码如下：

```java
// PrepRequestProcessor.java
LinkedBlockingQueue<Request> submittedRequests = new LinkedBlockingQueue<Request>();

public void processRequest(Request request) {
    request.prepQueueStartTime = Time.currentElapsedTime();
    submittedRequests.add(request);
    ServerMetrics.getMetrics().PREP_PROCESSOR_QUEUED.add(1);
}
```

上面方法中并未从 submittedRequests 队列中获取请求，如何执行请求的呢，因为PrepRequestProcessor 是一个线程，因此会在 run 中执行，我们查看 run 方法源码的时候发现它调用了 pRequest() 方法， pRequest() 方法源码如下：

```java
protected void pRequest(Request request) throws RequestProcessorException {
    // LOG.info("Prep>>> cxid = " + request.cxid + " type = " +
    // request.type + " id = 0x" + Long.toHexString(request.sessionId));
    request.setHdr(null);
    request.setTxn(null);

    if (!request.isThrottled()) {
        // 处理业务流程  过滤，权限校验
        pRequestHelper(request);
    }

    request.zxid = zks.getZxid();
    long timeFinishedPrepare = Time.currentElapsedTime();
    ServerMetrics.getMetrics().PREP_PROCESS_TIME.add(timeFinishedPrepare - request.prepStartTime);
    // 交给下一个业务链处理
    nextProcessor.processRequest(request);
    ServerMetrics.getMetrics().PROPOSAL_PROCESS_TIME.add(Time.currentElapsedTime() - timeFinishedPrepare);
}
```

首先先执行 pRequestHelper() 方法，该方法是 PrepRequestProcessor 处理核心业务流程，主要是一些过滤操作，操作完成后，会将请求交给下一个业务链，也就是SyncRequestProcessor.processRequest() 方法处理请求。
我们来看一下 PrepRequestProcessor.pRequestHelper() 方法做了哪些事，源码如下：

![image-20211031190927921](09_zookeeper源码01.assets/image-20211031190927921.png)

从上面源码可以看出 PrepRequestProcessor.pRequestHelper() 方法判断了客户端操作类型，但无论哪种操作类型几乎都调用了 pRequest2Txn() 方法。 而这个方法主要也是做权限校验的。

## 4.3 SyncRequestProcessor 剖析

SyncRequestProcessor ，该处理器主要是将请求数据高效率存入磁盘，并且请求在写入磁盘之前是不会被转发到下个处理器的。

我们先看请求被添加到队列的方法：

```java
// SyncRequestProcessor.java
private final BlockingQueue<Request> queuedRequests = new LinkedBlockingQueue<Request>();

public void processRequest(final Request request) {
    Objects.requireNonNull(request, "Request cannot be null");

    request.syncQueueStartTime = Time.currentElapsedTime();
    // 将请求添加到queuedRequests中
    queuedRequests.add(request);
    ServerMetrics.getMetrics().SYNC_PROCESSOR_QUEUED.add(1);
}
```

同样 SyncRequestProcessor 是一个线程，执行队列中的请求也在线程中触发，我们看它的run方法，
源码如下：

```java
@Override
public void run() {
    try {
        // we do this in an attempt to ensure that not all of the servers
        // in the ensemble take a snapshot at the same time
        //避免所有的server都同时进行snapshot，重置是否需要snapshot判断相关的统计
        resetSnapshotStats();
        lastFlushTime = Time.currentElapsedTime();
        while (true) {
            ServerMetrics.getMetrics().SYNC_PROCESSOR_QUEUE_SIZE.add(queuedRequests.size());

            long pollTime = Math.min(zks.getMaxWriteQueuePollTime(), getRemainingDelay());
            //获取一个需要处理的请求queuedRequests
            Request si = queuedRequests.poll(pollTime, TimeUnit.MILLISECONDS);
            if (si == null) {
                /* We timed out looking for more writes to batch, go ahead and flush immediately */
                flush();
                //阻塞方法获取一个请求
                si = queuedRequests.take();
            }

            if (si == REQUEST_OF_DEATH) {
                break;
            }

            long startProcessTime = Time.currentElapsedTime();
            ServerMetrics.getMetrics().SYNC_PROCESSOR_QUEUE_TIME.add(startProcessTime - si.syncQueueStartTime);

            // 跟踪写入日志的记录数量
            if (!si.isThrottled() && zks.getZKDatabase().append(si)) {
                //shouldSnapshot()：保存快照条件：根据logCount日志数量和logSize日志大小与snapCount快照的比较，具有随机性。
                if (shouldSnapshot()) {
                    //重置是否需要snapshot判断相关的统计
                    resetSnapshotStats();
                    //重置自上次rollLog以来的txn数量
                    zks.getZKDatabase().rollLog();
                    // take a snapshot
                    if (!snapThreadMutex.tryAcquire()) {
                        LOG.warn("Too busy to snap, skipping");
                    } else {
                        new ZooKeeperThread("Snapshot Thread") {
                            //创建线程保存快照数据
                            public void run() {
                                try {
                                    //保存快照数据
                                    zks.takeSnapshot();
                                } catch (Exception e) {
                                    LOG.warn("Unexpected exception", e);
                                } finally {
                                    snapThreadMutex.release();
                                }
                            }
                        }.start();
                    }
                }
            } else if (toFlush.isEmpty()) {
                // optimization for read heavy workloads
                // iff this is a read or a throttled request(which doesn't need to be written to the disk),
                // and there are no pending flushes (writes), then just pass this to the next processor
                if (nextProcessor != null) {
                    nextProcessor.processRequest(si);
                    if (nextProcessor instanceof Flushable) {
                        ((Flushable) nextProcessor).flush();
                    }
                }
                continue;
            }
            //将当前请求添加到toFlush队列中，toFlush队列是已经写入并等待刷新到磁盘的事务
            toFlush.add(si);
            if (shouldFlush()) {
                //提交数据
                flush();
            }
            ServerMetrics.getMetrics().SYNC_PROCESS_TIME.add(Time.currentElapsedTime() - startProcessTime);
        }
    } catch (Throwable t) {
        handleException(this.getName(), t);
    }
    LOG.info("SyncRequestProcessor exited!");
}
```

run 方法会从 queuedRequests 队列中获取一个请求，如果获取不到就会阻塞等待直到获取到一个请求对象，程序才会继续往下执行，接下来会调用 Snapshot Thread 线程实现将客户端发送的数据以快照的方式写入磁盘，最终调用 flush() 方法实现数据提交， flush() 方法源码如下：

```java
/***
     * 提交刷新事务
     * @throws IOException
     * @throws RequestProcessorException
     */
private void flush() throws IOException, RequestProcessorException {
    if (this.toFlush.isEmpty()) {
        return;
    }

    ServerMetrics.getMetrics().BATCH_SIZE.add(toFlush.size());

    long flushStartTime = Time.currentElapsedTime();
    //数据提交
    zks.getZKDatabase().commit();
    ServerMetrics.getMetrics().SYNC_PROCESSOR_FLUSH_TIME.add(Time.currentElapsedTime() - flushStartTime);

    if (this.nextProcessor == null) {
        this.toFlush.clear();
    } else {
        //this.toFlush.isEmpty()不为空，上面往toFlush中添加了当前请求
        while (!this.toFlush.isEmpty()) {
            final Request i = this.toFlush.remove();
            long latency = Time.currentElapsedTime() - i.syncQueueStartTime;
            ServerMetrics.getMetrics().SYNC_PROCESSOR_QUEUE_AND_FLUSH_TIME.add(latency);
            //执行下一个业务链对象,下一个业务链对象是FinalRequestProcessor
            this.nextProcessor.processRequest(i);
        }
        if (this.nextProcessor instanceof Flushable) {
            ((Flushable) this.nextProcessor).flush();
        }
    }
    lastFlushTime = Time.currentElapsedTime();
}
```

flush() 方法实现了数据提交，并且会将请求交给下一个业务链，下一个业务链为FinalRequestProcessor 。

## 4.4 FinalRequestProcessor 剖析

处理链中最后的一个处理器FinalRequestProcessor ,该业务处理对象主要用于返回Response。

就是相应给客户端的。懒得写了

在FinalRequestProcessor类中  public void processRequest(Request request) ； 方法上

## 4.5 ZK业务链处理优劣总结

Zookeeper业务链处理，思想遵循了AOP思想，但并未采用相关技术，为了提升效率，仍然大幅使用到了多线程。正因为有了业务链路处理先后顺序，似的Zookeeper业务处理流程更清晰更容易理解，但大量混入了多线程，也使得学习成本增加。

