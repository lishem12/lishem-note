# 1.Dubbo架构体系

官网  https://dubbo.apache.org/zh/

## 1.1 框架介绍

### 1.1.1 概述

Dubbo是阿里巴巴公司开源的一个高性能优秀的服务框架，使得应用可通过高性能的 RPC 实现服务的输出和输入功能，可以和 Spring框架无缝集成。

Dubbo是一款高性能、轻量级的开源Java RPC框架，它提供了三大核心能力：面向接口的远程方法调用，智能容错和负载均衡，以及服务自动注册和发现。  

### 1.1.2 运行架构

dubbo运行架构如下图示 ：

![dubbo-architucture](11_dubbo源码01.assets/dubbo-architecture.jpg)

节点角色说明：

- Provider：暴露服务的服务提供方
- Consumer：调用远程服务的服务消费方
- Registry：服务注册与发现的注册中心
- Monitor：统计服务的调用次数和调用时间的监控中心
- Container：服务运行容器

调用关系说明：

1. **服务容器(Container)**负责启动，加载，运行服务提供者。
2. 服务提供者在启动时，向**注册中心(Registry)**注册自己提供的服务。
3. 服务消费者在启动时，向注册中心**订阅**自己所需的服务。
4. 注册中心返回服务提供者地址列表给消费者，如果有变更，注册中心将基于长连接推送变更数据给消费者。
5. 服务消费者，从提供者地址列表中，基于**软负载均衡算法**，选一台提供者进行调用，如果调用失败，再选另一台调用。
6. 服务消费者和提供者，在内存中累计调用次数和调用时间，定时每分钟发送一次统计数据到监控中心。  

### 1.1.3 整体设计

![image-20211109184130315](11_dubbo源码01.assets/image-20211109184130315.png)

图例说明：

- 图中左边淡蓝背景的为**服务消费**方使用的接口，右边淡绿色背景的为**服务提供**方使用的接口，位于中轴线上的为双方都用到的接口。
- 图中从下至上分为十层，各层均为单向依赖，右边的黑色箭头代表层之间的依赖关系，每一层都可以剥离上层被复用，其中，Service 和 Config 层为 **API**，其它各层均为 **SPI**。
- 图中绿色小块的为扩展接口，蓝色小块为实现类，图中只显示用于关联各层的实现类。
- 图中蓝色虚线为初始化过程，即启动时组装链，红色实线为方法调用过程，即运行时调时链，紫色三角箭头为继承，可以把子类看作父类的同一个节点，线上的文字为调用的方法。  

各层说明：

- **config 配置层**：对外配置接口，以 ServiceConfig , ReferenceConfig 为中心，可以直接初始化配置类，也可以通过 spring 解析配置生成配置类。
- **proxy 服务代理层**：服务接口透明代理，生成服务的客户端 Stub 和服务器端 Skeleton, 以ServiceProxy 为中心，扩展接口为 ProxyFactory
- **registry 注册中心层**：封装服务地址的注册与发现，以服务 URL 为中心，扩展接口为RegistryFactory , Registry , RegistryService
- **cluster 路由层**：封装多个提供者的路由及负载均衡，并桥接注册中心，以 Invoker 为中心，扩展接口为 Cluster , Directory , Router , LoadBalance
- **monitor 监控层**：RPC 调用次数和调用时间监控，以 Statistics 为中心，扩展接口为MonitorFactory , Monitor , MonitorService
- **protocol 远程调用层**：封装 RPC 调用，以 Invocation , Result 为中心，扩展接口为Protocol , Invoker , Exporter
- **exchange 信息交换层**：封装请求响应模式，同步转异步，以 Request , Response 为中心，扩展接口为 Exchanger , ExchangeChannel , ExchangeClient , ExchangeServer
- **transport 网络传输层**：抽象 mina 和 netty 为统一接口，以 Message 为中心，扩展接口为Channel , Transporter , Client , Server , Codec
- **serialize 数据序列化层**：可复用的一些工具，扩展接口为 Serialization , ObjectInput ,ObjectOutput , ThreadPool

## 1.2 环境搭建

### 1.2.1 源码拉取

https://github.com/apache/dubbo

![image-20211110104604162](11_dubbo源码01.assets/image-20211110104604162.png)

找不到对应版本记得切到Tags里面，本文用的是2.7.8

然后进入下载的主目录，运行

```shell
mvn install -Dmaven.test.skip
```

大概10-20分钟，然后执行

```shell
mvn idea:idea
```

然后用idea 打开就可以调试了。

就是每次运行build的特别慢

### 1.2.2 源码结构

通过下图可以大致了解到Dubbo源码各个模块的作用：

![image-20211110104841854](11_dubbo源码01.assets/image-20211110104841854.png)

模块说明：

- **dubbo-common 公共逻辑模块**：包括Util类和通用模型
- **dubbo-remoting 远程通讯模块**：相当于Dubbo协议的实现，如果RPC用RMI协议则不需要使用此包
- **dubbo-rpc 远程调用模块**：抽象各种协议，以及动态代理，只包含一对一的调用，不关心集群的管理
- **dubbo-cluster 集群模块**：将多个服务提供方伪装成一个提供方，包括：负载均衡、容错、路由等。集群的地址列表可以是静态配置的，也可以是由注册中心下发的。
- **dubbo-registry 注册中心模块**：基于注册中心 下发地址 的集群方式，以及对各种注册中心的抽象。
- **dubbo-monitor 监控模块**：统计服务调用次数，调用时间，调用链路跟踪的服务。
- **dubbo-config 配置模块**：是Dubbo对外的API，用户通过Config使用Dubbo，隐藏Dubbo所有细节。
- **dubbo-container 容器模块**：是一个Standlone(独立启动)的容器，以简单的Main加载Spring启动，因为服务通常不需要Tomcat/JBoss等Web容器特性，没必要用Web容器去加载服务。

### 1.2.3 环境导入和管理控制台

1. dubbo-dubbo-2.7.8 貌似没什么问题，打开，导包就OK了   https://github.com/apache/dubbo

2. dubbo-admin-0.3.0 好像要改两个POM文件  https://github.com/apache/dubbo-admin

3. 需要一个Zookeeper 做注册中心，新的Zookeeper 记得在打开的时候，conf 文件夹下搞一个 zoo.cfg，然后启动

4. 在dubbo-admin-server给那个 admin项目配置一个端口号 server.port=7001，然后启动DubboAdminApplication 这个类。
5. 访问 127.0.0.1:7001 用户名密码是  root
6. 即可访问 dubbo-admin 项目

> 也可以给admin项目打好jar包，以后就用这个jar包
>
> ```powershell
> mvn clean package -Dmaven.test.skip=true
> ```
>
> 构建成功后在 dubbo-admin-distribution/target/里面可以找到dubbo-admin-0.3.0.jar
>
> 然后用一下命令运行这个jar包就可以直接运行admin项目
>
> ```shell
> java -jar dubbo-admin-0.3.0.jar
> ```

# 2. Dubbo实战运用

## 2.1 Dubbo与SpringBoot的整合

基于Zookeeper实现Dubbo与Spring Boot的集成整合。

![image-20211112162916695](11_dubbo源码01.assets/image-20211112162916695.png)

### 2.1.1 工程POM依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.lishem</groupId>
    <artifactId>dubbo-spring</artifactId>
    <version>1.0-SNAPSHOT</version>
    <modules>
        <module>dubbo-spring-interface</module>
        <module>dubbo-spring-provider</module>
        <module>dubbo-spring-consumer</module>
    </modules>
    <packaging>pom</packaging>
    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <dubbo-version>2.7.8</dubbo-version>
        <spring-boot.version>2.3.0.RELEASE</spring-boot.version>
    </properties>
    <dependencyManagement>
        <dependencies>
            <!-- Spring Boot -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <!-- Apache Dubbo  -->
            <dependency>
                <groupId>org.apache.dubbo</groupId>
                <artifactId>dubbo-dependencies-bom</artifactId>
                <version>${dubbo-version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>

            <dependency>
                <groupId>org.apache.dubbo</groupId>
                <artifactId>dubbo</artifactId>
                <version>${dubbo-version}</version>
                <exclusions>
                    <exclusion>
                        <groupId>org.springframework</groupId>
                        <artifactId>spring</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>javax.servlet</groupId>
                        <artifactId>servlet-api</artifactId>
                    </exclusion>
                    <exclusion>
                        <groupId>log4j</groupId>
                        <artifactId>log4j</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- Dubbo Spring Boot Starter -->
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
            <version>${dubbo-version}</version>
        </dependency>
        <!-- Dubbo核心组件 -->
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo</artifactId>
        </dependency>
        <!--Spring Boot 依赖 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>${spring-boot.version}</version>
        </dependency>
        <!-- Zookeeper客户端框架 -->
        <dependency>
            <groupId>org.apache.curator</groupId>
            <artifactId>curator-framework</artifactId>
            <version>4.0.1</version>
        </dependency>
        <!-- Zookeeper dependencies -->
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-dependencies-zookeeper</artifactId>
            <version>${dubbo-version}</version>
            <type>pom</type>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-log4j12</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>
</project>
```

### 2.1.2 公用RPC接口工程

为便于客户端与服务端的RPC接口引用， 这里对RPC接口做统一封装

![image-20211112164834130](11_dubbo源码01.assets/image-20211112164834130.png)

```java
public interface OrderService {

    /**
     * 获取订单详情
     * @param orderId
     * @return
     */
    String getOrder(Long orderId);
}
```

### 2.1.3 服务端工程

工程结构：

![image-20211112171239271](11_dubbo源码01.assets/image-20211112171239271.png)

POM依赖：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>dubbo-spring</artifactId>
        <groupId>com.lishem</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>dubbo-spring-provider</artifactId>

    <properties>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
    </properties>

    <dependencies>
        <!-- Dubbo Spring Boot Starter -->
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
            <version>${dubbo-version}</version>
        </dependency>
        <!-- Dubbo 核心依赖 -->
        <dependency>
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo</artifactId>
        </dependency>
        <!-- 公用RPC接口依赖 -->
        <dependency>
            <groupId>com.lishem</groupId>
            <artifactId>dubbo-spring-interface</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>
</project>
```

RPC接口服务:

```java
package com.lishem.dubbo.spring.provider.service;

import com.lishem.dubbo.spring.api.OrderService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Value;


@DubboService(version = "${dubbo.spring.provider.version}")
public class OrderServiceImpl implements OrderService {

    /**
     * 服务端口
     */
    @Value("${server.port}")
    private String serverPort;

    /**
     * 获取订单详情
     *
     * @param orderId
     * @return
     */
    @Override
    public String getOrder(Long orderId) {
        System.out.println("Service:"+serverPort);
        return "Get Order Detail, Id: " + orderId + ", serverPort: " +
            serverPort;
    }
}

```

工程配置：

```properties
# 服务端口
server.port=18081
# 应用程序名称
spring.application.name=dubbo-spring-provider
# Dubbo服务扫描路径
dubbo.scan.base-packages=com.lishem

# Dubbo 通讯协议
dubbo.protocol.name=dubbo
# Dubbo服务提供的端口， 配置为-1，代表为随机端口
dubbo.protocol.port=-1
#dubbo.protocol.port=28801

## Dubbo 注册器配置信息
dubbo.registry.address=zookeeper://127.0.0.1:2181
dubbo.registry.file = ${user.home}/dubbo-cache/${spring.application.name}/dubbo.cache
dubbo.spring.provider.version = 1.0.0
```

启动程序：

```java
package com.lishem.dubbo.spring.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.lishem"})
public class DubboSpringProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboSpringProviderApplication.class, args);
    }
}
```



### 2.1.4 消费端工程

工程结构：

![image-20211112213840119](11_dubbo源码01.assets/image-20211112213840119.png)

POM依赖：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>dubbo-spring</artifactId>
        <groupId>com.lishem</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>dubbo-spring-consumer</artifactId>

    <properties>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <!-- Dubbo Spring Boot Starter -->
            <groupId>org.apache.dubbo</groupId>
            <artifactId>dubbo-spring-boot-starter</artifactId>
            <version>${dubbo-version}</version>
        </dependency>
        <!-- 公用RPC接口依赖 -->
        <dependency>
            <groupId>com.lishem</groupId>
            <artifactId>dubbo-spring-interface</artifactId>
            <version>${project.version}</version>
        </dependency>
    </dependencies>
</project>
```

消费端调用:

```java
package com.lishem.dubbo.spring.consumer.controller;

import com.lishem.dubbo.spring.api.OrderService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     *  订单服务接口
     *
     */
    @DubboReference(version = "${dubbo.spring.provider.version}")
    private OrderService orderService;

    /**
     * 获取订单详情接口
     *
     * @param orderId
     * @return
     */
    @RequestMapping("/getOrder")
    @ResponseBody
    public String getOrder(Long orderId) {
        String result = null;
        try {
            result = orderService.getOrder(orderId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
```

工程配置:

```properties
# 服务端口
server.port=18082
#服务名称
spring.application.name=dubbo-spring-consumer
#服务版本号
dubbo.spring.provider.version = 1.0.0
#消费端注册器配置信息
dubbo.registry.address=zookeeper://127.0.0.1:2181
dubbo.registry.file = ${user.home}/dubbo-cache/${spring.application.name}/dubbo.cache
```

Spring Boot启动程序

```java
package com.lishem.dubbo.spring.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.lishem"})
public class DubboSpringConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboSpringConsumerApplication.class, args);
    }
}
```

### 2.1.5 工程调用验证

1. 启动zookeeper注册中心

2. 启动服务端，运行DubboSpringProviderApplication

3. 启动消费端， 运行DubboSpringConsumerApplication

4. 请求接口：http://127.0.0.1:18082/getOrder?orderId=1001

5. ![image-20211112214058839](11_dubbo源码01.assets/image-20211112214058839.png)

   

## 2.2 Dubbo高阶配置运用

### 2.2.1不同配置覆盖关系

![image-20211112214330192](11_dubbo源码01.assets/image-20211112214330192.png)

 上图 绿色是 生产方，绿色是消费方

配置规则：

- 方法级优先，接口次之，全局配置再次之
- 如果级别一样，消费方优先，提供方次之

注解配置：

```java
// ==============================生产方==============================
// 1. 方法级别
@DubboService(
    version = "${dubbo.spring.provider.version}"
    ,methods = {
        // 某个方法配置
        @Method(name = "getOrder", timeout = 2000)
    }
)
public class OrderServiceImpl implements OrderService {}

// 2. 接口级别
@DubboService(
    version = "${dubbo.spring.provider.version}"
    ,timeout = 1000
)
public class OrderServiceImpl implements OrderService {}

// 3. 全局配置
@Configuration
public class DubboConfig {
    /**
     * 服务端全局配置
     */
    public ProviderConfig registryConfig() {
        ProviderConfig config = new ProviderConfig();
        // 全局超时时间 1S
        config.setTimeout(1000);
        return config;
    }
}


// ==============================消费方==============================
// 1. 方法级别
// 在注入处
@DubboReference(
    version = "${dubbo.spring.provider.version}"
    , methods = {
        @Method(name = "getOrder", timeout = 1000)
    }
)
private OrderService orderService;

// 2. 接口级别
@DubboReference(
    version = "${dubbo.spring.provider.version}"
    ,timeout = 1000
)
private OrderService orderService;

// 3. 全局配置
@Configuration
public class DubboConfig {

    /**
     * 客户端全局操作
     */
    @Bean
    public ConsumerConfig  config(){
        ConsumerConfig config = new ConsumerConfig();
        config.setTimeout(4000);
        return config;
    }
}
```

### 2.2.2 属性配置优先级

![image-20211113124158739](11_dubbo源码01.assets/image-20211113124158739.png)

优先级从高到低：

- JVM -D 参数；
- XML(application.yaml/application.properties)配置会重写 dubbo.properties 中的；
- Properties默认配置，仅仅作用于以上两者没有配置时。

```shell
java -jar -Dserver.port=8001 dubbo-admin-0.3.0.jar
```

dubbo.properties: 一般配置一些公共属性。

XML等级：容易变更的数据

-D 运行时改变配置

### 2.2.3 容错处理与重试机制

1） 容错机制：

- Failfast Cluster

  快速失败，只发起一次调用，失败立即报错。通常用于非幂等性的写操作，比如新增记录。

- Failsafe Cluster

  失败安全，出现异常时，直接忽略。通常用于写入审计日志等操作。

- Failback Cluster

  失败自动恢复，后台记录失败请求，定时重发。通常用于消息通知操作。

- Forking Cluster

  并行调用多个服务器，只要一个成功即返回。通常用于实时性要求较高的读操作，但需要浪费更多服务资源。可通过 forks="2" 来设置最大并行数。

- Broadcast Cluster

  广播调用所有提供者，逐个调用，任意一台报错则报错。通常用于通知所有提供者更新缓存或日志等本地资源信息。

后面章节还会对其原理做详细讲解。

2）调整客户端重试次数

默认请求1次 重试2次，也就是总共3次。

目的是让请求更可靠，这个可以通过 消费方(客户端)更改

```java
@DubboReference(
    version = "${dubbo.spring.provider.version}"
    ,retries =  3
)
private OrderService orderService;
```

上面这样就会 请求1次，重试3次，一共访问4次。

在集群环境下，会遵循负载均衡，向不同服务端发起请求。

### 2.2.4 多版本控制

1. 启动三个服务端

   第一个服务端版本为1.0.0， 第二、三个服务端版本为2.0.0。

   修改application.properties配置：

   ```properties
   #服务版本号
   dubbo.spring.provider.version = 2.0.0
   ```

2. 消费端指定版本号为2.0.0

   修改application.properties配置：

   ```properties
   #服务版本号
   dubbo.spring.provider.version = 2.0.0
   ```

   在Dubbo自动注入的注解中，引用这个版本号：

   ```java
   @DubboReference(
       version = "${dubbo.spring.provider.version}" // 版本号，也可以直接在这里写死
       ,retries =  3
       ,loadbalance = "random"
   )
   private OrderService orderService;
   ```

   

3. 请求只会访问至版本号为2.0.0的服务节点上面。

### 2.2.5 本地存根调用

类似一种拦截器，执行远程调用之前，本地校验参数类型。

1） 实现流程：

![image-20211114115538706](11_dubbo源码01.assets/image-20211114115538706.png)

把 Stub 暴露给用户，Stub 可以决定要不要去调 Proxy。

2）客户端存根实现：

增加service接口，一般写在interface 项目里

![image-20211114120031597](11_dubbo源码01.assets/image-20211114120031597.png)

```java
public class OrderServiceStub implements OrderService {

    private final OrderService orderService;

    // 构造函数传入真正的远程代理对象
    public OrderServiceStub(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public String getOrder(Long orderId) throws InterruptedException {
        // 在客户端执行, 预先验证参数是否合法，等等
        try {
            if (null != orderId) {
                // 远程调用
                return orderService.getOrder(orderId);
            }
            return "参数校验错误！";
        } catch (Exception e) {
            //容错处理
            return "出现错误：" + e.getMessage();
        }
    }
}
```

3） 修改客户端调用配置

```java
@DubboReference(
    version = "${dubbo.spring.provider.version}"
    ,retries =  3
    ,loadbalance = "random"
    ,stub = "com.lishem.dubbo.spring.stub.OrderServiceStub" // 指定本地存根
)
private OrderService orderService;
```

### 2.2.6 负载均衡机制

Dubbo默认采用的是随机负载策略。

**1） Dubbo支持的负载均衡策略：**

- Random LoadBalance

  > 随机，按权重设置随机概率。 在一个截面上碰撞的概率高，但调用量越大分布越均匀，而且按概率使用权重后也比较均匀，有利于动态调整提供者权重。

- RoundRobin LoadBalance

  > 轮询，按公约后的权重设置轮询比率。 存在慢的提供者累积请求的问题，比如：第二台机器很慢，但没挂，当请求调到第二台时就卡在那，久而久之，所有请求都卡在调到第二台上。

- LeastActive LoadBalance

  > 最少活跃调用数，相同活跃数的随机，活跃数指调用前后计数差。 使慢的提供者收到更少请求，因为越慢的提供者的调用前后计数差会越大。

- ConsistentHash LoadBalance

  > 一致性 Hash，相同参数的请求总是发到同一提供者。 当某一台提供者挂时，原本发往该提供者的请求，基于虚拟节点，平摊到其它提供者，不会引起剧烈变动。

- ShortestResponse LoadBalance

  > 上次请求的最短响应时间，可以根据服务端的健康状况动态决定请求哪个服务端。

源码实现：org.apache.dubbo.rpc.cluster.loadbalance.AbstractLoadBalance

Dubbo提供了5种实现：

![image-20211113132506131](11_dubbo源码01.assets/image-20211113132506131.png)

2） Dubbo负载设置

负载均衡 是要客户端去设置的：

```java
// 客户端全局配置
@Configuration
public class DubboConfig {

    /**
     * 客户端全局操作
     */
    @Bean
    public ConsumerConfig  config(){
        ConsumerConfig config = new ConsumerConfig();
        // config.setTimeout(4000);
        // 负载均衡
        config.setLoadbalance("roundrobin"); // 轮训
        config.setLoadbalance("random"); // 随机
        config.setLoadbalance("consistenthash"); // 一致性哈希
        config.setLoadbalance("leastactive"); // 平均处理效率最高的节点
        config.setLoadbalance("shortestresponse"); // 最后一次相应时间最快的
        return config;
    }
}

// 接口配置

@DubboReference(
    version = "${dubbo.spring.provider.version}" // 多版本
    ,retries =  3 // 重试次数
    ,loadbalance = "roundrobin" // 负载均衡，轮训
)
private OrderService orderService;
```

这个调整权重。新版dubbo-admin  ：

![image-20211113134359312](11_dubbo源码01.assets/image-20211113134359312.png)

将两台节点的权重降低至20

![image-20211113134404646](11_dubbo源码01.assets/image-20211113134404646.png)

调整后可以看到权重配置已经生效：

![image-20211113134432574](11_dubbo源码01.assets/image-20211113134432574.png)



**3）四种配置方式**

优先级从上至下：

- 服务端服务级别

  ```xml
  <dubbo:service interface="..." loadbalance="roundrobin" />
  ```

- 客户端服务级别

  ```xml
  <dubbo:reference interface="..." loadbalance="roundrobin" />
  ```

- 服务端方法级别

  ```xml
  <dubbo:service interface="...">
      <dubbo:method name="..." loadbalance="roundrobin"/>
  </dubbo:service>
  ```

- 客户端方法级别

  ```xml
  <dubbo:reference interface="...">
      <dubbo:method name="..." loadbalance="roundrobin"/>
  </dubbo:reference>
  ```


### 2.2.7 服务降级运用

1）服务动态禁用

启动单个服务节点，进入Dubbo Admin，创建动态配置规则

```yaml
configVersion: v2.7
enabled: true
configs:
    - side: provider
    addresses:
    	- '0.0.0.0:20880'
    parameters:
        timeout: 3000
        disabled: true
```

将disabled属性设为true， 服务禁用， 可以错误提示：  

![image-20211114130219476](11_dubbo源码01.assets/image-20211114130219476.png)

将disabled属性改为false  

```java
configVersion: v2.7
enabled: true
configs:
	- side: provider
		addresses:
		    - '0.0.0.0:20880'
	parameters:
    	timeout: 3000
	    disabled: false
```

恢复正常访问：  

![image-20211114131207992](11_dubbo源码01.assets/image-20211114131207992.png)

2) 服务降级

进入Dubbo Admin进行配置：  

```yaml
configVersion: v2.7
enabled: true
configs:
    - side: consumer
        addresses:
        	- 0.0.0.0
    parameters:
        timeout: 3000
        mock: 'force:return null'
```

客户端调用， 会直接屏蔽， 并且服务端控制台不会有任何调用记录：  

![image-20211114131359576](11_dubbo源码01.assets/image-20211114131359576.png)

除了force 之外还有fail   方式



### 2.2.8 并发与连接 控制

execute 为key，优先级同2.2.1