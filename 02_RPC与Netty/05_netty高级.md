# 1.TCP粘包拆包问题

## 1.1 什么是TCP粘包半包问题

流传输，就是一串没有界限的数据。服务端收到数据，不确定是一条还是多条。

所以，客户端与服务端就需要约定好拆包的规则，客户端按照此规则进行粘包，而服务端按照此规则进
行拆包，这就是TCP的粘包与拆包，如果不约定好，就会出现服务端不能按照期望拿到数据。  

实际上，彼此约定的规则就是**协议**，自定义协议就是**自定义规则**。  

## 1.2 TCP粘包半包演示

### 1.2.1 服务端：

```java
package com.lishem.showproblem.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 演示粘包半包服务端
 */
public class NettyServer {

  public static void main(String[] args) throws Exception {
    // 主线程，不处理任何业务逻辑，只是接收客户的连接请求
    EventLoopGroup boss = new NioEventLoopGroup(1);
    // 工作线程，线程数默认是：cpu*2
    EventLoopGroup worker = new NioEventLoopGroup();

    try {
      // 服务器启动类
      ServerBootstrap serverBootstrap = new ServerBootstrap();
      serverBootstrap.group(boss, worker);
      //配置server通道
      serverBootstrap.channel(NioServerSocketChannel.class);
      //worker线程的处理器
      serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline().addLast(new ServerHandler());
        }
      });

      ChannelFuture future = serverBootstrap.bind(5566).sync();
      System.out.println("服务器启动完成。。。。。");
      //等待服务端监听端口关闭
      future.channel().closeFuture().sync();
    } finally {
      //优雅关闭
      boss.shutdownGracefully();
      worker.shutdownGracefully();
    }
  }
}
```

```java
package com.lishem.showproblem.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

public class ServerHandler extends SimpleChannelInboundHandler<ByteBuf> {

  private int count;

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
    System.out.println("服务端接收到消息：" + msg.toString(CharsetUtil.UTF_8));
    System.out.println("服务端接收到消息数量：" + (++count));
    ctx.writeAndFlush(Unpooled.copiedBuffer("ok", CharsetUtil.UTF_8));
  }
}
```

### 1.2.2 客户端：

```java
package com.lishem.showproblem.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * 演示粘包半包客户端
 */
public class NettyClient {

  public static void main(String[] args)throws Exception {
    EventLoopGroup worker = new NioEventLoopGroup();
    try {
      // 服务器启动类
      Bootstrap bootstrap = new Bootstrap();
      bootstrap.group(worker);
      bootstrap.channel(NioSocketChannel.class);
      bootstrap.handler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline().addLast(new ClientHandler());
        }
      });
      ChannelFuture future = bootstrap.connect("127.0.0.1", 5566).sync();
      future.channel().closeFuture().sync();
    }finally {
      worker.shutdownGracefully();
    }
  }
}
```

```java
package com.lishem.showproblem.client;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

public class ClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

  private int count;

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
    System.out.println("接收到服务端的消息：" + msg.toString(CharsetUtil.UTF_8));
    System.out.println("接收到服务端的消息数量：" + (++count));
  }

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    for (int i = 0; i < 10; i++) {
      ctx.writeAndFlush(Unpooled.copiedBuffer("from client a message!",
                                              CharsetUtil.UTF_8));
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    throws Exception {
    cause.printStackTrace();
    ctx.close();
  }
}
```

### 1.2.3 测试结果

```shell
# 服务端
服务器启动完成。。。。。
服务端接收到消息：from client a message!from client a message!from client a message!from client a message!from client a message!from client a message!from client a message!from client a message!from client a message!from client a message!
服务端接收到消息数量：1

# 客户端
接收到服务端的消息：ok
接收到服务端的消息数量：1
```

## 1.3 粘包半包解决方案

一般来讲有3种方法解决TCP粘包和半包问题

- 在发送的数据包中添加头，在头里存储数据的大小，服务端就可以按照此大小来读取数据，这样就知道界限在哪里了。
- 以固定的长度发送数据，超出的分多次发送，不足的以0填充，接收端就以固定长度接收即可。
- 在数据包之间设置边界，如添加特殊符号，这样，接收端通过这个边界就可以将不同的数据包拆分开

## 1.4 粘包半包解决方案实战

### 1.4.1 自定义协议

```java
package com.lishem.protocol;

public class MyProtocol {

  private Integer length; // 数据头，长度

  private byte[] body; // 数据体

  public Integer getLength() {
    return length;
  }

  public void setLength(Integer length) {
    this.length = length;
  }

  public byte[] getBody() {
    return body;
  }

  public void setBody(byte[] body) {
    this.body = body;
  }
}
```

### 1.4.2 编解码器

编码器继承自 MessageToByteEncoder

```java
public class MyEncoder extends MessageToByteEncoder<MyProtocol> {

  @Override
  protected void encode(ChannelHandlerContext ctx, MyProtocol msg, ByteBuf out) throws Exception {
    out.writeInt(msg.getLength());
    out.writeBytes(msg.getBody());
  }
}
```

解码器继承自ReplayingDecoder：

```java
public class MyDecoder extends ReplayingDecoder<MyProtocol> {

  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    int length = in.readInt(); //获取长度
    byte[] data = new byte[length]; //根据长度定义byte数组
    in.readBytes(data); //读取数据
    MyProtocol myProtocol = new MyProtocol();
    myProtocol.setLength(length);
    myProtocol.setBody(data);
    out.add(myProtocol);
  }
}
```

### 1.4.3 客户端实现

```java
public class NettyClient {

  public static void main(String[] args) throws Exception{
    EventLoopGroup worker = new NioEventLoopGroup();
    try {
      // 服务启动类
      Bootstrap bootstrap = new Bootstrap();
      bootstrap.group(worker);
      bootstrap.channel(NioSocketChannel.class);
      bootstrap.handler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline().addLast(new MyEncoder());
          ch.pipeline().addLast(new MyDecoder());
          ch.pipeline().addLast(new ClientHandler());

        }
      });
      ChannelFuture future = bootstrap.connect("127.0.0.1", 5566).sync();
      future.channel().closeFuture().sync();
    }finally {
      worker.shutdownGracefully();
    }
  }
}
```

```java
public class ClientHandler extends SimpleChannelInboundHandler<MyProtocol> {

  private int count;

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, MyProtocol msg) throws Exception {
    System.out.println("接收到服务端的消息：" + new String(msg.getBody(), CharsetUtil.UTF_8));
    System.out.println("接收到服务端的消息数量：" + (++count));
  }

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    for (int i = 0; i < 10; i++) {
      byte[] data = "from client a message!".getBytes(CharsetUtil.UTF_8);
      MyProtocol myProtocol = new MyProtocol();
      myProtocol.setLength(data.length);
      myProtocol.setBody(data);
      ctx.writeAndFlush(myProtocol);
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    cause.printStackTrace();
    ctx.close();
  }
}

```

### 1.4.4 服务端实现

```java
public class NettyServer {

  public static void main(String[] args) throws Exception {
    // 主线程，不处理任何业务逻辑，只是接收客户的连接请求
    EventLoopGroup boss = new NioEventLoopGroup(1);
    // 工作线程，线程数默认是：cpu*2
    EventLoopGroup worker = new NioEventLoopGroup();

    try{
      // 服务器启动类
      ServerBootstrap serverBootstrap = new ServerBootstrap();
      serverBootstrap.group(boss, worker);
      //配置server通道
      serverBootstrap.channel(NioServerSocketChannel.class);
      serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>(){
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline()
            .addLast(new MyDecoder())
            .addLast(new MyEncoder())
            .addLast(new ServerHandler());
        }
      });
      ChannelFuture future = serverBootstrap.bind(5566).sync();
      System.out.println("服务器启动完成。。。。。");
      //等待服务端监听端口关闭
      future.channel().closeFuture().sync();
    }finally {
      //优雅关闭
      boss.shutdownGracefully();
      worker.shutdownGracefully();
    }
  }
}
```

```java
public class ServerHandler extends SimpleChannelInboundHandler<MyProtocol> {

  private int count;

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, MyProtocol msg) throws Exception {

    System.out.println("服务端接收到消息：" + new String(msg.getBody(),
                                                CharsetUtil.UTF_8));
    System.out.println("服务端接收到消息数量：" + (++count));
    byte[] data = "ok".getBytes(CharsetUtil.UTF_8);
    MyProtocol myProtocol = new MyProtocol();
    myProtocol.setLength(data.length);
    myProtocol.setBody(data);
    ctx.writeAndFlush(myProtocol);
  }
}
```

### 1.4.5 测试

```shell
# 客户端
接收到服务端的消息：ok
接收到服务端的消息数量：1
接收到服务端的消息：ok
接收到服务端的消息数量：2
接收到服务端的消息：ok
接收到服务端的消息数量：3
接收到服务端的消息：ok
接收到服务端的消息数量：4
接收到服务端的消息：ok
接收到服务端的消息数量：5
接收到服务端的消息：ok
接收到服务端的消息数量：6
接收到服务端的消息：ok
接收到服务端的消息数量：7
接收到服务端的消息：ok
接收到服务端的消息数量：8
接收到服务端的消息：ok
接收到服务端的消息数量：9
接收到服务端的消息：ok
接收到服务端的消息数量：10
# 服务端
服务器启动完成。。。。。
服务端接收到消息：from client a message!
服务端接收到消息数量：1
服务端接收到消息：from client a message!
服务端接收到消息数量：2
服务端接收到消息：from client a message!
服务端接收到消息数量：3
服务端接收到消息：from client a message!
服务端接收到消息数量：4
服务端接收到消息：from client a message!
服务端接收到消息数量：5
服务端接收到消息：from client a message!
服务端接收到消息数量：6
服务端接收到消息：from client a message!
服务端接收到消息数量：7
服务端接收到消息：from client a message!
服务端接收到消息数量：8
服务端接收到消息：from client a message!
服务端接收到消息数量：9
服务端接收到消息：from client a message!
服务端接收到消息数量：10
```

# 2.Netty编解码器

## 2.1 什么是编解码器

在网络中传输数据时，无论以什么的格式发送（int、String、Long等）都会以字节流的方式进行传递，

将原来的格式数据转化为字节，称之为编码（encode）

将字节形式转化为原来的格式，称之为解码（decode）

编解码统称为codec。

编解码器包括编码器与解码器两部分，编码器负责出站数据操作，解码器负责入站数据操作

## 2.2 解码器及案例

### 2.2.1 解码器功能

解码器是负责入站的数据操作，那么解码器也一定实现了**ChannelInboundHandler**接口，所以编解码器本质上也是ChannelHandler。

Netty中提供了**ByteToMessageDecoder**的抽象实现，自定义解码器只需要继承该类，实现decode()即可。Netty也提供了一些常用的解码器实现，基本都是开箱即用的。比如：

- RedisDecoder 基于Redis协议的解码器
- XmlDecoder 基于XML格式的解码器
- JsonObjectDecoder 基于json数据格式的解码器
- HttpObjectDecoder 基于http协议的解码器

Netty也提供了**MessageToMessageDecoder**，将一种格式转化为另一种格式的解码器，也提供了一些实现：

- StringDecoder 将接收到ByteBuf转化为字符串
- ByteArrayDecoder 将接收到ByteBuf转化字节数组
- Base64Decoder 将由ByteBuf或US-ASCII字符串编码的Base64解码为ByteBuf。

### 2.2.2 解码器实现案例

将传入的字节流转为Integer类型

```java
/**
 * Byte转Integer的解码器
 */
public class ByteToIntegerDecoder extends ByteToMessageDecoder {

  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {

    // int类型占4个字节，所以需要判断是否存在4个字节
    if (in.readableBytes()>=4){
      //读取到int类型数据，放入到输出，完成数据类型的转化
      out.add(in.readInt());
    }
  }
}
```

在handler中使用：

```java
public class ServerHandler extends ChannelInboundHandlerAdapter {
  @Override
  public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    Integer i = (Integer) msg; //这里可以直接拿到Integer类型的数据
    System.out.println("服务端接收到的消息为：" + i);
  }
}
```

在pipeline中添加解码器：

```java
@Override
  protected void initChannel(SocketChannel ch) throws Exception {
  ch.pipeline()
    .addLast(new ByteToIntegerDecoder())
    .addLast(new ServerHandler());
}
```

## 2.3 解码器ReplayingDecoder

在前面案例中，当需要获取int数据时，需要进行判断是否够4个字节，如果解码业务过于复杂的话，这样的判断会显得非常的繁琐，在Netty中提供了ReplayingDecoder就可以解决这样的问题。

**ReplayingDecoder**也是继承了**ByteToMessageDecoder**进行的扩展。

如果用 ByteToMessageDecoder 解码：

```java
public class IntegerHeaderFrameDecoder extends ByteToMessageDecoder {
  @Override
  protected void decode(ChannelHandlerContext ctx,ByteBuf buf, List<Object> out) throws Exception {
    if (buf.readableBytes() < 4) {
      return;
    } 
    buf.markReaderIndex();
    int length = buf.readInt();
    if (buf.readableBytes() < length) {
      buf.resetReaderIndex();
      return;
    } 
    out.add(buf.readBytes(length));
  }
}
```

使用ReplayingDecoder后：

```java
public class IntegerHeaderFrameDecoder extends ReplayingDecoder<Void> {
  protected void decode(ChannelHandlerContext ctx,ByteBuf buf, List<Object> out) throws Exception {
    out.add(buf.readBytes(buf.readInt()));
  }
}
```

### 基本原理：

- 使用了特殊的ByteBuf，叫做ReplayingDecoderByteBuf，扩展了ByteBuf
- 重写了ByteBuf的readXxx()等方法，会先检查可读字节长度，一旦检测到不满足要求就直接抛出REPLAY（REPLAY继承ERROR）
- ReplayingDecoder重写了ByteToMessageDecoder的callDecode()方法，捕获Signal并在catch块中重置ByteBuf的readerIndex。
- 继续等待数据，直到有了数据后继续读取，这样就可以保证读取到需要读取的数据。
- 类定义中的泛型 S 是一个用于记录解码状态的状态机枚举类，在state(S s)、checkpoint(S s)等方法中会用到。在简单解码时也可以用java.lang.Void来占位。

需要注意：

- buffer的部分操作（readBytes(ByteBuffer dst)、retain()、release()等方法会直接抛出异常）
- 在某些情况下会影响性能（如多次对同一段消息解码）

TCP是基于流的，只保证接收到数据包分片顺序，而**不保证接收到的数据包每个分片大小**。因此在使用ReplayingDecoder时，即使不存在多线程，同一个线程也可能多次调用decode()方法。在decode中修改ReplayingDecoder的类变量时必须小心谨慎。

```java
// 这是个错误的例子
// 消息中包含了2个integer，代码中decode方法会被调用两次，此时队列size不等于2，这段代码达不到期望结果。
public class MyDecoder extends ReplayingDecoder<MyProtocol> {

  private final Queue<Integer> values = new LinkedList<Integer>();

  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    values.offer(buf.readInt());
    values.offer(buf.readInt());
    assert values.size() == 2;
    out.add(values.poll() + values.poll());
  }
}

// 正确做法
public class MyDecoder extends ReplayingDecoder<MyProtocol> {

  private final Queue<Integer> values = new LinkedList<Integer>();

  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    values.clear();
    values.offer(buf.readInt());
    values.offer(buf.readInt());
    assert values.size() == 2;
    out.add(values.poll() + values.poll());
  }
}
```

ByteToIntegerDecoder2的实现：（1 是继承 ByteToMessageDecoder）

```java
public class MyDecoder extends ReplayingDecoder<MyProtocol> {

  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    out.add(in.readInt()); //读取到int类型数据，放入到输出，完成数据类型的转化
  }
}
```

## 2.4 编码器及案例

### 2.4.1 编码器功能

编码器与解码器是相反的操作，将原有的格式转化为字节的过程，在Netty中提供了MessageToByteEncoder的抽象实现，它实现了ChannelOutboundHandler接口，本质上也是ChannelHandler。

一些实现类：

- ObjectEncoder 将对象（需要实现Serializable接口）编码为字节流
- SocksMessageEncoder 将SocksMessage编码为字节流
- HAProxyMessageEncoder 将HAProxyMessage编码成字节流

Netty也提供了MessageToMessageEncoder，将一种格式转化为另一种格式的编码器，也提供了一些实现：

- RedisEncoder 将Redis协议的对象进行编码
- StringEncoder 将字符串进行编码操作
- Base64Encoder 将Base64字符串进行编码操作

### 2.4.2 实现案例

将Integer类型编码为字节进行传递

自定义编码器:

```java
/**
 * Integer转为Byte的编码器
 */
public class IntegerToByteEncoder extends MessageToByteEncoder<Integer> {
  @Override
  protected void encode(ChannelHandlerContext ctx, Integer msg, ByteBuf out) throws Exception {
    out.writeInt(msg);
  }
}
```

在Handler直接输出数字即可：

```java
public class ClientHandler extends SimpleChannelInboundHandler<ByteBuf> {
  @Override
  protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws
    Exception {
    System.out.println("接收到服务端的消息：" + msg.toString(CharsetUtil.UTF_8));
  }
  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    ctx.writeAndFlush(123);
  } 
  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    throws Exception {
    cause.printStackTrace();
    ctx.close();
  }
}
```

在pipeline中添加编码器:

```java
@Override
protected void initChannel(SocketChannel ch) throws Exception {
  ch.pipeline().addLast(new IntegerToByteEncoder());
  ch.pipeline().addLast(new ClientHandler());
}
```

## 2.5 对象的编解码

对于JavaBean对象，Netty也支持了Object对象的编解码，其实也就是对象的序列化，要求java对象需要java.io.Serializable接口。

定义javabean对象：

```java
package com.lishem.objectcodec.codec;

import java.io.Serializable;

public class User implements Serializable {

  private static final long serialVersionUID = -528893814464880283L;

  private Long id;
  private String name;
  private Integer age;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "User{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", age=" + age +
      '}';
  }
}
```

### 2.5.1 服务端

```java
package com.lishem.objectcodec.server;


import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;

public class NettyObjectServer {

  public static void main(String[] args) throws Exception {
    // 主线程，不处理任何业务逻辑，只是接收客户的连接请求
    EventLoopGroup boss = new NioEventLoopGroup(1);
    // 工作线程，线程数默认是：cpu*2
    EventLoopGroup worker = new NioEventLoopGroup();
    try {
      // 服务器启动类
      ServerBootstrap serverBootstrap = new ServerBootstrap();
      serverBootstrap.group(boss, worker);
      //配置server通道
      serverBootstrap.channel(NioServerSocketChannel.class);
      serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline()
            .addLast(new ObjectDecoder(ClassResolvers.weakCachingResolver(this.getClass().getClassLoader())))
            .addLast(new ServerHandler());
        }
      }); //worker线程的处理器
      ChannelFuture future = serverBootstrap.bind(6677).sync();
      System.out.println("服务器启动完成。。。。。");
      //等待服务端监听端口关闭
      future.channel().closeFuture().sync();
    } finally {
      //优雅关闭
      boss.shutdownGracefully();
      worker.shutdownGracefully();
    }
  }
}
```

```java
package com.lishem.objectcodec.server;

import com.lishem.objectcodec.codec.User;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

public class ServerHandler extends SimpleChannelInboundHandler<User> {
  @Override
  public void channelRead0(ChannelHandlerContext ctx, User user) throws Exception {
    //获取到user对象
    System.out.println(user);
    ctx.writeAndFlush(Unpooled.copiedBuffer("ok", CharsetUtil.UTF_8));
  }
}
```

### 2.5.2 客户端

```java
package com.lishem.objectcodec.client;


import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ObjectEncoder;

public class NettyObjectClient {
  public static void main(String[] args) throws Exception {
    EventLoopGroup worker = new NioEventLoopGroup();
    try {
      // 服务器启动类
      Bootstrap bootstrap = new Bootstrap();
      bootstrap.group(worker);
      bootstrap.channel(NioSocketChannel.class);
      bootstrap.handler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline().addLast(new ObjectEncoder());
          ch.pipeline().addLast(new ClientHandler());
        }
      });
      ChannelFuture future = bootstrap.connect("127.0.0.1", 6677).sync();
      future.channel().closeFuture().sync();
    } finally {
      worker.shutdownGracefully();
    }
  }
}
```

```java
package com.lishem.objectcodec.client;

import com.lishem.objectcodec.codec.User;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

public class ClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    User user = new User();
    user.setId(1L);
    user.setName("张三");
    user.setAge(20);
    ctx.writeAndFlush(user);
  }

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
    System.out.println("接收到服务端的消息：" + msg.toString(CharsetUtil.UTF_8));
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    cause.printStackTrace();
    ctx.close();
  }
}
```

## 2.6 Hessian编解码

JDK序列化使用是比较方便，但是它的性能较差，序列化后的字节大小也比较大，所以一般在项目中不会使用自带的序列化，而是会采用第三方的序列化框架。

我们以Hessian为例，演示下如何与Netty整合进行编解码处理。

```xml
<dependency>
  <groupId>com.caucho</groupId>
  <artifactId>Hessian</artifactId>
  <version>4.0.63</version>
</dependency>
```

User对象

```java
package com.lishem.objectcodec.codec;

import java.io.Serializable;

public class User implements Serializable {

  private static final long serialVersionUID = -528893814464880283L;

  private Long id;
  private String name;
  private Integer age;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return "User{" +
      "id=" + id +
      ", name='" + name + '\'' +
      ", age=" + age +
      '}';
  }
}
```

### 2.6.1 编解码器

序列化工具：

```java
package com.lishem.hessiancodec.codec;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class HessianSerializer {

  public <T> byte[] serialize(T obj) {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    HessianOutput ho = new HessianOutput(os);
    try {
      ho.writeObject(obj);
      ho.flush();
      return os.toByteArray();
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      try {
        ho.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      try {
        os.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  public <T> Object deserialize(byte[] bytes, Class<T> clazz) {
    ByteArrayInputStream is = new ByteArrayInputStream(bytes);
    HessianInput hi = new HessianInput(is);
    try {
      return (T) hi.readObject(clazz);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      try {
        hi.close();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
      try {
        is.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
```

编码器：

```java
package com.lishem.hessiancodec.codec;

import com.lishem.hessiancodec.entity.User;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class HessianEncoder extends MessageToByteEncoder<User> {

  private HessianSerializer hessianSerializer = new HessianSerializer();

  @Override
  protected void encode(ChannelHandlerContext ctx, User msg, ByteBuf out) throws Exception {
    byte[] bytes = hessianSerializer.serialize(msg);
    out.writeBytes(bytes);
  }
}
```

解码器：

```java
package com.lishem.hessiancodec.codec;

import com.lishem.hessiancodec.entity.User;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

public class HessianDecoder extends ByteToMessageDecoder {

  private HessianSerializer hessianSerializer = new HessianSerializer();

  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    //复制一份ByteBuf数据，轻复制，非完全拷贝
    //避免出现异常：did not read anything but decoded a message
    //Netty检测没有读取任何字节就会抛出该异常
    ByteBuf in2 = in.retainedDuplicate();
    byte[] dst;
    if (in2.hasArray()) {//堆缓冲区模式
      dst = in2.array();
    } else {
      dst = new byte[in2.readableBytes()];
      in2.getBytes(in2.readerIndex(), dst);
    }
    in.skipBytes(in.readableBytes());//跳过所有的字节，表示已经读取过了
    Object obj = hessianSerializer.deserialize(dst, User.class);//反序列化
    out.add(obj);
  }
}
```

### 2.6.2 服务端

```java
package com.lishem.hessiancodec.service;


import com.lishem.hessiancodec.codec.HessianDecoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

public class NettyHessianServer {

  public static void main(String[] args) throws Exception {
    // System.setProperty("io.netty.noUnsafe", "true");
    // 主线程，不处理任何业务逻辑，只是接收客户的连接请求
    EventLoopGroup boss = new NioEventLoopGroup(1);
    // 工作线程，线程数默认是：cpu*2
    EventLoopGroup worker = new NioEventLoopGroup();
    try {
      // 服务器启动类
      ServerBootstrap serverBootstrap = new ServerBootstrap();
      serverBootstrap.group(boss, worker);
      //配置server通道
      serverBootstrap.channel(NioServerSocketChannel.class);
      //worker线程的处理器
      serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline()
            .addLast(new HessianDecoder())
            .addLast(new ServerHandler());
        }
      });
      // serverBootstrap.childOption(ChannelOption.ALLOCATOR,UnpooledByteBufAllocator.DEFAULT);
      ChannelFuture future = serverBootstrap.bind(6677).sync();
      System.out.println("服务器启动完成。。。。。");
      //等待服务端监听端口关闭
      future.channel().closeFuture().sync();
    } finally {
      //优雅关闭
      boss.shutdownGracefully();
      worker.shutdownGracefully();
    }
  }
}
```

```java
package com.lishem.hessiancodec.service;

import com.lishem.hessiancodec.entity.User;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

public class ServerHandler extends SimpleChannelInboundHandler<User> {
  @Override
  protected void channelRead0(ChannelHandlerContext ctx, User user) throws Exception {
    //获取到user对象
    System.out.println(user);
    ctx.writeAndFlush(Unpooled.copiedBuffer("ok", CharsetUtil.UTF_8));
  }
}

```

### 2.6.3 客户端

```java
package com.lishem.hessiancodec.client;

import com.lishem.hessiancodec.codec.HessianEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NettyHessianClient {

  public static void main(String[] args) throws Exception{
    EventLoopGroup worker = new NioEventLoopGroup();
    try {
      // 服务器启动类
      Bootstrap bootstrap = new Bootstrap();
      bootstrap.group(worker);
      bootstrap.channel(NioSocketChannel.class);
      bootstrap.handler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline().addLast(new HessianEncoder());
          ch.pipeline().addLast(new ClientHandler());
        }
      });
      ChannelFuture future = bootstrap.connect("127.0.0.1", 6677).sync();
      future.channel().closeFuture().sync();
    } finally {
      worker.shutdownGracefully();
    }
  }
}
```

```java
package com.lishem.hessiancodec.client;

import com.lishem.hessiancodec.entity.User;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;

public class ClientHandler extends SimpleChannelInboundHandler<ByteBuf> {

  @Override
  public void channelActive(ChannelHandlerContext ctx) throws Exception {
    User user = new User();
    user.setId(1L);
    user.setName("张三");
    user.setAge(20);
    ctx.writeAndFlush(user);
  }

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
    System.out.println("接收到服务端的消息：" + msg.toString(CharsetUtil.UTF_8));
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
    throws Exception {
    cause.printStackTrace();
    ctx.close();
  }
}

```

## 2.7 http服务器应用案例

![image-20211018232650765](05_netty高级.assets/image-20211018232650765.png)

Server:

```java
package com.lishem.http;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.stream.ChunkedWriteHandler;

public class NettyHttpServer {

  public static void main(String[] args) throws Exception {
    // 主线程，不处理任何业务逻辑，只是接收客户的连接请求
    EventLoopGroup boss = new NioEventLoopGroup(1);
    // 工作线程，线程数默认是：cpu*2
    EventLoopGroup worker = new NioEventLoopGroup();
    try {
      // 服务器启动类
      ServerBootstrap serverBootstrap = new ServerBootstrap();
      serverBootstrap.group(boss, worker);
      //配置server通道
      serverBootstrap.channel(NioServerSocketChannel.class);
      serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
          ch.pipeline()
            .addLast(new HttpRequestDecoder()) //http请求的解码器
            //将http请求中的uri以及请求体聚合成一个完整的FullHttpRequest对象
            .addLast(new HttpObjectAggregator(1024 * 128))
            .addLast(new HttpResponseEncoder()) //http响应的编码器
            .addLast(new ChunkedWriteHandler()) //支持异步的大文件传输，防止内存溢出
            .addLast(new ServerHandler());
        }
      }); //worker线程的处理器
      ChannelFuture future = serverBootstrap.bind(8080).sync();
      System.out.println("服务器启动完成。。。。。");
      //等待服务端监听端口关闭
      future.channel().closeFuture().sync();
    } finally {
      //优雅关闭
      boss.shutdownGracefully();
      worker.shutdownGracefully();
    }
  }
}
```

ServerHandler:

```java
package com.lishem.http;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;

import java.util.Map;

public class ServerHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
  @Override
  public void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request)
    throws Exception {
    //解析FullHttpRequest，得到请求参数
    Map<String, String> paramMap = new RequestParser(request).parse();
    String name = paramMap.get("name");
    //构造响应对象
    FullHttpResponse httpResponse = new
      DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
    httpResponse.headers().set(HttpHeaderNames.CONTENT_TYPE, "text/html;charset=utf-8");
    StringBuilder sb = new StringBuilder();
    sb.append("<h1>");
    sb.append("你好，" + name);
    sb.append("</h1>");
    httpResponse.content().writeBytes(Unpooled.copiedBuffer(sb, CharsetUtil.UTF_8));
    ctx.writeAndFlush(httpResponse).addListener(ChannelFutureListener.CLOSE); //操作完成后，将channel关闭
  }
}

```

RequestParser:

```java
package com.lishem.http;

import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * HTTP请求参数解析器, 支持GET, POST
 */
public class RequestParser {

  private FullHttpRequest fullReq;

  /**
     * 构造一个解析器
     * @param req
     */
  public RequestParser(FullHttpRequest req) {
    this.fullReq = req;
  }

  public Map<String, String> parse() throws Exception {
    HttpMethod method = fullReq.method();
    Map<String, String> paramMap = new HashMap<>();
    if (HttpMethod.GET == method) {
      // 是GET请求
      QueryStringDecoder decoder = new QueryStringDecoder(fullReq.uri());
      decoder.parameters().entrySet().forEach(entry -> {
        // entry.getValue()是一个List, 只取第一个元素
        paramMap.put(entry.getKey(), entry.getValue().get(0));
      });
    } else if (HttpMethod.POST == method) {
      // 是POST请求
      HttpPostRequestDecoder decoder = new
        HttpPostRequestDecoder(fullReq);
      decoder.offer(fullReq);
      List<InterfaceHttpData> paramList = decoder.getBodyHttpDatas();
      for (InterfaceHttpData param : paramList) {
        Attribute data = (Attribute) param;
        paramMap.put(data.getName(), data.getValue());
      }
    } else {
      // 不支持其它方法
      throw new RuntimeException("不支持其它方法"); // 这是个自定义的异常, 可删掉这一行
    }
    return paramMap;
  }
}

```

# 3.自研RPC实战

说在前面：

这个例子服务端的代理对象是自己手动创建的，并不能看到那种纯粹的透明调用的样子

## 3.1 设计说明

![image-20211019225249735](05_netty高级.assets/image-20211019225249735.png)

- 服务端：
	- 接受到客户端发来的消息后，进行解码操作
	- 根据消息中的接口信息，通过反射找到其实现类，执行目标方法
	- 将返回的数据再进行编码操作，发送给客户端
- 客户端
	- 客户端通过代理的方式，获取到接口的代理类
	- 通过代理类进行发送消息，实际上是向服务端发送消息
	- 通过编码器将消息进行编码操作
	- 接收到服务端的响应后，进行解码操作，返回数据给方法的调用方
- 注册中心
	- 暂时不做实现

![image-20211019231246449](05_netty高级.assets/image-20211019231246449.png)

工程说明：

- myrpc-core：RPC核心实现
- myrpc-api：示例api定义
- myrpc-server：服务端示例
- myrpc-client：服务端示例

maven依赖：

```xml
<dependencies>
  <!--Hessian序列化 -->
  <dependency>
    <groupId>com.caucho</groupId>
    <artifactId>Hessian</artifactId>
    <version>4.0.63</version>
  </dependency>
  <!-- netty -->
  <dependency>
    <groupId>io.netty</groupId>
    <artifactId>netty-all</artifactId>
    <version>4.1.50.Final</version>
  </dependency>
  <!--日志，本来是slf4j门面和log4j的桥接器，但是它会依赖log4j和slf4j -->
  <dependency>
    <groupId>org.slf4j</groupId>
    <artifactId>slf4j-log4j12</artifactId>
    <version>1.7.22</version>
  </dependency>
</dependencies>
```



## 3.2 自定义协议和编解码器

这一小节没什么好说的，就是统一一下传输格式。本例用到了Hessian 编解码器

**序列化工具**：

```java
package com.lishem.core.util;

public interface MySerializer {

  /**
     * 将对象序列化成字节数组
     *
     * @param obj
     * @return
     */
  <T> byte[] serialize(T obj);


  /**
     * 将字节数组反序列化成对象
     *
     * @param bytes
     * @param clazz
     * @return
     */
  <T> T deserialize(byte[] bytes, Class<T> clazz);
}

package com.lishem.core.util;

import com.caucho.hessian.io.HessianInput;
import com.caucho.hessian.io.HessianOutput;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Hessian序列化工具类
 */
public class HessianSerializer implements MySerializer{
  @Override
  public <T> byte[] serialize(T obj) {
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    HessianOutput ho = new HessianOutput(os);
    try {
      ho.writeObject(obj);
      ho.flush();
      return os.toByteArray();
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      try {
        ho.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
      try {
        os.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  @Override
  public <T> T deserialize(byte[] bytes, Class<T> clazz) {
    ByteArrayInputStream is = new ByteArrayInputStream(bytes);
    HessianInput hi = new HessianInput(is);
    try {
      return (T) hi.readObject(clazz);
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      try {
        hi.close();
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
      try {
        is.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
```

协议：

```java
package com.lishem.core.base;

import java.io.Serializable;

public class BaseRpcBean implements Serializable {

  // 请求id
  private String requestId;

  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }
}

```

```java
/**
 * 自定义请求对象
 */
public class RpcRequest extends BaseRpcBean {

  private static final long serialVersionUID = -4599165185347871064L;

  private long currentMillisTime; // 创建请求时间
  private String className; // 类名称，全包路径
  private String methodName; // 执行方法名
  private Class<?>[] parameterTypes; // 方法中传入参数类型
  private Object[] parameters; // 执行方法传入的参数

	// getter/setter 自己生成
}
```



```java
public class RpcResponse extends BaseRpcBean{

  private static final long serialVersionUID = 316225276588124313L;

  private String errorMsg; // 错误信息
  private Object result; // 结果数据

  // getter/setter 自己生成
}
```

**编解码器**：

```java
package com.lishem.core.codec;

import com.lishem.core.base.BaseRpcBean;
import com.lishem.core.util.HessianSerializer;
import com.lishem.core.util.MySerializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;

/**
 * 自定义解码器
 *
 * @param <T>
 */
public class MyDecoder<T extends BaseRpcBean> extends ReplayingDecoder<Void> {

  private static MySerializer hessianSerializer = new HessianSerializer();

  private Class<T> clazz;

  public MyDecoder(Class<T> clazz) {
    this.clazz = clazz;
  }

  @Override
  protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
    byte[] bytes = new byte[in.readInt()];
    in.readBytes(bytes);
    BaseRpcBean rpcBean = hessianSerializer.deserialize(bytes, clazz);
    out.add(rpcBean);
  }
}
```

```java
package com.lishem.core.codec;

import com.lishem.core.base.BaseRpcBean;
import com.lishem.core.util.HessianSerializer;
import com.lishem.core.util.MySerializer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * 自定义编码器
 */
public class MyEncoder extends MessageToByteEncoder<BaseRpcBean> {

  private static MySerializer hessianSerializer = new HessianSerializer();

  @Override
  protected void encode(ChannelHandlerContext ctx, BaseRpcBean msg, ByteBuf out) throws Exception {
    byte[] bytes = hessianSerializer.serialize(msg);
    out.writeInt(bytes.length);
    out.writeBytes(bytes);
  }
}
```

## 3.3 服务端组件

服务端思路整体比较简单，没有客户端的动态代理，不算绕。

其关键是当 ServerHandler 接受到解码后的，客户端发过来的信息后，用 ClassUtil中的 getAllClassByInterface() 方法，找到实现这个接口的实现类。这里默认是**保存第一个**，所以就算全找到，也只用第一个。

容易出问题的就是ClassUtil 去扫描的时候，可能是MAC下的”//“ 和windows不太一样，切字符串的时候找不到。

这里需要记住，Class.forName(className) 中的className 是类似于com.lishem.xxx.Xxx格式的。

并不是绝对路径，这里要根据全路径去切个，替换。

调用完成后，将结果返回即可。

```java
package com.lishem.core.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyServer {

  private static final Logger LOGGER = LoggerFactory.getLogger(NettyServer.class);

  public static void start(String host, int port) {
    doStart(host, port);
  }

  private static void doStart(String host, int port) {
    // 主线程，不处理任何业务，只接受客户端连接请求
    EventLoopGroup boss = new NioEventLoopGroup(1);
    // 工作线程，默认是CPU*2
    EventLoopGroup worker = new NioEventLoopGroup();

    try {
      // 服务端启动
      ServerBootstrap sbs = new ServerBootstrap();
      sbs.group(boss, worker)
        .channel(NioServerSocketChannel.class) // 配置server通道
        .childHandler((new ServerInitializer())); // worker线程处理器

      ChannelFuture future = sbs.bind(host, port).sync();
      LOGGER.info("服务器启动完成，地址为：" + host + ":" + port);
      //等待服务端监听端口关闭
      future.channel().closeFuture().sync();

    } catch (Exception e) {
      LOGGER.error("服务器启动失败！", e);
    } finally {
      boss.shutdownGracefully();
      worker.shutdownGracefully();
    }
  }
}

```

```java
package com.lishem.core.server;

import com.lishem.core.base.RpcRequest;
import com.lishem.core.base.RpcResponse;
import com.lishem.core.util.ClassUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServerHandler extends SimpleChannelInboundHandler<RpcRequest> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ServerHandler.class);

  private static final Map<Class<?>, Object> OBJECT_MAP = new HashMap<>();

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, RpcRequest request) throws Exception {
    RpcResponse rpcResponse = new RpcResponse();
    rpcResponse.setRequestId(request.getRequestId());

    LOGGER.info("开始处理消息：requestId = " + request.getRequestId());

    try {
      Class<?> clazz = Class.forName(request.getClassName());
      if (!OBJECT_MAP.containsKey(clazz)){
        // 获取接口的实现类，这里只用第一个实现类（第一个存下来就break），忽略其他
        List<Class> allClassByInterface = ClassUtil.getAllClassByInterface(clazz);
        for (Class c : allClassByInterface) {
          //将对象缓存起来，提升效率
          OBJECT_MAP.put(clazz, c.getDeclaredConstructor().newInstance());
          break;
        }
      }

      // 通过反射找到方法执行
      Method method = clazz.getMethod(request.getMethodName(), request.getParameterTypes());
      method.setAccessible(true);
      Object result = method.invoke(OBJECT_MAP.get(clazz), request.getParameters());
      rpcResponse.setResult(result);
    }catch (Exception e){
      LOGGER.error("处理失败... requestId = " + request.getRequestId(), e);
      //出错
      rpcResponse.setErrorMsg("error");
    }
    ctx.writeAndFlush(rpcResponse);
  }
}
```

```java
package com.lishem.core.server;

import com.lishem.core.base.RpcRequest;
import com.lishem.core.codec.MyDecoder;
import com.lishem.core.codec.MyEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class ServerInitializer extends ChannelInitializer<SocketChannel> {
  @Override
  protected void initChannel(SocketChannel ch) throws Exception {
    ch.pipeline()
      .addLast(new MyDecoder(RpcRequest.class)) // 解码器，需要解码的对象是RpcRequest
      .addLast(new MyEncoder()) // 编码器，用于数据响应
      .addLast(new ServerHandler()); // 自定义逻辑
  }
}
```

```java
package com.lishem.core.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * 获取接口的所有实现类 理论上也可以用来获取类的所有子类
 * 查询路径有限制，只局限于接口所在模块下，比如pandora-gateway,而非整个pandora（会递归搜索该文件夹下所以的实现类）
 * 路径中不可含中文，否则会异常。若要支持中文路径，需对该模块代码中url.getPath() 返回值进行urldecode.
 */
public class ClassUtil {
  private static final Logger LOG = LoggerFactory.getLogger(ClassUtil.class);

  public static ArrayList<Class> getAllClassByInterface(Class clazz) {
    ArrayList<Class> list = new ArrayList<>();
    // 判断是否是一个接口
    if (clazz.isInterface()) {
      try {
        ArrayList<Class> allClass = getAllClass(clazz.getPackage().getName());
        /**
                 * 循环判断路径下的所有类是否实现了指定的接口 并且排除接口类自己
                 */
        for (int i = 0; i < allClass.size(); i++) {
          /**
                     * 判断是不是同一个接口
                     */
          // isAssignableFrom:判定此 Class 对象所表示的类或接口与指定的 Class
          // 参数所表示的类或接口是否相同，或是否是其超类或超接口
          if (clazz.isAssignableFrom(allClass.get(i))) {
            if (!clazz.equals(allClass.get(i))) {
              // 自身并不加进去
              list.add(allClass.get(i));
            }
          }
        }
      } catch (Exception e) {
        LOG.error("出现异常{}", e.getMessage());
        throw new RuntimeException("出现异常" + e.getMessage());
      }
    }
    LOG.info("class list size :" + list.size());
    return list;
  }


  /**
     * 从一个指定路径下查找所有的类
     *
     * @param packagename
     */
  private static ArrayList<Class> getAllClass(String packagename) {

    LOG.info("packageName to search：" + packagename);
    List<String> classNameList = getClassName(packagename);
    ArrayList<Class> list = new ArrayList<>();

    for (String className : classNameList) {
      try {
        list.add(Class.forName(className));
      } catch (ClassNotFoundException e) {
        LOG.error("load class from name failed:" + className + e.getMessage());
        throw new RuntimeException("load class from name failed:" + className + e.getMessage());
      }
    }
    LOG.info("find list size :" + list.size());
    return list;
  }

  /**
     * 获取某包下所有类
     *
     * @param packageName 包名
     * @return 类的完整名称
     */
  public static List<String> getClassName(String packageName) {

    List<String> fileNames = null;
    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    String packagePath = packageName.replace(".", "/");
    URL url = loader.getResource(packagePath);
    if (url != null) {
      String type = url.getProtocol();
      LOG.debug("file type : " + type);
      if (type.equals("file")) {
        String fileSearchPath = url.getPath();
        LOG.debug("fileSearchPath: " + fileSearchPath);
        fileSearchPath = fileSearchPath.substring(0, fileSearchPath.indexOf("/classes"));
        LOG.debug("fileSearchPath: " + fileSearchPath);
        fileNames = getClassNameByFile(fileSearchPath);
      } else if (type.equals("jar")) {
        try {
          JarURLConnection jarURLConnection = (JarURLConnection) url.openConnection();
          JarFile jarFile = jarURLConnection.getJarFile();
          fileNames = getClassNameByJar(jarFile, packagePath);
        } catch (java.io.IOException e) {
          throw new RuntimeException("open Package URL failed：" + e.getMessage());
        }

      } else {
        throw new RuntimeException("file system not support! cannot load MsgProcessor！");
      }
    }
    return fileNames;
  }

  /**
     * 从项目文件获取某包下所有类
     *
     * @param filePath 文件路径
     * @return 类的完整名称
     */
  private static List<String> getClassNameByFile(String filePath) {
    List<String> myClassName = new ArrayList<String>();
    File file = new File(filePath);
    File[] childFiles = file.listFiles();
    for (File childFile : childFiles) {
      if (childFile.isDirectory()) {
        myClassName.addAll(getClassNameByFile(childFile.getPath()));
      } else {
        String childFilePath = childFile.getPath();
        if (childFilePath.endsWith(".class")) {
          childFilePath = childFilePath.substring(childFilePath.indexOf("/classes") + 9, childFilePath.lastIndexOf("."));
          childFilePath = childFilePath.replace("/", ".");
          myClassName.add(childFilePath);
        }
      }
    }
    return myClassName;
  }

  /**
     * 从jar获取某包下所有类
     *
     * @return 类的完整名称
     */
  private static List<String> getClassNameByJar(JarFile jarFile, String packagePath) {
    List<String> myClassName = new ArrayList<String>();
    try {
      Enumeration<JarEntry> entrys = jarFile.entries();
      while (entrys.hasMoreElements()) {
        JarEntry jarEntry = entrys.nextElement();
        String entryName = jarEntry.getName();
        //LOG.info("entrys jarfile:"+entryName);
        if (entryName.endsWith(".class")) {
          entryName = entryName.replace("/", ".").substring(0, entryName.lastIndexOf("."));
          myClassName.add(entryName);
          //LOG.debug("Find Class :"+entryName);
        }
      }
    } catch (Exception e) {
      LOG.error("发生异常:" + e.getMessage());
      throw new RuntimeException("发生异常:" + e.getMessage());
    }
    return myClassName;
  }
}
```



## 3.4 客户端组件

- 代理部分：
	- 创建对象的时候，本例手动用beanFactory创建代理对象，代理对象执行代理逻辑，然后用nettyClient(被代理对象)将请求发出。
- 异步请求部分：
	- 本例的精髓在RpcFutureResponse 类的设计，此类有个静态变量：MAP= {String, RpcFutureResponse}
	- 当请求发出的时候，MAP中存的是 请求ID和 RpcFutureResponse
	- 响应回来，从MAP中拿到 请求ID，然后去MAP中拿到RpcFutureResponse对象，这样就做到
		- 如果请求发出未回，此对象在MAP中
		- 如果请求发出已回，此对象在MAP中删除(本例未做删除)，将回复的RpcResponse赋给 RpcFutureResponse ，然后get阻塞被唤醒，再将RpcResponse给执行结果需要处
		- 也就是说RpcFutureResponse 就是在等远程消息回来，只要Netty将远程消息返回，就被唤醒。

两个处理代理的类：

```java
package com.lishem.core.client.proxy;

import com.lishem.core.base.RpcRequest;
import com.lishem.core.base.RpcResponse;
import com.lishem.core.client.NettyClient;
import com.lishem.core.client.RpcFutureResponse;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * 动态代理的业务逻辑实现
 */
public class ClientInvocationHandler implements InvocationHandler {

  private NettyClient nettyClient;

  /**
     * 需要将NettyClient传入，因为要基于此发送消息数据
     *
     * @param nettyClient
     */
  public ClientInvocationHandler(NettyClient nettyClient) {
    this.nettyClient = nettyClient;
  }

  @Override
  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    //获取类、方法、参数等信息
    String className = method.getDeclaringClass().getName();
    String methodName = method.getName();
    Class<?>[] parameterTypes = method.getParameterTypes();
    Object[] parameters = args;

    //封装请求对象
    RpcRequest request = new RpcRequest();
    request.setRequestId(UUID.randomUUID().toString());
    request.setCurrentMillisTime(System.currentTimeMillis());
    request.setClassName(className);
    request.setMethodName(methodName);
    request.setParameterTypes(parameterTypes);
    request.setParameters(parameters);

    //定义异步的响应对象
    RpcFutureResponse futureResponse = new RpcFutureResponse(request);

    //向服务端发送消息
    nettyClient.sendMsg(request);

    // 获取异步响应的消息
    RpcResponse rpcResponse = futureResponse.get(5,TimeUnit.SECONDS);
    if (rpcResponse.getErrorMsg() != null) {
      throw new RuntimeException(rpcResponse.getErrorMsg());
    }
    return rpcResponse.getResult();
  }
}
```

```java
package com.lishem.core.client.proxy;

import com.lishem.core.client.NettyClient;

import java.lang.reflect.Proxy;

/**
 * 通过动态代理生成代理对象
 */
public class BeanFactory {

  private NettyClient nettyClient;

  public BeanFactory(NettyClient nettyClient) {
    this.nettyClient = nettyClient;
  }

  public <T> T getBean(Class<?> clazz) {
    return (T) Proxy.newProxyInstance(
      Thread.currentThread().getContextClassLoader(),
      new Class<?>[]{clazz},
      new ClientInvocationHandler(nettyClient));
  }
}
```

处理通讯的类：

```java
package com.lishem.core.client;


import com.lishem.core.base.RpcRequest;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NettyClient {

  private Channel channel;

  private EventLoopGroup worker;

  public void start(String host, int port) {
    try {
      doStart(host, port);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void doStart(String host, int port) throws Exception {
    //定义工作线程组
    this.worker = new NioEventLoopGroup();
    Bootstrap bs = new Bootstrap();
    bs.group(worker)
      .channel(NioSocketChannel.class)
      .handler(new ClientInitializer());

    //连接到远程服务
    ChannelFuture future = bs.connect(host, port).sync();

    //保留channel对象，方便后面通过该通道发送消息
    this.channel = future.channel();
  }


  /**
     * 关闭客户端服务
     */
  public void close() {
    if (null != this.channel && this.channel.isActive()) {
      this.channel.close();
    }
    if (null != this.worker && !this.worker.isShutdown()) {
      this.worker.shutdownGracefully();
    }
  }

  /**
     * 发送消息
     *
     * @param request
     */
  public void sendMsg(RpcRequest request) {
    // 会开一个新线程执行
    // this.channel.writeAndFlush(request);
    // 交给EventLoopGroup执行
    channel.eventLoop().execute(() -> {
      channel.writeAndFlush(request);
    });
  }
}
```

```java
package com.lishem.core.client;

import com.lishem.core.base.RpcResponse;
import com.lishem.core.codec.MyDecoder;
import com.lishem.core.codec.MyEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class ClientInitializer extends ChannelInitializer<SocketChannel> {

  @Override
  protected void initChannel(SocketChannel ch) throws Exception {
    ch.pipeline()
      .addLast(new MyDecoder(RpcResponse.class))
      .addLast(new MyEncoder())
      .addLast(new ClientHandler());
  }
}
```

```java
package com.lishem.core.client;

import com.lishem.core.base.RpcResponse;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientHandler extends SimpleChannelInboundHandler<RpcResponse> {

  private static final Logger LOGGER = LoggerFactory.getLogger(ClientHandler.class);


  @Override
  protected void channelRead0(ChannelHandlerContext ctx, RpcResponse msg) throws Exception {
    LOGGER.info("接收到服务端的消息：requestId = " + msg.getRequestId());
    //接收到服务端的消息后，通知响应已经完成
    //通过RpcFutureResponse中的Map获取到RpcFutureResponse对象，进行方法回调
    RpcFutureResponse rpcFutureResponse = RpcFutureResponse.getRpcFutureResponse(msg.getRequestId());
    if (null != rpcFutureResponse) {
      rpcFutureResponse.setResponse(msg);
    } else {
      LOGGER.warn("没有找到对应的RpcFutureResponse对象~ requestId = " + msg.getRequestId());
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    LOGGER.error("客户端出错~ ", cause);
    ctx.close();
  }
}
```

```java
package com.lishem.core.client;

import com.lishem.core.base.RpcRequest;
import com.lishem.core.base.RpcResponse;

import java.util.Map;
import java.util.concurrent.*;

/**
 * 用于异步的响应处理
 */
public class RpcFutureResponse implements Future<RpcResponse> {

  // 存储RequestId与RPCFutureResponse对象的映射关系
  private static final Map<String, RpcFutureResponse> MAP = new ConcurrentHashMap<>();

  // 是否处理完成，默认是false
  private boolean done = false;

  // 定义一个对象用于锁操作
  private final Object lock = new Object();

  // 响应对象
  private RpcResponse response;

  /**
     * 在构造函数中将当前对象存到MAP中
     *
     * @param request
     */
  public RpcFutureResponse(RpcRequest request) {
    MAP.put(request.getRequestId(), this);
  }

  /**
     * 根据id查询RpcFutureResponse对象
     *
     * @param requestId
     * @return
     */
  public static RpcFutureResponse getRpcFutureResponse(String requestId) {
    return MAP.get(requestId);
  }


  @Override
  public boolean cancel(boolean mayInterruptIfRunning) {
    //TODO 暂时不实现
    return false;
  }

  @Override
  public boolean isCancelled() {
    // TODO 暂时不实现
    return false;
  }

  /**
     * 是否完成
     *
     * @return
     */
  @Override
  public boolean isDone() {
    return done;
  }

  /**
     * 获取数据，没有超时时间
     *
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
  @Override
  public RpcResponse get() throws InterruptedException, ExecutionException {
    return this.get(-1, TimeUnit.MILLISECONDS);
  }

  /**
     * 获取数据，指定了超时时间
     *
     * @param timeout 单位为毫秒
     * @param unit
     * @return
     * @throws InterruptedException
     * @throws ExecutionException
     */
  @Override
  public RpcResponse get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException {
    if (isDone()) {
      return this.response;
    }

    synchronized (lock) { //确保只有一个线程在一个时刻执行操作
      try {
        if (timeout < 0) { //如果没有设置超时时间的话，就一直等待
          lock.wait();
        } else {
          // 如果设置了超时时间的话，就在给定时间内进行等待
          long timeoutMillis = (TimeUnit.MILLISECONDS == unit) ? timeout : TimeUnit.MILLISECONDS.convert(timeout, unit);
          lock.wait(timeoutMillis);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    if (isDone()) {
      return this.response;
    }

    throw new RuntimeException("获取数据，等待超时。。。");
  }

  public void setResponse(RpcResponse response) {
    this.response = response;
    synchronized (lock) { //确保只有一个线程在一个时刻执行操作
      this.done = true;
      this.lock.notifyAll(); //唤醒等待的线程
    }
    //删除已缓存的对象
    MAP.remove(response.getRequestId());
  }
}
```

## 3.5 测试模块

测试模块服务端和客户端要依赖 myrpc-core 模块和 myrpc-api 模块

### 3.5.1 通用API：

```java
package com.lishem.order.pojo;

import java.io.Serializable;
import java.util.Arrays;

public class Item implements Serializable {

  private static final long serialVersionUID = -5988676714276612199L;

  private Long itemId;
  private String title;
  private String[] pics;
  private Long price;

  // get/set方法省略
}
```

```java
package com.lishem.order.pojo;

import java.io.Serializable;

public class Order implements Serializable {

  private static final long serialVersionUID = -9210792354495393631L;

  private String orderId;
  private Long userId;
  private Long date;
  private Integer itemCount;
  private Long totalPrice;
  // get/set方法省略
}
```

```java
package com.lishem.order.service;

import com.lishem.order.pojo.Item;
import com.lishem.order.pojo.Order;

import java.util.List;
// 远程调用接口
public interface OrderService {

  Order submitOrder(Long userId, List<Item> itemList);
}

```

## 3.5.2 服务端

```java
package com.lishem.order;

import com.lishem.core.server.NettyServer;

public class MyServer {
  public static void main(String[] args) {
    NettyServer.start("127.0.0.1",5566);
  }
}

```

```java
package com.lishem.order.service;

import com.lishem.order.pojo.Item;
import com.lishem.order.pojo.Order;

import java.util.List;
import java.util.UUID;

public class OrderServiceImpl implements OrderService {
  @Override
  public Order submitOrder(Long userId, List<Item> itemList) {
    Order order = new Order();
    order.setOrderId(UUID.randomUUID().toString());
    order.setDate(System.currentTimeMillis());
    order.setItemCount(itemList.size());
    order.setUserId(userId);

    Long count = 0L;
    for (Item item : itemList) {
      count += item.getPrice();
    }
    order.setTotalPrice(count);
    return order;
  }
}
```

### 3.5.3 客户端

```java
package com.lishem;

import com.lishem.core.client.NettyClient;
import com.lishem.core.client.proxy.BeanFactory;
import com.lishem.order.pojo.Item;
import com.lishem.order.pojo.Order;
import com.lishem.order.service.OrderService;

import java.util.ArrayList;
import java.util.List;

public class ClientServer {

  public static void main(String[] args) {
    //和服务端建立链接
    NettyClient nettyClient = new NettyClient();
    nettyClient.start("127.0.0.1", 5566);

    // nettyClient 有Channel属性，可以执行远程调用
    BeanFactory beanFactory = new BeanFactory(nettyClient);
    // 创建代理
    OrderService orderService = beanFactory.getBean(OrderService.class);

    List<Item> itemList = new ArrayList<>();
    Item item = new Item();
    item.setItemId(2001L);
    item.setPrice(100L);
    item.setTitle("铅笔");
    itemList.add(item);

    item = new Item();
    item.setItemId(2002L);
    item.setPrice(50L);
    item.setTitle("橡皮");
    itemList.add(item);

    for (int i = 0; i < 10; i++) {
      Order order = orderService.submitOrder(1001L, itemList);
      System.out.println("返回数据：" + order);
    }
    nettyClient.close();
  }
}
```

# 4.Netty高级优化

## 4.1使用EventLoop的任务调度

```java
// 方式一：直接使用channel写出数据
channel.writeAndFlush(request);

// 方式二：使用本channel的EventLoop写出数据
channel.eventLoop().execute(new Runnable(){
  @Override
  public void run(){
    channel.writeAndFlush(data)
  }
});
```

在writeAndFlush()底层，如果没有通过eventLoop执行的话，就会重新启动新的线程执行。

所以方式二性能更好，免去了线程的上下文切换。

## 4.2 减少ChannelPipline的调用长度

```java
public class YourHandler extends ChannelInboundHandlerAdapter{
  @Override
  public void channelActive(ChannelHandlerContext ctx) {
    // BAD (most of the times) all pipline
    ctx.channel().writeAndFlush(msg);
    // GOOD 
    ctx.writeAndFlush(msg);
  }
}
```

前者是将msg从整个ChannelPipline中走一遍，所有的handler都要经过，而后者是从当前handler一直

到pipline的尾部，调用更短。

## 4.3 减少ChannelHandler的创建

如果channelhandler是无状态的（即不需要保存任何状态参数），那么使用Sharable注解，并在bootstrap时只创建一个实例，减少GC。否则每次连接都会new出handler对象。

```java
@ChannelHandler.Shareable
  public class StatelessHandler extends ChannelInboundHandlerAdapter {
    @Override
    public void channelActive(ChannelHandlerContext ctx) {}
  }
public class MyInitializer extends ChannelInitializer<Channel> {
  private static final ChannelHandler INSTANCE = new StatelessHandler();
  @Override
  public void initChannel(Channel ch) {
    ch.pipeline().addLast(INSTANCE);
  }
}
```

注意：**ByteToMessageDecoder**之类的**编解码器**是有状态的，不能使用Sharable注解。编解码器！！

## 4.4 Boss与Worker线程配置优化

### 4.4.1 Boss线程优化

如果有大量设备客户端接入，需要对服务端的监听方式和线程模型做优化，即服务端监听多个端口，利用主从Reactor线程模型。由于同时监听了多个端口，每个ServerSocketChannel都对应一个独立的Acceptor线程，这样就能并行处理，加速客户端的接入速度，减少客户端连接超时失败率，提高单节点服务端的处理性能。

### 4.4.2 Worker线程优化

对于I/O工作线程池的优化，可以先采用系统默认值（cpu内核数*2）， 做性能测试，在测试过程中采集I/O线程的CPU占用率，看是否存在瓶颈，具体执行方法如下：

![image-20211020225802130](05_netty高级.assets/image-20211020225802130.png)

如果发现I/O线程停留在读或写操作时， 或停留在ChannelHandler，则可以通过加大NioEventLoop线程个数来提升网络的读写性能， 调整方式有两种：

1. 通过Netty的API接口指定：在创建NioEventLoopGroup时， 指定相应的线程数量。
2. 系统参数方式指定： 通过JDK运行时参数-Dio.netty.eventLoopThreads来指定NioEventLoopGroup线程池，采用这种方式要注意， 这属于系统层面配置， 所有创建NioEventLoopGroup地方都会采用该配置， 而不是默认的【CPU内核*2】。

## 4.5 线程隔离优化

为更好的提升性能， 将I/O线程与业务线程进行隔离优化：

![image-20211020230004807](05_netty高级.assets/image-20211020230004807.png)

## 4.6 接受和发送缓冲区优化

对于不同的应用场景，收发缓冲区的最优值可能不同，用户需要根据实际场景，结合性能测试数据进行针对性的调优。

比如， 客户端会周期性地上报数据和发送心跳， 单个链路的消息收发量并不大， 针对此类场景， 可以通过调小TCP的接收和发送缓冲区来降低单个TCP连接的资源占用率。

参数配置说明：

ChannelOption.SO_SNDBUF 和ChannelOption.SO_RCVBUF 参数说明： SO_SNDBUF 和SO_RCVBUF对应socket中的SO_SNDBUF和SO_RCVBUF参数，即设置发送缓冲区和接收缓冲区的大小，发送缓冲区用于保存发送数据，直到发送成功，接收缓冲区用于保存网络协议站内收到的数据，直到程序读取成功。

SO_RCVBUF参数，TCP数据接收缓冲区大小。该缓冲区即TCP接收滑动窗口，linux操作系统可使用命令：cat /proc/sys/net/ipv4/tcp_rmem查询其大小。一般情况下，该值可由用户在任意时刻设置，但当设置值超过64KB时，需要在连接到远端之前设置。 SO_SNDBUF参数，TCP数据发送缓冲区大小。该缓冲区即TCP发送滑动窗口，linux操作系统可使用命令：cat
/proc/sys/net/ipv4/tcp_smem查询其大小。

代码配置：

```java
// 配置接收区缓冲
serverBootstrap.childOption(ChannelOption.SO_RCVBUF, 4* 1024)
// 配置发送区缓冲
serverBootstrap.childOption(ChannelOption.SO_SNDBUF, 4*1024);
```

## 4.7 一些配置参数设置

ServerBootstrap启动时，通常bossGroup只需要设置为1即可，因为ServerSocketChannel在初始化阶段，只会注册到某一个eventLoop上，而这个eventLoop只会有一个线程在运行，所以没有必要设置为多线程。而 IO 线程，为了充分利用 CPU，同时考虑减少线上下文切换的开销，通常设置为 CPU 核数的两倍，这也是 Netty 提供的默认值。

在对于响应时间有高要求的场景，使用.childOption(ChannelOption.TCP_NODELAY, true)和.option(ChannelOption.TCP_NODELAY, true)来禁用nagle算法，不等待，立即发送

