Netty核心源码

# 1. 服务端启动过程剖析

```java
final ChannelFuture initAndRegister() {
    Channel channel = null;
    try {
        // 1.1 创建服务端Channel
        channel = channelFactory.newChannel();
        // 1.2 初始化服务端Channel
        init(channel);
    } catch (Throwable t) {
        if (channel != null) {
            channel.unsafe().closeForcibly();
            return new DefaultChannelPromise(channel, GlobalEventExecutor.INSTANCE).setFailure(t);
        }
       
        return new DefaultChannelPromise(new FailedChannel(), GlobalEventExecutor.INSTANCE).setFailure(t);
    }
    // 1.3 注册selector
    ChannelFuture regFuture = config().group().register(channel);
    if (regFuture.cause() != null) {
        if (channel.isRegistered()) {
            channel.close();
        } else {
            channel.unsafe().closeForcibly();
        }
    }
    return regFuture;
}
```



## 1.1 创建服务端Channel

主要流程：

- ServerBootstrap对象的bind()方法，也是入口方法
- AbstractBootstrap中的initAndRegister()进行创建Channel
- 创建Channel的工作由ReflectiveChannelFactory反射类中的newChannel()方法完成。
- NioServerSocketChannel中的构造方法中，通过jdk nio底层的SelectorProvider打开ServerSocketChannel。
- 在AbstractNioChannel的构造方法中，设置channel为非阻塞：ch.configureBlocking(false);
- 通过的AbstractChannel的构造方法，创建了id、unsafe、pipeline内容。
- 通过NioServerSocketChannelConfig获取tcp底层的一些参数  

## 1.2 初始化服务端Channel

主流程

- AbstractBootstrap中的initAndRegister()进行初始化channel，代码：init(channel);  
- 在ServerBootstrap中的init()方法设置channelOptions以及Attributes。  
- 紧接着，将用户自定义参数、属性保存到局部变量currentChildOptions、currentChildAttrs，以供后面使用  
- 如果设置了serverBootstrap.handler()的话，会加入到pipeline中。  
- 添加连接器ServerBootstrapAcceptor，有新连接加入后，将自定义的childHandler加入到连接的pipeline中：  

```java
// ServerBootstrap.java
@Override
void init(Channel channel) {
    setChannelOptions(channel, newOptionsArray(), logger);
    setAttributes(channel, attrs0().entrySet().toArray(EMPTY_ATTRIBUTE_ARRAY));

    ChannelPipeline p = channel.pipeline();

    final EventLoopGroup currentChildGroup = childGroup;
    final ChannelHandler currentChildHandler = childHandler;
    final Entry<ChannelOption<?>, Object>[] currentChildOptions;
    synchronized (childOptions) {
        currentChildOptions = childOptions.entrySet().toArray(EMPTY_OPTION_ARRAY);
    }
    final Entry<AttributeKey<?>, Object>[] currentChildAttrs = childAttrs.entrySet().toArray(EMPTY_ATTRIBUTE_ARRAY);

    p.addLast(new ChannelInitializer<Channel>() {
        @Override
        public void initChannel(final Channel ch) {
            final ChannelPipeline pipeline = ch.pipeline();
            ChannelHandler handler = config.handler();
            if (handler != null) {
                pipeline.addLast(handler);
            }

            ch.eventLoop().execute(new Runnable() {
                @Override
                public void run() {
                    pipeline.addLast(new ServerBootstrapAcceptor(
                        ch, currentChildGroup, currentChildHandler, currentChildOptions, currentChildAttrs));
                }
            });
        }
    });
}

// ServerBootstrap 的内部类 ServerBootstrapAcceptor
@Override
@SuppressWarnings("unchecked")
// 当客户端有链接的时候才会执行
public void channelRead(ChannelHandlerContext ctx, Object msg) {
    final Channel child = (Channel) msg;

    // 将自定义childHandler加入到连接的pipline中
    child.pipeline().addLast(childHandler);

    setChannelOptions(child, childOptions, logger);
    setAttributes(child, childAttrs);

    try {
        childGroup.register(child).addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                if (!future.isSuccess()) {
                    forceClose(child, future.cause());
                }
            }
        });
    } catch (Throwable t) {
        forceClose(child, t);
    }
}
```

## 1.3 注册selector

主要流程

- initAndRegister()方法中的ChannelFuture regFuture = config().group().register(channel); 进行注册

- 在io.netty.channel.AbstractChannel.AbstractUnsafe#register()中完成实际的注册

  - AbstractChannel.this.eventLoop = eventLoop; 进行eventLoop的赋值操作，后续的IO事件工作将在由该eventLoop执行。

  - 调用register0(promise)中的doRegister()进行实际的注册

  - io.netty.channel.nio.AbstractNioChannel#doRegister进行了方法实现

  - ```java
    //通过jdk底层进行注册多路复用器
    //javaChannel() --前面创建的channel
    //eventLoop().unwrappedSelector() -- 获取selector
    //注册感兴趣的事件为0，表明没有感兴趣的事件，后面会进行重新注册事件
    //将this对象以attachment的形式注册到selector，方便后面拿到当前对象的内容
    selectionKey = javaChannel().register(eventLoop().unwrappedSelector(), 0, this);
    ```

## 1.4 绑定端口

主要流程：

- 入口在io.netty.bootstrap.AbstractBootstrap#doBind0()，启动一个线程进行执行绑定端口操作

- 调用io.netty.channel.AbstractChannelHandlerContext#bind(java.net.SocketAddress,io.netty.channel.ChannelPromise)方法，再次启动线程执行

- 最终调用io.netty.channel.socket.nio.NioServerSocketChannel#doBind()方法进行绑定操作.

	- ```java
		//通过jdk底层的channel进行绑定
		@SuppressJava6Requirement(reason = "Usage guarded by java version check")
		@Override
		protected void doBind(SocketAddress localAddress) throws Exception {
		  if (PlatformDependent.javaVersion() >= 7) {
		    javaChannel().bind(localAddress, config.getBacklog());
		  } else {
		    javaChannel().socket().bind(localAddress, config.getBacklog());
		  }
		}
		```

- 什么时候更新selector的主从事件？

	- 最终在io.netty.channel.nio.AbstractNioChannel#doBeginRead()方法中完成的

	- ```java
		protected void doBeginRead() throws Exception {
		  // Channel.read() or ChannelHandlerContext.read() was called
		  final SelectionKey selectionKey = this.selectionKey;
		  if (!selectionKey.isValid()) {
		    return;
		  } 
		  readPending = true;
		  final int interestOps = selectionKey.interestOps();
		  if ((interestOps & readInterestOp) == 0) {
		    selectionKey.interestOps(interestOps | readInterestOp); //设置感兴趣的事件为OP_ACCEPT
		  }
		} 
		//在NioServerSocketChannel的构造方法中进行了赋值
		public NioServerSocketChannel(ServerSocketChannel channel) {
		  super(null, channel, SelectionKey.OP_ACCEPT);
		  config = new NioServerSocketChannelConfig(this,javaChannel().socket());
		}
		```

# 2. 连接请求过程源码剖析

## 2.1 新链接的接入

主要流程：

- 入口在io.netty.channel.nio.NioEventLoop#processSelectedKey(java.nio.channels.SelectionKey,io.netty.channel.nio.AbstractNioChannel)中

- 进入NioMessageUnsafe的read()方法

- 调用io.netty.channel.socket.nio.NioServerSocketChannel#doReadMessages() 方法，创建jdk底层的channel，封装成NioSocketChannel添加到List容器中

- ```java
	@Override
	protected int doReadMessages(List<Object> buf) throws Exception {
	  SocketChannel ch = SocketUtils.accept(javaChannel());
	  try {
	    if (ch != null) {
	      buf.add(new NioSocketChannel(this, ch));
	      return 1;
	    }
	  } catch (Throwable t) {
	    logger.warn("Failed to create a new channel from anaccepted socket.", t);
	    try {
	      ch.close();
	    } catch (Throwable t2) {
	      logger.warn("Failed to close a socket.", t2);
	    }
	  } 
	  return 0;
	}
	```

- 创建NioSocketChannel对象

	- new NioSocketChannel(this, ch)，通过new的方式进行创建
		- 调用super的构造方法
			- 传入SelectionKey.OP_READ事件标识
			- 创建id、unsafe、pipeline对象
			- 设置非阻塞 ch.configureBlocking(false);
	- 创建NioSocketChannelConfig对象

## 2.2 注册读事件

- 在io.netty.channel.nio.AbstractNioMessageChannel.NioMessageUnsafe中的：

	```java
	for (int i = 0; i < size; i ++) {
	  readPending = false;
	  pipeline.fireChannelRead(readBuf.get(i)); //传播读事件
	}
	```

- 在io.netty.channel.AbstractChannelHandlerContext#invokeChannelRead(java.lang.Object)方法中:

	```java
	private void invokeChannelRead(Object msg) {
	  if (invokeHandler()) {
	    try {
	      //执行channelRead，需要注意的是，第一次执行是HeadHandler，第二次是ServerBootstrapAcceptor
	      //通过ServerBootstrapAcceptor进入和 新连接接入的 注册selector相同的逻辑进行注册以及事件绑定
	      ((ChannelInboundHandler) handler()).channelRead(this, msg);
	    } catch (Throwable t) {
	      invokeExceptionCaught(t);
	    }
	  } else {
	    fireChannelRead(msg);
	  }
	}
	```

	

# 3. 核心Channel通信深入剖析

Netty 中的 **ChannelPipeline** 、 **ChannelHandler** 和 **ChannelHandlerContext** 是通信三大核心的组件, 接下来从源码层面来分析Netty 是如何设计这三个核心组件的，分析掌握是如何创建和协调工作。

![image-20211021201318499](06_netty核心源码.assets/image-20211021201318499.png)

## 3.1 创建过程梳理

1. 创建 ChannelSocket 的时， 会创建一个绑定的 pipeline，它们是一对一的关系，创建 pipeline 的时候也会创建 tail 节点和 head 节点，形成一个最初始的链表。
2. 调用 pipeline 的 addLast 方法，会根据给定的 handler 创建一个 Context，并且将这个 Context插入到链表的尾端。
3. 在Context 中包装 handler，多个 Context 在 pipeline 中会形成了双向链表。
4. 数据入站方向叫 inbound，由 head 节点开始，出站的方法叫 outbound ，由 tail 节点开始。

## 3.2 Handler调度分析

当服务端接收到一个请求时，ChannelPipeline 是如何调用内部的这些 handler 的呢

当一个请求进来的时候，会第一个调用 pipeline 的 相关方法，如果是入站事件，这些方法由 fire开头，表示开始管道的流动。接着让后面的 handler 继续处理。

源码剖析：

1.fire事件处理

这些都是inbound入站事件处理， 通过AbstractChannelHandlerContext的一些列invoke方法去做调用实现， 找到当前的下一个节点， 不断循环传播， 是一个过滤器形式完成对handler 的调度。

```java
..
  // channel失效处理
  @Override
  public final ChannelPipeline fireChannelInactive() {
  AbstractChannelHandlerContext.invokeChannelInactive(head);
  return this;
} 
// channel异常处理
@Override
public final ChannelPipeline fireExceptionCaught(Throwable cause) {
  AbstractChannelHandlerContext.invokeExceptionCaught(head, cause);
  return this;
} 
// channel用户事件触发处理
@Override
public final ChannelPipeline fireUserEventTriggered(Object event) {
  AbstractChannelHandlerContext.invokeUserEventTriggered(head, event);
  return this;
} 
// channel数据接收处理
@Override
public final ChannelPipeline fireChannelRead(Object msg) {
  AbstractChannelHandlerContext.invokeChannelRead(head, msg);
  return this;
} 
// channel 读取完成事件处理
@Override
public final ChannelPipeline fireChannelReadComplete() {
  AbstractChannelHandlerContext.invokeChannelReadComplete(head);
  return this;
} 
// channel 可写状态变化事件处理
@Override
public final ChannelPipeline fireChannelWritabilityChanged() {
  AbstractChannelHandlerContext.invokeChannelWritabilityChanged(head);
  return this;
}
...
```

2.AbstractChannelHandlerContext处理：

双向链表的主要代码，在 AbstractChannelHandlerContext类中。该类主要包含一个双向链表节点的前置和后置节点引用 prev、next，以及数据引用 handler，相当于链表数据结构中的 Node节点。

```java
abstract class AbstractChannelHandlerContext extends DefaultAttributeMap implements ChannelHandlerContext, ResourceLeakHint {
  // ChannelHandler的指针， 指向下一个
  volatile AbstractChannelHandlerContext next;
  // ChannelHandler的指针， 指向上一个
  volatile AbstractChannelHandlerContext prev;
  // ...
  // 调用channel通道注册事件处理
  static void invokeChannelRegistered(final AbstractChannelHandlerContext next) {
    EventExecutor executor = next.executor();
    if (executor.inEventLoop()) {
      next.invokeChannelRegistered();
    } else {
      executor.execute(new Runnable() {
        @Override
        public void run() {
          next.invokeChannelRegistered();
        }
      });
    }
  } 
  // 调用channel通道注销事件处理
  static void invokeChannelUnregistered(final AbstractChannelHandlerContext next) {
    EventExecutor executor = next.executor();
    if (executor.inEventLoop()) {
      next.invokeChannelUnregistered();
    } else {
      executor.execute(new Runnable() {
        @Override
        public void run() {
          next.invokeChannelUnregistered();
        }
      });
    }
  } 
  ...
```



## 3.3 handler注册机制

下面剖析ChannelHandler是如何注册到ChannelPipeline中。

1. 对ChannelHandler包装：

	DefaultChannelPipeline的newContent方法：

	```java
	private AbstractChannelHandlerContext newContext(EventExecutorGroup group,
	                                                 String name, ChannelHandler handler) {
	  // 将handler传入， 通过DefaultChannelHandlerContext对象进行包装
	  return new DefaultChannelHandlerContext(this, childExecutor(group),name, handler);
	}
	```

2. 加入链表并注册完成回调事件

	- 构建 AbstractChannelHandlerContext 节点，加入到了链表尾部。
	- 如果 channel 未注册到 EventLoop，就添加一个任务到 PendingHandlerCallback 上，后续channel 注册完毕，再调用 ChannelHandler.handlerAdded方法进行处理。
	- 如果已经注册，则调用 callHandlerAdded 方法来执行 ChannelHandler.handlerAdded 注册完成的回调函数.

	DefaultChannelPipeline的addLast方法：

	```java
	@Override
	public final ChannelPipeline addLast(EventExecutorGroup group, String name, ChannelHandler handler) {
	  final AbstractChannelHandlerContext newCtx;
	  synchronized (this) {
	    // 1. 检查如果不是共享和添加的状态，则抛出异常
	    checkMultiplicity(handler);
	    // 2. 构建 AbstractChannelHandlerContext 节点
	    newCtx = newContext(group, filterName(name, handler), handler);
	    // 3. 添加到链表尾部
	    addLast0(newCtx);
	    // 4. 如果registered状态为false, 代表channel尚未注册到EventLoop上
	    面。
	      // 符合条件， 则添加一个任务到 PendingHandlerCallback 上，后续注册完毕，再调用 ChannelHandler.handlerAdded
	      if (!registered) {
	        newCtx.setAddPending();
	        callHandlerCallbackLater(newCtx, true);
	        return this;
	      } 
	    // 5. 创建 EventLoop 线程
	    EventExecutor executor = newCtx.executor();
	    // 6. 判断当前线程是否是 EventLoop 线程。将ChannelHandler放入
	    EventLoop 线程池中 执行。
	      if (!executor.inEventLoop()) {
	        newCtx.setAddPending();
	        executor.execute(new Runnable() {
	          @Override
	          public void run() {
	            callHandlerAdded0(newCtx);
	          }
	        });
	        return this;
	      }
	  } 
	  // 7. 触发handler添加完成事件处理
	  callHandlerAdded0(newCtx);
	  return this;
	}
	```

实现流程:

1. 检查如果不是共享和添加的状态，则抛出异常。
2. 构建 AbstractChannelHandlerContext 节点。
3. 添加到链表尾部。
4. 如果registered状态为false, 代表channel尚未注册到EventLoop上面。符合条件， 则添加一个任务到 PendingHandlerCallback 上，后续注册完毕，再调用ChannelHandler.handlerAdded方法处理。
5. 创建 EventLoop 线程。
6. 判断当前线程是否是 EventLoop 线程。将ChannelHandler放入EventLoop 线程池中执行。
7. 触发handler添加完成事件处理。

# 4. 网络IO模型底层原理剖析

**网络IO模型分类**

分为5种：

- 同步阻塞 IO（BIO）
- 同步非阻塞 IO（NIO）
- IO 多路复用
- 信号驱动IO
- 异步非阻塞 IO（AIO）

常用的是同步阻塞IO 和 IO多路复用模型

什么是阻塞IO模型



# 5. 零拷贝