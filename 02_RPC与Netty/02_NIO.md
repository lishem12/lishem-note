# 1.NIO三大组件

## 1.1 Channel

channel 有一点类似于 stream，它就是读写数据的 **双向通道**，可以从 channel 将数据读入 buffer，也可以将 buffer 的数据写入 channel，而之前的 stream 要么是输入，要么是输出，channel 比 stream 更为底层.

![image-20211013201350977](02_NIO.assets/image-20211013201350977.png)

![img](02_NIO.assets/256dfe51-e3b3-4d5a-aa3b-aed493f660ba.png)

常见的Channel有：

- FileChannel （文件）
- DatagramChannel （UDP）
- SocketChannel （TCP）
- ServerSocketChannel （TCP服务器）

## 1.2 Buffer

Buffer用来缓冲读写数据，常见的buffer有：

- ByteBuffer

- - MappedByteBuffer
  - DirectByteBuffer
  - HeapByteBuffer

- ShortBuffer

- IntBuffer

- LongBuffer

- FloatBuffer

- DoubleBuffer

- CharBuffer

## 1.3 Selector

NIO多路复用的关键，一个select可以对应多个连接

Selector的演化：

### 1.3.1 多线程版

![img](02_NIO.assets/86ec0efd-229f-4578-a4cd-5c802bf567f8.png)

特点：一个socket对应一个thread

缺点：

- 内存占用高
- 上下文切换成本高
- 只适合连接少的情况

### 1.3.2 线程池版

![img](02_NIO.assets/8cd896d7-cbff-4c65-95bb-ae05d68b9e42.png)

缺点：

- 阻塞模式下，一个线程依然只能处理一个socket连接
- 仅适合短连接场景，长连接会长时间占用线程池中的线程

### 1.3.3 selector版改进

![img](02_NIO.assets/2eba5679-6bf9-47a7-b858-7037cda375fc.png)

selector 的作用就是配合一个线程来管理多个 channel，获取这些 channel 上发生的事件，这些 channel 工作在非阻塞模式下，不会让线程吊死在一个 channel 上。适合连接数特别多，但流量低的场景（low traffic）

调用selector的select() 方法，会阻塞知道channel 发生了读写就绪事件。这些事件发生，select方法就返回这些事件交给thread处理。

# 2.ByteBuffer

## 2.1 ByteBuffer 正确使用姿势

```java
@Slf4j
public class ChannelDemo1 {
    public static void main(String[] args) {
        try (RandomAccessFile file = new RandomAccessFile("helloword/data.txt", "rw")) {
            FileChannel channel = file.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(10);
            do {
                // 向 buffer 写入
                int len = channel.read(buffer);
                log.debug("读到字节数：{}", len);
                if (len == -1) {
                    break;
                }
                // 切换 buffer 读模式
                buffer.flip();
                while(buffer.hasRemaining()) {
                    log.debug("{}", (char)buffer.get());
                }
                // 切换 buffer 写模式
                buffer.clear();
            } while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

/*
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 读到字节数：10
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 1
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 2
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 3
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 4
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 5
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 6
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 7
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 8
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 9
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 0
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 读到字节数：4
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - a
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - b
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - c
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - d
    10:39:03 [DEBUG] [main] c.i.n.ChannelDemo1 - 读到字节数：-1
*/
```

1. 向buffer写入数据，例如调用 channel.read(buffer);
2. 调用flip() 切换至读模式
3. 从buffer读数据，例如调用buffer.get();
4. 调用clear()或者compact() 切换至写模式
5. 重复1-4步骤

## 2.2 ByteBuffer结构

ByteBuffer 有以下重要属性：

- capacity     容量
- position   工作活动指针
- limit  限制位，到这个位置停

一开始：

![img](02_NIO.assets/e3fc19f9-fcca-4baa-8da1-8e57776f0842.png)

写模式下，position是写入位置，limit等于容量，下图表示写入了4个字节后的状态

![img](02_NIO.assets/077b2100-c5e8-45db-9390-798e27a0f071.png)

flip动作发生后，position切换到读取位置，limit切换到读取限制：

![img](02_NIO.assets/0b1b3e98-c06b-49c0-a0fd-37fbba09a1ce.png)

读取四个字节后，状态：

![img](02_NIO.assets/e9f65836-18bc-4951-91cc-fc3d2964161a.png)

clear动作发生后，状态：

![img](02_NIO.assets/34915301-a725-460b-aa82-f8b7738b9e08.png)

compact 方法，是把未读完的部分向前压缩，然后切换至写模式：

![img](02_NIO.assets/159dc5fe-cbf9-4139-91ff-e3e986cfc71f.png)

## 2.3 ByteBuffer常见方法

### 2.3.1 分配空间

```java
ByteBuffer buffer = ByteBuffer.allocate(16); // java.nio.HeapByteBuffer
ByteBuffer buffer = ByteBuffer.allocateDirect(16); // java.nio.DirectByteBuffer
```

java.nio.HeapByteBuffer : Java堆内存，读写效率低，受垃圾回收影响

java.nio.DirectByteBuffer : 直接内存，读写效率高，少一次拷贝，但分配效率低，调用系统函数分配内存

### 2.3.2 向Buffer写入数据

有两种方法：

- 调用channel的read方法
- 调用buffer自己的put方法

```java
int readByte = channel.read(buffer); // 将channel缓冲区中的数据读出到buffer
buffer.put((byte)127); 
```

### 2.3.3 从Buffer读取数据

两种方法：

- 调用channel的 write方法
- 调用buffer自己的get方法

```java
int writeBytes = channel.write(buf); // 将buffer中的数据写入缓冲区
byte b = buf.get();
```

get 方法会让 position 读指针向后走，如果想重复读取数据

- 可以调用 rewind 方法将 position 重新置为 0
- 或者调用 get(int i) 方法获取索引 i 的内容，它不会移动读指针

### 2.3.4 mark和reset

mark 是在读取时，做一个标记，即使 position 改变，只要调用 reset 就能回到 mark 的位置

注意： rewind 和 flip 都会清除 mark 位置

### 2.3.5 字符串与ByteBuffer 互转

```java
// 方式1 需要手动flip()切换读模式
ByteBuffer buffer = ByteBuffer.allocate(16);
buffer.put("hello".getBytes());

// 方式2  已经切换成读模式
ByteBuffer buffer2 = StandardCharsets.UTF_8.encode("hello");

// 方式3 已经切换成读模式
ByteBuffer buffer3 = ByteBuffer.wrap("hello".getBytes());

// ByteBuffer 转 String
String s = StandardCharsets.UTF_8.decode(buffer2).toString();
```

ByteBuffer 转String，要求ByteBuffer转为读模式

**ByteBuffer 默认不是线程安全的**

## 2.4 Scattering Reads

**分散读取**

有一个文件如下：

```tcl
onetwothree
```

使用如下方式读取，可以将数据填充至多个 buffer

```java
public static void main(String[] args) {
  try (FileChannel channel = new RandomAccessFile("words.txt", "r").getChannel()) {
    ByteBuffer b1 = ByteBuffer.allocate(3);
    ByteBuffer b2 = ByteBuffer.allocate(3);
    ByteBuffer b3 = ByteBuffer.allocate(5);
    channel.read(new ByteBuffer[]{b1, b2, b3});
    b1.flip();
    b2.flip();
    b3.flip();
    debugAll(b1);
    debugAll(b2);
    debugAll(b3);
  } catch (IOException e) {
  }
}
```

## 2.5 Gathering Writes

**集中写** 使用如下方式写入，可以将多个 buffer 的数据填充至 channel

```java
public static void main(String[] args) {
  ByteBuffer b1 = StandardCharsets.UTF_8.encode("hello");
  ByteBuffer b2 = StandardCharsets.UTF_8.encode("world");
  ByteBuffer b3 = StandardCharsets.UTF_8.encode("你好");

  try (FileChannel channel = new RandomAccessFile("words.txt", "rw").getChannel()) {
    channel.write(new ByteBuffer[]{b1, b2, b3});
  } catch (IOException e) {
  }
}
```

## 2.6 粘包、半包练习

网络上有多条数据发送给服务端，数据之间使用 \n 进行分隔

但由于某种原因这些数据在接收时，被进行了重新组合，例如原始数据有3条为

- Hello,world\n
- I'm zhangsan\n
- How are you?\n

变成了下面的两个 byteBuffer (黏包，半包)

- Hello,world\nI'm zhangsan\nHo
- w are you?\n

现在要求你编写程序，将错乱的数据恢复成原始的按 \n 分隔的数据

```java
public class TestByteBufferExam {

  public static void main(String[] args) {
    ByteBuffer source = ByteBuffer.allocate(32);
    //                     11            24
    source.put("Hello,world\nI'm zhangsan\nHo".getBytes());
    split(source);

    source.put("w are you?\nhaha!\n".getBytes());
    split(source);
  }

  public static void split(ByteBuffer source) {
    source.flip(); // 先切换成读模式
    for (int i = 0; i < source.limit(); ++i) {
      if (source.get(i) == '\n') {
        // 把这条完整消息存入新的ByteBuffer
        int length = i + 1 - source.position();
        ByteBuffer target = ByteBuffer.allocate(length);
        // 从source读，向target写
        for (int j = 0; j < length; j++) {
          target.put(source.get());
        }
        debugAll(target);
      }
      source.compact(); // 最后切回写模式
    }
  }
}
```

# 3.文件编程

## 3.1 FileChannel

FileChannel只能工作在阻塞模式下，不能和selector 结合用

### 3.1.1 获取FileChannel

不能直接打开 FileChannel，必须通过 FileInputStream、FileOutputStream 或者 RandomAccessFile 来获取 FileChannel，它们都有 getChannel 方法

- 通过 FileInputStream 获取的 channel 只能读
- 通过 FileOutputStream 获取的 channel 只能写
- 通过 RandomAccessFile 是否能读写根据构造 RandomAccessFile 时的读写模式决定

### 3.1.2 读取

会从 channel 读取数据填充 ByteBuffer，返回值表示读到了多少字节，-1 表示到达了文件的末尾

```java
int readBytes = channel.read(buffer);
```

### 3.1.3 写入

写入的正确姿势如下：

```java
ByteBuffer buffer = ...;
buffer.put(...); // 存入数据
buffer.flip();   // 切换读模式

while(buffer.hasRemaining()) {
  channel.write(buffer);
}
```

在 while 中调用 channel.write 是因为 write 方法并不能保证一次将 buffer 中的内容全部写入 channel

### 3.1.4 关闭

channel 必须关闭，不过调用了 FileInputStream、FileOutputStream 或者 RandomAccessFile 的 close 方法会间接地调用 channel 的 close 方法

### 3.1.5 位置

```java
long pos = channel.position(); // 获取位置

// 设置位置
long newPos = ...;
channel.position(newPos);
```

设置当前位置时，如果设置为文件的末尾

- 这时读取会返回 -1 
- 这时写入，会追加内容，但要注意如果 position 超过了文件末尾，再写入时在新内容和原末尾之间会有空洞（00）

### 3.1.6 大小

使用 size 方法获取文件的大小

### 3.1.7 强制写入

操作系统出于性能的考虑，会将数据缓存，不是立刻写入磁盘。可以调用 force(true) 方法将文件内容和元数据（文件的权限等信息）立刻写入磁盘

## 3.2 两个Channel传输数据

 transferTo方法

```java
public static void main(String[] args) {
  try (FileChannel from = new FileInputStream("data.txt").getChannel();
       FileChannel to = new FileOutputStream("to.txt").getChannel()
      ) {
    // 比传统IO流效率高，底层用操作系统的零拷贝进行优化
    from.transferTo(0, from.size(), to);
  } catch (IOException e) {
    e.printStackTrace();
  }
}
```

超过2G 大小文件传输

```java
public static void main(String[] args) {
  try (FileChannel from = new FileInputStream("data.txt").getChannel();
       FileChannel to = new FileOutputStream("to.txt").getChannel()
      ) {
    // 比传统IO流效率高，底层用操作系统的零拷贝进行优化
    long size = from.size();
    // left 变量表示还剩多少字节
    for (long left = size;left>0;){
      left -=  from.transferTo(size-left, left, to);
    }
  } catch (IOException e) {
    e.printStackTrace();
  }
}
```

## 3.3 Path类

jdk7 引入了 Path 和 Paths 类

- Path 用来表示文件路径
- Paths 是工具类，用来获取 Path 实例

```java
Path source = Paths.get("1.txt"); // 相对路径 使用 user.dir 环境变量来定位 1.txt

Path source = Paths.get("d:\\1.txt"); // 绝对路径 代表了  d:\1.txt

Path source = Paths.get("d:/1.txt"); // 绝对路径 同样代表了  d:\1.txt

Path projects = Paths.get("d:\\data", "projects"); // 代表了  d:\data\projects
```

路径中： . 代表当前路径， .. 代表上级路径

例如目录结构如下：

```cobol
d:
	|- data
		|- projects
			|- a
			|- b
```

代码：

```java
Path path = Paths.get("d:\\data\\projects\\a\\..\\b");
System.out.println(path);
System.out.println(path.normalize()); // 正常化路径
/**
*  d:\data\projects\a\..\b
*	 d:\data\projects\b
*/
```

## 3.4 Files类

### 3.4.1 检查文件是否存在

```java
Path path = Paths.get("helloword/data.txt");
System.out.println(Files.exists(path));
```

### 3.4.2 创建一级目录

```java
Path path = Paths.get("helloword/d1");
Files.createDirectory(path);
```

- 如果目录已存在，会抛出异常FileAlreadyExistsException
- 不能一次创建多级目录，否则会抛异常 NoSuchFileException

### 3.4.3 创建多级目录

```java
Path path = Paths.get("helloword/d1/d2");
Files.createDirectories(path);
```

### 3.4.4 拷贝文件

```java
Path source = Paths.get("helloword/data.txt");
Path target = Paths.get("helloword/target.txt");

Files.copy(source, target);
```

- 如果文件已经存在，会抛出FileAlreadyExistsException
- 如果希望用 source 覆盖掉 target，需要用 StandardCopyOption 来控制

```java
Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
```

### 3.4.5 移动文件

```java
Path source = Paths.get("helloword/data.txt");
Path target = Paths.get("helloword/data.txt");

Files.move(source, target, StandardCopyOption.ATOMIC_MOVE);
```

StandardCopyOption.ATOMIC_MOVE 保证文件移动的原子性

### 3.4.6 删除文件

```java
Path target = Paths.get("helloword/target.txt");

Files.delete(target);
```

- 如果文件不存在，会抛异常 NoSuchFileException

### 3.4.7 删除目录

```java
Path target = Paths.get("helloword/d1");

Files.delete(target);
```

- 如果目录还有内容，会抛异常 DirectoryNotEmptyException

### 3.4.8 遍历目录文件

```java
public static void main(String[] args) throws IOException {
  Path path = Paths.get("/Library/Java/JavaVirtualMachines/jdk-11.0.12.jdk/");
  AtomicInteger dirCount = new AtomicInteger();
  AtomicInteger fileCount = new AtomicInteger();
  Files.walkFileTree(path, new SimpleFileVisitor<Path>(){

    // 遍历目录以前
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
      System.out.println(dir);
      dirCount.incrementAndGet();
      return super.preVisitDirectory(dir, attrs);
    }

    // 访问文件
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
      System.out.println(file);
      fileCount.incrementAndGet();
      return super.visitFile(file, attrs);
    }
  });

  System.out.println(dirCount); 
  System.out.println(fileCount); 
}
```

### 3.4.9 统计jar数目

```java
Path path = Paths.get("C:\\Program Files\\Java\\jdk1.8.0_91");
AtomicInteger fileCount = new AtomicInteger();
Files.walkFileTree(path, new SimpleFileVisitor<Path>(){
  @Override
  public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) 
    throws IOException {
    if (file.toFile().getName().endsWith(".jar")) {
      fileCount.incrementAndGet();
    }
    return super.visitFile(file, attrs);
  }
});
System.out.println(fileCount); // 724
```

### 3.4.10 删除多级目录

```java
Path path = Paths.get("d:\\a");
Files.walkFileTree(path, new SimpleFileVisitor<Path>(){
  @Override
  public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) 
    throws IOException {
    Files.delete(file);
    return super.visitFile(file, attrs);
  }

  @Override
  public FileVisitResult postVisitDirectory(Path dir, IOException exc) 
    throws IOException {
    Files.delete(dir);
    return super.postVisitDirectory(dir, exc);
  }
});
```

删除是危险操作，确保要递归删除的文件夹没有重要内容

### 3.4.11 拷贝多级目录

```java
long start = System.currentTimeMillis();
String source = "D:\\Snipaste-1.16.2-x64";
String target = "D:\\Snipaste-1.16.2-x64aaa";

Files.walk(Paths.get(source)).forEach(path -> {
  try {
    String targetName = path.toString().replace(source, target);
    // 是目录
    if (Files.isDirectory(path)) {
      Files.createDirectory(Paths.get(targetName));
    }
    // 是普通文件
    else if (Files.isRegularFile(path)) {
      Files.copy(path, Paths.get(targetName));
    }
  } catch (IOException e) {
    e.printStackTrace();
  }
});
long end = System.currentTimeMillis();
System.out.println(end - start);
```

# 4.网络编程

## 4.1 非阻塞 vs 阻塞

### 4.1.1 阻塞

- 阻塞模式下，相关方法都会导致线程暂停

	- ServerSocketChannel.accept 会在没有连接建立时让线程暂停
	- SocketChannel.read 会在没有数据可读时让线程暂停
	- 阻塞的表现其实就是线程暂停了，暂停期间不会占用 cpu，但线程相当于闲置

- 单线程下，阻塞方法之间相互影响，几乎不能正常工作，需要多线程支持

- 但多线程下，有新的问题，体现在以下方面

- - 32 位 jvm 一个线程 320k，64 位 jvm 一个线程 1024k，如果连接数过多，必然导致 OOM，并且线程太多，反而会因为频繁上下文切换导致性能降低
	- 可以采用线程池技术来减少线程数和线程上下文切换，但治标不治本，如果有很多连接建立，但长时间 inactive，会阻塞线程池中所有线程，因此不适合长连接，只适合短连接

测试代码：

服务器端：

```java
// 使用 nio 来理解阻塞模式, 单线程
// 0. ByteBuffer
ByteBuffer buffer = ByteBuffer.allocate(16);
// 1. 创建了服务器
ServerSocketChannel ssc = ServerSocketChannel.open();

// 2. 绑定监听端口
ssc.bind(new InetSocketAddress(8080));

// 3. 连接集合
List<SocketChannel> channels = new ArrayList<>();

while (true) {
  // 4. accept 建立与客户端连接， SocketChannel 用来与客户端之间通信
  log.debug("connecting...");
  SocketChannel sc = ssc.accept(); // 阻塞方法，线程停止运行
  log.debug("connected... {}", sc);
  channels.add(sc);
  for (SocketChannel channel : channels) {
    // 5. 接收客户端发送的数据
    log.debug("before read... {}", channel);
    channel.read(buffer); // 阻塞方法，线程停止运行
    buffer.flip();
    debugRead(buffer);
    buffer.clear();
    log.debug("after read...{}", channel);
  }
}
```

客户端：

```java
SocketChannel sc = SocketChannel.open();
sc.connect(new InetSocketAddress("localhost", 8080));
System.out.println("waiting...");
```

### 4.1.2 非阻塞

- 非阻塞模式下，相关方法都会不会让线程暂停

- - 在 ServerSocketChannel.accept 在没有连接建立时，会返回 null，继续运行
	- SocketChannel.read 在没有数据可读时，会返回 0，但线程不必阻塞，可以去执行其它 SocketChannel 的 read 或是去执行 ServerSocketChannel.accept 
	- 写数据时，线程只是等待数据写入 Channel 即可，无需等 Channel 通过网络把数据发送出去

- 但非阻塞模式下，即使没有连接建立，和可读数据，线程仍然在不断运行，白白浪费了 cpu

- 数据复制过程中，线程实际还是阻塞的（AIO 改进的地方）

服务器端，客户端代码不变

```java
// 使用 nio 来理解非阻塞模式, 单线程
// 0. ByteBuffer
ByteBuffer buffer = ByteBuffer.allocate(16);
// 1. 创建了服务器
ServerSocketChannel ssc = ServerSocketChannel.open();
ssc.configureBlocking(false); // 非阻塞模式!!!

// 2. 绑定监听端口
ssc.bind(new InetSocketAddress(8080));
// 3. 连接集合
List<SocketChannel> channels = new ArrayList<>();
while (true) {
  // 4. accept 建立与客户端连接， SocketChannel 用来与客户端之间通信
  SocketChannel sc = ssc.accept(); // 非阻塞，线程还会继续运行，如果没有连接建立，但sc是null
  if (sc != null) {
    log.debug("connected... {}", sc);
    sc.configureBlocking(false); // 非阻塞模式,影响下面的read(buffer)
    channels.add(sc);
  }
  for (SocketChannel channel : channels) {
    // 5. 接收客户端发送的数据
    int read = channel.read(buffer);// 非阻塞，线程仍然会继续运行，如果没有读到数据，read 返回 0
    if (read > 0) {
      buffer.flip();
      debugRead(buffer);
      buffer.clear();
      log.debug("after read...{}", channel);
    }
  }
}
```

### 4.1.3 多路复用

单线程可以配合 Selector 完成对多个 Channel 可读写事件的监控，这称之为多路复用

- 多路复用仅针对网络 IO、普通文件 IO 没法利用多路复用

- 如果不用 Selector 的非阻塞模式，线程大部分时间都在做无用功，而 Selector 能够保证

- - 有可连接事件时才去连接

	- 有可读事件才去读取

	- 有可写事件才去写入

	- - 限于网络传输能力，Channel 未必时时可写，一旦 Channel 可写，会触发 Selector 的可写事件

## 4.2 Selector

![img](02_NIO.assets/775d32a5-ce3b-4710-9b8b-a7fa09c4e479.png)

优点：

- 一个线程配合 selector 就可以监控多个 channel 的事件，事件发生线程才去处理。避免非阻塞模式下所做无用功
- 让这个线程能够被充分利用
- 节约了线程的数量
- 减少了线程上下文切换

### 4.2.1 创建

```java
Selector selector = Selector.open();
```

### 4.2.2 绑定CHannel事件

也称为注册事件，绑定的事件selector才会关心

```java
channel.configureBlocking(false); // 使Channel工作在非阻塞模式
SelectionKey key = channel.register(selector, 绑定事件);
```

- channel 必须工作在非阻塞模式

- FileChannel 没有非阻塞模式，因此不能配合 selector 一起使用

- 绑定的事件类型可以有

- - connect - 客户端连接成功时触发
	- accept - 服务器端成功接受连接时触发
	- read - 数据可读入时触发，有因为接收能力弱，数据暂不能读入的情况
	- write - 数据可写出时触发，有因为发送能力弱，数据暂不能写出的情况

### 4.2.3 监听Channel事件

可以通过下面三种方法来监听是否有事件发生，方法的返回值代表有多少 channel 发生了事件

```java
// 方法1：阻塞直到绑定事件发生
int count = selector.select();
// 方法2：阻塞直到绑定事件发生，或超时（事件单位 ms）
int count = selector.select(long timeout);
// 方法3：不会阻塞，也就是不管有没有事件，立刻返回，自己根据返回值检查是否有事件
int count = selector.selectNow();
```

### 4.2.4 select何时不阻塞?

- 客户端发生时

- - 客户端发起链接请求时，会触发accept事件
	- 客户端发送数据过来，客户端正常、异常关闭时，都会触发read() 事件，另外，如果发送的数据大于buffer缓冲区，会触发多次读取事件
	- channel 可写，会触发 write 事件
	- 在 linux 下 nio bug发生时

- 调用selector.wakeup()

- 调用selector.close()

- selector 所在线程 interrput

## 4.3 select处理accept事件

客户端代码：

```java
public class Client {
  public static void main(String[] args) {
    try (Socket socket = new Socket("localhost", 8080)) {
      System.out.println(socket);
      socket.getOutputStream().write("world".getBytes());
      System.in.read(); // 仅仅是阻塞住，不让程序关闭
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
```

服务端代码：

```java
@Slf4j
public class ChannelDemo6 {
  public static void main(String[] args) {
    try (ServerSocketChannel channel = ServerSocketChannel.open()) {
      channel.bind(new InetSocketAddress(8080));
      System.out.println(channel);
      Selector selector = Selector.open();
      channel.configureBlocking(false);
      channel.register(selector, SelectionKey.OP_ACCEPT);

      while (true) {
        int count = selector.select();
        // int count = selector.selectNow();
        log.debug("select count: {}", count);
        // if(count <= 0) {
        //	   continue;
        // }

        // 获取所有事件
        Set<SelectionKey> keys = selector.selectedKeys();

        // 遍历所有事件，逐一处理
        Iterator<SelectionKey> iter = keys.iterator();
        while (iter.hasNext()) {
          SelectionKey key = iter.next();
          // 判断事件类型
          if (key.isAcceptable()) {
            ServerSocketChannel c = (ServerSocketChannel) key.channel();
            // 必须处理
            SocketChannel sc = c.accept();
            log.debug("{}", sc);
          }
          // 处理完毕，必须将事件移除
          iter.remove();
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
```

**事件发生后能否不处理**

事件发生后，要么处理，要么取消（cancel），不能什么都不做，否则下次该事件仍会触发，这是因为 nio 底层使用的是水平触发

## 4.4. 处理read事件

```java
@Slf4j
public class ChannelDemo6 {
  public static void main(String[] args) {
    try (ServerSocketChannel channel = ServerSocketChannel.open()) {
      channel.bind(new InetSocketAddress(8080));
      System.out.println(channel);
      Selector selector = Selector.open();
      channel.configureBlocking(false);
      channel.register(selector, SelectionKey.OP_ACCEPT);

      while (true) {
        int count = selector.select();
        // int count = selector.selectNow();
        log.debug("select count: {}", count);
        // if(count <= 0) {
        //      continue;
        // }

        // 获取所有事件
        Set<SelectionKey> keys = selector.selectedKeys();

        // 遍历所有事件，逐一处理
        Iterator<SelectionKey> iter = keys.iterator();
        while (iter.hasNext()) {
          SelectionKey key = iter.next();
          // 判断事件类型
          if (key.isAcceptable()) {
            ServerSocketChannel c = (ServerSocketChannel) key.channel();
            // 必须处理
            SocketChannel sc = c.accept();
            sc.configureBlocking(false);
            sc.register(selector, SelectionKey.OP_READ);
            log.debug("连接已建立: {}", sc);
          } else if (key.isReadable()) {
            SocketChannel sc = (SocketChannel) key.channel();
            ByteBuffer buffer = ByteBuffer.allocate(128);
            int read = sc.read(buffer);
            // 正常断开 处理
            if(read == -1) {
              key.cancel();
              sc.close();
            } else {
              buffer.flip();
              debug(buffer);
            }
          }
          // 处理完毕，必须将事件移除
          iter.remove();
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
      key.cancel();// 因为客户端异常断开了，所以需要将key去取消，从selector的key集合中删除
    }
  }
}
```

**为何要iter.remove()**

因为 select 在事件发生后，就会将相关的 key 放入 selectedKeys 集合，但不会在处理完后从 selectedKeys 集合中移除，需要我们自己编码删除。例如

- 第一次触发了 ssckey 上的 accept 事件，没有移除 ssckey 
- 第二次触发了 sckey 上的 read 事件，但这时 selectedKeys 中还有上次的 ssckey ，在处理时因为没有真正的 serverSocket 连上了，就会导致空指针异常

**cancel的作用**

cancel 会取消注册在 selector 上的 channel，并从 keys 集合中删除 key 后续不会再监听事件

## 4.5. 粘包半包

![img](02_NIO.assets/48897f16-d488-4aef-8e2b-13d6f65a2691.png)

第一种情况：消息长度过长，只能对ByteBuffer扩容

总的解决思路：

![img](02_NIO.assets/e598d7a8-48fc-4a21-a1ec-89f744c570ff.jpg)

- 一种思路是固定消息长度，数据包大小一样，服务器按预定长度读取，缺点是浪费带宽

- 另一种思路是按分隔符拆分，缺点是效率低

- TLV 格式，即 Type 类型、Length 长度、Value 数据，类型和长度已知的情况下，就可以方便获取消息大小，分配合适的 buffer，缺点是 buffer 需要提前分配，如果内容过大，则影响 server 吞吐量

- - Http 1.1 是 TLV 格式
	- Http 2.0 是 LTV 格式

如果接收到的消息过长：

![img](02_NIO.assets/e53c0c99-a86e-48ad-b382-0d0401dae750.png)

 **固定分界符情况**

服务器端代码：

```java
@Slf4j
public class TestSelectorServer {

  private static void split(ByteBuffer source) {
    source.flip();
    for (int i = 0; i < source.limit(); i++) {
      // 找到一条完整消息
      if (source.get(i) == '\n') {
        int length = i + 1 - source.position();
        // 把这条完整消息存入新的 ByteBuffer
        ByteBuffer target = ByteBuffer.allocate(length);
        // 从 source 读，向 target 写
        for (int j = 0; j < length; j++) {
          target.put(source.get());
        }
        debugAll(target);
      }
    }
    source.compact(); // 0123456789abcdef  position 16 limit 16
  }

  public static void main(String[] args) throws IOException {
    // 1. 创建selector 管理多个channel
    Selector selector = Selector.open();

    ServerSocketChannel ssc = ServerSocketChannel.open();
    ssc.configureBlocking(false);

    // 2. 建立 selector 和 channel 的联系(注册)
    // SelectionKey 时间发生后通过它可以知道事件和哪个channel的事件
    SelectionKey sscKey = ssc.register(selector, 0, null);
    // key 只关注 accept 事件
    sscKey.interestOps(SelectionKey.OP_ACCEPT);
    log.debug("register key:{}", sscKey);

    ssc.bind(new InetSocketAddress(8080));
    while (true) {
      // 3. select方法，没有事件发生，线程阻塞，有事件，才会恢复运行
      // select 在时间未处理时，它不会阻塞，处理事件可以用accept或者cancel
      // 事件发生后，要么处理，要么取消，不能不处理
      selector.select();
      // 4. 处理事件,selectedKeys 内部包含了所有发生的事件
      Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
      while (iter.hasNext()) {
        SelectionKey key = iter.next();
        log.debug("key:{}", key);
        // 拿到key后 需要从 selectedKeys中删除，否则下次就会报错
        iter.remove();
        // 5. 处理区别类型
        if (key.isAcceptable()) {
          ServerSocketChannel channel = (ServerSocketChannel) key.channel();
          SocketChannel sc = channel.accept();
          sc.configureBlocking(false);
          ByteBuffer buffer = ByteBuffer.allocate(16); // attachment
          // 将一个ByteBuffer作为附件关联到selectionKey 上
          SelectionKey scKey = sc.register(selector, 0, buffer);
          scKey.interestOps(SelectionKey.OP_READ);
          log.debug("{}", sc);
          log.debug("scKey:{}", scKey);
        } else if (key.isReadable()) { // 如果是read事件
          try {
            SocketChannel channel = (SocketChannel) key.channel(); // 拿到channel的触发事件
            // 获取 selectionKey 上关联的附件
            ByteBuffer buffer = (ByteBuffer) key.attachment();
            int read = channel.read(buffer); // 如果是正常断开，read返回值是-1
            if (read == -1) {
              key.cancel();
            } else {
              //buffer.flip();
              // debugRead(buffer);
              //System.out.println(Charset.defaultCharset().decode(buffer));
              split(buffer);
              // 需要扩容
              if (buffer.position() == buffer.limit()) {
                ByteBuffer newBuffer = ByteBuffer.allocate(buffer.capacity() * 2);
                buffer.flip();
                newBuffer.put(buffer);
                key.attach(newBuffer);
              }
            }
          } catch (IOException e) {
            e.printStackTrace();
            key.cancel();// 因为客户端断开了，所以需要将key去取 消，从selector的key集合中删除
          }
        }
      }
    }
  }
}
```

客户端代码：

```java
SocketChannel sc = SocketChannel.open();
sc.connect(new InetSocketAddress("localhost", 8080));
SocketAddress address = sc.getLocalAddress();
// sc.write(Charset.defaultCharset().encode("hello\nworld\n"));
sc.write(Charset.defaultCharset().encode("0123\n456789abcdef"));
sc.write(Charset.defaultCharset().encode("0123456789abcdef3333\n"));
System.in.read();
```

**ByteBuffer大小分配**

- 每个 channel 都需要记录可能被切分的消息，因为 ByteBuffer 不能被多个 channel 共同使用，因此需要为每个 channel 维护一个独立的 ByteBuffer

- ByteBuffer 不能太大，比如一个 ByteBuffer 1Mb 的话，要支持百万连接就要 1Tb 内存，因此需要设计大小可变的 ByteBuffer

- - 一种思路是首先分配一个较小的 buffer，例如 4k，如果发现数据不够，再分配 8k 的 buffer，将 4k buffer 内容拷贝至 8k buffer，优点是消息连续容易处理，缺点是数据拷贝耗费性能，参考实现 
	- 另一种思路是用多个数组组成 buffer，一个数组不够，把多出来的内容写入新的数组，与前面的区别是消息存储不连续解析复杂，优点是避免了拷贝引起的性能损耗

## 4.5 处理 write 事件

**一次无法写完的例子**

- 非阻塞模式下，无法保证把 buffer 中所有数据都写入 channel，因此需要追踪 write 方法的返回值（代表实际写入字节数）

- 用 selector 监听所有 channel 的可写事件，每个 channel 都需要一个 key 来跟踪 buffer，但这样又会导致占用内存过多，就有两阶段策略

- - 当消息处理器第一次写入消息时，才将 channel 注册到 selector 上
	- selector 检查 channel 上的可写事件，如果所有的数据写完了，就取消 channel 的注册
	- 如果不取消，会每次可写均会触发 write 事件

服务器代码：

```java
public class WriteServer {

  public static void main(String[] args) throws IOException {
    ServerSocketChannel ssc = ServerSocketChannel.open();
    ssc.configureBlocking(false);
    ssc.bind(new InetSocketAddress(8080));

    Selector selector = Selector.open();
    ssc.register(selector, SelectionKey.OP_ACCEPT);

    while(true) {
      selector.select();

      Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
      while (iter.hasNext()) {
        SelectionKey key = iter.next();
        iter.remove();
        if (key.isAcceptable()) {
          SocketChannel sc = ssc.accept();
          sc.configureBlocking(false);
          SelectionKey sckey = sc.register(selector, SelectionKey.OP_READ);
          // 1. 向客户端发送内容
          StringBuilder sb = new StringBuilder();
          for (int i = 0; i < 3000000; i++) {
            sb.append("a");
          }
          ByteBuffer buffer = Charset.defaultCharset().encode(sb.toString());
          int write = sc.write(buffer);
          // 3. write 表示实际写了多少字节
          System.out.println("实际写入字节:" + write);
          // 4. 如果有剩余未读字节，才需要关注写事件
          if (buffer.hasRemaining()) {
            // read 1  write 4
            // 在原有关注事件的基础上，多关注 写事件
            sckey.interestOps(sckey.interestOps() + SelectionKey.OP_WRITE);
            // 把 buffer 作为附件加入 sckey
            sckey.attach(buffer);
          }
        } else if (key.isWritable()) {
          ByteBuffer buffer = (ByteBuffer) key.attachment();
          SocketChannel sc = (SocketChannel) key.channel();
          int write = sc.write(buffer);
          System.out.println("实际写入字节:" + write);
          if (!buffer.hasRemaining()) { // 写完了
            key.interestOps(key.interestOps() - SelectionKey.OP_WRITE);
            key.attach(null);
          }
        }
      }
    }
  }
}
```

客户端代码：

```java
public class WriteClient {
  public static void main(String[] args) throws IOException {
    Selector selector = Selector.open();
    SocketChannel sc = SocketChannel.open();
    sc.configureBlocking(false);
    sc.register(selector, SelectionKey.OP_CONNECT | SelectionKey.OP_READ);
    sc.connect(new InetSocketAddress("localhost", 8080));
    int count = 0;
    while (true) {
      selector.select();
      Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
      while (iter.hasNext()) {
        SelectionKey key = iter.next();
        iter.remove();
        if (key.isConnectable()) {
          System.out.println(sc.finishConnect());
        } else if (key.isReadable()) {
          ByteBuffer buffer = ByteBuffer.allocate(1024 * 1024);
          count += sc.read(buffer);
          buffer.clear();
          System.out.println(count);
        }
      }
    }
  }
}
```

**write为何要取消**

只要向 channel 发送数据时，socket 缓冲可写，这个事件会频繁触发，因此应当只在 socket 缓冲区写不下时再关注可写事件，数据写完之后再取消关注

## 4.6 Selector与多线程

现在都是多核CPU，设计时要充分考虑让CPU的力量被白白浪费

前面的代码只有一个选择器，没有充分利用多核 cpu，如何改进呢？

分两组选择器

- 单线程配一个selector，专门处理 accept 事件
- 创建 cpu 核心数的线程，每个线程配一个选择器，轮流处理 read 事件

这里有点用了reactor思想

服务端代码：

- 这里有个关键点：每次新的请求来，Boss接收后，要给worker放入一个队列中，并且把worker线程weakup()
- worker线程weakup以后，先去看阻塞队列里有没有任务执行，如果有，就先执行阻塞队列里的任务
- 不这样会出现的问题：selector.select(); 阻塞并不会因为accept事件唤醒。

```java
@Slf4j
public class NIOReactorServer {

    public static void main(String[] args) throws IOException {
        Thread.currentThread().setName("boss");
        ServerSocketChannel ssc = ServerSocketChannel.open(); // 获得NIO服务端
        ssc.configureBlocking(false); // 设置非阻塞
        // 配置Boss这个Selector
        Selector boss = Selector.open();
        SelectionKey bossKey = ssc.register(boss, 0, null);
        bossKey.interestOps(SelectionKey.OP_ACCEPT); // boss只关注accept事件

        // 服务端绑定8080 端口
        ssc.bind(new InetSocketAddress(8080));

        // 1. 创建worker并初始化
        Worker[] workers = new Worker[2];
        for (int i = 0; i < workers.length; i++) {
            workers[i] = new Worker("worker-"+i);
        }

        AtomicInteger index = new AtomicInteger();
        while (true) {
            boss.select();
            Iterator<SelectionKey> iter = boss.selectedKeys().iterator();
            while (iter.hasNext()) {
                SelectionKey key = iter.next();
                iter.remove();
                if (key.isAcceptable()) {
                    SocketChannel sc = ssc.accept();
                    sc.configureBlocking(false);
                    log.debug("connected...{}", sc.getRemoteAddress());
                    // 2. 建立链接 关联selector 读写操作交给worker线程去做
                    log.debug("before register...{}", sc.getRemoteAddress());

                    /**
                     * 这个Register一定要先于sc.register执行
                     * 因为一旦阻塞住，新注册的SocketChannel不会被监听
                     */
                    workers[index.getAndIncrement()% workers.length].register(sc);
                    log.debug("after register...{}", sc.getRemoteAddress());
                }
            }
        }
    }

    /**
     * 处理读写事件
     */
    static class Worker implements Runnable {
        private Thread thread; // worker所属线程
        private Selector selector; // worker持有selector
        private String name; // worker名字
        private volatile boolean start = false; //还未初始化
        private ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<>();

        public Worker(String name) {
            this.name = name;
        }

        /**
         * 初始化线程和selector
         */
        public void register(SocketChannel sc) throws IOException {
            // start变量标记此段代码只执行一次
            if (!start) {
                thread = new Thread(this, this.name);
                selector = Selector.open();
                thread.start();
                start = true;
            }
            // 向队列添加任务，但这个任务并没有立刻执行
            queue.add(() -> {
                try {
                    sc.register(selector, SelectionKey.OP_READ, null);
                } catch (ClosedChannelException e) {
                    e.printStackTrace();
                }
            });
            selector.wakeup(); // 唤醒selector 添加完任务，告诉selector 该干活了！
        }

        @Override
        public void run() {
            while (true) {
                try {
                    selector.select(); // 阻塞等事件
                    Runnable task = queue.poll();
                    if (task != null) {
                        task.run(); // 在这里执行了真正的注册
                    }
                    // 如果有事件发生，阻塞被唤醒，拿到所有事件并遍历
                    Iterator<SelectionKey> iter = selector.selectedKeys().iterator();
                    while (iter.hasNext()) {
                        SelectionKey key = iter.next();
                        iter.remove();
                        // 可读
                        if (key.isReadable()) {
                            ByteBuffer buffer = ByteBuffer.allocate(16);
                            SocketChannel channel = (SocketChannel) key.channel();
                            log.debug("read....{}", channel.getRemoteAddress());
                            channel.read(buffer);
                            buffer.flip();
                            System.out.println("打印BUFFER");
                            // 打印buffer
                        } else if (key.isWritable()) {
                            // 省略
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
```

客户端代码：

```java
public class NIOReactorClient {

    public static void main(String[] args) throws IOException {
        SocketChannel sc = SocketChannel.open();
        sc.connect(new InetSocketAddress("localhost", 8080));
        sc.write(Charset.defaultCharset().encode("1234567890abcdef"));
        System.in.read();
    }
}
```



## 4.7 UDP

- UDP 是无连接的，client 发送数据不会管 server 是否开启
- server 这边的 receive 方法会将接收到的数据存入 byte buffer，但如果数据报文超过 buffer 大小，多出来的数据会被默默抛弃

不是重点，演示一下：

服务器：

```java
public class UdpServer {
    public static void main(String[] args) {
        try (DatagramChannel channel = DatagramChannel.open()) {
            channel.socket().bind(new InetSocketAddress(9999));
            System.out.println("waiting...");
            ByteBuffer buffer = ByteBuffer.allocate(32);
            channel.receive(buffer);
            buffer.flip();
            debug(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

客户端：

```java
public class UdpClient {
    public static void main(String[] args) {
        try (DatagramChannel channel = DatagramChannel.open()) {
            ByteBuffer buffer = StandardCharsets.UTF_8.encode("hello");
            InetSocketAddress address = new InetSocketAddress("localhost", 9999);
            channel.send(buffer, address);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

# 5.NIO与BIO

## 5.1 stream vs channel

- stream 不会自动缓冲数据，channel 会利用系统提供的发送缓冲区、接收缓冲区（更为底层）
- stream 仅支持阻塞 API，channel 同时支持阻塞、非阻塞 API，网络 channel 可配合 selector 实现多路复用
- 二者均为全双工，即读写可以同时进行

## 5.2 IO模型

概念辨析：同步阻塞、同步非阻塞、同步多路复用、异步阻塞（没有此情况）、异步非阻塞

- 同步：线程自己去获取结果（一个线程）
- 异步：线程自己不去获取结果，而是由其它线程送结果（至少两个线程）

当调用一次 channel.read 或 stream.read 后，会切换至操作系统内核态来完成真正数据读取，而读取又分为两个阶段，分别为：

- 等待数据阶段
- 复制数据阶段

![image-20211014210006048](02_NIO.assets/image-20211014210006048.png)

### 5.2.1 阻塞IO

![image-20211014210155639](02_NIO.assets/image-20211014210155639.png)

### 5.2.2 非阻塞IO

![image-20211014210207936](02_NIO.assets/image-20211014210207936.png)

### 5.2.3 多路复用

![image-20211014210214380](02_NIO.assets/image-20211014210214380.png)

### 5.2.4 异步IO

![image-20211014210222645](02_NIO.assets/image-20211014210222645.png)

### 5.2.5 阻塞IO vs 多路复用

![image-20211014210259884](02_NIO.assets/image-20211014210259884.png)

![image-20211014210304264](02_NIO.assets/image-20211014210304264.png)

## 5.3 零拷贝

### 5.3.1 传统IO问题

传统的 IO 将一个文件通过 socket 写出

```java
File f = new File("helloword/data.txt");
RandomAccessFile file = new RandomAccessFile(file, "r");

byte[] buf = new byte[(int)f.length()];
file.read(buf);

Socket socket = ...;
socket.getOutputStream().write(buf);
```

内部工作流程是这样的：

![image-20211014210339892](02_NIO.assets/image-20211014210339892.png)

1. java 本身并不具备 IO 读写能力，因此 read 方法调用后，要从 java 程序的 **用户态** 切换至 **内核态** ，去调用操作系统（Kernel）的读能力，将数据读入 **内核缓冲区** 。这期间用户线程阻塞，操作系统使用 DMA（Direct Memory Access）来实现文件读，其间也不会使用 cpu

   DMA 也可以理解为硬件单元，用来解放 cpu 完成文件 IO

2.  从 **内核态** 切换回 **用户态** ，将数据从 **内核缓冲区** 读入 **用户缓冲区** （即 byte[] buf），这期间 cpu 会参与拷贝，无法利用 DMA

3. 调用 write 方法，这时将数据从 **用户缓冲区** （byte[] buf）写入 **socket 缓冲区**，cpu 会参与拷贝

4. 接下来要向网卡写数据，这项能力 java 又不具备，因此又得从 **用户态** 切换至 **内核态** ，调用操作系统的写能力，使用 DMA 将 **socket 缓冲区**的数据写入网卡，不会使用 cpu

可以看到中间环节较多，java 的 IO 实际不是物理设备级别的读写，而是缓存的复制，底层的真正读写是操作系统来完成的

- 用户态与内核态的切换发生了 3 次，这个操作比较重量级
- 数据拷贝了共 4 次

### 5.3.2 NIO优化

通过 DirectByteBuf 

- ByteBuffer.allocate(10) HeapByteBuffer 使用的还是 java 内存
- ByteBuffer.allocateDirect(10) DirectByteBuffer 使用的是操作系统内存

![image-20211014220719311](02_NIO.assets/image-20211014220719311.png)

大部分步骤与优化前相同，不再赘述。唯有一点：java 可以使用 DirectByteBuf 将堆外内存映射到 jvm 内存中来直接访问使用

- 这块内存不受 jvm 垃圾回收的影响，因此内存地址固定，有助于 IO 读写

- java 中的 DirectByteBuf 对象仅维护了此内存的虚引用，内存回收分成两步

- - DirectByteBuf 对象被垃圾回收，将虚引用加入引用队列
  - 通过专门线程访问引用队列，根据虚引用释放堆外内存 

- 减少了一次数据拷贝，用户态与内核态的切换次数没有减少

**进一步优化**（底层采用了 linux 2.1 后提供的 sendFile 方法），java 中对应着两个 channel 调用 transferTo/transferFrom 方法拷贝数据

![image-20211014220750286](02_NIO.assets/image-20211014220750286.png)

1. java 调用 transferTo 方法后，要从 java 程序的 **用户态**切换至 **内核态** ，使用 DMA将数据读入 **内核缓冲区** ，不会使用 cpu
2. 数据从 **内核缓冲区** 传输到 **socket 缓冲区** ，cpu 会参与拷贝
3. 最后使用 DMA 将 **socket 缓冲区** 的数据写入网卡，不会使用 cpu

可以看到

- 只发生了一次用户态与内核态的切换
- 数据拷贝了 3 次

 **进一步优化**（linux 2.4）

![image-20211014220841111](02_NIO.assets/image-20211014220841111.png)

- java 调用 transferTo 方法后，要从 java 程序的 **用户态** 切换至 **内核态** ，使用 DMA将数据读入 **内核缓冲区** ，不会使用 cpu
- 只会将一些 offset 和 length 信息拷入 **socket 缓冲区** ，几乎无消耗
- 使用 DMA 将 **内核缓冲区** 的数据写入网卡，不会使用 cpu

整个过程仅只发生了一次用户态与内核态的切换，数据拷贝了 2 次。所谓的【**零拷贝**】，并不是真正无拷贝，而是在不会拷贝重复数据到 jvm 内存中，零拷贝的优点有

- 更少的用户态与内核态的切换
- 不利用 cpu 计算，减少 cpu 缓存伪共享
- 零拷贝适合小文件传输

## 5.4 AIO

AIO 用来解决数据复制阶段的阻塞问题

- 同步意味着，在进行读写操作时，线程需要等待结果，还是相当于闲置
- 异步意味着，在进行读写操作时，线程不必等待结果，而是将来由操作系统来通过回调方式由另外的线程来获得结果

异步模型需要底层操作系统（Kernel）提供支持

Windows 系统通过 IOCP 实现了真正的异步 IO

Linux 系统异步 IO 在 2.6 版本引入，但其底层实现还是用多路复用模拟了异步 IO，性能没有优势

### 文件AIO

```java
@Slf4j
public class AioFileChannel {
    public static void main(String[] args) {
        try (AsynchronousFileChannel channel = AsynchronousFileChannel.open(Paths.get("data.txt"), StandardOpenOption.READ)) {
            // 参数1  ByteBuffer
            // 参数2  读取的起始位置
            // 参数3  附件
            // 参数4  回调对象
            ByteBuffer buffer = ByteBuffer.allocate(16);
            log.debug("read begin");
            channel.read(buffer, 0, buffer, new CompletionHandler<Integer, ByteBuffer>() {
                @Override // read 成功
                public void completed(Integer result, ByteBuffer attachment) {
                    log.debug("read completed..{}",result);
                    attachment.flip();
                    debugAll(attachment);
                }

                @Override // read失败
                public void failed(Throwable exc, ByteBuffer attachment) {
                    exc.printStackTrace();
                }
            });
            log.debug("read end...");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 因为异步IO返回结果的是守护线程，主线程现在还不能结束
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
/*
16:11:12 [DEBUG] [main] c.i.n.c.AioFileChannel - read begin
16:11:12 [DEBUG] [main] c.i.n.c.AioFileChannel - read end...
16:11:12 [DEBUG] [Thread-12] c.i.n.c.AioFileChannel - read completed..13
+--------+-------------------- all ------------------------+----------------+
position: [0], limit: [13]
         +-------------------------------------------------+
         |  0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f |
+--------+-------------------------------------------------+----------------+
|00000000| 31 32 33 34 35 36 37 38 39 30 61 62 63 00 00 00 |1234567890abc...|
+--------+-------------------------------------------------+----------------+
*/
```

可以看到

- 响应文件读取成功的是另一个线程 Thread-12
- 主线程并没有 IO 操作阻塞

文件 AIO 使用的线程都是守护线程，所以最后要执行 `System.in.read()` 以避免守护线程意外结束

### 网络AIO

```java
public class AioServer {
    public static void main(String[] args) throws IOException {
        AsynchronousServerSocketChannel ssc = AsynchronousServerSocketChannel.open();
        ssc.bind(new InetSocketAddress(8080));
        ssc.accept(null, new AcceptHandler(ssc));
        System.in.read();
    }

    private static void closeChannel(AsynchronousSocketChannel sc) {
        try {
            System.out.printf("[%s] %s close\n", Thread.currentThread().getName(), sc.getRemoteAddress());
            sc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class ReadHandler implements CompletionHandler<Integer, ByteBuffer> {
        private final AsynchronousSocketChannel sc;

        public ReadHandler(AsynchronousSocketChannel sc) {
            this.sc = sc;
        }

        @Override
        public void completed(Integer result, ByteBuffer attachment) {
            try {
                if (result == -1) {
                    closeChannel(sc);
                    return;
                }
                System.out.printf("[%s] %s read\n", Thread.currentThread().getName(), sc.getRemoteAddress());
                attachment.flip();
                System.out.println(Charset.defaultCharset().decode(attachment));
                attachment.clear();
                // 处理完第一个 read 时，需要再次调用 read 方法来处理下一个 read 事件
                sc.read(attachment, attachment, this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void failed(Throwable exc, ByteBuffer attachment) {
            closeChannel(sc);
            exc.printStackTrace();
        }
    }

    private static class WriteHandler implements CompletionHandler<Integer, ByteBuffer> {
        private final AsynchronousSocketChannel sc;

        private WriteHandler(AsynchronousSocketChannel sc) {
            this.sc = sc;
        }

        @Override
        public void completed(Integer result, ByteBuffer attachment) {
            // 如果作为附件的 buffer 还有内容，需要再次 write 写出剩余内容
            if (attachment.hasRemaining()) {
                sc.write(attachment);
            }
        }

        @Override
        public void failed(Throwable exc, ByteBuffer attachment) {
            exc.printStackTrace();
            closeChannel(sc);
        }
    }

    private static class AcceptHandler implements CompletionHandler<AsynchronousSocketChannel, Object> {
        private final AsynchronousServerSocketChannel ssc;

        public AcceptHandler(AsynchronousServerSocketChannel ssc) {
            this.ssc = ssc;
        }

        @Override
        public void completed(AsynchronousSocketChannel sc, Object attachment) {
            try {
                System.out.printf("[%s] %s connected\n", Thread.currentThread().getName(), sc.getRemoteAddress());
            } catch (IOException e) {
                e.printStackTrace();
            }
            ByteBuffer buffer = ByteBuffer.allocate(16);
            // 读事件由 ReadHandler 处理
            sc.read(buffer, buffer, new ReadHandler(sc));
            // 写事件由 WriteHandler 处理
            sc.write(Charset.defaultCharset().encode("server hello!"), ByteBuffer.allocate(16), new WriteHandler(sc));
            // 处理完第一个 accpet 时，需要再次调用 accept 方法来处理下一个 accept 事件
            ssc.accept(null, this);
        }

        @Override
        public void failed(Throwable exc, Object attachment) {
            exc.printStackTrace();
        }
    }
}
```

