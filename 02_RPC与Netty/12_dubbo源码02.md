# 1.Dubbo SPI机制

在 Dubbo 中，SPI 是一个非常重要的模块。基于 SPI，我们可以很容易的对 Dubbo 进行拓展。如果大家想要学习 Dubbo 的源码，SPI 机制务必弄懂。接下来，我们先来了解一下 Java SPI 与 Dubbo SPI 的用法，然后再来分析 Dubbo SPI 的源码。

## 1.1 SPI概述

### 1.1.1 SPI的主要作用

SPI全程Service Provider Interface，是**一种服务发现机制**。SPI 的本质是**将接口实现类的全限定名配置在文件中**，并由**服务加载器读取配置文件，加载实现类**。这样可以在运行时，动态为接口替换实现类。正因此特性，我们可以很容易的通过 SPI 机制为我们的程序提供拓展功能。

![image-20211114132817971](12_dubbo源码02.assets/image-20211114132817971.png)

Java SPI 实际上是“**基于接口的编程**＋**策略模式**＋**配置文件**”组合实现的动态加载机制。

### 1.1.2 入门案例

![image-20211114135807365](12_dubbo源码02.assets/image-20211114135807365.png)

首先，我们定义一个接口，名称为 Robot。

```java
public interface Robot {
    void sayHello();
}
```

接下来，定义两个实现类，分别是OptimusPrime 和 Bumblebee

```java
public class OptimusPrime implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Optimus Prime.");
    }
}

public class Bumblebee implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Bumblebee.");
    }
}
```

接下来 META-INF/services 文件夹下创建一个文件，名称为 Robot 的全限定名
com.itheima.java.spi.Robot。文件内容为实现类的全限定的类名，如下：

```
com.lishem.java.spi.impl.Bumblebee
com.lishem.java.spi.impl.OptimusPrime
```

做好所需的准备工作，接下来编写代码进行测试。

```java
public class JavaSPITest {

    public static void main(String[] args) {
        ServiceLoader<Robot> serviceLoader = ServiceLoader.load(Robot.class);
        System.out.println("Java SPI");
        serviceLoader.forEach(Robot::sayHello);
    }
}
```

执行后可以看到结果：

![image-20211114135956287](12_dubbo源码02.assets/image-20211114135956287.png)

从测试结果可以看出，我们的两个实现类被成功的加载，并输出了相应的内容。

### 1.1.3 总结

**调用过程：**

- 应用程序调用ServiceLoader.load方法，创建一个新的ServiceLoader，并实例化该类中的成员变量
- 应用程序通过迭代器接口获取对象实例，ServiceLoader先判断成员变量providers对象中(LinkedHashMap<String,S>类型)是否有缓存实例对象，如果有缓存，直接返回。 如果没有缓存，执行类的装载，

**优点：**

使用 Java SPI 机制的优势是实现解耦，使得接口的定义与具体业务实现分离，而不是耦合在一起。应用进程可以根据实际业务情况启用或替换具体组件。

**缺点：**

- 不能按需加载。虽然 ServiceLoader 做了延迟载入，但是基本只能通过遍历全部获取，也就是接口的实现类得全部载入并实例化一遍。如果你并不想用某些实现类，或者某些类实例化很耗时，它也被载入并实例化了，这就造成了浪费。
- 获取某个实现类的方式不够灵活，只能通过 Iterator 形式获取，不能根据某个参数来获取对应的实现类。
- 多个并发多线程使用 ServiceLoader 类的实例是不安全的。
- 加载不到实现类时抛出并不是真正原因的异常，错误很难定位。

## 1.2 Dubbo中的SPI

### 1.2.1 概述

Dubbo 并未使用 Java SPI，而是重新实现了一套功能更强的 SPI 机制。Dubbo SPI 的相关逻辑被封装在了 ExtensionLoader 类中，通过 ExtensionLoader，我们可以加载指定的实现类。

### 1.2.2 入门案例

![image-20211114145223471](12_dubbo源码02.assets/image-20211114145223471.png)

Maven 依赖：

```xml
<dependencies>
    <dependency>
        <groupId>org.apache.dubbo</groupId>
        <artifactId>dubbo-common</artifactId>
        <version>2.7.8</version>
    </dependency>
</dependencies>
```

Robot接口

```java
@SPI
public interface Robot {

    void sayHello();
}
```

两个实现类：

```java
public class Bumblebee implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Bumblebee.");
    }
}

public class OptimusPrime implements Robot {

    @Override
    public void sayHello() {
        System.out.println("Hello, I am Optimus Prime.");
    }
}
```

配置文件：  命名 com.lishem.dubbo.spi.Robot 接口全类名

```
optimusPrime=com.lishem.dubbo.spi.impl.OptimusPrime
bumblebee=com.lishem.dubbo.spi.impl.Bumblebee
```

测试：

```java
public class DubboSpiTest {

    public static void main(String[] args) {
        ExtensionLoader<Robot> extensionLoader =
            ExtensionLoader.getExtensionLoader(Robot.class);
        Robot optimusPrime = extensionLoader.getExtension("optimusPrime");
        optimusPrime.sayHello();
        Robot bumblebee = extensionLoader.getExtension("bumblebee");
        bumblebee.sayHello();
    }
}
```

这样就能拿到Dubbo SPI机制实例化出来的对象

也可以指定默认用哪个实例：

```java
@SPI("bumblebee") //bumblebee表示默认加载该对象实例
public interface Robot {

    void sayHello();
}

ExtensionLoader<Robot> extensionLoader = ExtensionLoader.getExtensionLoader(Robot.class);
Robot de = extensionLoader.getDefaultExtension();
de.sayHello();
```

### 1.2.3 源码分析

上面演示了Dubbo SPI的使用 方法，首先通过 ExtensionLoader 的getExtensionLoader() 方法获取一个ExtensionLoader实例，然后通过ExtensionLoader 的getExtension() 方法获取拓展类对象。

下面我们从 **ExtensionLoader** 的 **getExtensionLoader** 和 **getExtension**作为入口，对拓展类对象的获取过程进行详细的分析。

**1） getExtensionLoader**

```java
public static <T> ExtensionLoader<T> getExtensionLoader(Class<T> type) {
    // type是否为空
    if (type == null) {
        throw new IllegalArgumentException("Extension type == null");
    }
    // type是否为接口
    if (!type.isInterface()) {
        throw new IllegalArgumentException("Extension type (" + type + ") is not an interface!");
    }
    // 判断type上是否有SPI注解
    if (!withExtensionAnnotation(type)) {
        throw new IllegalArgumentException("Extension type (" + type +
                                           ") is not an extension, because it is NOT annotated with @" + SPI.class.getSimpleName() + "!");
    }

    // 从缓存中获取ExtensionLoader
    ExtensionLoader<T> loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    if (loader == null) {
        // 创建ExtensionLoader,并且存入到EXTENSION_LOADERS 中
        EXTENSION_LOADERS.putIfAbsent(type, new ExtensionLoader<T>(type));
        loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    }
    return loader;
}
```

**2) getExtension**

```java
// ExtensionLoader.java
@SuppressWarnings("unchecked")
public T getExtension(String name) {
    return getExtension(name, true);
}

public T getExtension(String name, boolean wrap) {
    if (StringUtils.isEmpty(name)) {
        throw new IllegalArgumentException("Extension name == null");
    }
    if ("true".equals(name)) {
        // 获取默认的拓展实现类
        return getDefaultExtension();
    }
    // Holder，顾名思义，用于持有目标对象
    final Holder<Object> holder = getOrCreateHolder(name);
    Object instance = holder.get();
    // 双重检查
    if (instance == null) {
        synchronized (holder) {
            instance = holder.get();
            if (instance == null) {
                // 创建拓展实例
                instance = createExtension(name, wrap);
                // 设置实例到 holder 中
                holder.set(instance);
            }
        }
    }
    return (T) instance;
}

private Holder<Object> getOrCreateHolder(String name) {
    Holder<Object> holder = cachedInstances.get(name);
    if (holder == null) {
        cachedInstances.putIfAbsent(name, new Holder<>());
        holder = cachedInstances.get(name);
    }
    return holder;
}
```

上面的代码逻辑比较简单，首先检查缓存，缓存未命中则创建拓展对象。

```java
private T createExtension(String name, boolean wrap) {
    // 从Map中获取指定name的字节码对象class
    // 从配置文件中加载所有的拓展类，可得到“配置项名称”到“配置类”的映射关系表
    Class<?> clazz = getExtensionClasses().get(name);
    if (clazz == null) {
        throw findException(name);
    }
    try {
        T instance = (T) EXTENSION_INSTANCES.get(clazz);
        // 通过反射创建实例
        if (instance == null) {
            EXTENSION_INSTANCES.putIfAbsent(clazz, clazz.newInstance());
            instance = (T) EXTENSION_INSTANCES.get(clazz);
        }
        // 向实例中注入依赖 1.3.1 
        injectExtension(instance);

        if (wrap) {
            List<Class<?>> wrapperClassesList = new ArrayList<>();
            if (cachedWrapperClasses != null) {
                wrapperClassesList.addAll(cachedWrapperClasses);
                wrapperClassesList.sort(WrapperComparator.COMPARATOR);
                Collections.reverse(wrapperClassesList);
            }

            if (CollectionUtils.isNotEmpty(wrapperClassesList)) {
                // 循环创建 Wrapper 实例
                for (Class<?> wrapperClass : wrapperClassesList) {
                    // 将当前 instance 作为参数传给 Wrapper 的构造方法，并通过反射创建Wrapper 实例。
                    // 然后向 Wrapper 实例中注入依赖，最后将 Wrapper 实例再次赋值给instance 变量
                    Wrapper wrapper = wrapperClass.getAnnotation(Wrapper.class);
                    if (wrapper == null
                        || (ArrayUtils.contains(wrapper.matches(), name) && !ArrayUtils.contains(wrapper.mismatches(), name))) {
                        instance = injectExtension((T) wrapperClass.getConstructor(type).newInstance(instance));
                    }
                }
            }
        }

        initExtension(instance);
        return instance;
    } catch (Throwable t) {
        throw new IllegalStateException("Extension instance (name: " + name + ", class: " +
                                        type + ") couldn't be instantiated: " + t.getMessage(), t);
    }
}
```

createExtension方法的逻辑：

1. 通过getExtensionClasses 获取所有的拓展类
2. 通过反射创建拓展对象
3. 向拓展对象中注入依赖
4. 将拓展对象包裹在相应的 Wrapper 对象中

以上步骤中，第一个步骤是加载拓展类的关键，第三和第四个步骤是 Dubbo IOC 与 AOP 的具体实现。由于此类设计源码较多，这里简单的总结下ExtensionLoader整个执行逻辑：

```
getExtension(String name) #根据key获取拓展对象
    -->createExtension(String name) #创建拓展实例
        -->getExtensionClasses #根据路径获取所有的拓展类
        	-->loadExtensionClasses #加载拓展类
        		-->cacheDefaultExtensionName #解析@SPI注解
            -->loadDirectory #方法加载指定文件夹配置文件
                -->loadResource #加载资源
                    -->loadClass #加载类，并通过 loadClass 方法对类进行缓存
```

## 1.3 SPI中的IOC与AOP

### 1.3.1 依赖注入

Dubbo IOC 是通过 setter 方法注入依赖。Dubbo 首先会通过反射获取到实例的所有方法，然后再遍历方法列表，检测方法名是否具有 setter 方法特征。若有，则通过 ObjectFactory 获取依赖对象，最后通过反射调用 setter 方法将依赖设置到目标对象中。整个过程对应的代码如下：

```java
/**
 * 依赖注入
 * @param instance
 * @return
 */
private T injectExtension(T instance) {

    if (objectFactory == null) {
        return instance;
    }

    try {
        // 获取所有方法
        for (Method method : instance.getClass().getMethods()) {
            // 判断方法是否是set方法
            if (!isSetter(method)) {
                continue;
            }
            /**
                 * Check {@link DisableInject} to see if we need auto injection for this property
                 */
            if (method.getAnnotation(DisableInject.class) != null) {
                continue;
            }
            // 获取第1个参数，只支持一个参数
            Class<?> pt = method.getParameterTypes()[0];
            if (ReflectUtils.isPrimitives(pt)) {
                continue;
            }

            try {
                // 获取set方法的名字（去除了set，就是属性名字）
                String property = getSetterProperty(method);
                // 获取参数实例 objectFactory 工厂对象
                Object object = objectFactory.getExtension(pt, property);
                if (object != null) {
                    // 调用set方法实现依赖注入
                    method.invoke(instance, object);
                }
            } catch (Exception e) {
                logger.error("Failed to inject via method " + method.getName()
                             + " of interface " + type.getName() + ": " + e.getMessage(), e);
            }

        }
    } catch (Exception e) {
        logger.error(e.getMessage(), e);
    }
    return instance;
}
```

在上面代码中，objectFactory 变量的类型为 **AdaptiveExtensionFactory**，AdaptiveExtensionFactory内部维护了一个 ExtensionFactory 列表，用于存储其他类型的 ExtensionFactory。Dubbo 目前提供了两种 ExtensionFactory，分别是 **SpiExtensionFactory** 和 **SpringExtensionFactory**。前者用于创建自适应的拓展，后者是用于从 Spring 的 IOC 容器中获取所需的拓展。

Dubbo IOC 目前仅支持setter方法注入。

### 1.3.2 动态增强

在用Spring的时候，我们经常会用到AOP功能。在目标类的方法前后插入其他逻辑。比如通常使用Spring AOP来实现日志，监控和鉴权等功能。 Dubbo的扩展机制，是否也支持类似的功能呢？答案是yes。在Dubbo中，有一种特殊的类，被称为Wrapper类。通过装饰者模式，使用包装类包装原始的扩展点实例。在原始扩展点实现前后插入其他逻辑，实现AOP功能。

#### 1)装饰者模式

装饰者模式：在不改变原类文件以及不使用继承的情况下，动态地将责任附加到对象上，从而实现动态拓展一个对象的功能。它是通过创建一个包装对象，也就是装饰来包裹真实的对象。

一般来说装饰者模式有下面几个参与者：

- **Component**:装饰者和被装饰者共同的父类，是一个接口或者抽象类，用来定义基本行为
- **ConcreteComponent**：定义具体对象，即被装饰者
- **Decorator**: 抽象装饰者，继承自继承自Component，从外类来扩展ConcreteComponent。对于ConcreteComponent来说，不需要知道Decorator的存在，Decorator是一个接口或抽象类
- **ConcreteDecorator**：具体装饰者，用于扩展ConcreteComponent

注：装饰者和被装饰者对象有**相同的超类型**，因为装饰者和被装饰者必须是一样的类型，这里利用继承是为了达到类型匹配，而不是利用继承获得行为。

#### 2)普通装饰者模式案例：

Component:

```java
public interface Shape {
    void draw();
}
```

ConcreteComponent:

```java
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("绘制圆形");
    }
}
```

ConcreteDecorator:

```java
/**
 * 装饰者
 *  对Circle增强
 *      1、执行增强逻辑
 *      2、执行被装饰者对象的方法
 *   前提条件，调用时，装饰者中需要持有被装饰者对象
 *   构造方法：
 *      赋值
 */
public class RedShapeDecorator implements Shape{

    private Shape shape;

    public RedShapeDecorator(Shape shape) {
        this.shape = shape;
    }

    private void setRedBorder(){
        System.out.println("红色，粗体");
    }

    public void draw(){
        //增强逻辑
        setRedBorder();
        //调用被装饰者内部方法，完成业务
        shape.draw();
    }
}
```

测试：

```java
public class DecoratorTest {

    public static void main(String[] args) {
        //被装饰对象
        Circle circle = new Circle();
        //创建装饰者对象
        RedShapeDecorator rd = new RedShapeDecorator(circle);
        //调用方法
        rd.draw();
    }
}
```

#### 3)Dubbo中的AOP

dubbo中的AOP是通过装饰者模式完成的。

案例：

首先定义一个接口：

```java
@SPI
public interface Phone {
    void call();
}
```

定义接口的实现类，也就是被装饰者:

```java
public class IphoneX implements Phone {

    @Override
    public void call() {
        System.out.println("iphone正在拨打电话");
    }
}
```

为了简单，这里省略了装饰者接口。仅仅定义一个装饰者，实现phone接口，内部配置增强逻辑方法:

```java
public class MusicPhone implements Phone {
    private Phone phone;

    public MusicPhone(Phone phone) {
        this.phone = phone;
    }

    @Override
    public void call() {
        System.out.println("播放彩铃");
        this.phone.call();
    }
}
```

添加拓展点配置文件META-INF/dubbo/com.lishem.dubbo.pattern.decorator.dubboAOP.Phone,内容如下

```
iphone = com.lishem.dubbo.pattern.decorator.dubboAOP.IphoneX
filter = com.lishem.dubbo.pattern.decorator.dubboAOP.MusicPhone
```

配置测试方法:

```java
public class DubboAopTest {

    public static void main(String[] args) {
        ExtensionLoader<Phone> extensionLoader = ExtensionLoader.getExtensionLoader(Phone.class);
        Phone phone = extensionLoader.getExtension("iphone");
        phone.call();
    }
}
```

具体执行效果如下：

![image-20211114204007606](12_dubbo源码02.assets/image-20211114204007606.png)

先调用装饰者增强，再调用目标方法完成业务逻辑。

通过测试案例，可以看到在Dubbo SPI中具有增强AOP的功能，我们只需要关注dubbo源码中这样一行代码就够了

```java
// ExtensionLoader.java
private T createExtension(String name, boolean wrap) {
    // 从配置文件中加载所有的拓展类，可得到“配置项名称”到“配置类”的映射关系表
    // 这里加载了所有配置，为后面实例化创造了条件
    Class<?> clazz = getExtensionClasses().get(name);

    // ....

    //检查是否具有装饰者类，如果有调用装饰者类的构造方法，并返回实例对象
    if (CollectionUtils.isNotEmpty(wrapperClasses)) {
        for (Class<?> wrapperClass : wrapperClasses) {
            instance = injectExtension(
                (T) wrapperClass.getConstructor(type).newInstance(instance));
        }
    }
    initExtension(instance);
    return instance;
}
```

逻辑相对复杂，但是这里要在配置里将 包装对象配置进去，才会自动加载包装对象

判断是不是包装类，这里也很简单：

```java
// 就是看有没有一个 所需类的构造方法
private boolean isWrapperClass(Class<?> clazz) {
    try {
        clazz.getConstructor(type);
        return true;
    } catch (NoSuchMethodException e) {
        return false;
    }
}
```

## 1.4 动态编译

### 1.4.1 SPI中的自适应

我们知道在 Dubbo 中，很多拓展都是通过 **SPI** 机制 进行加载的，比如 Protocol、Cluster、LoadBalance、ProxyFactory 等。有时，有些拓展并不想在框架启动阶段被加载，而是希望在拓展方法被调用时，根据运行时参数进行加载，即根据参数动态加载实现类。如下所示：

![image-20211114205434102](12_dubbo源码02.assets/image-20211114205434102.png)

这种在运行时，根据方法参数才动态决定使用具体的拓展，在dubbo中就叫做扩展点自适应实例。其实是一个扩展点的代理，将扩展的选择从Dubbo启动时，延迟到RPC调用时。Dubbo中每一个扩展点都有一个自适应类，如果没有显式提供，Dubbo会自动为我们创建一个，默认使用Javaassist。

**自适应拓展机制的实现逻辑：**

1. 首先Dubbo会为拓展接口生成具有代理功能的代码
2. 通过javassist或者jdk编译这段代码，得到Class类
3. 通过反射创建代理类
4. 在代理类中，通过URL对象的参数来确定到底是调用哪个实现类

### 1.4.2 javassist入门

Javassist是一个开源的分析、编辑和创建Java字节码的类库。它已加入了开放源代码JBoss 应用服务器项目,通过使用
**Javassist对字节码操作为JBoss实现动态AOP框架**。javassist是jboss的一个子项目，其主要的优点，在于简单，而且快速。直接使用java编码的形式，而不需要了解虚拟机指令，就能动态改变类的结构，或者动态生成类。为了方便更好的理解dubbo中的自适应，这里通过案例的形式来熟悉下Javassist的基本使用。

```java
/**
 * Javassist是一个开源的分析、编辑和创建Java字节码的类库
 * 能动态改变类的结构，或者动态生成类
 */
public class CompilerByJavassist {

    public static void main(String[] args) throws Exception {

        // ClassPool：class对象容器
        ClassPool pool = ClassPool.getDefault();

        // 通过ClassPool生成一个User类  Class
        CtClass ctClass = pool.makeClass("com.itheima.javassist.domain.User");

        // 添加属性     -- private String username
        CtField enameField = new CtField(pool.getCtClass("java.lang.String"),
                                         "username", ctClass);
        //修饰符
        enameField.setModifiers(Modifier.PRIVATE);
        //作为属性添加到CtClass中
        ctClass.addField(enameField);

        // 添加属性    -- private int age
        CtField enoField = new CtField(pool.getCtClass("int"), "age", ctClass);
        enoField.setModifiers(Modifier.PRIVATE);
        ctClass.addField(enoField);

        //添加方法
        ctClass.addMethod(CtNewMethod.getter("getUsername", enameField));
        ctClass.addMethod(CtNewMethod.setter("setUsername", enameField));
        ctClass.addMethod(CtNewMethod.getter("getAge", enoField));
        ctClass.addMethod(CtNewMethod.setter("setAge", enoField));

        // 无参构造器
        CtConstructor constructor = new CtConstructor(null, ctClass);
        constructor.setBody("{}");
        ctClass.addConstructor(constructor);

        // 添加构造函数
        //ctClass.addConstructor(new CtConstructor(new CtClass[] {}, ctClass));

        CtConstructor ctConstructor = new CtConstructor(new CtClass[]{pool.get(String.class.getName()), CtClass.intType}, ctClass);
        ctConstructor.setBody("{\n this.username=$1; \n this.age=$2;\n}");
        ctClass.addConstructor(ctConstructor);

        // 添加自定义方法
        CtMethod ctMethod = new CtMethod(CtClass.voidType, "printUser", new CtClass[]{}, ctClass);
        // 为自定义方法设置修饰符
        ctMethod.setModifiers(Modifier.PUBLIC);
        // 为自定义方法设置函数体
        StringBuffer buffer2 = new StringBuffer();
        buffer2.append("{\nSystem.out.println(\"用户信息如下\");\n")
            .append("System.out.println(\"用户名=\"+username);\n")
            .append("System.out.println(\"年龄=\"+age);\n").append("}");
        ctMethod.setBody(buffer2.toString());
        ctClass.addMethod(ctMethod);

        //生成一个class
        Class<?> clazz = ctClass.toClass();

        Constructor cons2 = clazz.getDeclaredConstructor(String.class, Integer.TYPE);

        Object obj = cons2.newInstance("itheima", 20);

        //反射 执行方法
        obj.getClass().getMethod("printUser", new Class[]{})
            .invoke(obj, new Object[]{});

        // 把生成的class文件写入文件
        /*
        byte[] byteArr = ctClass.toBytecode();
        FileOutputStream fos = new FileOutputStream(new File("D://User.class"));
        fos.write(byteArr);
        fos.close();
        */
    }
}
```

通过以上代码，我们可以知道使用**javassist**可以方便的在运行时，按需动态的创建java对象，并执行内部方法。而这也是dubbo中动态编译的核心

### 1.4.3 源码分析

#### 1）Adaptive注解

```java
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface Adaptive {
    String[] value() default {};
}
```

从上面的代码中可知，Adaptive 可注解在类或方法上。

- 标注在类上：Dubbo 不会为该类生成代理类。
- 标注在方法上：Dubbo 则会为该方法生成代理逻辑，表示当前方法需要根据 参数URL 调用对应的扩展点实现。

#### 2）获取自适应拓展类

dubbo中每一个扩展点都有一个自适应类，如果没有显式提供，Dubbo会自动为我们创建一个，默认使用Javaassist。 先来看下创建自适应扩展类的代码：

这段代码从上到下看就行

```java
ExtensionLoader<Phone> extensionLoader = ExtensionLoader.getExtensionLoader(Phone.class);

public static <T> ExtensionLoader<T> getExtensionLoader(Class<T> type) {
    // type 检查...
    // 从缓存中获取ExtensionLoader
    ExtensionLoader<T> loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    if (loader == null) {
        // 创建ExtensionLoader,并且存入到EXTENSION_LOADERS 中
        EXTENSION_LOADERS.putIfAbsent(type, new ExtensionLoader<T>(type));
        loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    }
    return loader;
}

private ExtensionLoader(Class<?> type) {
    this.type = type;
    objectFactory = (type == ExtensionFactory.class ? null : ExtensionLoader.getExtensionLoader(ExtensionFactory.class).getAdaptiveExtension());
}

public T getAdaptiveExtension() {
    Object instance = cachedAdaptiveInstance.get();
    if (instance == null) {
        if (createAdaptiveInstanceError != null) {
            throw new IllegalStateException("Failed to create adaptive instance: " +
                                            createAdaptiveInstanceError.toString(),
                                            createAdaptiveInstanceError);
        }

        synchronized (cachedAdaptiveInstance) {
            instance = cachedAdaptiveInstance.get();
            if (instance == null) {
                try {
                    instance = createAdaptiveExtension();
                    cachedAdaptiveInstance.set(instance);
                } catch (Throwable t) {
                    createAdaptiveInstanceError = t;
                    throw new IllegalStateException("Failed to create adaptive instance: " + t.toString(), t);
                }
            }
        }
    }

    return (T) instance;
}

// 继续看createAdaptiveExtension
private T createAdaptiveExtension() {
    try {
        return injectExtension((T) getAdaptiveExtensionClass().newInstance());
    } catch (Exception e) {
        throw new IllegalStateException("Can't create adaptive extension " + type + ", cause: " + e.getMessage(), e);
    }
}

// getAdaptiveExtensionClass
private Class<?> getAdaptiveExtensionClass() {
    getExtensionClasses();
    if (cachedAdaptiveClass != null) {
        return cachedAdaptiveClass;
    }
    return cachedAdaptiveClass = createAdaptiveExtensionClass();
}
```

看这个createAdaptiveExtensionClass方法，它首先会生成自适应类的Java源码，然后再将源码编译成Java的字节码，加载到JVM中。

```java
private Class<?> createAdaptiveExtensionClass() {
    // 生成的类 字符串
    String code = new AdaptiveClassCodeGenerator(type, cachedDefaultName).generate();
    ClassLoader classLoader = findClassLoader();
    org.apache.dubbo.common.compiler.Compiler compiler = ExtensionLoader.getExtensionLoader(org.apache.dubbo.common.compiler.Compiler.class).getAdaptiveExtension();
    return compiler.compile(code, classLoader);
}
```

Compiler的代码，默认实现是javassist。

```java
@SPI("javassist")
public interface Compiler {
	Class<?> compile(String code, ClassLoader classLoader);
}
```

# 2.服务暴露与发现

## 2.1 概述

dubbo是一个简单易用的RPC框架，通过简单的提供者，消费者配置就能完成无感网络调用。那么在dubbo中是如何将**提供者的服务暴露**出去，消费者又是如何**获取到提供者的相关信息**的呢？

## 2.2 Spring中自定义Schema

在了解dubbo的服务注册和服务发现之前，我们首先需要掌握一个知识点：Spring中自定义Schema。  

Dubbo现在的设计时完全无入侵，也就是使用者只依赖于**配置契约**。在Dubbo中，可以使用XML配置相关信息，也可以用来引入服务或者导出服务。配置完成，启动工程，Spring会自动读取配置文件，生成注入相关Bean。

从Spring2.0 开始，Spring开始提供一种基于XML Schema格式拓展机制，用于定义和配置Bean

### 2.2.1 案例使用

Spring XML Schema拓展机制步骤：

1. 创建配置属性的JavaBean对象
2. 创建XML Schema文件，描述自定义的合法构建模块，也就是xsd文件
3. 自定义处理类，并实现NamespaceHandler接口。
4. 自定义解析器，实现BeanDefinitionParser接口(最关键部分)
5. 编写Spring.handlers 和 Spring.schemas文件配置所有部件

1）定义JavaBean对象，在Spring中此对象会根据配置自动创建

```java
public class User{
    private String id;
    private String name;
    private Integer age;
    // 此处省略getter setter toString方法
}
```

2）在META-INF下定义 user.xsd 文件，使用xsd用于描述标签的规则  

```xml
<?xml version="1.0" encoding="UTF-8"?>
<xsd:schema
            xmlns="http://www.lishem.com/schema/user"
            xmlns:xsd="http://www.w3.org/2001/XMLSchema"
            xmlns:beans="http://www.springframework.org/schema/beans"
            targetNamespace="http://www.lishem.com/schema/user"
            elementFormDefault="qualified"
            attributeFormDefault="unqualified">
    <xsd:import namespace="http://www.springframework.org/schema/beans" />
    <xsd:element name="user">
        <xsd:complexType>
            <xsd:complexContent>
                <xsd:extension base="beans:identifiedType">
                    <xsd:attribute name="name" type="xsd:string" />
                    <xsd:attribute name="age" type="xsd:int" />
                </xsd:extension>
            </xsd:complexContent>
        </xsd:complexType>
    </xsd:element>
</xsd:schema>
```

3）Spring读取XML文件时，会根据标签的命名空间找其对应的NamespaceHandler，我们在NamespaceHandler内会注册标签对应的解析器BeanDefinitionParser 。

```java
public class UserNamespaceHandler extends NamespaceHandlerSupport {

    @Override
    public void init() {
        /***
         *  user.xsd文件中 name="user"
         *  解析user节点
         */
        registerBeanDefinitionParser("user",new UserBeanDefinitionParser());
    }
}
```

4）BeanDefinitionParser是标签对应的解析器，Spring读取到对应标签时会使用该类进行解析；  

```java
package com.lishem.dubbo.springschema;

import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.xml.AbstractSingleBeanDefinitionParser;
import org.springframework.util.StringUtils;
import org.w3c.dom.Element;

public class UserBeanDefinitionParser extends AbstractSingleBeanDefinitionParser {

    protected Class getBeanClass(Element element) {
        return User.class;
    }

    protected void doParse(Element element, BeanDefinitionBuilder bean) {
        String name = element.getAttribute("name");
        String age = element.getAttribute("age");
        String id = element.getAttribute("id");

        if (StringUtils.hasText(id)) {
            bean.addPropertyValue("id", id);
        }
        if (StringUtils.hasText(name)) {
            bean.addPropertyValue("name", name);
        }
        if (StringUtils.hasText(age)) {
            bean.addPropertyValue("age", Integer.valueOf(age));
        }
    }
}
```

5）定义spring.handlers文件，内部保存命名空间与NamespaceHandler  类的对应关系，必须放在classpath下的META-INF文件夹中

```
http\://www.lishem.com/schema/user=com.lishem.dubbo.springschema.UserNamespaceHandler
```

6） 定义spring.schemas文件，内部保存命名空间对应的xsd文件位置；必须放在classpath下的META-INF文件夹中。  

```
http\://www.lishem.com/schema/user.xsd=META-INF/user.xsd
```

7） 代码准备好了之后，就可以在spring工程中进行使用和测试，定义spring配置文件，导入对应约束  

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:util="http://www.springframework.org/schema/util"
       xmlns:task="http://www.springframework.org/schema/task"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:lishem="http://www.lishem.com/schema/user"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util.xsd
                           http://www.springframework.org/schema/task http://www.springframework.org/schema/task/spring-task.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd
                           http://www.lishem.com/schema/user http://www.lishem.com/schema/user.xsd">

    <!--    创建了一个对象，并交给SpringIOC容器管理-->
    <lishem:user id="user" name="zhangsan" age="12"></lishem:user>

</beans>

```

8) 编写测试类，通过spring容器获取对象user  

```java
package com.lishem.dubbo.springschema;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SchemaDemo {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("/spring/applicationContext.xml");
        User user = (User) context.getBean("user");
        System.out.println(user);
    }
}

```

### 2.2.2 dubbo中的相关对象

Dubbo是运行在spring容器中，dubbo的配置文件也是通过spring的配置文件applicationContext.xml来加载，所以dubbo的自定义配置标签实现，其实同样依赖spring的xml schema机制  

```java
// DubboNamespaceHandler.java
@Override
public void init() {
    registerBeanDefinitionParser("application", new DubboBeanDefinitionParser(ApplicationConfig.class, true));
    registerBeanDefinitionParser("module", new DubboBeanDefinitionParser(ModuleConfig.class, true));
    // RegistryConfig 定义注册中心的核心配置对象
    registerBeanDefinitionParser("registry", new DubboBeanDefinitionParser(RegistryConfig.class, true));
    registerBeanDefinitionParser("config-center", new DubboBeanDefinitionParser(ConfigCenterBean.class, true));
    registerBeanDefinitionParser("metadata-report", new DubboBeanDefinitionParser(MetadataReportConfig.class, true));
    registerBeanDefinitionParser("monitor", new DubboBeanDefinitionParser(MonitorConfig.class, true));
    registerBeanDefinitionParser("metrics", new DubboBeanDefinitionParser(MetricsConfig.class, true));
    registerBeanDefinitionParser("ssl", new DubboBeanDefinitionParser(SslConfig.class, true));
    registerBeanDefinitionParser("provider", new DubboBeanDefinitionParser(ProviderConfig.class, true));
    registerBeanDefinitionParser("consumer", new DubboBeanDefinitionParser(ConsumerConfig.class, true));
    registerBeanDefinitionParser("protocol", new DubboBeanDefinitionParser(ProtocolConfig.class, true));
    // ServiceBean 服务提供者暴露服务的核心对象
    registerBeanDefinitionParser("service", new DubboBeanDefinitionParser(ServiceBean.class, true));
    // ReferenceBean 服务消费者发现服务的核心对象
    registerBeanDefinitionParser("reference", new DubboBeanDefinitionParser(ReferenceBean.class, false));
    registerBeanDefinitionParser("annotation", new AnnotationBeanDefinitionParser());
}
```

可以看出Dubbo所有的组件都是由 DubboBeanDefinitionParser 解析，并通过registerBeanDefinitionParser方法来注册到spring中最后解析对应的对象。这些对象中我们重点关注的有以下三个：  

- ServiceBean：服务提供者**暴露服务**的核心对象 
- ReferenceBean：服务消费者**发现服务**的核心对象 
- RegistryConfig：定义**注册中心的核心配置对象**  

## 2.3 服务暴露机制

### 2.3.1 术语解释

在Dubbo的核心领域模型中：

- **Invoker** 是实体域，它是Dubbo的核心模型，其它模型都向它靠拢或者转换成它。它代表一个可执行体，可向它发起invoke调用。它有可能是一个本地实现，也有可能是一个远程实现，也可能是一个集群实现。
  - 在服务提供方，Invoker用于调用服务提供类。
  - 在服务消费方，Invoker用于执行远程调用。
- **Protocol** 是服务域，它是 Invoker 暴露和引用的主功能入口，它负责 Invoker 的生命周期管理。 
  - export：暴露远程服务
  - refer：引用远程服务
- **proxyFactory**：获取一个接口的代理类
  - getInvoker：针对server端，将服务对象，如DemoServiceImpl包装成一个Invoker对象  
  - getProxy ：针对client端，创建接口的代理对象，例如DemoService的接口
- **Invocation** 是会话域，它持有调用过程中的变量，比如方法名，参数等。

### 2.3.2 流程机制

dubbo的服务暴露原理：

![image-20211115120643996](12_dubbo源码02.assets/image-20211115120643996.png)

在整体上看，Dubbo 框架做**服务暴露**分为两大部分 ， 第一步将持有的服务实例通过代理转换成**Invoker**, 第二步会把 **Invoker** 通过具体的协议 （ 比如 Dubbo ） 转换成 **Exporter**, 框架做了这层抽象也大大方便了功能扩展 。  

服务提供方暴露服务的蓝色初始化链，时序图如下：

![image-20211115142023862](12_dubbo源码02.assets/image-20211115142023862.png)

### 2.3.3 源码分析

#### （1） 导出入口

Dubbo2.7.5 以后多了个类 DubboBootstrapApplicationListener，继承自OneTimeExecutionApplicationContextEventListener，而 OneTimeExecutionApplicationContextEventListener 实现了ApplicationListener接口，拥有 onApplicationEvent 方法，所以，Spring 容器启动，将 DubboBootstrapApplicationListener 对象注入后，会调用其 onApplicationEvent  方法。

> 继承Spring容器后，若注入的对象实现ApplicationListener 接口，则容器启动完毕会自动调用实现此接口的 onApplicationEvent  方法

```java
//OneTimeExecutionApplicationContextEventListener.java
public final void onApplicationEvent(ApplicationEvent event) {
    if (isOriginalEventSource(event) && event instanceof ApplicationContextEvent) {
        onApplicationContextEvent((ApplicationContextEvent) event);
    }
}

// DubboBootstrapApplicationListener.java
@Override
public void onApplicationContextEvent(ApplicationContextEvent event) {
    if (event instanceof ContextRefreshedEvent) {
        onContextRefreshedEvent((ContextRefreshedEvent) event);
    } else if (event instanceof ContextClosedEvent) {
        onContextClosedEvent((ContextClosedEvent) event);
    }
}

private void onContextRefreshedEvent(ContextRefreshedEvent event) {
    // dubbo启动入口
    dubboBootstrap.start();
}
```

这里可以看到 有一个DubboBootstrap 的类，作为Dubbo的启动器，负责启动Dubbo服务。可以看到 DubboBootstrap 对象是单例对象，懒汉式在构造的时候赋值的，不是Spring容器注入的。

```java
// DubboBootstrapApplicationListener.java
private final DubboBootstrap dubboBootstrap;

public DubboBootstrapApplicationListener() {
    this.dubboBootstrap = DubboBootstrap.getInstance();
}
// DubboBootstrap.java
public static DubboBootstrap getInstance() {
    if (instance == null) {
        synchronized (DubboBootstrap.class) {
            if (instance == null) {
                instance = new DubboBootstrap();
            }
        }
    }
    return instance;
}
```

接下来看看start() 方法

```java
/**
     * Start the bootstrap
     */
public DubboBootstrap start() {
    // 只能初始化一次
    if (started.compareAndSet(false, true)) {
        ready.set(false);
        //调用初始化方法
        initialize();
        if (logger.isInfoEnabled()) {
            logger.info(NAME + " is starting...");
        }
        // 1. 暴露服务，服务端使用
        exportServices();

        // Not only provider register
        if (!isOnlyRegisterProvider() || hasExportedServices()) {
            // 2. 暴露本地元数据服务
            exportMetadataService();
            //3. 将dubbo实例注册到注册中心
            registerServiceInstance();
        }

        referServices();
        if (asyncExportingFutures.size() > 0) {
            new Thread(() -> {
                try {
                    this.awaitFinish();
                } catch (Exception e) {
                    logger.warn(NAME + " exportAsync occurred an exception.");
                }
                ready.set(true);
                if (logger.isInfoEnabled()) {
                    logger.info(NAME + " is ready.");
                }
            }).start();
        } else {
            ready.set(true);
            if (logger.isInfoEnabled()) {
                logger.info(NAME + " is ready.");
            }
        }
        if (logger.isInfoEnabled()) {
            logger.info(NAME + " has started.");
        }
    }
    return this;
}
```

来看暴露服务

```java
    private void exportServices() {
        configManager.getServices().forEach(sc -> {
            // TODO, compatible with ServiceConfig.export()
            ServiceConfig serviceConfig = (ServiceConfig) sc;
            serviceConfig.setBootstrap(this);

            if (exportAsync) {
                ExecutorService executor = executorRepository.getServiceExporterExecutor();
                Future<?> future = executor.submit(() -> {
                    sc.export();
                    exportedServices.add(sc);
                });
                asyncExportingFutures.add(future);
            } else {
                sc.export();
                exportedServices.add(sc);
            }
        });
    }
```

... 唉。。 读完Spring再来读这个。。。





#### （2） 导出服务到本地

#### （3） 导出服务到远程

#### （4） 开启Netty服务

#### （5） 服务注册

### 2.3.4 总结

## 2.4 服务发现

在学习了服务暴露原理之后 ， 接下来重点探讨服务是如何消费的 。 这里主要讲解如何**通过注册中心进行服务发现进行远程服务调用**等细节 。

### 2.4.1 服务发现流程

在详细探讨服务暴露细节之前 ， 我们先看一下整体duubo的服务消费原理

![image-20211115142152011](12_dubbo源码02.assets/image-20211115142152011.png)

在整体上看 ， Dubbo 框架做服务消费也分为两大部分 ， 第一步通过持有远程服务实例生成**Invoker**,这个 **Invoker** 在客户端是核心的远程代理对象 。 第二步会把 Invoker 通过动态代理转换成**实现用户接口的动态代理**引用 。

### 2.4.2 源码分析

### 2.4.3 总结





