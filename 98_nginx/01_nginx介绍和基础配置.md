# 1. 简介

## 1.1 名词解释

1. Web服务器

   WEB服务器也叫网页服务器，英文名叫Web Server，主要功能是为用户提供网上信息浏览服务。

2. HTTP

   HTTP是超文本传输协议的缩写，是用于从WEB服务器传输超文本到本地浏览器的传输协议，也是互联网上应用最为广泛的一种网络协议。HTTP是一个客户端和服务器端请求和应答的标准，客户端是终端用户，服务端是网站，通过使用Web浏览器、网络爬虫或者其他工具，客户端发起一个到服务器上指定端口的**HTTP请求**。

3. POP3/SMTP/IMAP

   POP3(Post Offic Protocol 3)邮局协议的第三个版本，

   SMTP(Simple Mail Transfer Protocol)简单邮件传输协议，

   IMAP(Internet Mail Access Protocol)交互式邮件存取协议



## 1.2 常见服务器

#### IIS

​	全称(Internet Information Services)即互联网信息服务，是由微软公司提供的基于windows系统的互联网基本服务。windows作为服务器在稳定性与其他一些性能上都不如类UNIX操作系统，因此在需要高性能Web服务器的场合下，IIS可能就会被"冷落".

#### Tomcat

​	Tomcat是一个运行Servlet和JSP的Web应用软件，Tomcat技术先进、性能稳定而且开放源代码，因此深受Java爱好者的喜爱并得到了部分软件开发商的认可，成为目前比较流行的Web应用服务器。但是Tomcat天生是一个重量级的Web服务器，对静态文件和高并发的处理比较弱。

#### Apache

​	Apache的发展时期很长，同时也有过一段辉煌的业绩。从上图可以看出大概在2014年以前都是市场份额第一的服务器。Apache有很多优点，如稳定、开源、跨平台等。但是它出现的时间太久了，在它兴起的年代，互联网的产业规模远远不如今天，所以它被设计成一个重量级的、不支持高并发的Web服务器。在Apache服务器上，如果有数以万计的并发HTTP请求同时访问，就会导致服务器上消耗大量能存，操作系统内核对成百上千的Apache进程做进程间切换也会消耗大量的CUP资源，并导致HTTP请求的平均响应速度降低，这些都决定了Apache不可能成为高性能的Web服务器。这也促使了Lighttpd和Nginx的出现。

#### Lighttpd

​	Lighttpd是德国的一个开源的Web服务器软件，它和Nginx一样，都是轻量级、高性能的Web服务器，欧美的业界开发者比较钟爱Lighttpd,而国内的公司更多的青睐Nginx，同时网上Nginx的资源要更丰富些。



## 1.3 常见开源协议

![image-20211123204724426](01_nginx介绍和基础配置.assets/image-20211123204724426.png)

nginx采用的是BSD许可证。

OpenRestry [Nginx+Lua]   Tengine[淘宝]

## 1.4 Nginx核心组成

- nginx二进制可执行文件
- nginx.conf配置文件
- error.log错误的日志记录
- access.log访问日志记录

# 2.Nginx 安装

官网：http://nginx.org/

下载地址：http://nginx.org/en/download.html       http://nginx.org/download/

0.7.52 才有win版

在linux上有三种安装方式

- 通过Nginx源码简单安装
- 通过Nginx复杂安装
- 通过yum安装

## 2.1 环境准备

1） 确认centos内核

linux 内核2.6以及以上才支持epoll，而Nginx需要解决高并发压力需要用到epoll。

用命令

```shell
uname -a
```

可以查看linux 内核版本。

![image-20211124104411847](01_nginx介绍和基础配置.assets/image-20211124104411847.png)

2）确认centos能联网

ping 百度就行。

> 如果用虚拟机，三种网络连接方式介绍:
>
> **桥接**：主机与虚拟机通过**交换机**连接，此时虚拟机就是局域网内一台电脑
>
> **仅主机**：主机与虚拟机直连，两台电脑之间可以通信，但是虚拟机无法上网
>
> **NAT**：主机与虚拟机共享一个网络，主机可以联网，那虚拟机也可以联网，但是虚拟机对外网不可见，它存在于主机所构建的虚拟网络中。

3）确认关闭防火墙

4）关闭selinux

这两步可以去看linux命令积累

5）安装前置软件

```shell
yum install -y gcc # 安装gcc
gcc --version # 查看gcc版本
yum install -y pcre pcre-devel # 安装PCRE库
rpm -qa pcre pcre-devel # 检查上面两个是否安装成功
yum install -y zlib zlib-devel # 安装zlib
rpm -qa zlib zlib-devel  # 检查zlib 是否安装成功
yum install -y openssl openssl-devel  # 安装openssl
rpm -qa openssl openssl-devel # 检查openssl是否安装成功

############################################################
#######              	  一键安装  			  	 ######
############################################################
yum install -y gcc pcre pcre-devel zlib zlib-devel openssl openssl-devel
rpm -qa pcre pcre-devel zlib zlib-devel openssl openssl-devel
```

![image-20211124110357035](01_nginx介绍和基础配置.assets/image-20211124110357035.png)

## 2.2 通过Nginx源码简单安装

1. 进入官网查找需要下载版本的链接地址，然后使用wget命令进行下载

```shell
wget http://nginx.org/download/nginx-1.16.1.tar.gz
```

2. 建议大家将下载的资源进行包管理

```shell
mkdir -p nginx/core
mv nginx-1.16.1.tar.gz nginx/core
```

3. 进入nginx/core 解压缩

```shell
tar -xzf nginx-1.16.1.tar.gz
```

4. 进入 /root/nginx/core/nginx-1.16.1，执行 configure

```shell
./configure
```

5. 编译

```shell
make
```

6. 安装

```shell
make install
```

安装的时候，其实是可以通过configure 配置安装路径的，如果没有配置，默认安装在

```shell
/usr/local/nginx
```

![image-20211124111241731](01_nginx介绍和基础配置.assets/image-20211124111241731.png)

7. 启动nginx 

```shell
cd sbin
./nginx
```

然后访问 centos域名，就表示启动成功：

![image-20211124112112706](01_nginx介绍和基础配置.assets/image-20211124112112706.png)

## 2.3 通过yum直接安装

使用源码进行简单安装，我们会发现安装的过程比较繁琐，需要提前准备GCC编译器、PCRE兼容正则表达式库、zlib压缩库、OpenSSL安全通信的软件库包，然后才能进行Nginx的安装。

http://nginx.org/  -> documentation -> Installing nginx -> Installation on Linux的 packages  -> 选择对应的系统，查看安装文档

详细步骤：

（1）安装yum-utils

```shell
sudo yum  install -y yum-utils
```

（2）添加yum源文件

```shell
vim /etc/yum.repos.d/nginx.repo
```

```shell
[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true

[nginx-mainline]
name=nginx mainline repo
baseurl=http://nginx.org/packages/mainline/centos/$releasever/$basearch/
gpgcheck=1
enabled=0
gpgkey=https://nginx.org/keys/nginx_signing.key
module_hotfixes=true
```

（3）查看是否安装成功

```shell
yum list | grep nginx
```

![1581416861684](01_nginx介绍和基础配置.assets/1581416861684.png)

（4）使用yum进行安装

```shell
yun install -y nginx
```

（5）查看nginx的安装位置

```shell
whereis nginx
```

![image-20211124113115083](01_nginx介绍和基础配置.assets/image-20211124113115083.png)

（6）启动测试

```shell
cd /usr/sbin
./nginx
```

然后访问 centosIp 就可以看到同上效果



两种安装方式的区别：

找到nginx二进制文件 通过 ./nginx -V 查看nginx版本信息和参数

简单安装：

![image-20211124113725261](01_nginx介绍和基础配置.assets/image-20211124113725261.png)

yum安装：

![image-20211124113825731](01_nginx介绍和基础配置.assets/image-20211124113825731.png)

所以 yum安装默认帮我们做了很多配置。

## 2.4 通过Nginx源码复杂安装

安装以前，先删除一下nginx吧：

```shell
# 停止Nginx软件
/usr/local/nginx/sbin/nginx -s stop

# 找到根目录下所有nginx 的文件
find / -name nginx

# 删掉安装的文件 
rm -rf -/usr/local/... 根据自己情况

# 安装包之前编译的环境清除
# 在 /root/nginx/core/nginx-1.16.1 下执行
make clean

# 如果开启了开机自启
chkconfig nginx off
rm -rf /etc/init.d/nginx
```



复杂安装和简单的安装配置不同的地方在第一步，通过 ./configure 来对编译参数进行设置，需要我们手动来指定。那么都有哪些参数可以进行设置，接下来我们进行一个详细的说明。

通过 ./configure --help 可以看到可以设置哪些参数

![image-20211124140334943](01_nginx介绍和基础配置.assets/image-20211124140334943.png)

- PATH:是和路径相关的配置信息

- with:是启动模块，默认是关闭的

- without:是关闭模块，默认是开启的

我们先来认识一些简单的路径配置已经通过这些配置来完成一个简单的编译：

```shell
--prefix=PATH     # 指向Nginx的安装目录，默认值为/usr/local/nginx 也就是下面的 <prefix>/sbin/nginx
--sbin-path=PATH  # 指向(执行)程序文件(nginx)的路径,默认值为<prefix>/sbin/nginx
--modules-path=PATH  # 指向Nginx动态模块安装目录，默认值为<prefix>/modules
--conf-path=PATH  # 指向配置文件(nginx.conf)的路径,默认值为<prefix>/conf/nginx.conf
--error-log-path=PATH   # 指向错误日志文件的路径,默认值为<prefix>/logs/error.log
--http-log-path=PATH  # 指向访问日志文件的路径,默认值为<prefix>/logs/access.log
--pid-path=PATH    # 指向Nginx启动后进行ID的文件路径，默认值为<prefix>/logs/nginx.pid
--lock-path=PATH   # 指向Nginx锁文件的存放路径,默认值为<prefix>/logs/nginx.lock
```

现在来编译一下，进入 /root/nginx/core/nginx-1.16.1目录，执行以下命令：

```shell
./configure --prefix=/usr/local/nginx \
--sbin-path=/usr/local/nginx/sbin/nginx \
--modules-path=/usr/local/nginx/modules \
--conf-path=/usr/local/nginx/conf/nginx.conf \
--error-log-path=/usr/local/nginx/logs/error.log \
--http-log-path=/usr/local/nginx/logs/access.log \
--pid-path=/usr/local/nginx/logs/nginx.pid \
--lock-path=/usr/local/nginx/logs/nginx.lock
```

配置完成后执行 make && make install，就可以完成自定义安装nginx

然后可以去查看配置是否生效

进入 /usr/local/nginx/sbin， 执行 ./nginx -V 就可以看到配置成功没

![image-20211124142506514](01_nginx介绍和基础配置.assets/image-20211124142506514.png)

# 3. 初识Nginx

## 3.1 nginx 的目录结构

首先需要安装一个工具

```shell
yum install -y tree
```

然后通过 tree /usr/local/nginx (nginx的安装目录)，获取目录结构

![image-20211124200938634](01_nginx介绍和基础配置.assets/image-20211124200938634.png)

```shell
├── conf
│   ├── fastcgi.conf # fastcgi相关配置文件
│   ├── fastcgi.conf.default  # fastcgi.conf的备份文件
│   ├── fastcgi_params  # fastcgi参数文件
│   ├── fastcgi_params.default  # fastcgi的参数备份文件
│   ├── koi-utf  # 编码转换映射配置文件
│   ├── koi-win  # 编码转换映射配置文件
│   ├── mime.types # 记录的是HTTP协议中的Content-Type的值和文件后缀名的对应关系
│   ├── mime.types.default  # mime.types的备份文件
│   ├── nginx.conf  # 这个是Nginx的核心配置文件，这个文件非常重要，也是我们即将要学习的重点
│   ├── nginx.conf.default  # nginx.conf的备份文件
│   ├── scgi_params  # scgi的参数文件
│   ├── scgi_params.default  # scgi的参数备份文件
│   ├── uwsgi_params  # uwsgi的参数文件
│   ├── uwsgi_params.default  # uwsgi的参数备份文件
│   └── win-utf  # 编码转换映射配置文件
├── html		# 存放nginx自带的两个静态的html页面
│   ├── 50x.html # 访问失败后的失败页面
│   └── index.html  # 成功访问的默认首页
│   └── nginx.pid   # nginx 启动后的pid，启动才有，不启动没有（kill 杀掉不会删）
├── logs  # 日志，当nginx服务器启动后，这里面会有 access.log error.log 和nginx.pid三个文件出现。
└── sbin	# 是存放执行程序文件nginx
    └── nginx  # 用来控制Nginx的启动和停止等相关的命令。
```

CGI(Common Gateway Interface)通用网关【接口】，主要解决的问题是从客户端发送一个请求和数据，服务端获取到请求和数据后可以调用调用CGI【程序】处理及相应结果给客户端的一种标准规范。

## 3.2 nginx 服务启停命令

### 3.2.1 信号控制



### 3.2.2 命令行控制



3.3 nginx













